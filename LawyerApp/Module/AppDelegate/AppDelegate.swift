//
//  AppDelegate.swift
//  LegalStart
//
//  Created by Adapting Social on 18/04/17.
//  Copyright © 2017 Adapting Social. All rights reserved.
//

import UIKit
import SwiftyDropbox
import GoogleAPIClientForREST
import GTMOAuth2
import Fabric
import Crashlytics
import GoogleMaps
import Stripe
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,NotificationServiceDelegate {

    var window: UIWindow?
    var selectedMenuIndex : Int = 0
    var userInfoData : SignInCheckUserEmailAndPassword! = nil
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        DropboxClientsManager.setupWithAppKey(Constant.AppKeys.kDropBoxAPPKey) 

        Fabric.with([Crashlytics.self])
//        Stripe.setDefaultPublishableKey(Constant.AppKeys.kStripePublicationKey)
        STPPaymentConfiguration.shared().publishableKey = Constant.AppKeys.kStripePublicationKey
//        STPPaymentConfiguration.shared().publishableKey = Constant.AppKeys.kStripeTestPublicationKey
        
        // Set Google Map API key...
        GMSServices.provideAPIKey(Constant.kGoogleApiKey)
        SVProgressHUD.setDefaultMaskType(.clear)
        
        QBSettings.applicationID = Constant.AppKeys.kQBApplicationID
        QBSettings.authKey = Constant.AppKeys.kQBAuthKey
        QBSettings.authSecret = Constant.AppKeys.kQBAuthSecret
        QBSettings.accountKey = Constant.AppKeys.kQBAccountKey
//        QBSettings.carbonsEnabled = true
//        QBSettings.logLevel = QBLogLevel.nothing
//        QBSettings.enableXMPPLogging()
//        QBSettings.setChatDNSLookupCacheEnabled();
        
        if let storedUserData = UserDefaults.object(forKey: Constant.UserDefaultKeys.kUserDataModelKey) as? Data {
            if let retrieveUserData = NSKeyedUnarchiver.unarchiveObject(with: storedUserData) as? SignInCheckUserEmailAndPassword {
                self.userInfoData = retrieveUserData
                let user : QBUUser = QBUUser()
                user.login = self.userInfoData.blogEmail!
                user.password = self.userInfoData.blogPasswsord!
                Utility.sharedInstance.loginUseToQuickBloxWithUser(user: user, completionHandler: { (user, error) in
                    
                })
            }
        }
        
        var configureError: NSError?
        GGLContext.sharedInstance().configureWithError(&configureError)
        assert(configureError == nil, "Error configuring Google services: \(String(describing: configureError))")
        
        Utility.registerForRemoteNotification()
        Utility.checkUserSubscriptionStatus()
       

        
        // Handle notification
        if launchOptions != nil{
            print(launchOptions ?? "NIL LAUNCH")

            if let remoteNotification = launchOptions![UIApplicationLaunchOptionsKey.remoteNotification]{
                
                self.application(application, didReceiveRemoteNotification: remoteNotification as! [AnyHashable : Any])
            }
        }
        
        
        
        return true
    }
    class func sharedInstance() -> AppDelegate {
        
        
        return UIApplication.shared.delegate as! AppDelegate
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let deviceIdentifier: String = UIDevice.current.identifierForVendor!.uuidString
        var token: String = ""
        for i in 0..<deviceToken.count {
            token += String(format: "%02.2hhx", deviceToken[i] as CVarArg)
        }
        print("APNS Token:",token)
        UserDefaults.set(token, forKey: DEVICETOKEN_STRING)
        let subscription: QBMSubscription! = QBMSubscription()
        
        subscription.notificationChannel = QBMNotificationChannel.APNS
        subscription.deviceUDID = deviceIdentifier
        subscription.deviceToken = deviceToken
        UserDefaults.set(deviceToken, forKey: DEVICETOKEN)
        QBRequest.createSubscription(subscription, successBlock: { (response: QBResponse!, objects: [QBMSubscription]?) -> Void in
            //
        }) { (response: QBResponse!) -> Void in
            //
        }
    }
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        
        print(userInfo)

        if application.applicationState == .active {
        
            if let dialog_id = userInfo["dialog_id"] as? String{
                //quickblox
                print(dialog_id)
                self.quickBloxPushNotification(userInfo: userInfo)
                
            }else{
            
            self.handleNotificationInActiveMode(userInfo: userInfo)
            }
            
        }else{
        
            if let dialog_id = userInfo["dialog_id"] as? String{
                //quickblox
                print(dialog_id)
                self.quickBloxPushNotification(userInfo: userInfo)
            }else{
                self.handleNotificationInBackGroundMode(userInfo: userInfo)
                
            }
        
        }
    }
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        if let authResult = DropboxClientsManager.handleRedirectURL(url) {
            switch authResult {
            case .success:
                NotificationCenter.default.post(name: Notification.Name(rawValue: "Dropboxlistrefresh"), object: nil)
            case .cancel:
                print("Authorization flow was manually canceled by user!")
            case .error(_, let description):
                print("Error: \(description)")
            }
        }else if let authGoogleResult = MDPickerGoogleDriveManager.handleURL(url: url, options: options){

            switch authGoogleResult {
            case .success: break
                
            case .cancel:
                print("Authorization flow was manually canceled by user!")
            default: break
                
            }
        }
        return true
    }
    
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        application.applicationIconBadgeNumber = 0
        ServicesManager.instance().chatService.disconnect { (error) in
            
        }
        
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        
        ServicesManager.instance().chatService.connect { (error) in
            if error != nil {
                print("Error in quickeblox \(error!.localizedDescription)")
            }
        }
        Utility.checkUserSubscriptionStatus()
        // Handle notification
        // Handle notification
        
        
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        ServicesManager.instance().chatService.connect { (error) in
            if error != nil {
                print("Error in quickeblox \(error!.localizedDescription)")
            }
        }
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        ServicesManager.instance().chatService.disconnect { (error) in
            
        }
    }

    
    //MARK: UTILITY
    func redirectWithStackArray(navArray : [UIViewController]) {
        
        let nav : UINavigationController = UINavigationController()
        nav.setViewControllers(navArray, animated: true)
        nav.setNavigationBarHidden(true, animated: false)
        Utility.sharedInstance.drawerController.centerViewController = nav
    }

    func handleNotificationInActiveMode(userInfo : [AnyHashable:Any])  {
        
        if let aps : [String:Any] = userInfo["aps"] as? [String : Any]{
            if let alert : [String:Any] = aps["alert"] as? [String : Any]{
                let vc = self.window?.rootViewController
                
                if let type = userInfo["type"] as? String,type == "ADMIN_END_CHAT"{
                    UIAlertController.showAlertWithOkButton(vc!, aStrMessage: alert["body"] as? String, completion: { (index, title) in
                        self.adminEndChat()
                    })
                }else if let type = userInfo["type"] as? String,type == "ADMIN_NEW_DOCUMENT_UPLOADED"{
                    UIAlertController.showAlert(vc!, aStrMessage: alert["body"] as? String, style: .alert, aCancelBtn: "LATER", aDistrutiveBtn: nil, otherButtonArr: ["VIEW"]) { (index, title) in
                        if title == "VIEW"{
                            
                            self.redirectPushWith(userInfo: userInfo)
                            
                            
                        }
                    }
                
                }else if let type = userInfo["type"] as? String,type == "MY_DOCUMENT"{
                    UIAlertController.showAlert(vc!, aStrMessage: alert["body"] as? String, style: .alert, aCancelBtn: "LATER", aDistrutiveBtn: nil, otherButtonArr: ["VIEW"]) { (index, title) in
                        if title == "VIEW"{
                            
                            self.redirectPushWith(userInfo: userInfo)
                            
                            
                        }
                    }
                    
                }
                
               
            }
            
           
        }
        
    }
    func handleNotificationInBackGroundMode(userInfo : [AnyHashable:Any])  {
        if let type = userInfo["type"] as? String,type == "ADMIN_NEW_DOCUMENT_UPLOADED" || type == "MY_DOCUMENT"{
        
            self.redirectPushWith(userInfo: userInfo)
        
        }
        if let type = userInfo["type"] as? String,type == "ADMIN_END_CHAT"{
            
            self.adminEndChat()
            
        }
    }
    func adminEndChat()  {
        let dialogID : String = UserDefaults.value(forKey: CHAT_DIALOG_ID) as! String
        UserDefaults.setValue("NO", forKey: IS_CHAT_DIALOG_CREATED)
        ServicesManager.instance().chatService.deleteDialog(withID: dialogID) { (response) in
            let storyBoard : UIStoryboard = UIStoryboard(name: "DashBoardMaster", bundle: nil)
            let dashBoardVC : DashBoardViewController = storyBoard.instantiateViewController(withIdentifier: "DashBoardViewController") as! DashBoardViewController
            let arrVC : [UIViewController] = [dashBoardVC]
            self.redirectWithStackArray(navArray: arrVC)
        }
        
        
    }
    func redirectPushWith(userInfo : [AnyHashable:Any])  {
        
        
        if let type = userInfo["type"] as? String,type == "ADMIN_NEW_DOCUMENT_UPLOADED"{
            let storyBoard : UIStoryboard = UIStoryboard(name: "CaseMaster", bundle: nil)
            let caseList : CaseListingViewController = storyBoard.instantiateViewController(withIdentifier: "CaseListingViewController") as! CaseListingViewController
            let caseListDetail : CaseDetailViewController = storyBoard.instantiateViewController(withIdentifier: "CaseDetailViewController") as! CaseDetailViewController
            let case_id : String = userInfo["case_id"] as! String
            caseListDetail.handleNotification = (true,case_id)
            AppDelegate.sharedInstance().selectedMenuIndex = 2
            let arrVC : [UIViewController] = [caseList,caseListDetail]
            
            self.redirectWithStackArray(navArray: arrVC)
        }else if let type = userInfo["type"] as? String,type == "MY_DOCUMENT"{
            let storyBoard : UIStoryboard = UIStoryboard(name: "DocumentMaster", bundle: nil)
            let documentList : MyDocumentViewController = storyBoard.instantiateViewController(withIdentifier: "MyDocumentViewController") as! MyDocumentViewController
            AppDelegate.sharedInstance().selectedMenuIndex = 9
            let arrVC : [UIViewController] = [documentList]
            
            self.redirectWithStackArray(navArray: arrVC)
        
        }
        
        
        
        
    }
    func quickBloxPushNotification(userInfo : [AnyHashable : Any] ) {
        
        
        guard let dialogID = userInfo["dialog_id"] as? String else {
            return
        }
        
        guard !dialogID.isEmpty else {
            return
        }
        
        
        let dialogWithIDWasEntered: String? = ServicesManager.instance().currentDialogID
        if dialogWithIDWasEntered == dialogID {
            return
        }
        
        ServicesManager.instance().notificationService.pushDialogID = dialogID
        
        // calling dispatch async for push notification handling to have priority in main queue
        DispatchQueue.main.async {
            ServicesManager.instance().notificationService.handlePushNotificationWithDelegate(delegate: self)
        }
        

    }

    //MARK: NotificationServiceDelegate
    func notificationServiceDidSucceedFetchingDialog(chatDialog: QBChatDialog!){
    
        let storyBoard : UIStoryboard = UIStoryboard(name: "ChatMaster", bundle: nil)
        let chtHandler : ChatHandlerViewController = storyBoard.instantiateViewController(withIdentifier: "ChatHandlerViewController") as! ChatHandlerViewController
        Utility.sharedInstance.drawerController.setCenterView(chtHandler, withCloseAnimation: true, completion: nil)
        
    }
    
    
    func notificationServiceDidStartLoadingDialogFromServer(){
    
    }
    
    
    func notificationServiceDidFinishLoadingDialogFromServer(){
    
    }
    
    
    func notificationServiceDidFailFetchingDialog(){
    
    
    }

}
extension AppDelegate {
    
    class func getAppDelegate() -> AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }


}
