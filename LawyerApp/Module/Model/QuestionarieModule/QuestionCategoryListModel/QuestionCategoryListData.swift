//
//  QuestionCategoryListData.swift
//
//  Created by Adapting Social on 11/08/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class QuestionCategoryListData: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let descriptionValue = "description"
    static let categoryName = "category_name"
    static let amount = "amount"
    static let image = "image"
    static let questionCategoryId = "question_category_id"
  }

  // MARK: Properties
  public var descriptionValue: String?
  public var categoryName: String?
  public var amount: String?
  public var image: String?
  public var questionCategoryId: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    descriptionValue = json[SerializationKeys.descriptionValue].string
    categoryName = json[SerializationKeys.categoryName].string
    amount = json[SerializationKeys.amount].string
    image = json[SerializationKeys.image].string
    questionCategoryId = json[SerializationKeys.questionCategoryId].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = descriptionValue { dictionary[SerializationKeys.descriptionValue] = value }
    if let value = categoryName { dictionary[SerializationKeys.categoryName] = value }
    if let value = amount { dictionary[SerializationKeys.amount] = value }
    if let value = image { dictionary[SerializationKeys.image] = value }
    if let value = questionCategoryId { dictionary[SerializationKeys.questionCategoryId] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.descriptionValue = aDecoder.decodeObject(forKey: SerializationKeys.descriptionValue) as? String
    self.categoryName = aDecoder.decodeObject(forKey: SerializationKeys.categoryName) as? String
    self.amount = aDecoder.decodeObject(forKey: SerializationKeys.amount) as? String
    self.image = aDecoder.decodeObject(forKey: SerializationKeys.image) as? String
    self.questionCategoryId = aDecoder.decodeObject(forKey: SerializationKeys.questionCategoryId) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(descriptionValue, forKey: SerializationKeys.descriptionValue)
    aCoder.encode(categoryName, forKey: SerializationKeys.categoryName)
    aCoder.encode(amount, forKey: SerializationKeys.amount)
    aCoder.encode(image, forKey: SerializationKeys.image)
    aCoder.encode(questionCategoryId, forKey: SerializationKeys.questionCategoryId)
  }

}
