//
//  QuestionarieStateListData.swift
//
//  Created by Adapting Social on 11/08/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class QuestionarieStateListData: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let questionStateCategoryId = "question_state_category_id"
    static let amount = "amount"
    static let stateName = "state_name"
    static let otherInformation = "other_information"
    static let corporationPrice = "Corporation_price"
  }

  // MARK: Properties
  public var questionStateCategoryId: String?
  public var amount: String?
  public var stateName: String?
  public var otherInformation: String?
  public var corporationPrice: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    questionStateCategoryId = json[SerializationKeys.questionStateCategoryId].string
    amount = json[SerializationKeys.amount].string
    stateName = json[SerializationKeys.stateName].string
    otherInformation = json[SerializationKeys.otherInformation].string
    corporationPrice = json[SerializationKeys.corporationPrice].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = questionStateCategoryId { dictionary[SerializationKeys.questionStateCategoryId] = value }
    if let value = amount { dictionary[SerializationKeys.amount] = value }
    if let value = stateName { dictionary[SerializationKeys.stateName] = value }
    if let value = otherInformation { dictionary[SerializationKeys.otherInformation] = value }
    if let value = corporationPrice { dictionary[SerializationKeys.corporationPrice] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.questionStateCategoryId = aDecoder.decodeObject(forKey: SerializationKeys.questionStateCategoryId) as? String
    self.amount = aDecoder.decodeObject(forKey: SerializationKeys.amount) as? String
    self.stateName = aDecoder.decodeObject(forKey: SerializationKeys.stateName) as? String
    self.otherInformation = aDecoder.decodeObject(forKey: SerializationKeys.otherInformation) as? String
    self.corporationPrice = aDecoder.decodeObject(forKey: SerializationKeys.corporationPrice) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(questionStateCategoryId, forKey: SerializationKeys.questionStateCategoryId)
    aCoder.encode(amount, forKey: SerializationKeys.amount)
    aCoder.encode(stateName, forKey: SerializationKeys.stateName)
    aCoder.encode(otherInformation, forKey: SerializationKeys.otherInformation)
    aCoder.encode(corporationPrice, forKey: SerializationKeys.corporationPrice)
  }

}
