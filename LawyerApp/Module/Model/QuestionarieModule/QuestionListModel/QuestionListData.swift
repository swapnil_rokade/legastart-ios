//
//  QuestionListData.swift
//
//  Created by Adapting Social on 08/09/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class QuestionListData: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let question = "question"
    static let questionId = "question_id"
    static let flowFinish = "flow_finish"
    static let optionListing = "option_listing"
    static let questionType = "question_type"
    static let textfieldOpen = "textfield_open"
    static let questionTypeCode = "question_type_code"
    static let addAnother = "add_another"
    static let questionTypeName = "question_type_name"
  }

  // MARK: Properties
  public var question: String?
  public var questionId: String?
  public var flowFinish: String?
  public var optionListing: [QuestionListOptionListing]?
  public var questionType: String?
  public var textfieldOpen: String?
  public var questionTypeCode: String?
  public var addAnother: String?
  public var questionTypeName: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    question = json[SerializationKeys.question].string
    questionId = json[SerializationKeys.questionId].string
    flowFinish = json[SerializationKeys.flowFinish].string
    if let items = json[SerializationKeys.optionListing].array { optionListing = items.map { QuestionListOptionListing(json: $0) } }
    questionType = json[SerializationKeys.questionType].string
    textfieldOpen = json[SerializationKeys.textfieldOpen].string
    questionTypeCode = json[SerializationKeys.questionTypeCode].string
    addAnother = json[SerializationKeys.addAnother].string
    questionTypeName = json[SerializationKeys.questionTypeName].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = question { dictionary[SerializationKeys.question] = value }
    if let value = questionId { dictionary[SerializationKeys.questionId] = value }
    if let value = flowFinish { dictionary[SerializationKeys.flowFinish] = value }
    if let value = optionListing { dictionary[SerializationKeys.optionListing] = value.map { $0.dictionaryRepresentation() } }
    if let value = questionType { dictionary[SerializationKeys.questionType] = value }
    if let value = textfieldOpen { dictionary[SerializationKeys.textfieldOpen] = value }
    if let value = questionTypeCode { dictionary[SerializationKeys.questionTypeCode] = value }
    if let value = addAnother { dictionary[SerializationKeys.addAnother] = value }
    if let value = questionTypeName { dictionary[SerializationKeys.questionTypeName] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.question = aDecoder.decodeObject(forKey: SerializationKeys.question) as? String
    self.questionId = aDecoder.decodeObject(forKey: SerializationKeys.questionId) as? String
    self.flowFinish = aDecoder.decodeObject(forKey: SerializationKeys.flowFinish) as? String
    self.optionListing = aDecoder.decodeObject(forKey: SerializationKeys.optionListing) as? [QuestionListOptionListing]
    self.questionType = aDecoder.decodeObject(forKey: SerializationKeys.questionType) as? String
    self.textfieldOpen = aDecoder.decodeObject(forKey: SerializationKeys.textfieldOpen) as? String
    self.questionTypeCode = aDecoder.decodeObject(forKey: SerializationKeys.questionTypeCode) as? String
    self.addAnother = aDecoder.decodeObject(forKey: SerializationKeys.addAnother) as? String
    self.questionTypeName = aDecoder.decodeObject(forKey: SerializationKeys.questionTypeName) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(question, forKey: SerializationKeys.question)
    aCoder.encode(questionId, forKey: SerializationKeys.questionId)
    aCoder.encode(flowFinish, forKey: SerializationKeys.flowFinish)
    aCoder.encode(optionListing, forKey: SerializationKeys.optionListing)
    aCoder.encode(questionType, forKey: SerializationKeys.questionType)
    aCoder.encode(textfieldOpen, forKey: SerializationKeys.textfieldOpen)
    aCoder.encode(questionTypeCode, forKey: SerializationKeys.questionTypeCode)
    aCoder.encode(addAnother, forKey: SerializationKeys.addAnother)
    aCoder.encode(questionTypeName, forKey: SerializationKeys.questionTypeName)
  }

}
