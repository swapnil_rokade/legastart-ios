//
//  QuestionListOptionListing.swift
//
//  Created by Adapting Social on 08/09/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class QuestionListOptionListing: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let questionOption = "question_option"
    static let questionOptionId = "question_option_id"
  }

  // MARK: Properties
  public var questionOption: String?
  public var questionOptionId: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    questionOption = json[SerializationKeys.questionOption].string
    questionOptionId = json[SerializationKeys.questionOptionId].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = questionOption { dictionary[SerializationKeys.questionOption] = value }
    if let value = questionOptionId { dictionary[SerializationKeys.questionOptionId] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.questionOption = aDecoder.decodeObject(forKey: SerializationKeys.questionOption) as? String
    self.questionOptionId = aDecoder.decodeObject(forKey: SerializationKeys.questionOptionId) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(questionOption, forKey: SerializationKeys.questionOption)
    aCoder.encode(questionOptionId, forKey: SerializationKeys.questionOptionId)
  }

}
