//
//  DocumentQuestionListData.swift
//
//  Created by Adapting Social on 04/09/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class DocumentQuestionListData: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let documentId = "document_id"
    static let flowFinish = "flow_finish"
    static let questionTypeId = "question_type_id"
    static let questionName = "question_name"
    static let optionListing = "option_listing"
    static let textfieldOpen = "textfield_open"
    static let questionTypeCode = "question_type_code"
    static let addAnother = "add_another"
    static let documentQuestionId = "document_question_id"
    static let questionTypeName = "question_type_name"
  }

  // MARK: Properties
  public var documentId: String?
  public var flowFinish: String?
  public var questionTypeId: String?
  public var questionName: String?
  public var optionListing: [DocumentQuestionListOptionListing]?
  public var textfieldOpen: String?
  public var questionTypeCode: String?
  public var addAnother: String?
  public var documentQuestionId: String?
  public var questionTypeName: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    documentId = json[SerializationKeys.documentId].string
    flowFinish = json[SerializationKeys.flowFinish].string
    questionTypeId = json[SerializationKeys.questionTypeId].string
    questionName = json[SerializationKeys.questionName].string
    if let items = json[SerializationKeys.optionListing].array { optionListing = items.map { DocumentQuestionListOptionListing(json: $0) } }
    textfieldOpen = json[SerializationKeys.textfieldOpen].string
    questionTypeCode = json[SerializationKeys.questionTypeCode].string
    addAnother = json[SerializationKeys.addAnother].string
    documentQuestionId = json[SerializationKeys.documentQuestionId].string
    questionTypeName = json[SerializationKeys.questionTypeName].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = documentId { dictionary[SerializationKeys.documentId] = value }
    if let value = flowFinish { dictionary[SerializationKeys.flowFinish] = value }
    if let value = questionTypeId { dictionary[SerializationKeys.questionTypeId] = value }
    if let value = questionName { dictionary[SerializationKeys.questionName] = value }
    if let value = optionListing { dictionary[SerializationKeys.optionListing] = value.map { $0.dictionaryRepresentation() } }
    if let value = textfieldOpen { dictionary[SerializationKeys.textfieldOpen] = value }
    if let value = questionTypeCode { dictionary[SerializationKeys.questionTypeCode] = value }
    if let value = addAnother { dictionary[SerializationKeys.addAnother] = value }
    if let value = documentQuestionId { dictionary[SerializationKeys.documentQuestionId] = value }
    if let value = questionTypeName { dictionary[SerializationKeys.questionTypeName] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.documentId = aDecoder.decodeObject(forKey: SerializationKeys.documentId) as? String
    self.flowFinish = aDecoder.decodeObject(forKey: SerializationKeys.flowFinish) as? String
    self.questionTypeId = aDecoder.decodeObject(forKey: SerializationKeys.questionTypeId) as? String
    self.questionName = aDecoder.decodeObject(forKey: SerializationKeys.questionName) as? String
    self.optionListing = aDecoder.decodeObject(forKey: SerializationKeys.optionListing) as? [DocumentQuestionListOptionListing]
    self.textfieldOpen = aDecoder.decodeObject(forKey: SerializationKeys.textfieldOpen) as? String
    self.questionTypeCode = aDecoder.decodeObject(forKey: SerializationKeys.questionTypeCode) as? String
    self.addAnother = aDecoder.decodeObject(forKey: SerializationKeys.addAnother) as? String
    self.documentQuestionId = aDecoder.decodeObject(forKey: SerializationKeys.documentQuestionId) as? String
    self.questionTypeName = aDecoder.decodeObject(forKey: SerializationKeys.questionTypeName) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(documentId, forKey: SerializationKeys.documentId)
    aCoder.encode(flowFinish, forKey: SerializationKeys.flowFinish)
    aCoder.encode(questionTypeId, forKey: SerializationKeys.questionTypeId)
    aCoder.encode(questionName, forKey: SerializationKeys.questionName)
    aCoder.encode(optionListing, forKey: SerializationKeys.optionListing)
    aCoder.encode(textfieldOpen, forKey: SerializationKeys.textfieldOpen)
    aCoder.encode(questionTypeCode, forKey: SerializationKeys.questionTypeCode)
    aCoder.encode(addAnother, forKey: SerializationKeys.addAnother)
    aCoder.encode(documentQuestionId, forKey: SerializationKeys.documentQuestionId)
    aCoder.encode(questionTypeName, forKey: SerializationKeys.questionTypeName)
  }

}
