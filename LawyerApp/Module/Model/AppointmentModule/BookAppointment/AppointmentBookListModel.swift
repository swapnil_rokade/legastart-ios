//
//  AppointmentBookListModel.swift
//
//  Created by Adapting Social on 5/19/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class AppointmentBookListModel: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let settings = "settings"
    static let data = "data"
  }

  // MARK: Properties
  public var settings: AppointmentBookListSettings?
  public var data: AppointmentBookListData?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    settings = AppointmentBookListSettings(json: json[SerializationKeys.settings])
    data = AppointmentBookListData(json: json[SerializationKeys.data])
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = settings { dictionary[SerializationKeys.settings] = value.dictionaryRepresentation() }
    if let value = data { dictionary[SerializationKeys.data] = value.dictionaryRepresentation() }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.settings = aDecoder.decodeObject(forKey: SerializationKeys.settings) as? AppointmentBookListSettings
    self.data = aDecoder.decodeObject(forKey: SerializationKeys.data) as? AppointmentBookListData
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(settings, forKey: SerializationKeys.settings)
    aCoder.encode(data, forKey: SerializationKeys.data)
  }

}
