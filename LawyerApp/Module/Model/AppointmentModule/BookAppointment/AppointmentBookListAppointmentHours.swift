//
//  AppointmentBookListAppointmentHours.swift
//
//  Created by Adapting Social on 5/19/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class AppointmentBookListAppointmentHours: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let currentServerTime = "current_server_time"
    static let startTime = "start_time"
    static let appointmentHoursId = "appointment_hours_id"
    static let endTime = "end_time"
    static let day = "day"
  }

  // MARK: Properties
  public var currentServerTime: String?
  public var startTime: String?
  public var appointmentHoursId: String?
  public var endTime: String?
  public var day: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    currentServerTime = json[SerializationKeys.currentServerTime].string
    startTime = json[SerializationKeys.startTime].string
    appointmentHoursId = json[SerializationKeys.appointmentHoursId].string
    endTime = json[SerializationKeys.endTime].string
    day = json[SerializationKeys.day].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = currentServerTime { dictionary[SerializationKeys.currentServerTime] = value }
    if let value = startTime { dictionary[SerializationKeys.startTime] = value }
    if let value = appointmentHoursId { dictionary[SerializationKeys.appointmentHoursId] = value }
    if let value = endTime { dictionary[SerializationKeys.endTime] = value }
    if let value = day { dictionary[SerializationKeys.day] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.currentServerTime = aDecoder.decodeObject(forKey: SerializationKeys.currentServerTime) as? String
    self.startTime = aDecoder.decodeObject(forKey: SerializationKeys.startTime) as? String
    self.appointmentHoursId = aDecoder.decodeObject(forKey: SerializationKeys.appointmentHoursId) as? String
    self.endTime = aDecoder.decodeObject(forKey: SerializationKeys.endTime) as? String
    self.day = aDecoder.decodeObject(forKey: SerializationKeys.day) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(currentServerTime, forKey: SerializationKeys.currentServerTime)
    aCoder.encode(startTime, forKey: SerializationKeys.startTime)
    aCoder.encode(appointmentHoursId, forKey: SerializationKeys.appointmentHoursId)
    aCoder.encode(endTime, forKey: SerializationKeys.endTime)
    aCoder.encode(day, forKey: SerializationKeys.day)
  }

}
