//
//  AppointmentBookListData.swift
//
//  Created by Adapting Social on 5/19/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class AppointmentBookListData: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let appointmentHours = "appointment_hours"
    static let appointmentBookListing = "appointment_book_listing"
  }

  // MARK: Properties
  public var appointmentHours: [AppointmentBookListAppointmentHours]?
  public var appointmentBookListing: [AppointmentBookListAppointmentBookListing]?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    if let items = json[SerializationKeys.appointmentHours].array { appointmentHours = items.map { AppointmentBookListAppointmentHours(json: $0) } }
    if let items = json[SerializationKeys.appointmentBookListing].array { appointmentBookListing = items.map { AppointmentBookListAppointmentBookListing(json: $0) } }
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = appointmentHours { dictionary[SerializationKeys.appointmentHours] = value.map { $0.dictionaryRepresentation() } }
    if let value = appointmentBookListing { dictionary[SerializationKeys.appointmentBookListing] = value.map { $0.dictionaryRepresentation() } }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.appointmentHours = aDecoder.decodeObject(forKey: SerializationKeys.appointmentHours) as? [AppointmentBookListAppointmentHours]
    self.appointmentBookListing = aDecoder.decodeObject(forKey: SerializationKeys.appointmentBookListing) as? [AppointmentBookListAppointmentBookListing]
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(appointmentHours, forKey: SerializationKeys.appointmentHours)
    aCoder.encode(appointmentBookListing, forKey: SerializationKeys.appointmentBookListing)
  }

}
