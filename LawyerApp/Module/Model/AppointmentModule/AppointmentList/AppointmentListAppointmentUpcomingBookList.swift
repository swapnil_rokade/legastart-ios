//
//  AppointmentListAppointmentUpcomingBookList.swift
//
//  Created by Adapting Social on 5/16/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class AppointmentListAppointmentUpcomingBookList: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let availabilityStartTime = "availability_start_time"
    static let availabilityDate = "availability_date"
    static let availabilityEndTime = "availability_end_time"
    static let appointmentTitle = "appointment_title"
    static let usersId = "users_id"
    static let appointmentId = "appointment_id"
  }

  // MARK: Properties
  public var availabilityStartTime: String?
  public var availabilityDate: String?
  public var availabilityEndTime: String?
  public var appointmentTitle: String?
  public var usersId: String?
  public var appointmentId: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    availabilityStartTime = json[SerializationKeys.availabilityStartTime].string
    availabilityDate = json[SerializationKeys.availabilityDate].string
    availabilityEndTime = json[SerializationKeys.availabilityEndTime].string
    appointmentTitle = json[SerializationKeys.appointmentTitle].string
    usersId = json[SerializationKeys.usersId].string
    appointmentId = json[SerializationKeys.appointmentId].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = availabilityStartTime { dictionary[SerializationKeys.availabilityStartTime] = value }
    if let value = availabilityDate { dictionary[SerializationKeys.availabilityDate] = value }
    if let value = availabilityEndTime { dictionary[SerializationKeys.availabilityEndTime] = value }
    if let value = appointmentTitle { dictionary[SerializationKeys.appointmentTitle] = value }
    if let value = usersId { dictionary[SerializationKeys.usersId] = value }
    if let value = appointmentId { dictionary[SerializationKeys.appointmentId] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.availabilityStartTime = aDecoder.decodeObject(forKey: SerializationKeys.availabilityStartTime) as? String
    self.availabilityDate = aDecoder.decodeObject(forKey: SerializationKeys.availabilityDate) as? String
    self.availabilityEndTime = aDecoder.decodeObject(forKey: SerializationKeys.availabilityEndTime) as? String
    self.appointmentTitle = aDecoder.decodeObject(forKey: SerializationKeys.appointmentTitle) as? String
    self.usersId = aDecoder.decodeObject(forKey: SerializationKeys.usersId) as? String
    self.appointmentId = aDecoder.decodeObject(forKey: SerializationKeys.appointmentId) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(availabilityStartTime, forKey: SerializationKeys.availabilityStartTime)
    aCoder.encode(availabilityDate, forKey: SerializationKeys.availabilityDate)
    aCoder.encode(availabilityEndTime, forKey: SerializationKeys.availabilityEndTime)
    aCoder.encode(appointmentTitle, forKey: SerializationKeys.appointmentTitle)
    aCoder.encode(usersId, forKey: SerializationKeys.usersId)
    aCoder.encode(appointmentId, forKey: SerializationKeys.appointmentId)
  }

}
