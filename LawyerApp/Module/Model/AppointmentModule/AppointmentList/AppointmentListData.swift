//
//  AppointmentListData.swift
//
//  Created by Adapting Social on 5/16/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class AppointmentListData: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let appointmentUpcomingBookList = "appointment_upcoming_book_list"
    static let appointmentPastcomingBookList = "appointment_pastcoming_book_list"
  }

  // MARK: Properties
  public var appointmentUpcomingBookList: [AppointmentListAppointmentUpcomingBookList]?
  public var appointmentPastcomingBookList: [AppointmentListAppointmentPastcomingBookList]?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    if let items = json[SerializationKeys.appointmentUpcomingBookList].array { appointmentUpcomingBookList = items.map { AppointmentListAppointmentUpcomingBookList(json: $0) } }
    if let items = json[SerializationKeys.appointmentPastcomingBookList].array { appointmentPastcomingBookList = items.map { AppointmentListAppointmentPastcomingBookList(json: $0) } }
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = appointmentUpcomingBookList { dictionary[SerializationKeys.appointmentUpcomingBookList] = value.map { $0.dictionaryRepresentation() } }
    if let value = appointmentPastcomingBookList { dictionary[SerializationKeys.appointmentPastcomingBookList] = value.map { $0.dictionaryRepresentation() } }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.appointmentUpcomingBookList = aDecoder.decodeObject(forKey: SerializationKeys.appointmentUpcomingBookList) as? [AppointmentListAppointmentUpcomingBookList]
    self.appointmentPastcomingBookList = aDecoder.decodeObject(forKey: SerializationKeys.appointmentPastcomingBookList) as? [AppointmentListAppointmentPastcomingBookList]
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(appointmentUpcomingBookList, forKey: SerializationKeys.appointmentUpcomingBookList)
    aCoder.encode(appointmentPastcomingBookList, forKey: SerializationKeys.appointmentPastcomingBookList)
  }

}
