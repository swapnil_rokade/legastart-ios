//
//  CompanyListSettings.swift
//
//  Created by Adapting Social on 08/07/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class CompanyListSettings: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let fields = "fields"
    static let success = "success"
    static let nextPage = "next_page"
    static let count = "count"
    static let currPage = "curr_page"
    static let perPage = "per_page"
    static let message = "message"
    static let prevPage = "prev_page"
  }

  // MARK: Properties
  public var fields: [String]?
  public var success: String?
  public var nextPage: String?
  public var count: String?
  public var currPage: String?
  public var perPage: String?
  public var message: String?
  public var prevPage: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    if let items = json[SerializationKeys.fields].array { fields = items.map { $0.stringValue } }
    success = json[SerializationKeys.success].string
    nextPage = json[SerializationKeys.nextPage].string
    count = json[SerializationKeys.count].string
    currPage = json[SerializationKeys.currPage].string
    perPage = json[SerializationKeys.perPage].string
    message = json[SerializationKeys.message].string
    prevPage = json[SerializationKeys.prevPage].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = fields { dictionary[SerializationKeys.fields] = value }
    if let value = success { dictionary[SerializationKeys.success] = value }
    if let value = nextPage { dictionary[SerializationKeys.nextPage] = value }
    if let value = count { dictionary[SerializationKeys.count] = value }
    if let value = currPage { dictionary[SerializationKeys.currPage] = value }
    if let value = perPage { dictionary[SerializationKeys.perPage] = value }
    if let value = message { dictionary[SerializationKeys.message] = value }
    if let value = prevPage { dictionary[SerializationKeys.prevPage] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.fields = aDecoder.decodeObject(forKey: SerializationKeys.fields) as? [String]
    self.success = aDecoder.decodeObject(forKey: SerializationKeys.success) as? String
    self.nextPage = aDecoder.decodeObject(forKey: SerializationKeys.nextPage) as? String
    self.count = aDecoder.decodeObject(forKey: SerializationKeys.count) as? String
    self.currPage = aDecoder.decodeObject(forKey: SerializationKeys.currPage) as? String
    self.perPage = aDecoder.decodeObject(forKey: SerializationKeys.perPage) as? String
    self.message = aDecoder.decodeObject(forKey: SerializationKeys.message) as? String
    self.prevPage = aDecoder.decodeObject(forKey: SerializationKeys.prevPage) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(fields, forKey: SerializationKeys.fields)
    aCoder.encode(success, forKey: SerializationKeys.success)
    aCoder.encode(nextPage, forKey: SerializationKeys.nextPage)
    aCoder.encode(count, forKey: SerializationKeys.count)
    aCoder.encode(currPage, forKey: SerializationKeys.currPage)
    aCoder.encode(perPage, forKey: SerializationKeys.perPage)
    aCoder.encode(message, forKey: SerializationKeys.message)
    aCoder.encode(prevPage, forKey: SerializationKeys.prevPage)
  }

}
