//
//  CompanyListData.swift
//
//  Created by Adapting Social on 08/07/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class CompanyListData: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let latitude = "latitude"
    static let companyId = "company_id"
    static let companyBackGroundImage = "company_back_ground_image"
    static let cityState = "city_state"
    static let mobileNumber = "mobile_number"
    static let companyName = "company_name"
    static let address = "address"
    static let companyEmail = "company_email"
    static let longitude = "longitude"
    static let companyCategoryName = "company_category_name"
    static let companyImage = "company_image"
    static let companyDescription = "company_description"
  }

  // MARK: Properties
  public var latitude: String?
  public var companyId: String?
  public var companyBackGroundImage: String?
  public var cityState: String?
  public var mobileNumber: String?
  public var companyName: String?
  public var address: String?
  public var companyEmail: String?
  public var longitude: String?
  public var companyCategoryName: String?
  public var companyImage: String?
  public var companyDescription: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    latitude = json[SerializationKeys.latitude].string
    companyId = json[SerializationKeys.companyId].string
    companyBackGroundImage = json[SerializationKeys.companyBackGroundImage].string
    cityState = json[SerializationKeys.cityState].string
    mobileNumber = json[SerializationKeys.mobileNumber].string
    companyName = json[SerializationKeys.companyName].string
    address = json[SerializationKeys.address].string
    companyEmail = json[SerializationKeys.companyEmail].string
    longitude = json[SerializationKeys.longitude].string
    companyCategoryName = json[SerializationKeys.companyCategoryName].string
    companyImage = json[SerializationKeys.companyImage].string
    companyDescription = json[SerializationKeys.companyDescription].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = latitude { dictionary[SerializationKeys.latitude] = value }
    if let value = companyId { dictionary[SerializationKeys.companyId] = value }
    if let value = companyBackGroundImage { dictionary[SerializationKeys.companyBackGroundImage] = value }
    if let value = cityState { dictionary[SerializationKeys.cityState] = value }
    if let value = mobileNumber { dictionary[SerializationKeys.mobileNumber] = value }
    if let value = companyName { dictionary[SerializationKeys.companyName] = value }
    if let value = address { dictionary[SerializationKeys.address] = value }
    if let value = companyEmail { dictionary[SerializationKeys.companyEmail] = value }
    if let value = longitude { dictionary[SerializationKeys.longitude] = value }
    if let value = companyCategoryName { dictionary[SerializationKeys.companyCategoryName] = value }
    if let value = companyImage { dictionary[SerializationKeys.companyImage] = value }
    if let value = companyDescription { dictionary[SerializationKeys.companyDescription] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.latitude = aDecoder.decodeObject(forKey: SerializationKeys.latitude) as? String
    self.companyId = aDecoder.decodeObject(forKey: SerializationKeys.companyId) as? String
    self.companyBackGroundImage = aDecoder.decodeObject(forKey: SerializationKeys.companyBackGroundImage) as? String
    self.cityState = aDecoder.decodeObject(forKey: SerializationKeys.cityState) as? String
    self.mobileNumber = aDecoder.decodeObject(forKey: SerializationKeys.mobileNumber) as? String
    self.companyName = aDecoder.decodeObject(forKey: SerializationKeys.companyName) as? String
    self.address = aDecoder.decodeObject(forKey: SerializationKeys.address) as? String
    self.companyEmail = aDecoder.decodeObject(forKey: SerializationKeys.companyEmail) as? String
    self.longitude = aDecoder.decodeObject(forKey: SerializationKeys.longitude) as? String
    self.companyCategoryName = aDecoder.decodeObject(forKey: SerializationKeys.companyCategoryName) as? String
    self.companyImage = aDecoder.decodeObject(forKey: SerializationKeys.companyImage) as? String
    self.companyDescription = aDecoder.decodeObject(forKey: SerializationKeys.companyDescription) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(latitude, forKey: SerializationKeys.latitude)
    aCoder.encode(companyId, forKey: SerializationKeys.companyId)
    aCoder.encode(companyBackGroundImage, forKey: SerializationKeys.companyBackGroundImage)
    aCoder.encode(cityState, forKey: SerializationKeys.cityState)
    aCoder.encode(mobileNumber, forKey: SerializationKeys.mobileNumber)
    aCoder.encode(companyName, forKey: SerializationKeys.companyName)
    aCoder.encode(address, forKey: SerializationKeys.address)
    aCoder.encode(companyEmail, forKey: SerializationKeys.companyEmail)
    aCoder.encode(longitude, forKey: SerializationKeys.longitude)
    aCoder.encode(companyCategoryName, forKey: SerializationKeys.companyCategoryName)
    aCoder.encode(companyImage, forKey: SerializationKeys.companyImage)
    aCoder.encode(companyDescription, forKey: SerializationKeys.companyDescription)
  }

}
