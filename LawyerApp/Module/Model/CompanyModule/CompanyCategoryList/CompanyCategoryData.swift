//
//  CompanyCategoryData.swift
//
//  Created by Adapting Social on 5/2/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class CompanyCategoryData: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let companyCategoryId = "company_category_id"
    static let companyCategoryName = "company_category_name"
  }

  // MARK: Properties
  public var companyCategoryId: String?
  public var companyCategoryName: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    companyCategoryId = json[SerializationKeys.companyCategoryId].string
    companyCategoryName = json[SerializationKeys.companyCategoryName].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = companyCategoryId { dictionary[SerializationKeys.companyCategoryId] = value }
    if let value = companyCategoryName { dictionary[SerializationKeys.companyCategoryName] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.companyCategoryId = aDecoder.decodeObject(forKey: SerializationKeys.companyCategoryId) as? String
    self.companyCategoryName = aDecoder.decodeObject(forKey: SerializationKeys.companyCategoryName) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(companyCategoryId, forKey: SerializationKeys.companyCategoryId)
    aCoder.encode(companyCategoryName, forKey: SerializationKeys.companyCategoryName)
  }

}
