//
//  CompanyCategorySettings.swift
//
//  Created by Adapting Social on 5/2/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class CompanyCategorySettings: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let fields = "fields"
    static let success = "success"
    static let message = "message"
  }

  // MARK: Properties
  public var fields: [String]?
  public var success: String?
  public var message: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    if let items = json[SerializationKeys.fields].array { fields = items.map { $0.stringValue } }
    success = json[SerializationKeys.success].string
    message = json[SerializationKeys.message].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = fields { dictionary[SerializationKeys.fields] = value }
    if let value = success { dictionary[SerializationKeys.success] = value }
    if let value = message { dictionary[SerializationKeys.message] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.fields = aDecoder.decodeObject(forKey: SerializationKeys.fields) as? [String]
    self.success = aDecoder.decodeObject(forKey: SerializationKeys.success) as? String
    self.message = aDecoder.decodeObject(forKey: SerializationKeys.message) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(fields, forKey: SerializationKeys.fields)
    aCoder.encode(success, forKey: SerializationKeys.success)
    aCoder.encode(message, forKey: SerializationKeys.message)
  }

}
