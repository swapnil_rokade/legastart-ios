//
//  SignInData.swift
//
//  Created b Adapting Social on 10/11/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class SignInData: NSObject,NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let checkUserEmailAndPassword = "check_user_email_and_password"
    static let randomFunction = "random_function"
  }

  // MARK: Properties
  public var checkUserEmailAndPassword: [SignInCheckUserEmailAndPassword]?
  public var randomFunction: [SignInRandomFunction]?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    if let items = json[SerializationKeys.checkUserEmailAndPassword].array { checkUserEmailAndPassword = items.map { SignInCheckUserEmailAndPassword(json: $0) } }
    if let items = json[SerializationKeys.randomFunction].array { randomFunction = items.map { SignInRandomFunction(json: $0) } }
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = checkUserEmailAndPassword { dictionary[SerializationKeys.checkUserEmailAndPassword] = value.map { $0.dictionaryRepresentation() } }
    if let value = randomFunction { dictionary[SerializationKeys.randomFunction] = value.map { $0.dictionaryRepresentation() } }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.checkUserEmailAndPassword = aDecoder.decodeObject(forKey: SerializationKeys.checkUserEmailAndPassword) as? [SignInCheckUserEmailAndPassword]
    self.randomFunction = aDecoder.decodeObject(forKey: SerializationKeys.randomFunction) as? [SignInRandomFunction]
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(checkUserEmailAndPassword, forKey: SerializationKeys.checkUserEmailAndPassword)
    aCoder.encode(randomFunction, forKey: SerializationKeys.randomFunction)
  }

}
