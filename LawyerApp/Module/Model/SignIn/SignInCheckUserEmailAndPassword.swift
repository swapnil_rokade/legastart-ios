//
//  SignInCheckUserEmailAndPassword.swift
//
//  Created by Adapting Social on 10/11/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class SignInCheckUserEmailAndPassword: NSObject,NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let blogEmail = "blog_email"
    static let lastName = "last_name"
    static let firstName = "first_name"
    static let usersId = "users_id"
    static let moblieNumber = "moblie_number"
    static let blogPasswsord = "blog_passwsord"
    static let uPaymentStatus = "u_payment_status"
    static let subscriptionId = "subscription_id"
    static let profilePicture = "profile_picture"
    static let blogId = "blog_id"
  }

  // MARK: Properties
  public var blogEmail: String?
  public var lastName: String?
  public var firstName: String?
  public var usersId: String?
  public var moblieNumber: String?
  public var blogPasswsord: String?
  public var uPaymentStatus: String?
  public var subscriptionId: String?
  public var profilePicture: String?
  public var blogId: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    blogEmail = json[SerializationKeys.blogEmail].string
    lastName = json[SerializationKeys.lastName].string
    firstName = json[SerializationKeys.firstName].string
    usersId = json[SerializationKeys.usersId].string
    moblieNumber = json[SerializationKeys.moblieNumber].string
    blogPasswsord = json[SerializationKeys.blogPasswsord].string
    uPaymentStatus = json[SerializationKeys.uPaymentStatus].string
    subscriptionId = json[SerializationKeys.subscriptionId].string
    profilePicture = json[SerializationKeys.profilePicture].string
    blogId = json[SerializationKeys.blogId].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = blogEmail { dictionary[SerializationKeys.blogEmail] = value }
    if let value = lastName { dictionary[SerializationKeys.lastName] = value }
    if let value = firstName { dictionary[SerializationKeys.firstName] = value }
    if let value = usersId { dictionary[SerializationKeys.usersId] = value }
    if let value = moblieNumber { dictionary[SerializationKeys.moblieNumber] = value }
    if let value = blogPasswsord { dictionary[SerializationKeys.blogPasswsord] = value }
    if let value = uPaymentStatus { dictionary[SerializationKeys.uPaymentStatus] = value }
    if let value = subscriptionId { dictionary[SerializationKeys.subscriptionId] = value }
    if let value = profilePicture { dictionary[SerializationKeys.profilePicture] = value }
    if let value = blogId { dictionary[SerializationKeys.blogId] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.blogEmail = aDecoder.decodeObject(forKey: SerializationKeys.blogEmail) as? String
    self.lastName = aDecoder.decodeObject(forKey: SerializationKeys.lastName) as? String
    self.firstName = aDecoder.decodeObject(forKey: SerializationKeys.firstName) as? String
    self.usersId = aDecoder.decodeObject(forKey: SerializationKeys.usersId) as? String
    self.moblieNumber = aDecoder.decodeObject(forKey: SerializationKeys.moblieNumber) as? String
    self.blogPasswsord = aDecoder.decodeObject(forKey: SerializationKeys.blogPasswsord) as? String
    self.uPaymentStatus = aDecoder.decodeObject(forKey: SerializationKeys.uPaymentStatus) as? String
    self.subscriptionId = aDecoder.decodeObject(forKey: SerializationKeys.subscriptionId) as? String
    self.profilePicture = aDecoder.decodeObject(forKey: SerializationKeys.profilePicture) as? String
    self.blogId = aDecoder.decodeObject(forKey: SerializationKeys.blogId) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(blogEmail, forKey: SerializationKeys.blogEmail)
    aCoder.encode(lastName, forKey: SerializationKeys.lastName)
    aCoder.encode(firstName, forKey: SerializationKeys.firstName)
    aCoder.encode(usersId, forKey: SerializationKeys.usersId)
    aCoder.encode(moblieNumber, forKey: SerializationKeys.moblieNumber)
    aCoder.encode(blogPasswsord, forKey: SerializationKeys.blogPasswsord)
    aCoder.encode(uPaymentStatus, forKey: SerializationKeys.uPaymentStatus)
    aCoder.encode(subscriptionId, forKey: SerializationKeys.subscriptionId)
    aCoder.encode(profilePicture, forKey: SerializationKeys.profilePicture)
    aCoder.encode(blogId, forKey: SerializationKeys.blogId)
  }

}
