//
//  CaseListData.swift
//
//  Created by Adapting Social on 15/06/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class CaseListData: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let documentListings = "document_listings"
    static let concerns = "concerns"
    static let agreement = "agreement"
    static let usersName = "users_name"
    static let caseId = "case_id"
    static let documentType = "document_type"
    static let caseTitle = "case_title"
  }

  // MARK: Properties
  public var documentListings: [CaseListDocumentListings]?
  public var concerns: String?
  public var agreement: String?
  public var usersName: String?
  public var caseId: String?
  public var documentType: String?
  public var caseTitle: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    if let items = json[SerializationKeys.documentListings].array { documentListings = items.map { CaseListDocumentListings(json: $0) } }
    concerns = json[SerializationKeys.concerns].string
    agreement = json[SerializationKeys.agreement].string
    usersName = json[SerializationKeys.usersName].string
    caseId = json[SerializationKeys.caseId].string
    documentType = json[SerializationKeys.documentType].string
    caseTitle = json[SerializationKeys.caseTitle].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = documentListings { dictionary[SerializationKeys.documentListings] = value.map { $0.dictionaryRepresentation() } }
    if let value = concerns { dictionary[SerializationKeys.concerns] = value }
    if let value = agreement { dictionary[SerializationKeys.agreement] = value }
    if let value = usersName { dictionary[SerializationKeys.usersName] = value }
    if let value = caseId { dictionary[SerializationKeys.caseId] = value }
    if let value = documentType { dictionary[SerializationKeys.documentType] = value }
    if let value = caseTitle { dictionary[SerializationKeys.caseTitle] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.documentListings = aDecoder.decodeObject(forKey: SerializationKeys.documentListings) as? [CaseListDocumentListings]
    self.concerns = aDecoder.decodeObject(forKey: SerializationKeys.concerns) as? String
    self.agreement = aDecoder.decodeObject(forKey: SerializationKeys.agreement) as? String
    self.usersName = aDecoder.decodeObject(forKey: SerializationKeys.usersName) as? String
    self.caseId = aDecoder.decodeObject(forKey: SerializationKeys.caseId) as? String
    self.documentType = aDecoder.decodeObject(forKey: SerializationKeys.documentType) as? String
    self.caseTitle = aDecoder.decodeObject(forKey: SerializationKeys.caseTitle) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(documentListings, forKey: SerializationKeys.documentListings)
    aCoder.encode(concerns, forKey: SerializationKeys.concerns)
    aCoder.encode(agreement, forKey: SerializationKeys.agreement)
    aCoder.encode(usersName, forKey: SerializationKeys.usersName)
    aCoder.encode(caseId, forKey: SerializationKeys.caseId)
    aCoder.encode(documentType, forKey: SerializationKeys.documentType)
    aCoder.encode(caseTitle, forKey: SerializationKeys.caseTitle)
  }

}
