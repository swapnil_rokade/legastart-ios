//
//  CaseListDocumentListings.swift
//
//  Created by Adapting Social on 15/06/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class CaseListDocumentListings: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let documentType = "document_type"
    static let date = "date"
    static let document = "document"
    static let documentName = "document_name"
    static let documentId = "document_id"
    static let usersId = "users_id"
  }

  // MARK: Properties
  public var documentType: String?
  public var date: String?
  public var document: String?
  public var documentName: String?
  public var documentId: String?
  public var usersId: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    documentType = json[SerializationKeys.documentType].string
    date = json[SerializationKeys.date].string
    document = json[SerializationKeys.document].string
    documentName = json[SerializationKeys.documentName].string
    documentId = json[SerializationKeys.documentId].string
    usersId = json[SerializationKeys.usersId].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = documentType { dictionary[SerializationKeys.documentType] = value }
    if let value = date { dictionary[SerializationKeys.date] = value }
    if let value = document { dictionary[SerializationKeys.document] = value }
    if let value = documentName { dictionary[SerializationKeys.documentName] = value }
    if let value = documentId { dictionary[SerializationKeys.documentId] = value }
    if let value = usersId { dictionary[SerializationKeys.usersId] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.documentType = aDecoder.decodeObject(forKey: SerializationKeys.documentType) as? String
    self.date = aDecoder.decodeObject(forKey: SerializationKeys.date) as? String
    self.document = aDecoder.decodeObject(forKey: SerializationKeys.document) as? String
    self.documentName = aDecoder.decodeObject(forKey: SerializationKeys.documentName) as? String
    self.documentId = aDecoder.decodeObject(forKey: SerializationKeys.documentId) as? String
    self.usersId = aDecoder.decodeObject(forKey: SerializationKeys.usersId) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(documentType, forKey: SerializationKeys.documentType)
    aCoder.encode(date, forKey: SerializationKeys.date)
    aCoder.encode(document, forKey: SerializationKeys.document)
    aCoder.encode(documentName, forKey: SerializationKeys.documentName)
    aCoder.encode(documentId, forKey: SerializationKeys.documentId)
    aCoder.encode(usersId, forKey: SerializationKeys.usersId)
  }

}
