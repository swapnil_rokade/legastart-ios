//
//  BlogCommentLikeDislikeData.swift
//
//  Created by Adapting Social on 03/05/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class BlogCommentLikeDislikeData: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let blogLike = "blog_like"
    static let countLike = "count_like"
  }

  // MARK: Properties
  public var blogLike: String?
  public var countLike: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    blogLike = json[SerializationKeys.blogLike].string
    countLike = json[SerializationKeys.countLike].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = blogLike { dictionary[SerializationKeys.blogLike] = value }
    if let value = countLike { dictionary[SerializationKeys.countLike] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.blogLike = aDecoder.decodeObject(forKey: SerializationKeys.blogLike) as? String
    self.countLike = aDecoder.decodeObject(forKey: SerializationKeys.countLike) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(blogLike, forKey: SerializationKeys.blogLike)
    aCoder.encode(countLike, forKey: SerializationKeys.countLike)
  }

}
