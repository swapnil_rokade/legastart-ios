//
//  BlogListData.swift
//
//  Created by Adapting Social on 02/05/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class BlogListData: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let blogImage = "blog_image"
    static let blogDate = "blog_date"
    static let blogName = "blog_name"
    static let commentListing = "comment_listing"
    static let blogDescripation = "blog_descripation"
    static let blogId = "blog_id"
  }

  // MARK: Properties
  public var blogImage: String?
  public var blogDate: String?
  public var blogName: String?
  public var commentListing: [BlogListCommentListing]?
  public var blogDescripation: String?
  public var blogId: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    blogImage = json[SerializationKeys.blogImage].string
    blogDate = json[SerializationKeys.blogDate].string
    blogName = json[SerializationKeys.blogName].string
    if let items = json[SerializationKeys.commentListing].array { commentListing = items.map { BlogListCommentListing(json: $0) } }
    blogDescripation = json[SerializationKeys.blogDescripation].string
    blogId = json[SerializationKeys.blogId].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = blogImage { dictionary[SerializationKeys.blogImage] = value }
    if let value = blogDate { dictionary[SerializationKeys.blogDate] = value }
    if let value = blogName { dictionary[SerializationKeys.blogName] = value }
    if let value = commentListing { dictionary[SerializationKeys.commentListing] = value.map { $0.dictionaryRepresentation() } }
    if let value = blogDescripation { dictionary[SerializationKeys.blogDescripation] = value }
    if let value = blogId { dictionary[SerializationKeys.blogId] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.blogImage = aDecoder.decodeObject(forKey: SerializationKeys.blogImage) as? String
    self.blogDate = aDecoder.decodeObject(forKey: SerializationKeys.blogDate) as? String
    self.blogName = aDecoder.decodeObject(forKey: SerializationKeys.blogName) as? String
    self.commentListing = aDecoder.decodeObject(forKey: SerializationKeys.commentListing) as? [BlogListCommentListing]
    self.blogDescripation = aDecoder.decodeObject(forKey: SerializationKeys.blogDescripation) as? String
    self.blogId = aDecoder.decodeObject(forKey: SerializationKeys.blogId) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(blogImage, forKey: SerializationKeys.blogImage)
    aCoder.encode(blogDate, forKey: SerializationKeys.blogDate)
    aCoder.encode(blogName, forKey: SerializationKeys.blogName)
    aCoder.encode(commentListing, forKey: SerializationKeys.commentListing)
    aCoder.encode(blogDescripation, forKey: SerializationKeys.blogDescripation)
    aCoder.encode(blogId, forKey: SerializationKeys.blogId)
  }

}
