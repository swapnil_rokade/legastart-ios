//
//  BlogAddCommentCommentListing.swift
//
//  Created by Adapting Social on 03/05/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class BlogAddCommentCommentListing: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let name = "name"
    static let usersId = "users_id"
    static let date = "date"
    static let comment = "comment"
    static let blogCommentId = "blog_comment_id"
    static let blogLike = "blog_like"
    static let profilePicture = "profile_picture"
    static let countLike = "count_like"
  }

  // MARK: Properties
  public var name: String?
  public var usersId: String?
  public var date: String?
  public var comment: String?
  public var blogCommentId: String?
  public var blogLike: String?
  public var profilePicture: String?
  public var countLike: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    name = json[SerializationKeys.name].string
    usersId = json[SerializationKeys.usersId].string
    date = json[SerializationKeys.date].string
    comment = json[SerializationKeys.comment].string
    blogCommentId = json[SerializationKeys.blogCommentId].string
    blogLike = json[SerializationKeys.blogLike].string
    profilePicture = json[SerializationKeys.profilePicture].string
    countLike = json[SerializationKeys.countLike].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = name { dictionary[SerializationKeys.name] = value }
    if let value = usersId { dictionary[SerializationKeys.usersId] = value }
    if let value = date { dictionary[SerializationKeys.date] = value }
    if let value = comment { dictionary[SerializationKeys.comment] = value }
    if let value = blogCommentId { dictionary[SerializationKeys.blogCommentId] = value }
    if let value = blogLike { dictionary[SerializationKeys.blogLike] = value }
    if let value = profilePicture { dictionary[SerializationKeys.profilePicture] = value }
    if let value = countLike { dictionary[SerializationKeys.countLike] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.name = aDecoder.decodeObject(forKey: SerializationKeys.name) as? String
    self.usersId = aDecoder.decodeObject(forKey: SerializationKeys.usersId) as? String
    self.date = aDecoder.decodeObject(forKey: SerializationKeys.date) as? String
    self.comment = aDecoder.decodeObject(forKey: SerializationKeys.comment) as? String
    self.blogCommentId = aDecoder.decodeObject(forKey: SerializationKeys.blogCommentId) as? String
    self.blogLike = aDecoder.decodeObject(forKey: SerializationKeys.blogLike) as? String
    self.profilePicture = aDecoder.decodeObject(forKey: SerializationKeys.profilePicture) as? String
    self.countLike = aDecoder.decodeObject(forKey: SerializationKeys.countLike) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(name, forKey: SerializationKeys.name)
    aCoder.encode(usersId, forKey: SerializationKeys.usersId)
    aCoder.encode(date, forKey: SerializationKeys.date)
    aCoder.encode(comment, forKey: SerializationKeys.comment)
    aCoder.encode(blogCommentId, forKey: SerializationKeys.blogCommentId)
    aCoder.encode(blogLike, forKey: SerializationKeys.blogLike)
    aCoder.encode(profilePicture, forKey: SerializationKeys.profilePicture)
    aCoder.encode(countLike, forKey: SerializationKeys.countLike)
  }

}
