//
//  BlogAddCommentData.swift
//
//  Created by Adapting Social on 03/05/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class BlogAddCommentData: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let addComment = "add_comment"
    static let commentListing = "comment_listing"
    static let commentDetails = "Comment_details"
  }

  // MARK: Properties
  public var addComment: BlogAddCommentAddComment?
  public var commentListing: [BlogAddCommentCommentListing]?
  public var commentDetails: [BlogAddCommentCommentDetails]?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    addComment = BlogAddCommentAddComment(json: json[SerializationKeys.addComment])
    if let items = json[SerializationKeys.commentListing].array { commentListing = items.map { BlogAddCommentCommentListing(json: $0) } }
    if let items = json[SerializationKeys.commentDetails].array { commentDetails = items.map { BlogAddCommentCommentDetails(json: $0) } }
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = addComment { dictionary[SerializationKeys.addComment] = value.dictionaryRepresentation() }
    if let value = commentListing { dictionary[SerializationKeys.commentListing] = value.map { $0.dictionaryRepresentation() } }
    if let value = commentDetails { dictionary[SerializationKeys.commentDetails] = value.map { $0.dictionaryRepresentation() } }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.addComment = aDecoder.decodeObject(forKey: SerializationKeys.addComment) as? BlogAddCommentAddComment
    self.commentListing = aDecoder.decodeObject(forKey: SerializationKeys.commentListing) as? [BlogAddCommentCommentListing]
    self.commentDetails = aDecoder.decodeObject(forKey: SerializationKeys.commentDetails) as? [BlogAddCommentCommentDetails]
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(addComment, forKey: SerializationKeys.addComment)
    aCoder.encode(commentListing, forKey: SerializationKeys.commentListing)
    aCoder.encode(commentDetails, forKey: SerializationKeys.commentDetails)
  }

}
