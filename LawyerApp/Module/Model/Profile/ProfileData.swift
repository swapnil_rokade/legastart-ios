//
//  ProfileData.swift
//
//  Created by Adapting Social on 5/1/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class ProfileData: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let lastName = "last_name"
    static let email = "email"
    static let firstName = "first_name"
    static let backGroundImage = "back_ground_image"
    static let moblieNumber = "moblie_number"
    static let usersId = "users_id"
    static let profileImage = "profile_image"
    static let dateOfBirth = "date_of_birth"
  }

  // MARK: Properties
  public var lastName: String?
  public var email: String?
  public var firstName: String?
  public var backGroundImage: String?
  public var moblieNumber: String?
  public var usersId: String?
  public var profileImage: String?
  public var dateOfBirth: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    lastName = json[SerializationKeys.lastName].string
    email = json[SerializationKeys.email].string
    firstName = json[SerializationKeys.firstName].string
    backGroundImage = json[SerializationKeys.backGroundImage].string
    moblieNumber = json[SerializationKeys.moblieNumber].string
    usersId = json[SerializationKeys.usersId].string
    profileImage = json[SerializationKeys.profileImage].string
    dateOfBirth = json[SerializationKeys.dateOfBirth].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = lastName { dictionary[SerializationKeys.lastName] = value }
    if let value = email { dictionary[SerializationKeys.email] = value }
    if let value = firstName { dictionary[SerializationKeys.firstName] = value }
    if let value = backGroundImage { dictionary[SerializationKeys.backGroundImage] = value }
    if let value = moblieNumber { dictionary[SerializationKeys.moblieNumber] = value }
    if let value = usersId { dictionary[SerializationKeys.usersId] = value }
    if let value = profileImage { dictionary[SerializationKeys.profileImage] = value }
    if let value = dateOfBirth { dictionary[SerializationKeys.dateOfBirth] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.lastName = aDecoder.decodeObject(forKey: SerializationKeys.lastName) as? String
    self.email = aDecoder.decodeObject(forKey: SerializationKeys.email) as? String
    self.firstName = aDecoder.decodeObject(forKey: SerializationKeys.firstName) as? String
    self.backGroundImage = aDecoder.decodeObject(forKey: SerializationKeys.backGroundImage) as? String
    self.moblieNumber = aDecoder.decodeObject(forKey: SerializationKeys.moblieNumber) as? String
    self.usersId = aDecoder.decodeObject(forKey: SerializationKeys.usersId) as? String
    self.profileImage = aDecoder.decodeObject(forKey: SerializationKeys.profileImage) as? String
    self.dateOfBirth = aDecoder.decodeObject(forKey: SerializationKeys.dateOfBirth) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(lastName, forKey: SerializationKeys.lastName)
    aCoder.encode(email, forKey: SerializationKeys.email)
    aCoder.encode(firstName, forKey: SerializationKeys.firstName)
    aCoder.encode(backGroundImage, forKey: SerializationKeys.backGroundImage)
    aCoder.encode(moblieNumber, forKey: SerializationKeys.moblieNumber)
    aCoder.encode(usersId, forKey: SerializationKeys.usersId)
    aCoder.encode(profileImage, forKey: SerializationKeys.profileImage)
    aCoder.encode(dateOfBirth, forKey: SerializationKeys.dateOfBirth)
  }

}
