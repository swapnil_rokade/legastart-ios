//
//  MyDocumentListData.swift
//
//  Created by Adapting Social on 08/07/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class MyDocumentListData: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let docFile = "doc_file"
    static let userDocumentsId = "user_documents_id"
    static let documemtName = "documemt_name"
  }

  // MARK: Properties
  public var docFile: String?
  public var userDocumentsId: String?
  public var documemtName: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    docFile = json[SerializationKeys.docFile].string
    userDocumentsId = json[SerializationKeys.userDocumentsId].string
    documemtName = json[SerializationKeys.documemtName].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = docFile { dictionary[SerializationKeys.docFile] = value }
    if let value = userDocumentsId { dictionary[SerializationKeys.userDocumentsId] = value }
    if let value = documemtName { dictionary[SerializationKeys.documemtName] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.docFile = aDecoder.decodeObject(forKey: SerializationKeys.docFile) as? String
    self.userDocumentsId = aDecoder.decodeObject(forKey: SerializationKeys.userDocumentsId) as? String
    self.documemtName = aDecoder.decodeObject(forKey: SerializationKeys.documemtName) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(docFile, forKey: SerializationKeys.docFile)
    aCoder.encode(userDocumentsId, forKey: SerializationKeys.userDocumentsId)
    aCoder.encode(documemtName, forKey: SerializationKeys.documemtName)
  }

}
