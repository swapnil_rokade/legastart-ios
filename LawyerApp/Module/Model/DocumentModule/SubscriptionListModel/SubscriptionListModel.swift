//
//  SubscriptionListModel.swift
//
//  Created by Adapting Social on 23/06/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class SubscriptionListModel: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let settings = "settings"
    static let data = "data"
  }

  // MARK: Properties
  public var settings: SubscriptionListSettings?
  public var data: [SubscriptionListData]?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    settings = SubscriptionListSettings(json: json[SerializationKeys.settings])
    if let items = json[SerializationKeys.data].array { data = items.map { SubscriptionListData(json: $0) } }
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = settings { dictionary[SerializationKeys.settings] = value.dictionaryRepresentation() }
    if let value = data { dictionary[SerializationKeys.data] = value.map { $0.dictionaryRepresentation() } }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.settings = aDecoder.decodeObject(forKey: SerializationKeys.settings) as? SubscriptionListSettings
    self.data = aDecoder.decodeObject(forKey: SerializationKeys.data) as? [SubscriptionListData]
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(settings, forKey: SerializationKeys.settings)
    aCoder.encode(data, forKey: SerializationKeys.data)
  }

}
