//
//  DocumentListingData.swift
//
//  Created by Adapting Social on 24/07/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class DocumentListingData: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let usersDocumentStatus = "users_document_status"
    static let documentName = "document_name"
    static let amount = "amount"
    static let usersPaymentStatus = "users_payment_status"
    static let documentType = "document_type"
    static let documentId = "document_id"
    static let dcoument = "dcoument"
  }

  // MARK: Properties
  public var usersDocumentStatus: String?
  public var documentName: String?
  public var amount: String?
  public var usersPaymentStatus: String?
  public var documentType: String?
  public var documentId: String?
  public var dcoument: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    usersDocumentStatus = json[SerializationKeys.usersDocumentStatus].string
    documentName = json[SerializationKeys.documentName].string
    amount = json[SerializationKeys.amount].string
    usersPaymentStatus = json[SerializationKeys.usersPaymentStatus].string
    documentType = json[SerializationKeys.documentType].string
    documentId = json[SerializationKeys.documentId].string
    dcoument = json[SerializationKeys.dcoument].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = usersDocumentStatus { dictionary[SerializationKeys.usersDocumentStatus] = value }
    if let value = documentName { dictionary[SerializationKeys.documentName] = value }
    if let value = amount { dictionary[SerializationKeys.amount] = value }
    if let value = usersPaymentStatus { dictionary[SerializationKeys.usersPaymentStatus] = value }
    if let value = documentType { dictionary[SerializationKeys.documentType] = value }
    if let value = documentId { dictionary[SerializationKeys.documentId] = value }
    if let value = dcoument { dictionary[SerializationKeys.dcoument] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.usersDocumentStatus = aDecoder.decodeObject(forKey: SerializationKeys.usersDocumentStatus) as? String
    self.documentName = aDecoder.decodeObject(forKey: SerializationKeys.documentName) as? String
    self.amount = aDecoder.decodeObject(forKey: SerializationKeys.amount) as? String
    self.usersPaymentStatus = aDecoder.decodeObject(forKey: SerializationKeys.usersPaymentStatus) as? String
    self.documentType = aDecoder.decodeObject(forKey: SerializationKeys.documentType) as? String
    self.documentId = aDecoder.decodeObject(forKey: SerializationKeys.documentId) as? String
    self.dcoument = aDecoder.decodeObject(forKey: SerializationKeys.dcoument) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(usersDocumentStatus, forKey: SerializationKeys.usersDocumentStatus)
    aCoder.encode(documentName, forKey: SerializationKeys.documentName)
    aCoder.encode(amount, forKey: SerializationKeys.amount)
    aCoder.encode(usersPaymentStatus, forKey: SerializationKeys.usersPaymentStatus)
    aCoder.encode(documentType, forKey: SerializationKeys.documentType)
    aCoder.encode(documentId, forKey: SerializationKeys.documentId)
    aCoder.encode(dcoument, forKey: SerializationKeys.dcoument)
  }

}
