//
//  CALayer+RuntimeAttribute.swift
//  LegalStart
//
//  Created by Adapting Social on 4/24/17.
//  Copyright © 2017 Adapting Social. All rights reserved.
//

import UIKit


extension UIView {
    
    var borderIBColor: UIColor? {
        set(newColor) {
            
            self.layer.borderColor = newColor?.cgColor
        }
        get {
            return UIColor(cgColor: self.layer.borderColor!)
        }
    }
    
    // Dismiss the keyBoard for current view
    func hideKeyBoard() -> Void {
        self.endEditing(true)
    }
}

