//
//  UITextView+CustomTextView.swift
//  ReHQ Agent
//
//  Created by Adapting Social on 20/06/16.
//  Copyright © 2016 ReHQ. All rights reserved.
//

import UIKit


extension UITextField {
    
     func isTextFieldBlank() -> Bool {
        let strText = self.text
        if self.isEmptyString(strText!) {
            self.becomeFirstResponder()
            return true
        }
        return false
    }
    
     func isEmptyString(_ strText: String) -> Bool {
        
        let tempText = strText.trimmingCharacters(in: CharacterSet.whitespaces)
        if tempText.isEmpty {
            return true
        }
        return false
    }
    
    func isValidEmail() -> Bool {
        
    
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let result = emailTest.evaluate(with: self.text)
        
        if !result {
            self.becomeFirstResponder()
        }
        return result
    }
    
    
    func isValidPhoneNumber() -> Bool {
        
        let numberRegEx = "[+]?[(]?[0-9]{3}[)]?[-\\s]?[0-9]{3}[-\\s]?[0-9]{4,9}"
        let numberPred = NSPredicate(format: "SELF MATCHES %@", numberRegEx)
        let result =  numberPred.evaluate(with: self.text)
        
        if !result {
            self.becomeFirstResponder()
        }
        return result
    }
    
    
    // Password must contain 6 characr or more
    func isValidPassword() -> Bool {
        let strText = self.text!
        if  self.isEmptyString(strText) {
            self.becomeFirstResponder()
            return false
        }
        else if strText.characters.count >= 6 {
            return true
        }
        self.becomeFirstResponder()
        return false
    }
}


