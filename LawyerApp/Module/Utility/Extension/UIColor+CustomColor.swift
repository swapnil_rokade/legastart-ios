//
//  UIColor+CustomColor.swift
//  ReHQ Agent
//
//  Created by Adapting Social on 20/06/16.
//  Copyright © 2016 ReHQ. All rights reserved.
//

import UIKit

extension UIColor {
    
    public static func colorFromCode(_ code: Int) -> UIColor {
        let red = CGFloat(((code & 0xFF0000) >> 16)) / 255
        let green = CGFloat(((code & 0xFF00) >> 8)) / 255
        let blue = CGFloat((code & 0xFF)) / 255
        
        return UIColor(red: red, green: green, blue: blue, alpha: 1)
    }
    
    public static func colorFromCode(_ code: Int, AndAlpha alpha: CGFloat) -> UIColor {
        let red = CGFloat(((code & 0xFF0000) >> 16)) / 255
        let green = CGFloat(((code & 0xFF00) >> 8)) / 255
        let blue = CGFloat((code & 0xFF)) / 255
        
        return UIColor(red: red, green: green, blue: blue, alpha: alpha)
    }
    
//    func ColorWithHex(_ hexColor: Int) -> UIColor {
//        return self.ColorWithHex(hexColor, AndAlpha: 1.0)
//    }
//    
//    func ColorWithHex(_ hexColor: Int, AndAlpha alpha: CGFloat) -> UIColor {
//        
//        let red = CGFloat((hexColor & 0xFF0000) >> 16) / 0xFF
//        let green = CGFloat((hexColor & 0x00FF00) >> 8) / 0xFF
//        let blue = CGFloat(hexColor & 0x0000FF) / 0xFF
//        
//        let alpha = CGFloat(1.0)
//        
//        return UIColor(red: red, green: green, blue: blue, alpha: alpha)
//        
//    }
//    
//    convenience init(red: Int, green: Int, blue: Int) {
//        assert(red >= 0 && red <= 255, "Invalid red component")
//        assert(green >= 0 && green <= 255, "Invalid green component")
//        assert(blue >= 0 && blue <= 255, "Invalid blue component")
//        
//        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
//    }
//    
//    convenience init(netHex:Int) {
//        self.init(red:(netHex >> 16) & 0xff, green:(netHex >> 8) & 0xff, blue:netHex & 0xff)
//    }
}
