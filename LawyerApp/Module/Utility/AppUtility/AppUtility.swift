//
//  AppUtility.swift
//  LegalStart
//
//  Created by Adapting Social on 26/04/17.
//  Copyright © 2017 Adapting Social. All rights reserved.
//

import Foundation
import UIKit

extension Date{

    func stringFromDate(format : String) -> String {
        
        if format.characters.count == 0 {
            return ""
        }
        let df = DateFormatter()
        df.dateFormat = format
        
        return df.string(from: self)
    }
    

}
extension String {
    
    var isAlphanumeric : Bool {
        
        return range(of: "^[a-zA-Z0-9\\s]+$", options: .regularExpression, range: nil, locale: nil) != nil
    }
    var isAlphabetsAndSpaces : Bool {
        return range(of: "^[a-zA-Z\\s]+$", options: .regularExpression, range: nil, locale: nil) != nil
    }
    var isPhoneNumber : Bool {
        return range(of: "^[0-9]{6,14}$", options: .regularExpression, range: nil, locale: nil) != nil
        
    }
    var isNumber : Bool {
        return range(of: "^([0-9]*|[0-9]*[.][0-9]*)$", options: .regularExpression, range: nil, locale: nil) != nil
        
    }
    var isEmail : Bool {
        return range(of: "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}", options: .regularExpression, range: nil, locale: nil) != nil

    }
    var isZipCode : Bool {
        return range(of: "^(0[289][0-9]{2})|([1345689][0-9]{3})|(2[0-8][0-9]{2})|(290[0-9])|(291[0-4])|(7[0-4][0-9]{2})|(7[8-9][0-9]{2})$", options: .regularExpression, range: nil, locale: nil) != nil
    }
    
    func isEmptyString() -> Bool {
        let abc : Array<String> = []
        let tempText = self.trimmingCharacters(in: CharacterSet.whitespaces)
        if tempText.isEmpty {
            return true
        }
        return false
    }
    
    func validatePassword() -> Bool {
        //        let regExPattern = "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%]).{6,40})"
        let regExPattern = "((?=.*\\d)(?=.*[a-zA-Z]).{6,40})"
        
        let passwordValid = NSPredicate(format:"SELF MATCHES %@", regExPattern)
        
        let boolValidator = passwordValid.evaluate(with: self)
        
        return boolValidator
    }
    
    func isValidPassword() -> Bool
    {
        let emailRegEx = "^(?=.*[A-Z])(?=.*[0-9])(?=.*[a-z]).{6,}$"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        
        return emailTest.evaluate(with: self)
        
    }

    func validateMinimumLength(_ minimumLength : NSInteger) -> Bool {
        
        let strValue = self.removeWhiteSpace()
        
        if (strValue.characters.count < minimumLength) {
            return false
        }
        
        return true
    }
    
    func validateMaximumLength(_ maximumLength : NSInteger) -> Bool {
        
        let strValue = self.removeWhiteSpace()
        
        if (strValue.characters.count > maximumLength) {
            return false
        }
        
        return true
        
    }


    /**
     Method will remove white Space from String
     
     - returns: New String after removing White space from existing String.
     */
    func removeWhiteSpace() -> String {
        let strValue = self.trimmingCharacters(in: NSMutableCharacterSet.whitespaceAndNewline() as CharacterSet)
        return strValue
    }
    var html2AttributedString: NSAttributedString? {
        guard
            let data = data(using: String.Encoding.utf8)
            else { return nil }
        do {
            return try NSAttributedString(data: data, options: [NSDocumentTypeDocumentAttribute:NSHTMLTextDocumentType,NSCharacterEncodingDocumentAttribute:String.Encoding.utf8], documentAttributes: nil)
        } catch let error as NSError {
            print(error.localizedDescription)
            return  nil
        }
    }
    var html2String: String {
        return html2AttributedString?.string ?? ""
    }

}
extension String {
    func attributedStringFromHTML(completionBlock:@escaping (NSAttributedString?) ->()) {
        guard let data = data(using: String.Encoding.utf8) else {
            print("Unable to decode data from html string: \(self)")
            return completionBlock(nil)
        }
        
        let options = [NSDocumentTypeDocumentAttribute : NSHTMLTextDocumentType,
                       NSCharacterEncodingDocumentAttribute: NSNumber(value:String.Encoding.utf8.rawValue)] as [String : Any]
        
        DispatchQueue.main.async {
            if let attributedString = try? NSAttributedString(data: data, options: options, documentAttributes: nil) {
                completionBlock(attributedString)
            } else {
                print("Unable to create attributed string from html string: \(self)")
                completionBlock(nil)
            }
        }
    }
}

extension UIImage {
    
    var uncompressedPNGData: Data      { return UIImagePNGRepresentation(self)!     }
    var highestQualityJPEGNSData: Data { return UIImageJPEGRepresentation(self, 1.0)!  }
    var highQualityJPEGNSData: Data    { return UIImageJPEGRepresentation(self, 0.75)! }
    var mediumQualityJPEGNSData: Data  { return UIImageJPEGRepresentation(self, 0.5)!  }
    var lowQualityJPEGNSData: Data     { return UIImageJPEGRepresentation(self, 0.25)! }
    var lowestQualityJPEGNSData:Data   { return UIImageJPEGRepresentation(self, 0.0)!  }
    
    
    // Resize image to new width
    func resizeImage(_ newWidth: CGFloat) -> UIImage {
        
        let aNewWidth = min(newWidth, self.size.width)
        let scale = aNewWidth / self.size.width
        let newHeight = self.size.height * scale
        UIGraphicsBeginImageContext(CGSize(width: aNewWidth, height: newHeight))
        self.draw(in: CGRect(x: 0, y: 0, width: aNewWidth, height: newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
}
extension String {
    
    func widthOfString(usingFont font: UIFont) -> CGFloat {
        let fontAttributes = [NSFontAttributeName: font]
        let size = self.size(attributes: fontAttributes)
        return size.width
    }
    
    func heightOfString(usingFont font: UIFont) -> CGFloat {
        let fontAttributes = [NSFontAttributeName: font]
        let size = self.size(attributes: fontAttributes)
        return size.height
    }
    
}
extension String {
    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSFontAttributeName: font], context: nil)
        
        return boundingBox.height
    }
    
    func width(withConstraintedHeight height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSFontAttributeName: font], context: nil)
        
        return boundingBox.width
    }
}
extension UIViewController {
    var className: String {
        return NSStringFromClass(self.classForCoder).components(separatedBy: ".").last!;
    }
    open override func awakeFromNib() {
        super.awakeFromNib()
        print("============SCREEEN NAME : \(className) : SCREEEN NAME============")
        print("============STORYBOARD NAME : \(String(describing: self.storyboard?.value(forKey: "name"))) : STORYBOARD NAME============")
    }
    
}
