//
//  Utility.swift
//  LegalStart
//
//  Created by Adapting Social on 4/28/17.
//  Copyright © 2017 Adapting Social. All rights reserved.
//

import UIKit
import SystemConfiguration
import MobileCoreServices
import MMDrawerController
import Photos
import MessageUI
import Alamofire
import SwiftyJSON
class Utility: NSObject, UIPickerViewDelegate,UIPickerViewDataSource,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIActionSheetDelegate, MFMailComposeViewControllerDelegate, CLLocationManagerDelegate {

    // MARK:singleton sharedInstance
    static let sharedInstance = Utility()
    
    internal var loginModel:SignInModel! = nil
    
    internal var drawerController : MMDrawerController! = nil

    //Picker
    typealias pickerCompletionBlock = (_ picker:AnyObject?, _ buttonIndex : Int,_ firstindex:Int) ->Void
    var pickerType :String?
    var datePicker : UIDatePicker?
    var simplePicker : UIPickerView?
    var pickerDataSource : NSMutableArray?
    var pickerBlock : pickerCompletionBlock?
    var pickerSelectedIndex :Int = 0
    // // //  //  //
    
    
    //Camera Picker
    typealias openCameraCallBackBlock = (_ selectedImage : UIImage?) ->Void
    var cameraCallBackBlock :openCameraCallBackBlock?
    var cameraController : UIViewController?
    var mailController : UIViewController?
    
    // Location
    var locationManager = CLLocationManager()
    var currentLocation: CLLocation!

    func sucessfullyLogin(withController controller : UIViewController) {
        
        
        UserDefaults.setValue(Utility.sharedInstance.loginModel.data!.checkUserEmailAndPassword![0].uPaymentStatus!, forKey: USER_PAYMENT_SUBSCRIPTION)
        UserDefaults.setValue(Utility.sharedInstance.loginModel.data!.checkUserEmailAndPassword![0].subscriptionId!, forKey: USER_SUBSCRIPTION_ID)

        let access_token = Utility.sharedInstance.loginModel.data?.randomFunction?[0].accessToken
        UserDefaults.set(access_token, forKey: ACCESSTOKEN)
        let user : QBUUser = QBUUser()
        user.login = AppDelegate.getAppDelegate().userInfoData.blogEmail!
        user.password = AppDelegate.getAppDelegate().userInfoData.blogPasswsord!
        self.loginUseToQuickBloxWithUser(user: user) { (user, error) in
            print(user ?? "")
            print(error ?? "")
        }
        self.loginToDrawerFrom(controller,animated:true)
    }
    
    func loginToDrawerFrom(_ controller : UIViewController,animated: Bool) {
        
        let aStoryboardUser = UIStoryboard.init(name: SB_USER_MASTER, bundle: nil)
        let leftDrawer = aStoryboardUser.instantiateViewController(withIdentifier: "LeftDrawerVC")
        
        let aStoryboardAppointments = UIStoryboard.init(name: SB_APPOINTMENTS, bundle: nil)
        let center = aStoryboardAppointments.instantiateViewController(withIdentifier: "DashBoardViewController") as! DashBoardViewController
        
        drawerController = MMDrawerController.init(center: center, leftDrawerViewController: leftDrawer)
        
        drawerController?.setMaximumLeftDrawerWidth((Constant.DeviceType.IS_IPHONE_5 ? 250 : (Constant.DeviceType.IS_IPHONE_6 ? 290 :(Constant.DeviceType.IS_IPHONE_6P ? 320 : 290))), animated: false, completion: nil)
        drawerController?.restorationIdentifier = "MMDrawer"

        drawerController?.closeDrawerGestureModeMask = .all
        drawerController?.showsShadow = true
        controller.navigationController?.pushViewController(drawerController!, animated: animated)
    }
    func loginUseToQuickBloxWithUser(user : QBUUser,completionHandler :@escaping (QBUUser?,Swift.Error?) -> Swift.Void)  {
        
        
        //Old code
        /*
        if (ServicesManager.instance().isAuthorized) {
            
            print("-------------------- Chat Session is Authorozed")
//            QBSettings.setAutoReconnectEnabled(true);
            ServicesManager.instance().chatService.connect(completionBlock: { (error) in
                if error != nil {
                    print("Error in quickeblox \(error!.localizedDescription)")
                }
                
            })
            completionHandler(user, nil)
            return;
            
        }

        if WebService.isConnectedToNetwork() {
           
            ServicesManager.instance().logIn(with: user) { (isLogin, value) in
                
                print(value ?? "QB USER LOGIN SUCCESS")
                if value == nil && isLogin{
                
                    Utility.registerForRemoteNotification()
                    completionHandler(user,nil)
                }else{
                
                    completionHandler(nil,value as? Swift.Error)
                
                }
                
            }
        }
        */
        
        //Love8 Code
        
        if (ServicesManager.instance().isAuthorized && ServicesManager.instance().currentUser.id != 0) {
            
            print("-------------------- Chat Session is Authorozed-----")
            
            completionHandler(user, nil)
            return;
            
        }
        
        if WebService.isConnectedToNetwork() {
            ServicesManager.instance().authService.logOut { (response) in
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5 , execute: {
                    ServicesManager.instance().logIn(with: user) { (isLogin, value) in
                        
                        if value == nil && isLogin{
                            
                            print("=======QUICKBLOX LOGIN SUCCESS=======")
                            print(user.email ?? "QB EMAIL NIL")
                            print("=======QUICKBLOX LOGIN SUCCESS=======")
                            
                            Utility.registerForRemoteNotification()
                            completionHandler(user,nil)
                        }else{
                            if value != nil{
                                print("=======QUICKBLOX LOGIN FAILED=======")
                                print(value ?? "QUICKBLOX LOGIN FAILED")
                                print("=======QUICKBLOX LOGIN FAILED=======")
                                
                                
                            }
                            completionHandler(nil,value as? Error)
                            
                        }
                        
                    }
                    
                })
            }
        }else{
            completionHandler(nil,nil)
        }
    }
    func getPrivateDiaLogWithOpponentUser(user : QBUUser,completion: @escaping ((QBResponse, QBChatDialog?) -> Swift.Void))  {
        ServicesManager.instance().chatService.createPrivateChatDialog(withOpponent: user) { (response, dialog) in
            completion(response, dialog)
        }
    }
    class func registerForRemoteNotification() {
        // Register for push in iOS 8
        if #available(iOS 8.0, *) {
            let settings = UIUserNotificationSettings(types: [UIUserNotificationType.alert, UIUserNotificationType.badge, UIUserNotificationType.sound], categories: nil)
            UIApplication.shared.registerUserNotificationSettings(settings)
            UIApplication.shared.registerForRemoteNotifications()
        } else {
            // Register for push in iOS 7
            UIApplication.shared.registerForRemoteNotifications(matching: [UIRemoteNotificationType.badge, UIRemoteNotificationType.sound, UIRemoteNotificationType.alert])
        }
    }

    class func checkUserSubscriptionStatus() {

        if AppDelegate.getAppDelegate().userInfoData == nil{
        
        
            return;
        }
        
            if let userId = AppDelegate.getAppDelegate().userInfoData.usersId,let accessToken = UserDefaults.value(forKey: ACCESSTOKEN) as? String  {
                
                var params : [String:Any] = [:]
                params["users_id"] = userId
                params["access_token"] = accessToken
            Alamofire.request(Constant.API.kBaseURLWithAPIName("user_subscription_status"), method: .post, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON { (responseObject) in
                print(responseObject)
                
                if responseObject.result.isSuccess{
                    if let aJSON = responseObject.result.value {
                        
                        let json = JSON(aJSON)
                        print("---- GET SUCCESS RESPONSE : \(json)")
                        if json["settings"]["success"].string == "1"{
                        
                            if let status = json["data"][0]["u_payment_status"].string{
                            
                                UserDefaults.setValue(status, forKey: USER_PAYMENT_SUBSCRIPTION)
                            
                            }
                        
                        }
                        
                    }
                    
                }
                if responseObject.result.isFailure {
                    let error: NSError = responseObject.result.error! as NSError
                    
                }
            }
        }
      
    }
    class func reloginUserWith(viewController : UIViewController)  {
        
        Utility.callWebServiceLogOut(viewController: viewController)
        
    }
    class func callWebServiceLogOut(viewController : UIViewController)  {
        Utility.logoutFromQuickBlox(viewController: viewController)
        let aStrDeviceToken:String?
        
        if UserDefaults.string(forKey: DEVICETOKEN_STRING) == nil {
            aStrDeviceToken = "0"
        }
        else{
            aStrDeviceToken = UserDefaults.string(forKey: DEVICETOKEN_STRING)
        }
        
        let deviceId = UIDevice.current.identifierForVendor!.uuidString
        let aStrUserId = AppDelegate.getAppDelegate().userInfoData.usersId!
        
        let aDictParam:[String:Any] = ["users_id": aStrUserId,"device_token":aStrDeviceToken!, "device_id": deviceId, "device_type":"Ios"];
        
        WebService.POST(Constant.API.kBaseURLWithAPIName("logout"), param: aDictParam, controller: viewController, successBlock: { (jsonResponse) in
            
            print("success response is received")
            if  (jsonResponse["settings"]["success"].string == "1") {
                
                UserDefaults.removeObject(forKey: Constant.UserDefaultKeys.kUserDataModelKey)
                
                //UserDefaults.removeObject(forKey: IS_CHAT_DIALOG_CREATED)
                UserDefaults.removeObject(forKey: CHAT_DIALOG_ID)
                UserDefaults.removeObject(forKey: USER_PAYMENT_SUBSCRIPTION)
                UserDefaults.removeObject(forKey: USER_SUBSCRIPTION_ID)
                AppDelegate.getAppDelegate().userInfoData = nil
                AppDelegate.getAppDelegate().selectedMenuIndex = 0
                let storyBoard : UIStoryboard = UIStoryboard(name: "UserMaster", bundle: nil)
                let loginVC : SignInVC = storyBoard.instantiateViewController(withIdentifier: "SignInVC") as! SignInVC
                viewController.navigationController?.pushViewController(loginVC, animated: true)
                
            }else{
                UIAlertController.showAlertWithOkButton(viewController, aStrMessage: jsonResponse["settings"]["message"].string, completion: nil)
            }
            
        }) { (error, isTimeOut) in
            
            if isTimeOut {
                print("Request Timeout")
            } else {
                print(error?.localizedDescription ?? "")
            }
            
        }
    }
    class func logoutFromQuickBlox(viewController : UIViewController) {
        if WebService.isConnectedToNetwork() {
            
            ServicesManager.instance().logout {
                
                
                
            }
        }else{
            UIAlertController.showAlertWithOkButton(viewController, aStrMessage: Constant.AlertMessage.kAlertMsgNoInternetConnection, completion:nil)
        }
        
    }
    class func convertObjectToJson(from object: Any) -> String? {
        if let objectData = try? JSONSerialization.data(withJSONObject: object, options: JSONSerialization.WritingOptions(rawValue: 0)) {
            let objectString = String(data: objectData, encoding: .utf8)
            return objectString
        }
        return nil
    }
    //MARK: Picker
    
    func addPicker(_ controller:UIViewController,onTextField:UITextField,typePicker:String,pickerArray:[String], setMaxDate : Bool = false, withCompletionBlock:@escaping pickerCompletionBlock)  {
        
        pickerType = typePicker
        onTextField.tintColor = UIColor.clear
        
        let keyboardDateToolbar = UIToolbar.init(frame: CGRect(x: 0, y: 0, width: controller.view.bounds.size.width, height: 44))
        keyboardDateToolbar.barTintColor = UIColor.darkGray
        //        (R: 49.0, G: 171.0, B: 81.0, Alpha: 1.0)
        
        let flexSpace = UIBarButtonItem.init(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        
        let done = UIBarButtonItem.init(title: "Done", style: .done, target: self, action:  #selector(pickerDone))
        done.tintColor = UIColor.white
        
        let cancel = UIBarButtonItem.init(title: "Cancel", style: .done, target: self, action:  #selector(pickerCancel))
        cancel.tintColor = UIColor.white
        
        keyboardDateToolbar.setItems([cancel,flexSpace,done], animated: true)
        onTextField.inputAccessoryView = keyboardDateToolbar
        
        if typePicker == "Date" {
            
            datePicker = UIDatePicker.init()
            datePicker!.backgroundColor = UIColor.white
            datePicker!.datePickerMode = .date
            
            let dateFormatter = DateFormatter.init()
            dateFormatter.dateFormat = Constant.kDateFormat_MMDDYYYY
            
            if setMaxDate {
                datePicker!.maximumDate = Date()
            }
            
            if let date = dateFormatter.date(from: onTextField.text!)
            {
                datePicker?.date = date
            }
            onTextField.inputView = datePicker
            
        } else if typePicker == "Time" {
            
            datePicker = UIDatePicker.init()
            datePicker!.backgroundColor = UIColor.white
            datePicker!.datePickerMode = .time
            datePicker!.date = Date()
            
            onTextField.inputView = datePicker
            
        } else {
            
            pickerDataSource = NSMutableArray.init(array: pickerArray)
            simplePicker = UIPickerView.init()
            simplePicker!.backgroundColor = UIColor.white
            simplePicker!.delegate = self
            simplePicker!.dataSource = self
            
            if let index = pickerArray.index(of: onTextField.text!) {
                simplePicker!.selectRow(index, inComponent: 0, animated: true)
            }
            onTextField.inputView = simplePicker
        }
        
        pickerBlock = withCompletionBlock
        onTextField.becomeFirstResponder()
    }
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int{
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if (pickerDataSource != nil) {
            return pickerDataSource!.count;
        }
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerDataSource![row] as? String
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int){
        
        pickerSelectedIndex = row
    }
    
    func pickerDone()
    {
        print(pickerBlock)
        if pickerType == "Date" {
            
            pickerBlock!(datePicker!,1,0)
        } else if pickerType == "Time"  {
            pickerBlock!(datePicker!,1,0)
        } else {
            pickerBlock!(simplePicker!,1,pickerSelectedIndex)
        }
    }
    
    func pickerCancel()
    {
        pickerBlock!(nil,0,0)
    }
    
    //Open Camera Picker
    
    func openCameraInController(_ controller:UIViewController,completionBlock:@escaping openCameraCallBackBlock)
    {
        cameraCallBackBlock = completionBlock
        cameraController = controller
        
        UIAlertController.showActionsheetForImagePicker(cameraController!, aStrMessage: nil) { (index, strTitle) in
            if index == 1{
                
                if !UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera){
                    
                    UIAlertController.showAlertWithOkButton(self.cameraController!, aStrMessage: "Camera not available.", completion: nil)
                }
                else{
                    
                    let authStatus = AVCaptureDevice.authorizationStatus(forMediaType: AVMediaTypeVideo)
                    if (authStatus == .authorized){
                        self.openPickerWithCamera(true)
                    }else if (authStatus == .notDetermined){
                        
                        print("Camera access not determined. Ask for permission.")
                        AVCaptureDevice.requestAccess(forMediaType: AVMediaTypeVideo, completionHandler: { (granted) in
                            if(granted)
                            {
                                self.openPickerWithCamera(true)
                            }
                        })
                    }else if (authStatus == .restricted){
                        UIAlertController.showAlertWithOkButton(self.cameraController!, aStrMessage: "You've been restricted from using the camera on this device. Please contact the device owner so they can give you access.", completion: nil)
                    }else{
                        
                        UIAlertController.showAlert(self.cameraController!, aStrMessage: "It looks like your privacy settings are preventing us from accessing your camera. Goto Setting ->Camera: turn on.", style: .alert, aCancelBtn: "Cancel", aDistrutiveBtn: nil, otherButtonArr: ["OK"], completion: { (index, strTitle) in
                            
                            if index == 0{
                                
                                UIApplication.shared.openURL(NSURL(string: UIApplicationOpenSettingsURLString)! as URL)
                            }
                        })
                    }
                }
            }else if index == 0{
                
                let authorizationStatus = PHPhotoLibrary.authorizationStatus()
                
                if (authorizationStatus == .authorized) {
                    // Access has been granted.
                    self.openPickerWithCamera(false)
                }else if (authorizationStatus == .restricted) {
                    // Restricted access - normally won't happen.
                    UIAlertController.showAlertWithOkButton(self.cameraController!, aStrMessage: "You've been restricted from using the photos on this device. Please contact the device owner so they can give you access.", completion: nil)
                }else if (authorizationStatus == .notDetermined) {
                    
                    // Access has not been determined.
                    PHPhotoLibrary.requestAuthorization({ (status) in
                        if (status == .authorized) {
                            // Access has been granted.
                            self.openPickerWithCamera(false)
                        }else {
                            // Access has been denied.
                        }
                    })
                }else{
                    UIAlertController.showAlert(self.cameraController!, aStrMessage: "It looks like your privacy settings are preventing us from accessing your photos. Goto Setting ->Photos: turn on.", style: .alert, aCancelBtn: "Cancel", aDistrutiveBtn: nil, otherButtonArr: ["OK"], completion: { (index, strTitle) in
                        
                        if index == 0{
                            
                            UIApplication.shared.openURL(NSURL(string: UIApplicationOpenSettingsURLString)! as URL)
                        }
                    })
                }
            }
        }
    }
    
    func openPickerWithCamera(_ isopenCamera:Bool) {
        
        let captureImg = UIImagePickerController()
        captureImg.delegate = self
        if(isopenCamera){
            captureImg.sourceType = UIImagePickerControllerSourceType.camera
        }else{
            captureImg.sourceType = UIImagePickerControllerSourceType.photoLibrary
        }
        captureImg.mediaTypes = [kUTTypeImage as String]
        captureImg.allowsEditing = true
        
        if Constant.DeviceType.IS_IPAD {
            captureImg.modalPresentationStyle = .popover
            
        }else{
        
        
        
        }
        
        cameraController?.present(captureImg, animated: true, completion: nil)
    }
    
    // MARK: - UIImagePicker Controller Delegate
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]){
        
        if let img = info[UIImagePickerControllerEditedImage] as? UIImage{
            
            if (cameraCallBackBlock != nil){
                
                cameraCallBackBlock!(UIImage(data: img.mediumQualityJPEGNSData))
            }
        }
        
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController){
        
        if (cameraCallBackBlock != nil){
            
            cameraCallBackBlock!(nil)
        }
        picker.dismiss(animated: true, completion: nil)
    }

    class func checkFileTypeWith(fileExtension : String) -> UIImage{
        
        switch fileExtension {
        case "png":
            return UIImage(named: "FILE-PNG")!
            
            
        case "jpeg":
            return UIImage(named: "FILE-JPG")!
            
            
        case "jpg":
            return UIImage(named: "FILE-JPG")!
            
            
        case "xls":
            return UIImage(named: "FILE-XLS")!
            
            
        case "doc":
            return UIImage(named: "FILE-DOC")!
            
            
        case "docx":
            return UIImage(named: "FILE-DOC")!
            
            
        case "pdf":
            return UIImage(named: "FILE-PDF")!
            
            
        case "txt":
            return UIImage(named: "FILE-TXT")!
            
            
        default:
            return UIImage(named: "logo")!
         }
    }

    func getDateFromString(_ date : String , dateFormat : String) -> Date {
        
        let dateFormatter  = DateFormatter()
        dateFormatter.dateFormat = dateFormat
        dateFormatter.timeZone = TimeZone(identifier: "UTC")
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        return dateFormatter.date(from: date)!
    }

    func getStringFromDate(_ format : String , date : Date) -> String {
        
        let dateFormatter  = DateFormatter()
        dateFormatter.dateFormat = format
        //                dateFormatter.timeZone = TimeZone(identifier: "UTC")
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        
        return dateFormatter.string(from: date)
    }

    func doCall(strPhoneNo: String, controller:UIViewController) {
        
        if let url = URL(string: "tel://\(strPhoneNo)") {
            
            if UIApplication.shared.canOpenURL(url) {
                UIApplication.shared.openURL(url)
            }
            else
            {
                UIAlertController.showAlertWithOkButton(controller, aStrMessage: Constant.AlertMessage.kAlertMsgNoCallFunctionalityAvailable, completion:nil)
            }
            
        }
    }
    
    func doEmail(strEmail: String, controller:UIViewController) {
        
        mailController = controller
        if MFMailComposeViewController.canSendMail() {
            
            let mailComposerVC = MFMailComposeViewController()
            mailComposerVC.mailComposeDelegate = self
            mailComposerVC.setToRecipients([strEmail])
//            mailComposerVC.setSubject(Constant.displayMessage.kInviteEmailSubject)
//            mailComposerVC.setMessageBody(Constant.displayMessage.kInviteEmailBody, isHTML: false)

            controller.present(mailComposerVC, animated: true, completion: nil)
        }
        else
        {
            
            UIAlertController.showAlert(controller, aStrMessage: Constant.AlertMessage.kAlertMsgAddEmailAddress, style: .alert, aCancelBtn: "Cancel", aDistrutiveBtn: nil, otherButtonArr: ["Settings"], completion: { (index, title) in
                if title == "Settings"{
                
                    if UIApplication.shared.canOpenURL(URL(string:"App-Prefs:root=ACCOUNT_SETTINGS")!){
                    
                        if #available(iOS 10.0, *) {
                            UIApplication.shared.open(URL(string:"App-Prefs:root=ACCOUNT_SETTINGS")!)
                        } else {
                            // Fallback on earlier versions
                            UIApplication.shared.openURL(URL(string:"App-Prefs:root=ACCOUNT_SETTINGS")!)
                        }
                    }
                }
            })
        }

    }
    
    // MARK: -----------------
    // MARK: MFMailComposeViewControllerDelegate
    // MARK: -----------------
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Swift.Error?) {
        
        controller.dismiss(animated: true, completion: nil)
        if error != nil {
            UIAlertController.showAlertWithOkButton(mailController!, aStrMessage: error?.localizedDescription, completion: nil)
        }
    }

    // MARK: -----------------
    // MARK: CLLocationManager Methods
    // MARK: -----------------

    func getUserCurrentLocation() {
        
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.delegate = self
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.startUpdatingLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        currentLocation = locations[0] as CLLocation
        
        // Call stopUpdatingLocation() to stop listening for location updates,
        // other wise this function will be called every time when user location changes.

        print("user latitude = \(currentLocation.coordinate.latitude)")
        print("user longitude = \(currentLocation.coordinate.longitude)")

        manager.stopUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Swift.Error)
    {
        print("Error \(error)")
    }
}
