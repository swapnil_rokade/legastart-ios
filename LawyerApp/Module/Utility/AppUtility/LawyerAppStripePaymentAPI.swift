//
//  LegalStartStripePaymentAPI.swift
//  LegalStart
//
//  Created by Adapting Socialon 07/12/17.
//  Copyright © 2017 Adapting Social. All rights reserved.
//

import UIKit
import PassKit
import Stripe

class LawyerAppStripePaymentAPI : NSObject,PKPaymentAuthorizationViewControllerDelegate {

    
    static let sharedInstance = LawyerAppStripePaymentAPI()
    
    var completionHandlerInstance : kCompletionLawyerAppStripPaymentHandler! = nil
    
    func presentStripPaymentVC(viewController : UIViewController,amount : String,completionHandler : @escaping kCompletionLawyerAppStripPaymentHandler)   {
        
        self.completionHandlerInstance = completionHandler
        if PKPaymentAuthorizationViewController.canMakePayments() {
            let request = PKPaymentRequest()
            request.merchantIdentifier = ApplePaySwagMerchantID
            request.supportedNetworks = SupportedPaymentNetworks
            request.merchantCapabilities = PKMerchantCapability.capability3DS
            request.countryCode = "US"
            request.currencyCode = "USD"
            request.paymentSummaryItems = [PKPaymentSummaryItem.init(label: "LegalStart,INC", amount: NSDecimalNumber.init(value: Double.init(amount)!))]
            let applePayController = PKPaymentAuthorizationViewController.init(paymentRequest: request)
            applePayController.delegate = self
            viewController.present(applePayController, animated: true, completion: nil)
            
        }else{
        
           completionHandlerInstance(nil,.error,nil)
        
        }
        
        
        
    }
    
    func paymentAuthorizationViewController(_ controller: PKPaymentAuthorizationViewController, didAuthorizePayment payment: PKPayment, completion: @escaping (PKPaymentAuthorizationStatus) -> Void) {
        
        
        STPAPIClient.shared().createToken(with: payment) { (token, error) in
            
            if token != nil{
    
                controller.dismiss(animated: true, completion: {
                    self.completionHandlerInstance(token!.tokenId,.success,nil)
                })
            }else{
        
                controller.dismiss(animated: true, completion: {
                    self.completionHandlerInstance(nil,.error,error)
                })
                
            }
            
        }
        
        
        
    }
    func paymentAuthorizationViewControllerDidFinish(_ controller: PKPaymentAuthorizationViewController) {
        
        
        controller.dismiss(animated: true) { 
            self.completionHandlerInstance(nil,.cancel,nil)
        }
    }

}
