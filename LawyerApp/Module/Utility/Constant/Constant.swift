//
//  Constant.swift
//  LegalStart
//
//  Created by Adapting Social on 4/24/17.
//  Copyright © 2017 Adapting Social. All rights reserved.
//

import Foundation
import UIKit
import PassKit
import Stripe

public let appName = Bundle.main.infoDictionary![kCFBundleNameKey as String] as! String
public let kAllowedFileTypes = ["png","jpeg","jpg","xls","doc","docx","pdf","txt"]
public let kBaseURL : String = "http://p.configure.it/lawyerapp32412/WS/"
public let kBaseURL1 : String = "http://php54.indianic.com/lawyerapp/WS/"
public let kBaseLiveURL : String = "http://34.195.154.175/WS/"


let SupportedPaymentNetworks = [PKPaymentNetwork.visa, PKPaymentNetwork.masterCard, PKPaymentNetwork.amex]
let ApplePaySwagMerchantID = "merchant.com.adaptingsocial.laywerPayment"

let UserDefaults = Foundation.UserDefaults.standard

// Max limits for validation
let MAX_LENGTH_FOR_PHONE_NO = 10
let MAX_LENGTH_FOR_PASSWORD = 40
let MAX_LENGTH_FOR_REVIEW = 100
let MAX_LENGTH_FOR_ABOUT_US = 100
let MAX_LENGTH_FOR_DISTANCE = 5
let MAX_LENGTH_FOR_PIN_NO = 6

// Storyboard names...
let SB_USER_MASTER = "UserMaster"
let SB_APPOINTMENTS = "DashBoardMaster"
let SB_TUTOR =          "Tutor"
let SB_TIMELINE =       "Timeline"
let SB_OTHER =          "Other"

// User Data Keys...

let DEVICETOKEN = "deviceToken"
let DEVICETOKEN_STRING = "deviceTokenString"
let ACCESSTOKEN = "accessToken"
let TOOL_TIP_FOR_BLOG = "toolTipForBlog"
// Appointment Data Keys...
let APPOINTMENT_START_TIME = "appointmentStartTime"
let APPOINTMENT_END_TIME = "appointmentEndTime"
let USER_PAYMENT_SUBSCRIPTION = "userPaymentSubscription"
let USER_SUBSCRIPTION_ID = "userSubscriptionId"
let ADMIN_EMAIL = "adaptingsocial@gmail.com"
let IS_CHAT_DIALOG_CREATED = "isDiaLogCreated"
let CHAT_DIALOG_ID = "chatDialogId"
enum CaseModifier {
    case add
    case edit
}
public enum StripPaymentStatus {
    case success
    case cancel
    case error
}
public typealias kCompletionLawyerAppStripPaymentHandler = (String?,StripPaymentStatus,Swift.Error?) -> Swift.Void
struct Constant {
    
    struct AppKeys {
        static let kDropBoxAPPKey = "qwskm82tgg7nm2y"
        static let kQBApplicationID : UInt = 58084
        static let kQBAuthKey = "Y6DRkAuxrK5-j-L"
        static let kQBAuthSecret = "n34tJbccbB3bVrL"
        static let kQBAccountKey = "hDsgo8X94tePKD1hYPs2"
        static let kStripePublicationKey = "pk_live_HI6lKeYyFKcSKzuYRDpHh5sR"
//        static let kStripeTestPublicationKey = "pk_test_tsqhEWanNq2mJnVRTWXh4III"
        
    }
    struct ScreenSize
    {
        static let SCREEN_WIDTH         = UIScreen.main.bounds.size.width
        static let SCREEN_HEIGHT        = UIScreen.main.bounds.size.height
        static let SCREEN_MAX_LENGTH    = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
        static let SCREEN_MIN_LENGTH    = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
    }
    
    struct DeviceType
    {
        static let IS_IPHONE_4_OR_LESS  = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH < 568.0
        static let IS_IPHONE_5          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 568.0
        static let IS_IPHONE_6          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0
        static let IS_IPHONE_6P         = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 736.0
        static let IS_IPAD              = UIDevice.current.userInterfaceIdiom == .pad && ScreenSize.SCREEN_MAX_LENGTH == 1024.0
        static let IS_IPAD_PRO          = UIDevice.current.userInterfaceIdiom == .pad && ScreenSize.SCREEN_MAX_LENGTH == 1366.0
    }
    
    struct FontName {
        static let kFontRobotoRegular = "Roboto-Regular"
        static let kFontRobotoMedium = "Roboto-Medium"
    }

    struct SegueName {
        static let kSegueGoToLoginVC = "goToLoginVCSegue"
        static let kSegueLogOut = "LogOutSegue"
    }
    
    struct NoDataFound {
        static let kCompanyMessage = "No company found."
        static let kUpcomingAppointmentMessage = "No upcoming Appointment is available."
        static let kPastAppointmentMessage = "No appointment found."
        static let kTimeSlotMessage = "No TimeSlot available."
    }
    struct UserDefaultKeys {
        static let kUserDataModelKey = "userDataModelKey"
    }

    struct AlertMessage {
        
        static let kAlertMsgEnterEmail = "Please enter email address."
        static let kAlertMsgValidEmail = "Please enter valid email address."
        static let kAlertMsgEnterPassword = "Please enter password."
        static let kAlertMsgEnterNewPassword = "Please enter new password."
        static let kAlertMsgEnterConfirmPassword = "Please enter confirm password."
        static let kAlertMsgEnterFisrtName  = "Please enter first name."
        static let kAlertMsgEnterLastName  = "Please enter last name."
        static let kAlertMsgEnterMobileNo  = "Please enter mobile no."
        static let kAlertMsgSelectDateOfBirth  = "Please select date of birth."
        static let kAlertMsgPasswordMinLength = "Password can not be less than 6 characters."
        static let kAlertMsgPasswordMaxLength = "Password should not be more than 10 characters."
        static let kAlertMsgPasswordMustContain = "Password must contain at least one Capital letter and one digit."
        static let kAlertMsgPassNotMatch   = "Confirm password does not match."
        static let kAlertMsgValidPhoneNumber = "Please enter valid phone number"
        static let kAlertMsgValidPassword = "The password must be 6 characters long or more"
        static let kAlertMsgConfirmPassNotMatch = "Confirm password must match with password"
        static let kAlertMsgSuccessLogin = "Successfully login"
        static let kAlertMsgSuccessSignUp = "Successfully SignUp"
        static let kAlertMsgInvalidEmail = "Invalid email address"
        static let kAlertMsgEmailAlreadyInUse = "Email address is already in use."
        static let kAlertMsgNetworkError = "Ooops, some network error occurs."
        static let kAlertMsgInvalidCredential = "Invalid Email Or Password"
        static let kAlertMsgLimitDocumentUpload = "Only 10 document allowed"
        
        static let kAlertMsgEnterAppointmentTitle  = "Please enter appointment name."
        static let kAlertMsgSelectAppointmentTimeSlot  = "Please select time slot to book an appointment."
        
        static let kAlertMsgLogoutConfirmation = "Are you sure want to logout?"
        static let kAlertMsgDeleteAppointmentConfirmation = "Are you sure want to delete appointment?"
        
        static let kAlertMsgNoInternetConnection = "Please check your internet connection and try again."
        
        static let kAlertMessageRequiredField : (String) -> String = {name in
            return "Please enter \(name)"
        }
        static let kAlertMsgAdminEmailSuccess = "Email has been sent to admin to get online for chat"
        static let kAlertMsgDeleteCase = "Are you sure want to delete this document?"
        static let kalertMsgCaseAddedSuccess = "Schedule your 15-minute call with a Wall Street attorney, now!"
        static let kAlertMsgNoCallFunctionalityAvailable = "Call functionality is not available in your device."
        static let kAlertMsgNoEmailFunctionalityAvailable = "Email functionality is not available in your device."
        static let kAlertMsgNotAbleToCompletePayment = "Sorry, we are not able to proceed with your payment request."
        static let kAlertMsgSomeThingWentWrong = "Something went wrong please try again later"
        static let kAlertMsgFileUploadedSuccessFully = "File uploaded successfully"
        static let kAlertMsgRequestTimeOut = "Request time out please try again later"
        static let kAlertMsgAddEmailAddress = "It seems your mail account is not been configured.Please select settings and configure your account."
        static let kAlertMsgSignUpPrivacyPolicy = "By signing up, I agree to the Privacy Policy and the Terms & Conditions."
        static let kAlertMsgForPaidSubscriber = "This is for paid subscribers only"
        static let kAlertMsgForPaymentSuccess = "Your payment has been done successfully."
        static let kAlertMsgRegisterSuccess = "Congratulation! You have been registered successfully"
        
    }
    
    // Date formatters
    static let kDateFormat_DDMMYYYY = "dd/MM/yyyy"
    static let kDateFormat_MMDDYYYY = "MM/dd/yyyy"
    static let kDateFormat_YYYYMMDD = "yyyy-MM-dd"
    static let kDateFormat_YYYYMMDD_HHMMSS = "yyyy-MM-dd HH:mm:ss"
    
    // Google Keys...
    // Live mode...
//    static let kGoogleClientID = ""
//    static let kGoogleApiKey = ""
    
    // Dev mode
    static let kGoogleClientID = ""
    static let kGoogleApiKey = "AIzaSyB4lj_1qAryZ7-9BxlNmydBSmls_a2PHkU"
    

    struct API {
        static let kBaseURLWithAPIName : (String) -> String = {name in
//            return "\(kBaseURL1)\(name)"
            return "\(kBaseLiveURL)\(name)"
            
        }
    }
}
