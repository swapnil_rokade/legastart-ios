//
//  ForgotPasswordVC.swift
//  LegalStart
//
//  Created by Adapting Social on 4/24/17.
//  Copyright © 2017 Adapting Social. All rights reserved.
//

import UIKit

class ForgotPasswordVC: UIViewController {

    // MARK: - Global Outlets
    
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var btnForgotPassword: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    // MARK: - Button Click Events
    
    @IBAction func btnCloseClick(_ sender: UIButton){
        
        self.view.hideKeyBoard()
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnForgotPasswordClick(_ sender: UIButton){
        
        self.view.hideKeyBoard()
        sender.isUserInteractionEnabled = false;
        
        if validationForForgotPassword() {
            
            self.callWebServiceForForgotPassword(txtEmail.text!)
            
        }else{
            
            let delayTime = DispatchTime.now() + Double(Int64(1.5 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
            DispatchQueue.main.asyncAfter(deadline: delayTime) {
                sender.isUserInteractionEnabled = true;
            };
        }
        
    }
    
    // MARK: - Other Methods
    
    func validationForForgotPassword() -> Bool {
        
        if self.txtEmail.text!.isEmptyString() {
            UIAlertController.showAlertWithOkButton(self, aStrMessage: Constant.AlertMessage.kAlertMsgEnterEmail, completion: nil)
            return false
        }
        else if !self.txtEmail.isValidEmail(){
            UIAlertController.showAlertWithOkButton(self, aStrMessage: Constant.AlertMessage.kAlertMsgValidEmail, completion: nil)
            return false
        }
        return true
    }
    
    // MARK: - Webservice Methods
    
    func callWebServiceForForgotPassword(_ strEmail:String)  {
        
        let aDictParam:[String:Any] = ["email":strEmail]
        
        WebService.POST(Constant.API.kBaseURLWithAPIName("forgot_password"), param: aDictParam, controller: self, successBlock: { (jsonResponse) in
            
            print("success response is received")
            
            self.btnForgotPassword.isUserInteractionEnabled = true;
    
            if (jsonResponse["settings"]["success"].string == "1") {
                
                UIAlertController.showAlertWithOkButton(self, aStrMessage: jsonResponse["settings"]["message"].string, completion: nil)
                self.txtEmail.text = ""
            }else if (jsonResponse["settings"]["success"].string == "100")  {
                Utility.reloginUserWith(viewController: self)
            }else{
                UIAlertController.showAlertWithOkButton(self, aStrMessage: jsonResponse["settings"]["message"].string, completion: nil)
            }
        }) { (error, isTimeOut) in
            
            self.btnForgotPassword.isUserInteractionEnabled = true
            if isTimeOut {
                print("Request Timeout")
            } else {
                print(error?.localizedDescription ?? "")
            }
            
        }
        
    }

}
