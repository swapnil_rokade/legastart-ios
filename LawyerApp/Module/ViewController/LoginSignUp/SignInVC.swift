//
//  SignInVC.swift
//  LegalStart
//
//  Created by Adapting Social on 4/24/17.
//  Copyright © 2017 Adapting Social. All rights reserved.
//

import UIKit

class SignInVC: UIViewController {

    // MARK: - Global Variables
    
    
    var playerLooper: NSObject?
    var playerLayer:AVPlayerLayer!
    var queuePlayer: AVQueuePlayer?
    var player : AVPlayer?
    var isStopped : Bool!
    var directLoadCreateProfileVC : Bool = false
    var time : CMTime?
    // MARK: - Global Outlets
    
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var btnSignIn: UIButton!
    
    // MARK: - ViewController Life Cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.playVideoInBackend()
        // Do any additional setup after loading the view.
        if(AppDelegate.getAppDelegate().userInfoData != nil)
        {
            Utility.sharedInstance.loginToDrawerFrom(self, animated: false)
        }

    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        NotificationCenter.default.addObserver(self, selector: #selector(replayVideo), name: NSNotification.Name.UIApplicationDidBecomeActive, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(storeTimer), name: NSNotification.Name.UIApplicationWillResignActive, object: nil)
        replayVideo()
    }
    func storeTimer(){
        
        if #available(iOS 10.0, *) {
            if self.queuePlayer != nil {
                time = self.queuePlayer?.currentTime()
                self.queuePlayer?.pause()
            }
        } else {
            if self.player != nil{
                time = self.player?.currentTime()
                self.player?.pause()
            }
        }
    }
    
    func replayVideo(){
        
        if #available(iOS 10.0, *) {
            if ( queuePlayer != nil) {
                // player is playing
                if(time != nil){
                    self.queuePlayer?.seek(to: time!)
                }
                self.queuePlayer?.play()
            }
        } else {
            
            if ( player != nil) {
                // player is playing
                if(time != nil){
                    self.player?.seek(to: time!)
                }
                self.player?.play()
            }
        }
    }

    func playVideoInBackend(){
        
        
        let filmName = "LawyerApp"
        
        if let path = Bundle.main.path(forResource: filmName, ofType: "mp4") {
            let url =  URL(fileURLWithPath: path)
            
            if #available(iOS 10.0, *) {
                
                // Use a new player looper with the queue player and template item
                let playerItem = AVPlayerItem(url: url as URL)
                self.queuePlayer = AVQueuePlayer(items: [playerItem])
                self.playerLayer = AVPlayerLayer(player: self.queuePlayer)
                
                self.playerLayer.videoGravity = AVLayerVideoGravityResizeAspectFill
                self.playerLooper = AVPlayerLooper(player: self.queuePlayer!, templateItem: playerItem)
                self.view.layer.insertSublayer(self.playerLayer!, at: 0)
                self.queuePlayer?.rate = 1.0
                self.playerLayer?.frame = self.view.frame
                self.queuePlayer?.play()
                
                
            } else {
                // Fallback on earlier versions, this solution has hicup at end
                let playerItem = AVPlayerItem(url: url as URL)
                
                player = AVPlayer(playerItem : playerItem)
                self.playerLayer = AVPlayerLayer(player: self.player)
                self.playerLayer.videoGravity = AVLayerVideoGravityResizeAspectFill
                self.view.layer.insertSublayer(self.playerLayer!, at: 0)
                
                self.playerLayer?.frame = self.view.frame
                player?.play()
                loopVideo(player!)
                
                
            }
            
        }
    }
    func loopVideo(_ videoPlayer: AVPlayer) {
        NotificationCenter.default.addObserver(forName: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: nil, queue: nil) { notification in
            if(!self.isStopped){
                videoPlayer.seek(to: kCMTimeZero)
                videoPlayer.play()
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    

    // MARK: - Button Click Events
    
    @IBAction func btnSignInClick(_ sender: UIButton){
        
        self.view.hideKeyBoard()
        sender.isUserInteractionEnabled = false;
        
        if validationForLogin() {
            
            self.callWebServiceForLogin(txtEmail.text!, strPassword: txtPassword.text!)
            
        }else{
            
            let delayTime = DispatchTime.now() + Double(Int64(1.5 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
            DispatchQueue.main.asyncAfter(deadline: delayTime) {
                sender.isUserInteractionEnabled = true;
            };
        }

    }
    
    @IBAction func btnUnwindLogoutClick(_ sender: UIStoryboardSegue) {
        
    }

    // MARK: - Other Methods
    
    func validationForLogin() -> Bool {
        
        if self.txtEmail.text!.isEmptyString() {
            UIAlertController.showAlertWithOkButton(self, aStrMessage: Constant.AlertMessage.kAlertMsgEnterEmail, completion: nil)
            return false
        }
        else if !self.txtEmail.isValidEmail(){
            UIAlertController.showAlertWithOkButton(self, aStrMessage: Constant.AlertMessage.kAlertMsgValidEmail, completion: nil)
            return false
        }
        else if self.txtPassword.text!.isEmptyString() {
            UIAlertController.showAlertWithOkButton(self, aStrMessage: Constant.AlertMessage.kAlertMsgEnterPassword, completion: nil)
            return false
        }
        
        return true
    }

    // MARK: - Webservice Methods
    
    func callWebServiceForLogin(_ strEmail:String,strPassword:String)  {
        
        let aStrDeviceToken:String?
        
        if UserDefaults.string(forKey: DEVICETOKEN_STRING) == nil {
            aStrDeviceToken = "0"
        }
        else{
            aStrDeviceToken = UserDefaults.string(forKey: DEVICETOKEN_STRING)
        }
        
        let deviceId = UIDevice.current.identifierForVendor!.uuidString

        let aDictParam:[String:Any] = ["email":strEmail,"password":strPassword,"device_token":aStrDeviceToken!, "device_id": deviceId, "device_type":"Ios"];
        
        WebService.POST(Constant.API.kBaseURLWithAPIName("users_login"), param: aDictParam, controller: self, successBlock: { (jsonResponse) in
            
            print("success response is received")
            
            self.btnSignIn.isUserInteractionEnabled = true;
            Utility.sharedInstance.loginModel = SignInModel(json: jsonResponse)
            
            if (Utility.sharedInstance.loginModel.settings?.success == "1") {
                
                self.txtEmail.text = ""
                self.txtPassword!.text = ""
                
                
                let userDataInfo = NSKeyedArchiver.archivedData(withRootObject: Utility.sharedInstance.loginModel.data!.checkUserEmailAndPassword![0])
                UserDefaults.setValue(userDataInfo, forKey: Constant.UserDefaultKeys.kUserDataModelKey)
                UserDefaults.synchronize()
                AppDelegate.getAppDelegate().userInfoData = Utility.sharedInstance.loginModel.data!.checkUserEmailAndPassword?[0]
                Utility.sharedInstance.sucessfullyLogin(withController: self)
            }
            else
            {
                UIAlertController.showAlertWithOkButton(self, aStrMessage: Utility.sharedInstance.loginModel.settings?.message, completion: nil)
            }

        }) { (error, isTimeOut) in
            
            self.btnSignIn.isUserInteractionEnabled = true
            if isTimeOut {
                print("Request Timeout")
            } else {
                print(error?.localizedDescription ?? "")
            }
            
        }
        
    }

}
