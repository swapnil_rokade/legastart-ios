//
//  SplashVideoVC.swift
//  LegalStart
//
//  Created by Adapting Social on 4/27/17.
//  Copyright © 2017 Adapting Social. All rights reserved.
//

import UIKit
import AVFoundation

class SplashVideoVC: UIViewController {

    // MARK: - Global Outlets
    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var btnSkip: UIButton!
    
    var player: AVPlayer?
    var time : CMTime?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Display Skip button after some secs due to delay in playing video...
        let delayTime = DispatchTime.now() + 1.25
        DispatchQueue.main.asyncAfter(deadline: delayTime) {
            self.btnSkip.isHidden = false
        };

        // Do any additional setup after loading the view.
        if(AppDelegate.getAppDelegate().userInfoData.usersId != nil)
        {
            self.performSegue(withIdentifier: Constant.SegueName.kSegueGoToLoginVC, sender: self)
        }
        else
        {
            self.playVideo()
        }
        
    }

    override var prefersStatusBarHidden: Bool{
        return true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(replayVideo), name: NSNotification.Name.UIApplicationDidBecomeActive, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(storeTimer), name: NSNotification.Name.UIApplicationWillResignActive, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        storeTimer()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    func storeTimer(){
        
        if self.player != nil{
            time = self.player?.currentTime()
            self.player?.pause()
        }
    }
    
    func replayVideo(){
        
        if ( player != nil) {
            // player is playing
            if(time != nil){
                self.player?.seek(to: time!)
            }
            
            if #available(iOS 10.0, *) {
                player?.playImmediately(atRate: 1.0)
            } else {
                // Fallback on earlier versions
                player?.play()
            }
        }
    }
    
    func playVideo() {
        
        let videoName = "LawyerApp"
        
        if let path = Bundle.main.path(forResource: videoName, ofType: "mp4") {
            let url =  URL(fileURLWithPath: path)
            
            player = AVPlayer(url : url)
            let playerLayer = AVPlayerLayer(player: player)
            
            playerLayer.frame = self.view.frame
            playerLayer.videoGravity = AVLayerVideoGravityResizeAspectFill
            playerLayer.zPosition = -1
            
            self.viewContainer.layer.addSublayer(playerLayer)
            
            player?.seek(to: kCMTimeZero)
            if #available(iOS 10.0, *) {
                player?.playImmediately(atRate: 1.0)
            } else {
                // Fallback on earlier versions
                 player?.play()
            }
            NotificationCenter.default.addObserver(self, selector:#selector(self.endVideo),name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: player?.currentItem)
            
        }
    }

    func endVideo() {
        NotificationCenter.default.removeObserver(self)
        self.performSegue(withIdentifier: Constant.SegueName.kSegueGoToLoginVC, sender: self)
    }

    @IBAction func btnSkipClick(_ sender: UIButton) {
        self.endVideo()
    }
}
