//
//  TermsAndConditionsVC.swift
//  LegalStart
//
//  Created by Adapting Social on 05/09/17.
//  Copyright © 2017 Adapting Social. All rights reserved.
//

import UIKit

class TermsAndConditionsVC: UIViewController,UIGestureRecognizerDelegate {

    @IBOutlet weak var mainWebView: UIWebView!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        var param : [String:Any] = [:]
        param["page_code"] = "termsconditions"
        
        WebService.GET(Constant.API.kBaseURLWithAPIName("get_cms_pages_listing"), param: param, controller: self, callSilently: false, successBlock: { (jsonResponse) in
            
            if (jsonResponse["settings"]["success"].string == "1"){
                
                
                let htmlString = jsonResponse["data"][0]["page_content"].string!
                let newString : String = "<html><title></title><body>\(htmlString)</body></html>"
                self.mainWebView.loadHTMLString(newString, baseURL: nil)
                
                
            }else if (jsonResponse["settings"]["success"].string == "0"){
                
                UIAlertController.showAlertWithOkButton(self, aStrMessage: jsonResponse["settings"]["message"].string, completion: nil)
                
            }else if (jsonResponse["settings"]["success"].string == "100"){
                
            }else{
                
                UIAlertController.showAlertWithOkButton(self, aStrMessage: Constant.AlertMessage.kAlertMsgSomeThingWentWrong, completion: nil)
            }
            
            
        }) { (error, isTimeOut) in
            
        }

    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func btnSidePanelAction(_ sender: UIButton) {
        
        self.mm_drawerController.toggle(.left, animated: true) { (completed) in
            
        }
    }
    

    

}
