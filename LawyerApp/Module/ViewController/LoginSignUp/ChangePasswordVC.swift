//
//  ChangePasswordVC.swift
//  LegalStart
//
//  Created by Adapting Social on 4/26/17.
//  Copyright © 2017 Adapting Social. All rights reserved.
//

import UIKit

class ChangePasswordVC: UIViewController,UIGestureRecognizerDelegate {
    
    // MARK: - Global Outlets
    
    @IBOutlet weak var txtOldPassword: UITextField!
    @IBOutlet weak var txtNewPassword: UITextField!
    @IBOutlet weak var txtConfirmPassword: UITextField!
    @IBOutlet weak var btnChangePassword: UIButton!
    let token = UserDefaults.value(forKey: ACCESSTOKEN) as! String
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        // Do any additional setup after loading the view.
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // MARK: - Button Click Events
    
    @IBAction func btnCloseClick(_ sender: UIButton){
        
        self.view.hideKeyBoard()
        self.mm_drawerController?.toggle(.left, animated: true, completion: nil)
    }
    
    @IBAction func btnChangePasswordClick(_ sender: UIButton){
        
        self.view.hideKeyBoard()
        sender.isUserInteractionEnabled = false;
        
        if validationForChangePassword() {
            
            self.callWebServiceForChangePassword(txtOldPassword.text!, strNewPassword: txtNewPassword.text!)
            
        }else{
            
            let delayTime = DispatchTime.now() + Double(Int64(1.5 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
            DispatchQueue.main.asyncAfter(deadline: delayTime) {
                sender.isUserInteractionEnabled = true;
            };
        }
        
    }
    
    // MARK: - Other Methods
    
    func validationForChangePassword() -> Bool {
        
        if self.txtOldPassword.text!.isEmptyString() {
            UIAlertController.showAlertWithOkButton(self, aStrMessage: Constant.AlertMessage.kAlertMsgEnterPassword, completion: nil)
            return false
        }
        else if self.txtNewPassword.text!.isEmptyString() {
            UIAlertController.showAlertWithOkButton(self, aStrMessage: Constant.AlertMessage.kAlertMsgEnterNewPassword, completion: nil)
            return false
        }
        else if !txtNewPassword.text!.validateMinimumLength(6) {
            UIAlertController.showAlertWithOkButton(self, aStrMessage: Constant.AlertMessage.kAlertMsgPasswordMinLength, completion: nil)
            return false
        }
        else if !txtNewPassword.text!.validateMaximumLength(10) {
            UIAlertController.showAlertWithOkButton(self, aStrMessage: Constant.AlertMessage.kAlertMsgPasswordMaxLength, completion: nil)
            return false
        }
        else if !txtNewPassword.text!.isValidPassword() {
            UIAlertController.showAlertWithOkButton(self, aStrMessage: Constant.AlertMessage.kAlertMsgPasswordMustContain, completion: nil)
            return false
        }
        else if !(self.txtNewPassword.text! == self.txtConfirmPassword.text!) {
            
            UIAlertController.showAlertWithOkButton(self, aStrMessage: Constant.AlertMessage.kAlertMsgPassNotMatch, completion: nil)
            return false
        }

        return true
    }
    
    // MARK: - Webservice Methods
    
    func callWebServiceForChangePassword(_ strOldPassword:String, strNewPassword:String)  {
        
        let aStrUserId = AppDelegate.getAppDelegate().userInfoData.usersId!
        let aDictParam:[String:Any] = ["old_password":strOldPassword, "new_password":strNewPassword, "users_id":aStrUserId,"access_token":token]
        
        WebService.POST(Constant.API.kBaseURLWithAPIName("change_password"), param: aDictParam, controller: self, successBlock: { (jsonResponse) in
            
            print("success response is received")
            
            self.btnChangePassword.isUserInteractionEnabled = true;
            
            if (jsonResponse["settings"]["success"].string == "1") {
                
                UIAlertController.showAlertWithOkButton(self, aStrMessage: jsonResponse["settings"]["message"].string, completion: nil)
                self.txtOldPassword.text = ""
                self.txtNewPassword.text = ""
                self.txtConfirmPassword.text = ""
                
            }else if (jsonResponse["settings"]["success"].string == "100") {
                Utility.reloginUserWith(viewController: self)
            }else{
                UIAlertController.showAlertWithOkButton(self, aStrMessage: jsonResponse["settings"]["message"].string, completion: nil)
            }
        }) { (error, isTimeOut) in
            
            self.btnChangePassword.isUserInteractionEnabled = true
            if isTimeOut {
                print("Request Timeout")
            } else {
                print(error?.localizedDescription ?? "")
            }
            
        }
        
    }

}
