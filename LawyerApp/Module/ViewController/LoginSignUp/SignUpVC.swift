//
//  SignUpVC.swift
//  LegalStart
//
//  Created by Adapting Social on 4/24/17.
//  Copyright © 2017 Adapting Social. All rights reserved.
//

import UIKit

class SignUpVC: UIViewController {

    // MARK: - Global Variables
    
    
    // MARK: - Global Outlets
    
    @IBOutlet weak var txtFirstName: UITextField!
    @IBOutlet weak var txtLastName: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtConfirmPassword: UITextField!
    @IBOutlet weak var btnSignUp: UIButton!
    
    @IBOutlet weak var lblTermsAndConditions : UILabel!
    
    // MARK: - ViewController Life Cycle

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
   
    
    func validationForSignUp() -> Bool {
        
        if self.txtFirstName.text!.isEmptyString() {
            UIAlertController.showAlertWithOkButton(self, aStrMessage: Constant.AlertMessage.kAlertMsgEnterFisrtName, completion: nil)
            return false
        }
        else if self.txtLastName.text!.isEmptyString(){
            UIAlertController.showAlertWithOkButton(self, aStrMessage: Constant.AlertMessage.kAlertMsgEnterLastName, completion: nil)
            return false
        }
        else if self.txtEmail.text!.isEmptyString() {
            UIAlertController.showAlertWithOkButton(self, aStrMessage: Constant.AlertMessage.kAlertMsgEnterEmail, completion: nil)
            return false
        }
        else if !self.txtEmail.isValidEmail() {
            UIAlertController.showAlertWithOkButton(self, aStrMessage: Constant.AlertMessage.kAlertMsgValidEmail, completion: nil)
            return false
        }
        else if self.txtPassword.text!.isEmptyString() {
            UIAlertController.showAlertWithOkButton(self, aStrMessage: Constant.AlertMessage.kAlertMsgEnterPassword, completion: nil)
            return false
        }
        else if !txtPassword.text!.validateMinimumLength(6) {
            UIAlertController.showAlertWithOkButton(self, aStrMessage: Constant.AlertMessage.kAlertMsgPasswordMinLength, completion: nil)
            return false
        }
        else if !txtPassword.text!.validateMaximumLength(10) {
            UIAlertController.showAlertWithOkButton(self, aStrMessage: Constant.AlertMessage.kAlertMsgPasswordMaxLength, completion: nil)
            return false
        }
        else if !txtPassword.text!.isValidPassword() {
            UIAlertController.showAlertWithOkButton(self, aStrMessage: Constant.AlertMessage.kAlertMsgPasswordMustContain, completion: nil)
            return false
        }
        else if self.txtConfirmPassword.text!.isEmptyString() {
            UIAlertController.showAlertWithOkButton(self, aStrMessage: Constant.AlertMessage.kAlertMsgEnterConfirmPassword, completion: nil)
            return false
        }
        else if !(self.txtPassword.text! == self.txtConfirmPassword.text!) {
            
            UIAlertController.showAlertWithOkButton(self, aStrMessage: Constant.AlertMessage.kAlertMsgPassNotMatch, completion: nil)
            return false
        }
        
        return true
    }

    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    // MARK: - Button Click Events
    
    @IBAction func btnCloseClick(_ sender: UIButton){
        
        self.view.hideKeyBoard()
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnSignUpClick(_ sender: UIButton){
        
        self.view.hideKeyBoard()
        sender.isUserInteractionEnabled = false;
        
        if validationForSignUp() {
            
            UIAlertController.showAlert(self, aStrMessage: Constant.AlertMessage.kAlertMsgSignUpPrivacyPolicy, style: .alert, aCancelBtn: "CANCEL", aDistrutiveBtn: nil, otherButtonArr: ["AGREE"], completion: { (index, title) in
                
                if title == "AGREE"{
                
                self.callWebServiceForSignUp(self.txtEmail.text!, strPassword: self.txtPassword.text! ,strFirstName: self.txtFirstName.text!, strLastName: self.txtLastName.text!)
                }else{
                
                    sender.isUserInteractionEnabled = true;
                }
                
                
            })
  
        }else{
            
            let delayTime = DispatchTime.now() + Double(Int64(1.5 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
            DispatchQueue.main.asyncAfter(deadline: delayTime) {
                sender.isUserInteractionEnabled = true;
            };
        }
        
    }
    
    @IBAction func btnPrivacyPolicyAction(_ sender: UIButton) {
        
        var param : [String:Any] = [:]
        param["page_code"] = "privacypolicy"
        
        WebService.GET(Constant.API.kBaseURLWithAPIName("get_cms_pages_listing"), param: param, controller: self, callSilently: false, successBlock: { (jsonResponse) in

            if (jsonResponse["settings"]["success"].string == "1"){
                
                let vc : MasterWebBrowser = MasterWebBrowser(nibName: "MasterWebBrowser", bundle: nil)
                vc.htmlString = jsonResponse["data"][0]["page_content"].string!
                self.present(vc, animated: true, completion: nil)
                
                
            }else if (jsonResponse["settings"]["success"].string == "0"){
                
                UIAlertController.showAlertWithOkButton(self, aStrMessage: jsonResponse["settings"]["message"].string, completion: nil)
                
            }else if (jsonResponse["settings"]["success"].string == "100"){
                
            }else{
                
                UIAlertController.showAlertWithOkButton(self, aStrMessage: Constant.AlertMessage.kAlertMsgSomeThingWentWrong, completion: nil)
            }

            
        }) { (error, isTimeOut) in
            
        }
        
    }
    @IBAction func btnTermsConditionAction(_ sender: UIButton) {
        
        var param : [String:Any] = [:]
        param["page_code"] = "termsconditions"
        
        WebService.GET(Constant.API.kBaseURLWithAPIName("get_cms_pages_listing"), param: param, controller: self, callSilently: false, successBlock: { (jsonResponse) in
            
            if (jsonResponse["settings"]["success"].string == "1"){
                
                let vc : MasterWebBrowser = MasterWebBrowser(nibName: "MasterWebBrowser", bundle: nil)
                vc.htmlString = jsonResponse["data"][0]["page_content"].string!
                self.present(vc, animated: true, completion: nil)
                
                
            }else if (jsonResponse["settings"]["success"].string == "0"){
                
                UIAlertController.showAlertWithOkButton(self, aStrMessage: jsonResponse["settings"]["message"].string, completion: nil)
                
            }else if (jsonResponse["settings"]["success"].string == "100"){
                
            }else{
                
                UIAlertController.showAlertWithOkButton(self, aStrMessage: Constant.AlertMessage.kAlertMsgSomeThingWentWrong, completion: nil)
            }
            
            
        }) { (error, isTimeOut) in
            
        }
        
    }
    // MARK: - Webservice Calls
    
    func callWebServiceForSignUp(_ strEmail:String,strPassword:String, strFirstName:String, strLastName:String)  {
        
        let aStrDeviceToken:String?
        
        if UserDefaults.string(forKey: DEVICETOKEN_STRING) == nil {
            aStrDeviceToken = "0"
        }
        else{
            aStrDeviceToken = UserDefaults.string(forKey: DEVICETOKEN_STRING)
        }
        let deviceId = UIDevice.current.identifierForVendor!.uuidString
        let aDictParam:[String:Any] = ["email":strEmail ,"password":strPassword , "first_name":strFirstName, "last_name":strLastName,"device_token":aStrDeviceToken!,"device_id":deviceId,"device_type":"Ios"];
        
        WebService.POST(Constant.API.kBaseURLWithAPIName("users_registration"), param: aDictParam, controller: self, successBlock: { (jsonResponse) in
            
            print("success response is received")
            self.btnSignUp.isUserInteractionEnabled = true;
            
            if  (jsonResponse["settings"]["success"].string == "1") {
                
                self.txtFirstName.text = ""
                self.txtLastName.text = ""
                self.txtEmail.text = ""
                self.txtPassword!.text = ""
                self.txtConfirmPassword.text = ""
                
                UIAlertController.showAlertWithOkButton(self, aStrMessage: Constant.AlertMessage.kAlertMsgRegisterSuccess, completion: { (index, title) in
                    self.dismiss(animated: true, completion: nil)
                })
            
                
            }else{
                UIAlertController.showAlertWithOkButton(self, aStrMessage: jsonResponse["settings"]["message"].string, completion: nil)
            }
            
        }) { (error, isTimeOut) in
            
            self.btnSignUp.isUserInteractionEnabled = true
            if isTimeOut {
                print("Request Timeout")
            } else {
                print(error?.localizedDescription ?? "")
            }
            
        }
        
    }
    
}
