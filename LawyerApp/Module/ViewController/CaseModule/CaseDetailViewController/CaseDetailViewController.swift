//
//  CaseDetailViewController.swift
//  LegalStart
//
//  Created by Adapting Social on 25/04/17.
//  Copyright © 2017 Adapting Social. All rights reserved.
//

import UIKit

class CaseDetailViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,UIButtonMasterDocumentUploaderDelegate {

    @IBOutlet var caseDetailTableView: UITableView!
    @IBOutlet var navBarTitle : UILabel!

    var  caseDetails : CaseListData?
    let user_id = AppDelegate.getAppDelegate().userInfoData.usersId!
    let token = UserDefaults.value(forKey: ACCESSTOKEN)!
    private var shouldLoadData : Bool = false
    var shouldLoadList : (Bool) -> () = {_ in}
    var handleNotification : (Bool,String)?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.shouldLoadList(shouldLoadData)
        caseDetailTableView.estimatedSectionHeaderHeight = 281
        caseDetailTableView.sectionHeaderHeight = UITableViewAutomaticDimension
        caseDetailTableView.estimatedRowHeight = 90
        caseDetailTableView.rowHeight = UITableViewAutomaticDimension
        if self.caseDetails != nil {
            self.navBarTitle.text = self.caseDetails?.caseTitle
        }
        if handleNotification != nil {
            self.callWSToLoadCaseDetail(caseID: (handleNotification?.1)!)
        }
        
    }
    
    func shouldUploadDocument(uploaded : UIButtonMasterDocumentUploader) -> Bool{
    
    
        return true
    }
    func masterDocumentUploaded(uploader : UIButtonMasterDocumentUploader,fileURL : URL,fileExtension : String){
    
        UIAlertController.showAlertWithOkButton(self, aStrMessage: Constant.AlertMessage.kAlertMsgFileUploadedSuccessFully, completion: nil)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if caseDetails == nil {
            return 0
        }
        
        return (caseDetails?.documentListings?.count)!
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : CaseDetailVCDocumentCell = tableView.dequeueReusableCell(withIdentifier: "CaseDetailVCDocumentCell") as! CaseDetailVCDocumentCell
        if let documentData = caseDetails?.documentListings?[indexPath.row] {
            cell.documentTitle.text = documentData.documentName
            let fileExtension = documentData.documentType
            cell.documentImageView.image = Utility.checkFileTypeWith(fileExtension: fileExtension!)
            cell.uploadedDate.text = documentData.date
            cell.btnDownloadDocument.tag = indexPath.row
            cell.btnDownloadDocument.delegate = self
            cell.btnDownloadDocument.exportFILEURL = documentData.document
            cell.btnDownloadDocument.exportFILENAME = documentData.documentName
            if documentData.usersId == "0" {
                cell.uploadedBy.text = "Uploaded by admin"
            }else{
                cell.uploadedBy.text = "Uploaded by me"
            }
        }

        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell : CaseDetailVCCaseDetailCell = tableView.dequeueReusableCell(withIdentifier: "CaseDetailVCCaseDetailCell") as! CaseDetailVCCaseDetailCell
        if caseDetails != nil {
            cell.caseTitle.text = caseDetails?.caseTitle
            cell.caseConcerns.text = caseDetails?.concerns
            cell.documentType.text = caseDetails?.documentType
//            cell.partiesInAgreement.text = caseDetails?.agreement
        }
       
        return cell
    }
    @IBAction func btnBackAction(_ sender: UIButton) {
        self.shouldLoadList(shouldLoadData)
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnEditCaseAction(_ sender: UIButton) {
        
        if self.caseDetails != nil {
            self.addUpdateCaseWith(caseModifier: .edit, caseListData: self.caseDetails)
        }else{
        
            UIAlertController.showAlertWithOkButton(self, aStrMessage: "No Case details found", completion: nil)
        
        }
        
        
    }
    func addUpdateCaseWith(caseModifier : CaseModifier,caseListData : CaseListData?){
        
        let objAddCase : CaseAddViewController = (self.storyboard?.instantiateViewController(withIdentifier: "CaseAddViewController") as? CaseAddViewController)!
        if caseModifier == .edit{
            
            objAddCase.caseDetailsModel = caseListData
        }
        objAddCase.caseModifier = caseModifier
        objAddCase.isChanged = {
            isCaseAdded,loadCases in
            if isCaseAdded == true && loadCases == true {
                self.shouldLoadData = true
                self.callWSToLoadCaseDetail(caseID: self.caseDetails!.caseId!)
            }else if isCaseAdded == true && loadCases == false{
                self.shouldLoadData = true
                self.callWSToLoadCaseDetail(caseID: self.caseDetails!.caseId!)
                let storyBoard : UIStoryboard = UIStoryboard(name: "Appointments", bundle: Bundle.main)
                let centerVC = storyBoard.instantiateViewController(withIdentifier: "BookAppointmentVC") as? BookAppointmentVC
                self.navigationController?.pushViewController(centerVC!, animated: true)
                
            }
        }
        self.navigationController?.pushViewController(objAddCase, animated: true)
    }

    //MARK: LOAD CASE-DETAIL
    func callWSToLoadCaseDetail(caseID : String) {
        let params : [String:Any] = ["case_id":caseID,"users_id":user_id,"access_token":token]
        WebService.GET(Constant.API.kBaseURLWithAPIName("case_details"), param: params, controller: self, callSilently: false, successBlock: { (responseObject) in
            
            
            if responseObject["settings"]["success"].string == "1"{
                
                var newDataDict : [String:Any] = [:]
                newDataDict["case_id"] = responseObject["data"]["case_details"][0]["case_id"].string
                newDataDict["case_title"] = responseObject["data"]["case_details"][0]["case_title"].string
                newDataDict["document_type"] = responseObject["data"]["case_details"][0]["document_type"].string
//                newDataDict["agreement"] = responseObject["data"]["case_details"][0]["agreement"].string
                newDataDict["concerns"] = responseObject["data"]["case_details"][0]["concerns"].string
                newDataDict["users_name"] = responseObject["data"]["case_details"][0]["users_name"].string
                newDataDict["document_listings"] = responseObject["data"]["document_listing"].array
                
                let newCaseDetails = CaseListData(object: newDataDict)
                self.caseDetails = newCaseDetails
                self.navBarTitle.text = self.caseDetails?.caseTitle
                
                self.caseDetailTableView.reloadData()
            }else if responseObject["settings"]["success"].string == "100" {
                Utility.reloginUserWith(viewController: self)
            }else{
                
                UIAlertController.showAlertWithOkButton(self, aStrMessage: responseObject["settings"]["message"].string, completion: nil)
            }
            
            
        }) { (error, isTimeOut) in
            
        }
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
