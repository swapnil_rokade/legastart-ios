//
//  CaseListingViewController.swift
//  LegalStart
//
//  Created by Adapting Social on 25/04/17.
//  Copyright © 2017 Adapting Social. All rights reserved.
//

import UIKit

class CaseListingViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UIGestureRecognizerDelegate {

    //MARK: ATTRIBUTES
    @IBOutlet weak var lblNoRecordFound: UILabel!
    @IBOutlet var caseListTableView: UITableView!
    var caseListModel : CaseListModel! = nil
    let user_id = AppDelegate.getAppDelegate().userInfoData.usersId!
    let token = UserDefaults.value(forKey: ACCESSTOKEN)!
    //MARK: VIEWDID LOAD
    override func viewDidLoad() {
        super.viewDidLoad()

        caseListTableView.estimatedRowHeight = 130
        caseListTableView.rowHeight = UITableViewAutomaticDimension
        
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self as! UIGestureRecognizerDelegate
        self.callWSToLoadCasesWith(user_id: user_id as! String)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
    }
    //MARK: TABLEVIEW DATASOURCE
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if caseListModel == nil {
            
            self.lblNoRecordFound.isHidden = false
            
            return 0
        }
        if caseListModel.data?.count == 0 {
            self.lblNoRecordFound.text = caseListModel.settings?.message
            self.lblNoRecordFound.isHidden = false
            return 0
        }
        
        self.lblNoRecordFound.isHidden = true
        return (caseListModel.data?.count)!
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : CaseListingVCCell = tableView.dequeueReusableCell(withIdentifier: "CaseListingVCCell") as! CaseListingVCCell

        if let dataModel =  caseListModel.data?[indexPath.row]{
        
            cell.caseTitle.text = dataModel.caseTitle
//            cell.courtName.text = dataModel.courtName
//            cell.caseOnBeHalfOf.text = dataModel.caseBehalfOf
//            cell.caseNumber.text = dataModel.caseNumber
           
        }
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    //MARK: DELEGATE
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let aObjCaseDetail : CaseDetailViewController = self.storyboard?.instantiateViewController(withIdentifier: "CaseDetailViewController") as! CaseDetailViewController
        
        if let dataDict = caseListModel.data?[indexPath.row] {
            aObjCaseDetail.caseDetails = dataDict
            aObjCaseDetail.shouldLoadList = {
            
                loadData in
                if loadData {
                    self.callWSToLoadCasesWith(user_id: self.user_id as! String)
                }
            }
            self.navigationController?.pushViewController(aObjCaseDetail, animated: true)
        }
       
        
    }
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
   
   
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let editAction = UITableViewRowAction(style: UITableViewRowActionStyle.default, title: "✏️\n Edit", handler:{action, indexpath in

            let caseData = self.caseListModel.data?[indexPath.row]
            self.editCaseAction(caseData: caseData!)
        });
        editAction.backgroundColor = UIColor.colorFromCode(0x041b54)
        
        
        let deleteAction = UITableViewRowAction(style: UITableViewRowActionStyle.default, title: "🗑️\n Delete", handler:{action, indexpath in
            if let dataDict = self.caseListModel.data?[indexPath.row] {
                
                self.deleteCase(caseData: dataDict)
            }
        });
        deleteAction.backgroundColor = UIColor.colorFromCode(0xf44237)
        return [deleteAction,editAction]
    }
    //MARK: UTILITY
    func editCaseAction(caseData : CaseListData)  {
        
        self.addUpdateCaseWith(caseModifier: .edit, caseListData: caseData)
    }
    func deleteCase(caseData : CaseListData)  {
        
        UIAlertController.showAlert(self, aStrMessage: Constant.AlertMessage.kAlertMsgDeleteCase, style: .alert, aCancelBtn: "Cancel", aDistrutiveBtn: "Delete", otherButtonArr:nil) { (index, message) in
        
            if message == "Delete" && index == 0{
                self.callWSToDeleteCase(case_id: caseData.caseId!, user_id: self.user_id as! String)
            }
            
            
        }
        
        
    }
    
    //MARK: LOAD-CASES
    func callWSToDeleteCase(case_id : String,user_id : String) {
        var param : [String: Any] = [:]
        param["users_id"] = user_id
        param["case_id"] = case_id
        param["access_token"] = token
        WebService.POST(Constant.API.kBaseURLWithAPIName("delete_case"), param: param, controller: self, successBlock: { (jsonResponse) in
            
            if jsonResponse["settings"]["success"].string == "1"{
            
                self.callWSToLoadCasesWith(user_id: user_id)
            }else if jsonResponse["settings"]["success"].string == "100" {
                Utility.reloginUserWith(viewController: self)
            }else{
            
            UIAlertController.showAlertWithOkButton(self, aStrMessage: jsonResponse["settings"]["message"].string, completion: nil)
            }
            
        }) { (error, isTimeOut) in
            
        }
        
    }
    func callWSToLoadCasesWith(user_id : String)  {
        
        
        WebService.GET(Constant.API.kBaseURLWithAPIName("case_listing"), param: ["users_id":user_id,"access_token":token ], controller: self, callSilently: false, successBlock: { (jsonResponse) in
            print(jsonResponse)
            let objCaseListModel = CaseListModel(json: jsonResponse)
            if objCaseListModel.settings?.success == "1"{
            
            self.caseListModel = objCaseListModel
            self.caseListTableView.reloadData()
            }else if objCaseListModel.settings?.success == "0"{
                
                UIAlertController.showAlertWithOkButton(self, aStrMessage: objCaseListModel.settings?.message, completion: nil)
            }else if objCaseListModel.settings?.success == "99"{
                self.caseListModel = objCaseListModel
                self.caseListTableView.reloadData()

            }else if objCaseListModel.settings?.success == "100" {
                Utility.reloginUserWith(viewController: self)
            }else{
            
                UIAlertController.showAlertWithOkButton(self, aStrMessage: Constant.AlertMessage.kAlertMsgSomeThingWentWrong, completion: nil)
            }
            
        }) { (error, isTimeOut) in
            
           
            
        }
        

    }
    //MARK: BUTTON-ACTION
    @IBAction func btnSidePanelAction(_ sender: UIButton) {
        
        self.mm_drawerController.toggle(.left, animated: true, completion: nil)
    }
    @IBAction func btnAddAction(_ sender: UIButton) {
        
        self.addUpdateCaseWith(caseModifier: .add, caseListData: nil)
        
        
    }
    func addUpdateCaseWith(caseModifier : CaseModifier,caseListData : CaseListData?){
        
        let objAddCase : CaseAddViewController = (self.storyboard?.instantiateViewController(withIdentifier: "CaseAddViewController") as? CaseAddViewController)!
        objAddCase.caseDetailsModel = caseListData
        objAddCase.caseModifier = caseModifier
        objAddCase.isChanged = {
            isCaseAdded,loadCases in
            if isCaseAdded == true && loadCases == true {
                
                self.callWSToLoadCasesWith(user_id: self.user_id as! String)
            }else if isCaseAdded == true && loadCases == false{
                
                self.callWSToLoadCasesWith(user_id: self.user_id as! String)
                let storyBoard : UIStoryboard = UIStoryboard(name: "Appointments", bundle: Bundle.main)
                let centerVC = storyBoard.instantiateViewController(withIdentifier: "BookAppointmentVC") as? BookAppointmentVC
                self.navigationController?.pushViewController(centerVC!, animated: true)

            }
        }
        self.navigationController?.pushViewController(objAddCase, animated: true)
    }
    
    //MARK: MEMORY-WARNING
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}
