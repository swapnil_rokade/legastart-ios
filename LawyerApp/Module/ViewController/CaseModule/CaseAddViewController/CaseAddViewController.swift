//
//  CaseAddViewController.swift
//  LegalStart
//
//  Created by Adapting Social on 26/04/17.
//  Copyright © 2017 Adapting Social. All rights reserved.
//

import UIKit

class CaseAddViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UIButtonMasterDocumentPickerDelegate,UITextViewDelegate {

    //MARK: ATTRIBUTES
    @IBOutlet var addCaseTableView: UITableView!
    
    @IBOutlet weak var lblScreenTitle: UILabel!
    var arrDeletedDocs : [CaseListDocumentListings] = []
    var arrDocs : [CaseListDocumentListings] = []

    private var headerCell : CaseAddVCDetailCell?
    var isChanged : (Bool,Bool) -> () = {_ in}
    var caseModifier : CaseModifier?
    var caseDetailsModel : CaseListData?
    
    var masterDocView : MasterWebBrowser?
    
    let token = UserDefaults.value(forKey: ACCESSTOKEN)!
    let users_id = AppDelegate.getAppDelegate().userInfoData.usersId!
    //MARK: VIEWDID-LOAD
    override func viewDidLoad() {
        super.viewDidLoad()

    
        self.isChanged(false, false)
        addCaseTableView.estimatedRowHeight = 100
        addCaseTableView.rowHeight = UITableViewAutomaticDimension
        let aHeaderCell : CaseAddVCDetailCell = addCaseTableView.dequeueReusableCell(withIdentifier: "CaseAddVCDetailCell") as! CaseAddVCDetailCell
        self.headerCell = aHeaderCell
       
        
        let aFooterCell : CaseAddVCAddButtonCell = addCaseTableView.dequeueReusableCell(withIdentifier: "CaseAddVCAddButtonCell") as! CaseAddVCAddButtonCell
        aFooterCell.btnAddCase.addTarget(self, action: #selector(btnAddCaseAction(_:)), for: .touchUpInside)
        
        if caseModifier == .edit && caseDetailsModel != nil{
            //HEADER
            
            self.lblScreenTitle.text = "UPDATE DOCUMENT"
            aHeaderCell.lblDetailTitle.text = "UPDATE DOCUMENT"
            aHeaderCell.txtCaseTitle.text = caseDetailsModel?.caseTitle
            aHeaderCell.txtDocumentType.text = caseDetailsModel?.documentType
//            aHeaderCell.txtPartiesInAgreeMent.text = caseDetailsModel?.agreement
            aHeaderCell.txtViewConcers.text = caseDetailsModel?.concerns
            arrDocs = (caseDetailsModel?.documentListings)!
            //FOOTER
            aFooterCell.btnAddCase.setTitle("UPDATE DOCUMENT", for: .normal)
        }
        
        addCaseTableView.tableFooterView = aFooterCell.contentView
        addCaseTableView.tableHeaderView = aHeaderCell.contentView
     
    }
    //MARK: MasterDocument Picker
    func shouldOpenMasterDocumentPicker(picker: UIButtonMasterDocumentPicker) -> Bool {
        
        if arrDocs.count < 10 {
            return true
        }else{
            UIAlertController.showAlertWithOkButton(self, aStrMessage: Constant.AlertMessage.kAlertMsgLimitDocumentUpload, completion: nil)
            
            return false
        }
    }
    
    func masterDocumentDidFinishPicking(picker : UIButtonMasterDocumentPicker,fileURL : URL,fileExtension : String){
        var aMutDict :  [String:Any] = [:]
        aMutDict["document"] = fileURL.path
        aMutDict["document_type"] = fileExtension
        aMutDict["document_name"] = fileURL.lastPathComponent
        aMutDict["date"] = Date().stringFromDate(format: "dd MMM yyyy")
        aMutDict["document_id"] = ""
        aMutDict["users_id"] = users_id
        
        let doc = CaseListDocumentListings(object: aMutDict)
        arrDocs.append(doc)

        self.addCaseTableView.reloadData()
    
    }
    //MARK: TABLEVIEW DATASOURCE
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        if arrDocs.count > 0 {
            return arrDocs.count
        }
        return 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            
        let aCell : CaseAddVCDocumentCell = tableView.dequeueReusableCell(withIdentifier: "CaseAddVCDocumentCell") as! CaseAddVCDocumentCell
    
            aCell.btnRemoveDocument.tag = indexPath.row
            aCell.btnRemoveDocument.addTarget(self, action: #selector(btnRemoveDocumentAction(_:)), for: .touchUpInside)
            let dataDict = arrDocs[indexPath.row]
            let fileExtension = dataDict.documentType
            aCell.documentImageView.image = Utility.checkFileTypeWith(fileExtension: fileExtension!)
            aCell.documentTitle.text = dataDict.documentName
            aCell.documentUploadedDate.text = dataDict.date
            if dataDict.usersId == "0" {
                aCell.btnRemoveDocument.isHidden = true
                aCell.documentUploadedBy.text = "Uploaded by admin"
            }else{
                aCell.btnRemoveDocument.isHidden = false
                aCell.documentUploadedBy.text = "Uploaded by me"
            }
        
        return aCell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {

            return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let data = arrDocs[indexPath.row]
        masterDocView = MasterWebBrowser(nibName: "MasterWebBrowser", bundle: nil)
        
        masterDocView?.urlToLoad = data.document!
        self.present(masterDocView!, animated: true, completion: nil)
    }
    //MARK: TEXTVIEW DELEGATE
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "Concern/Comments you may have" {
            textView.text = ""
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == "" {
            textView.text = "Concern/Comments you may have"
        }
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            self.view.endEditing(true)
            return false
        }
        
        return true
    }
    //MARK: UTILITY
    func validationWith(cell : CaseAddVCDetailCell) -> Bool{
        
        if cell.txtCaseTitle.text!.isEmpty {
             UIAlertController.showAlertWithOkButton(self, aStrMessage: Constant.AlertMessage.kAlertMessageRequiredField("a valid document title"), completion: nil)
            
            return false
        }
        return true
    }

    
    //MARK: BUTTON-ACTION
    @IBAction func btnAddCaseAction(_ sender: UIButton) {
        
        if headerCell != nil{
            
            if self.validationWith(cell: headerCell!){
                var param : [String:Any] = [:]
                param["users_id"] = AppDelegate.getAppDelegate().userInfoData.usersId!
                param["case_title"] = headerCell?.txtCaseTitle.text
                param["document_text_type"] = headerCell?.txtDocumentType.text
//                param["agreement"] = headerCell?.txtPartiesInAgreeMent.text
                param["concerns"] = headerCell?.txtViewConcers.text
                param["access_token"] = token
                if caseModifier == .edit {
                    self.updateCase(param: param)
                }else{
                    self.addCase(param: param)
                }
             
            }
        }
    }

    @IBAction func btnRemoveDocumentAction(_ sender: UIButton) {
        
        let docToDelete = arrDocs[sender.tag]
        if caseModifier == .edit && docToDelete.documentId != ""{
            
            arrDeletedDocs.append(docToDelete)
            
        }
        
        arrDocs.remove(at: sender.tag)
        
        self.addCaseTableView.reloadData()
    }

    @IBAction func btnBackAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    //MARK: WEB-SERVICE
    func updateCase(param : [String:Any]) {
        var paramTosend = param
        paramTosend["case_id"] = caseDetailsModel?.caseId
        
        var arrNewFiles : [CaseListDocumentListings] = []
        for obj in arrDocs {
            if obj.documentId == "" {
                
                arrNewFiles.append(obj)

            }
        }
        for (index,obj) in arrNewFiles.enumerated() {
            paramTosend["document[\(index)]"] = URL(fileURLWithPath: obj.document!)
            paramTosend["document_name[\(index)]"] = obj.documentName
            paramTosend["document_type[\(index)]"] = obj.documentType
        }
        
        var commSepDocs : [String] = []
        for (index,obj) in arrDeletedDocs.enumerated(){
        
            if obj.documentId != "" {
                commSepDocs.append(obj.documentId!)
            }
        
        }
        paramTosend["delete_attchement_id"] = commSepDocs.joined(separator: ",")
        print(paramTosend)
        self.callWSWithURL(url: Constant.API.kBaseURLWithAPIName("update_case"), param: paramTosend)
    }
    func addCase(param : [String:Any]) {
        var paramTosend = param
        
        for (index,obj) in arrDocs.enumerated() {
            
            paramTosend["document[\(index)]"] = URL(fileURLWithPath: obj.document!)
            paramTosend["document_name[\(index)]"] = obj.documentName
            paramTosend["document_type[\(index)]"] = obj.documentType
            
        }
        print(paramTosend)
        self.callWSWithURL(url: Constant.API.kBaseURLWithAPIName("add_case"), param: paramTosend)
    }
    
    
    func callWSWithURL(url : String,param : [String : Any]) {
        
        WebService.WSPostAPIMultiPart(url: url, params: param, controller: self, progressBlock: { (progress) in
            
        }, successBlock: { (jsonResponse) in
            print(jsonResponse)
            
            if (jsonResponse["settings"]["success"].string == "1"){
                
//                UIAlertController.showAlert(self, aStrMessage: Constant.AlertMessage.kalertMsgCaseAddedSuccess , style: .alert, aCancelBtn: nil, aDistrutiveBtn: "Schedule Later", otherButtonArr: ["Schedule Now"], completion: { (index, value) in
//                    if value == "Schedule Later" && index == 1{
//                        self.isChanged(true, true)
//                        self.navigationController?.popViewController(animated: true)
//                    }else{
//                        self.navigationController?.popViewController(animated: true)
//                        self.isChanged(true, false)
//                        
//                    
//                    }
//                })
                UIAlertController.showAlertWithOkButton(self, aStrMessage: "You have successfully uploaded your document", completion: { (index, title) in
                        self.isChanged(true, true)
                        self.navigationController?.popViewController(animated: true)
                })
                
            }else if (jsonResponse["settings"]["success"].string == "0"){
                
                UIAlertController.showAlertWithOkButton(self, aStrMessage: jsonResponse["settings"]["message"].string, completion: nil)
                
            }else if (jsonResponse["settings"]["success"].string == "100"){
                Utility.reloginUserWith(viewController: self)
            }else{
                
                UIAlertController.showAlertWithOkButton(self, aStrMessage: Constant.AlertMessage.kAlertMsgSomeThingWentWrong, completion: nil)
            }
        }) { (error, isTimeOut) in
            print(error ?? "")
            
        }
    }
    //MARK: MEMORY-WARNING
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    

}
