//
//  AppointmentVC.swift
//  LegalStart
//
//  Created by Adapting Social on 07/07/17.
//  Copyright © 2017 Adapting Social. All rights reserved.
//

import UIKit
enum EventType {
    case upcoming
    case past
}
class AppointmentVC: UIViewController,MasterPageControllerDataSource,UIGestureRecognizerDelegate {

    @IBOutlet weak var mainPageController: MasterPageController!
    
    var objAppointMentListModel : AppointmentListModel?
    
    var objBookAppointMentVC : BookAppointmentVC?
    var objUpcomingAppointMentVC : AppointmentListVC?
    var objPastAppointMentVC : AppointmentListVC?
    let token = UserDefaults.value(forKey: ACCESSTOKEN) as! String
    let users_id = AppDelegate.getAppDelegate().userInfoData.usersId!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        self.callWebServiceForAppointmentList()
        // Do any additional setup after loading the view.
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
    }
    func numberOfViewController(pageController: MasterPageController) -> ([String], [UIViewController]) {
        
        objBookAppointMentVC = self.storyboard?.instantiateViewController(withIdentifier: "BookAppointmentVC") as? BookAppointmentVC
        objBookAppointMentVC?.appointmentBookedCallback = {
        
            self.callWebServiceForAppointmentList()
        
        
        }
        objUpcomingAppointMentVC = self.storyboard?.instantiateViewController(withIdentifier: "AppointmentListVC") as? AppointmentListVC
        objUpcomingAppointMentVC?.eventTypeValue = .upcoming
        objUpcomingAppointMentVC?.objAppointmentListModel = self.objAppointMentListModel?.data
        
        objPastAppointMentVC = self.storyboard?.instantiateViewController(withIdentifier: "AppointmentListVC") as? AppointmentListVC
        objPastAppointMentVC?.eventTypeValue = .past
        objPastAppointMentVC?.objAppointmentListModel = self.objAppointMentListModel?.data
        
        return (["Book Appointment","Upcoming Appointments","Past Appointments"],[objBookAppointMentVC!,objUpcomingAppointMentVC!,objPastAppointMentVC!])
    }
    func didMoveToViewController(pageController: MasterPageController, index: Int, viewController: UIViewController) {
        
    }

    func willMoveToViewController(pageController: MasterPageController, index: Int, viewController: UIViewController) {
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnSidePanelAction(_ sender: UIButton) {
        
        self.mm_drawerController.toggle(.left, animated: true) { (index) in
            
        }
    }

    func callWebServiceForAppointmentList() {
        
        let aDictParam:[String:Any] = ["users_id":users_id,"access_token":token]
        
        WebService.POST(Constant.API.kBaseURLWithAPIName("appointment_listing"), param: aDictParam, controller: self, successBlock: { (jsonResponse) in
            
            if (jsonResponse["settings"]["success"].string == "1") {
            self.objAppointMentListModel = nil
            self.objAppointMentListModel = AppointmentListModel(json: jsonResponse)
            
                self.mainPageController.datasource = self
                self.mainPageController.reloadData()

            }else if (jsonResponse["settings"]["success"].string == "100") {
                Utility.reloginUserWith(viewController: self)
            }else{
                UIAlertController.showAlertWithOkButton(self, aStrMessage: jsonResponse["settings"]["message"].string, completion: nil)
            }
        }) { (error, isTimeOut) in
            
            if isTimeOut {
                print("Request Timeout")
            } else {
                print(error?.localizedDescription ?? "")
            }
            
        }
        
    }

    

}
