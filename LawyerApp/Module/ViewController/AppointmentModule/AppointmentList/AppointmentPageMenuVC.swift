
//
//  AppointmentPageMenuVC.swift
//  LegalStart
//
//  Created by Adapting Social on 5/8/17.
//  Copyright © 2017 Adapting Social. All rights reserved.
//

import UIKit

class AppointmentPageMenuVC: UIViewController, UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var lblMessage : UILabel!
    @IBOutlet var tblViewAppointmentList: UITableView!
    
    // MARK: - Global Variables
    var isUpcomingAppointment = true
    var objAppointmentListModel : AppointmentListModel?
    let token = UserDefaults.value(forKey: ACCESSTOKEN) as! String
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        tblViewAppointmentList.estimatedRowHeight = 80.0
        tblViewAppointmentList.rowHeight = UITableViewAutomaticDimension

        // Get user current location...
        if Utility.sharedInstance.currentLocation == nil {
            Utility.sharedInstance.getUserCurrentLocation()
        }

        self.callWebServiceForAppointmentList()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - UITableViewDataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if isUpcomingAppointment {
            
            if self.objAppointmentListModel?.data?.appointmentUpcomingBookList != nil {
                return (self.objAppointmentListModel?.data?.appointmentUpcomingBookList?.count)!
            }
            return 0
            
        }
        else
        {
            if self.objAppointmentListModel?.data?.appointmentPastcomingBookList != nil {
                return (self.objAppointmentListModel?.data?.appointmentPastcomingBookList?.count)!
            }
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AppointmentListCell") as! AppointmentListCell
        
        if isUpcomingAppointment {
            
            let objUpcomingAppointmentData = self.objAppointmentListModel?.data?.appointmentUpcomingBookList?[indexPath.row]
            cell.lblAppointmentTitle.text = objUpcomingAppointmentData?.appointmentTitle
            cell.lblDate.text = objUpcomingAppointmentData?.availabilityDate
            cell.lblTime.text = (objUpcomingAppointmentData?.availabilityStartTime)! + " - " + (objUpcomingAppointmentData?.availabilityEndTime)!
        }
        else
        {
            let objPastAppointmentData = self.objAppointmentListModel?.data?.appointmentPastcomingBookList?[indexPath.row]
            cell.lblAppointmentTitle.text = objPastAppointmentData?.appointmentTitle
            cell.lblDate.text = objPastAppointmentData?.availabilityDate
            cell.lblTime.text = (objPastAppointmentData?.availabilityStartTime)! + " - " + (objPastAppointmentData?.availabilityEndTime)!
        }
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return isUpcomingAppointment
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        //        let editAction = UITableViewRowAction(style: UITableViewRowActionStyle.default, title: "Edit", handler:{action, indexpath in
        //
        //            let caseData = self.caseListModel.data?[indexPath.row]
        //            self.editCaseAction(caseData: caseData!)
        //        });
        //        editAction.backgroundColor = UIColor.colorFromCode(0x041b54)
        
        let deleteAction = UITableViewRowAction(style: UITableViewRowActionStyle.default, title: "Cancel", handler:{action, indexpath in
            
            self.deleteAppointment(index : indexPath.row)
        });
        
        deleteAction.backgroundColor = UIColor.colorFromCode(0xF44237)
        return [deleteAction]
    }
    
    // MARK: - UITableViewDelegate
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
    }
    
    func showAppointmentData(){
        
        var arrAppointmentData : [Any]
        var aStrMessage = ""
        
        if isUpcomingAppointment {
            arrAppointmentData = (self.objAppointmentListModel?.data?.appointmentUpcomingBookList)!
            aStrMessage = Constant.NoDataFound.kUpcomingAppointmentMessage
        }
        else
        {
            arrAppointmentData = (self.objAppointmentListModel?.data?.appointmentPastcomingBookList)!
            aStrMessage = Constant.NoDataFound.kPastAppointmentMessage
        }
        
        if (arrAppointmentData.count > 0) {
            
            lblMessage.isHidden = true
            tblViewAppointmentList.isHidden = false
            tblViewAppointmentList.reloadData()
        }
        else
        {
            lblMessage.isHidden = false
            tblViewAppointmentList.isHidden = true
            lblMessage.text = aStrMessage
        }
        
    }
    
    func deleteAppointment(index : Int)  {
        
        UIAlertController.showAlert(self, aStrMessage: Constant.AlertMessage.kAlertMsgDeleteAppointmentConfirmation, style: .alert, aCancelBtn: "Cancel", aDistrutiveBtn: "Delete", otherButtonArr:nil) { (index, message) in
            
            if message == "Delete" && index == 0 {
                self.callWebServiceForDeleteAppointment(selectedIndex: index)
            }
            
        }
        
    }

    // MARK: - Button Click Events
    
    @IBAction func btnSideMenuClick(_ sender: UIButton)
    {
        self.mm_drawerController?.toggle(.left, animated: true, completion: nil)
    }
    
    @IBAction func btnBookAppointmentClick(_ sender: UIButton)
    {
        let objBookAppointmentVC = self.storyboard?.instantiateViewController(withIdentifier: "BookAppointmentVC") as! BookAppointmentVC
        objBookAppointmentVC.appointmentBookedCallback = { () in
            
            self.callWebServiceForAppointmentList()
        }

        self.navigationController?.pushViewController(objBookAppointmentVC, animated: true)
    }

    // MARK: - Webservice Methods
    
    func callWebServiceForAppointmentList() {
        
        let aStrUserId = AppDelegate.getAppDelegate().userInfoData.usersId
        
        let aDictParam:[String:Any] = ["users_id":aStrUserId,"access_token":token]
        
        WebService.POST(Constant.API.kBaseURLWithAPIName("appointment_listing"), param: aDictParam, controller: self, successBlock: { (jsonResponse) in
            
            print("success response is received")
    

            self.objAppointmentListModel = AppointmentListModel(json: jsonResponse)
            if (self.objAppointmentListModel?.settings?.success == "1") {
                
                // Show Appointment Data...
                self.showAppointmentData()
                
            }else if (self.objAppointmentListModel?.settings?.success == "100") {
                Utility.reloginUserWith(viewController: self)
            }else{
                UIAlertController.showAlertWithOkButton(self, aStrMessage: self.objAppointmentListModel?.settings?.message, completion: nil)
            }
        }) { (error, isTimeOut) in
            
            if isTimeOut {
                print("Request Timeout")
            } else {
                print(error?.localizedDescription ?? "")
            }
            
        }
        
    }
    
    func callWebServiceForDeleteAppointment(selectedIndex : Int) {
        
        let objUpcomingAppointmentData = self.objAppointmentListModel?.data?.appointmentUpcomingBookList?[selectedIndex]
        let aStrUserId = AppDelegate.getAppDelegate().userInfoData.usersId!
        let aStrAppointmentId = objUpcomingAppointmentData?.appointmentId!
        
        let aDictParam:[String:Any] = ["users_id": aStrUserId, "appointment_id": aStrAppointmentId!,"access_token":token]
        
        WebService.POST(Constant.API.kBaseURLWithAPIName("delete_appointment"), param: aDictParam, controller: self, successBlock: { (jsonResponse) in
            
            print("success response is received")
            if  (jsonResponse["settings"]["success"].string == "1") {
                
                // Delete data...
                self.objAppointmentListModel?.data?.appointmentUpcomingBookList?.remove(at: selectedIndex)
                let indexPath = IndexPath.init(row: selectedIndex, section: 0)
                self.tblViewAppointmentList.deleteRows(at: [indexPath], with: .none)
            }else if (jsonResponse["settings"]["success"].string == "100") {
                Utility.reloginUserWith(viewController: self)
            }else{
                UIAlertController.showAlertWithOkButton(self, aStrMessage: jsonResponse["settings"]["message"].string, completion: nil)
            }
        }) { (error, isTimeOut) in
            
            if isTimeOut {
                print("Request Timeout")
            } else {
                print(error?.localizedDescription ?? "")
            }
            
        }
        
    }
}
