//
//  AppointmentListVC.swift
//  LegalStart
//
//  Created by Adapting Social on 5/8/17.
//  Copyright © 2017 Adapting Social. All rights reserved.
//

import UIKit

class AppointmentListVC: UIViewController, UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var lblMessage : UILabel!
    @IBOutlet weak var tblViewAppointmentList: UITableView!
    
   
    
    // MARK: - Global Variables
    var eventTypeValue : EventType = .upcoming
    var objAppointmentListModel : AppointmentListData?
    let token = UserDefaults.value(forKey: ACCESSTOKEN) as! String
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        tblViewAppointmentList.estimatedRowHeight = 80.0
        tblViewAppointmentList.rowHeight = UITableViewAutomaticDimension
        
        // Get user current location...
        if Utility.sharedInstance.currentLocation == nil {
            Utility.sharedInstance.getUserCurrentLocation()
        }
        
        // Set font of segment control...
        var attributes = [ NSFontAttributeName: UIFont(name: Constant.FontName.kFontRobotoRegular, size: CGFloat(14.0))!, NSForegroundColorAttributeName: UIColor.colorFromCode(0x9B9B9B)]
       
        
        attributes = [ NSFontAttributeName: UIFont(name: Constant.FontName.kFontRobotoRegular, size: CGFloat(14.0))!, NSForegroundColorAttributeName: UIColor.colorFromCode(0x041B54)]
       


        
    }
    func reloadDataWithModel(dataModel : AppointmentListData) {
        self.objAppointmentListModel = nil
        self.objAppointmentListModel = dataModel
        self.tblViewAppointmentList.reloadData()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
   
    // MARK: - UITableViewDataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        
        if self.objAppointmentListModel != nil {
            
            if self.eventTypeValue == .upcoming {
                
                if self.objAppointmentListModel!.appointmentUpcomingBookList != nil{
                    return self.objAppointmentListModel!.appointmentUpcomingBookList!.count
                }
                lblMessage.isHidden = false
                tblViewAppointmentList.isHidden = true
                lblMessage.text = Constant.NoDataFound.kUpcomingAppointmentMessage
                return 0
            }else{
                if self.objAppointmentListModel!.appointmentPastcomingBookList != nil {
                    return self.objAppointmentListModel!.appointmentPastcomingBookList!.count
                }
                lblMessage.isHidden = false
                tblViewAppointmentList.isHidden = true
                lblMessage.text = Constant.NoDataFound.kPastAppointmentMessage
                return 0
            
            }
            
            
        }
        lblMessage.isHidden = false
        tblViewAppointmentList.isHidden = true
        
        if self.eventTypeValue == .upcoming {
            
            
            lblMessage.text = Constant.NoDataFound.kUpcomingAppointmentMessage
        }else{
            
            
            lblMessage.text = Constant.NoDataFound.kPastAppointmentMessage
        }
        
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        
        if self.eventTypeValue == .upcoming {
            let cell = tableView.dequeueReusableCell(withIdentifier: "AppointmentListCell") as! AppointmentListCell
            cell.indexPath = indexPath
            if let objUpcomingAppointmentData = self.objAppointmentListModel?.appointmentUpcomingBookList?[indexPath.row]{
                cell.lblAppointmentTitle.text = objUpcomingAppointmentData.appointmentTitle
                cell.lblDate.text = objUpcomingAppointmentData.availabilityDate
                cell.btnDeleteAppointment.isHidden = false
                
                cell.didDeleteButtonClicked = {
                    cell,selectedIndexPath in
                    
        
                        self.deleteAppointment(selectedIndex: selectedIndexPath)
                    
                    
                }
                
                cell.lblTime.text = (objUpcomingAppointmentData.availabilityStartTime)! + " - " + (objUpcomingAppointmentData.availabilityEndTime)!
            }
            
            return cell
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "AppointmentListCell") as! AppointmentListCell
            cell.indexPath = indexPath
            if let objPastAppointmentData = self.objAppointmentListModel?.appointmentPastcomingBookList?[indexPath.row]{
                cell.lblAppointmentTitle.text = objPastAppointmentData.appointmentTitle
                cell.lblDate.text = objPastAppointmentData.availabilityDate
                cell.btnDeleteAppointment.isHidden = true
                cell.lblTime.text = (objPastAppointmentData.availabilityStartTime)! + " - " + (objPastAppointmentData.availabilityEndTime)!
            
            }
            
            return cell
        }
        
        
        
    }
    
    
    
    
    // MARK: - UITableViewDelegate
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
    }
    
   
    
    func deleteAppointment(selectedIndex : IndexPath)  {
        
        UIAlertController.showAlert(self, aStrMessage: Constant.AlertMessage.kAlertMsgDeleteAppointmentConfirmation, style: .alert, aCancelBtn: "Cancel", aDistrutiveBtn: "Delete", otherButtonArr:nil) { (index, message) in
            
            if message == "Delete" && index == 0 {
                self.callWebServiceForDeleteAppointment(selectedIndex: selectedIndex)
            }
            
        }
        
    }
    
    // MARK: - Button Click Events
    @IBAction func btnSideMenuClick(_ sender: UIButton)
    {
        self.mm_drawerController?.toggle(.left, animated: true, completion: nil)
    }
    
//    @IBAction func btnBookAppointmentClick(_ sender: UIButton)
//    {
//        let objBookAppointmentVC = self.storyboard?.instantiateViewController(withIdentifier: "BookAppointmentVC") as! BookAppointmentVC
//        objBookAppointmentVC.appointmentBookedCallback = { () in
//            
////            self.callWebServiceForAppointmentList()
//          
//            self.isUpcomingAppointment = true
//        }
//        
//        self.navigationController?.pushViewController(objBookAppointmentVC, animated: true)
//    }
    
    
    
    // MARK: - Webservice Methods
    
        
    func callWebServiceForDeleteAppointment(selectedIndex : IndexPath) {
        
        let objUpcomingAppointmentData = self.objAppointmentListModel!.appointmentUpcomingBookList![selectedIndex.row]
        let aStrUserId = AppDelegate.getAppDelegate().userInfoData.usersId!
        
        let aStrAppointmentId = objUpcomingAppointmentData.appointmentId!
        
        let aDictParam:[String:Any] = ["users_id": aStrUserId, "appointment_id": aStrAppointmentId,"access_token":token]
        
        WebService.POST(Constant.API.kBaseURLWithAPIName("delete_appointment"), param: aDictParam, controller: self, successBlock: { (jsonResponse) in
            
            print("success response is received")
            if  (jsonResponse["settings"]["success"].string == "1") {
                
                // Delete data...
                self.objAppointmentListModel!.appointmentUpcomingBookList!.remove(at: selectedIndex.row)
//                let indexPath = IndexPath.init(row: selectedIndex, section: 0)
                self.tblViewAppointmentList.deleteRows(at: [selectedIndex], with: .none)
                self.tblViewAppointmentList.reloadData()
            }else if (jsonResponse["settings"]["success"].string == "100") {
                Utility.reloginUserWith(viewController: self)
            }else{
                UIAlertController.showAlertWithOkButton(self, aStrMessage: jsonResponse["settings"]["message"].string, completion: nil)
            }
        }) { (error, isTimeOut) in
            
            if isTimeOut {
                print("Request Timeout")
            } else {
                print(error?.localizedDescription ?? "")
            }
            
        }
        
    }
}

