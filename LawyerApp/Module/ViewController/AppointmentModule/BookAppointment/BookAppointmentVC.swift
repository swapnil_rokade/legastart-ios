//
//  BookAppointmentVC.swift
//  LegalStart
//
//  Created by Adapting Social on 5/16/17.
//  Copyright © 2017 Adapting Social. All rights reserved.
//

import UIKit

class BookAppointmentVC: UIViewController, FSCalendarDataSource, FSCalendarDelegate , FSCalendarDelegateAppearance, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    struct Color {
        static let selectedText = UIColor.white
        static let text = UIColor(red: 84.0/255.0, green: 84.0/255.0, blue: 84.0/255.0, alpha: 1.0) // UIColor.black // KP
        static let textDisabled = UIColor(red: 204.0/255.0, green: 204.0/255.0, blue: 204.0/255.0, alpha: 1.0) // UIColor.gray // KP
        static let selectionBackground = UIColor(red: 251.0/255.0, green: 199.0/255.0, blue: 0.0/255.0, alpha: 1.0) // UIColor(red: 0.2, green: 0.2, blue: 1.0, alpha: 1.0) // KP
        static let sundayText = text // UIColor(red: 1.0, green: 0.2, blue: 0.2, alpha: 1.0) // KP
        static let sundayTextDisabled = textDisabled // UIColor(red: 1.0, green: 0.6, blue: 0.6, alpha: 1.0) // KP
        static let sundaySelectionBackground = UIColor(red: 247.0/255.0, green: 182.0/255.0, blue: 44.0/255.0, alpha: 1.0) // sundayText // KP
    }
    
    private let formatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy/MM/dd"
        return formatter
    }()
    
    private let monthFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "MMM YYYY"
        return formatter
    }()
    
    private var gregorian: Calendar! = Calendar(identifier:Calendar.Identifier.gregorian)
    
    
    // MARK: - Properties & Outlets
    
    @IBOutlet weak var lblMonthName: UILabel!
    @IBOutlet weak var viewOptions: UIView!
    @IBOutlet weak var viewTimeSlot: UIView!
    @IBOutlet weak var viewBottom: UIView!
    @IBOutlet weak var lblMessage : UILabel!
    
    @IBOutlet weak var calendar: FSCalendar!
    @IBOutlet weak var calendarHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var contentViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var txtAppointmentTitle: UITextField!
    @IBOutlet weak var btnCalendarNext: UIButton!
    @IBOutlet weak var btnCalendarPrevious: UIButton!
    @IBOutlet weak var btnBookAppointment: UIButton!
    
    @IBOutlet weak var collectionViewTimings: UICollectionView!

    // MARK: - Global Variables

    var strSelectedDate : String = ""
    var arrTimeSlot : [[String : String]] = []

    var objAppointmentBookListModel : AppointmentBookListModel?
    var selectedTimeSlotIndex = -99
    
    typealias appointmentBookedCallback = () ->Void
    var appointmentBookedCallback : appointmentBookedCallback?
    
    var timerRefresh : Timer?
    let token = UserDefaults.value(forKey: ACCESSTOKEN) as! String
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        // Set Calendar View...
        self.setUpCalendar()
        self.callWebServiceForAppointmentBookList()
        
        // Set Option View shadow...
        // Make the shadow
        viewOptions.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        viewOptions.layer.shadowRadius = 1.0
        viewOptions.layer.shadowColor = UIColor.black.cgColor
        viewOptions.layer.shadowOpacity = 0.4

        // Set ContentView height...
        contentViewHeightConstraint.constant = self.view.frame.height - 164
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.startTimer()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        self.stopTimer()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    // MARK: - FSCalendar Methods
    func setUpCalendar() {
        
        self.calendar.appearance.caseOptions = [.headerUsesUpperCase,.weekdayUsesUpperCase]
        self.calendar.select(Date())
        self.calendar.scope = .week
        self.calendar.scopeGesture.isEnabled = false
        
        lblMonthName.text = monthFormatter.string(from: calendar.currentPage)
        
        self.calendar.appearance.titleFont = UIFont(name: Constant.FontName.kFontRobotoRegular, size: 17)
        self.calendar.appearance.weekdayFont = UIFont(name: Constant.FontName.kFontRobotoRegular, size: 13)
        self.calendar.appearance.weekdayTextColor = UIColor.colorFromCode(0xFFFFFF, AndAlpha: 0.5)
        
        self.calendar.appearance.selectionColor = UIColor.colorFromCode(0xFF7561)
        
        strSelectedDate = Utility.sharedInstance.getStringFromDate("yyyy-MM-dd", date: calendar.selectedDate)
        
    }
    
    // MARK: - Button Click Events
    
    
    
    @IBAction func btnCalendarPreviousClick(_ sender: UIButton) {
        
        let currentWeek = self.calendar.currentPage
        let weekEndDate = self.calendar.begining(ofWeek: currentWeek)
        
        if (weekEndDate.compare(self.calendar.minimumDate) == .orderedDescending) {
            
            let previousWeek = self.gregorian.date(byAdding: .weekOfMonth, value: -1, to: currentWeek)
            self.calendar.setCurrentPage(previousWeek!, animated: true)
        }

    }
    
    @IBAction func btnCalendarNextClick(_ sender: UIButton) {
        
        let currentWeek = self.calendar.currentPage
        let weekEndDate = self.calendar.end(ofWeek: currentWeek)
        
        if (weekEndDate.compare(self.calendar.maximumDate) == .orderedAscending) {
            
            let nextWeek = self.gregorian.date(byAdding: .weekOfMonth, value: 1, to: currentWeek)
            self.calendar.setCurrentPage(nextWeek!, animated: true)
        }

    }

    @IBAction func btnBookAppointmentClick(_ sender: UIButton) {
        
        self.view.hideKeyBoard()
        sender.isUserInteractionEnabled = false;
        
        if validationForBookAppointment() {
            
            self.callWebServiceForBookAppointment()
            
        }else{
            
            let delayTime = DispatchTime.now() + Double(Int64(1.5 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
            DispatchQueue.main.asyncAfter(deadline: delayTime) {
                sender.isUserInteractionEnabled = true;
            };
        }

    }
    
    // MARK: - FSCalendarDataSource And FSCalendarDelegate Methods
    
    func minimumDate(for calendar: FSCalendar) -> Date {
        // Minimum date is today...
        return Date()
    }
    
//    func maximumDate(for calendar: FSCalendar) -> Date {
//        //        return self.formatter.date(from: "2016/11/06")!
//        let nextMonth = self.gregorian.date(byAdding: .month, value: 1, to: Date(), options: .init(rawValue: 0))
//        return nextMonth!
//    }
    
    func calendar(_ calendar: FSCalendar, numberOfEventsFor date: Date) -> Int {
        return 0
    }
    
    func calendarCurrentPageDidChange(_ calendar: FSCalendar) {
        NSLog("change page to \(self.formatter.string(from: calendar.currentPage))")
        
        lblMonthName.text = monthFormatter.string(from: calendar.currentPage)
        
        btnCalendarNext.isEnabled = true
        btnCalendarPrevious.isEnabled = true
        
        let currentWeek = self.calendar.currentPage
        let weekEndDate = self.calendar.end(ofWeek: currentWeek)
        
//        // Code to Enable / Disable Next Button
//        if (weekEndDate.compare(self.calendar.maximumDate) == .orderedAscending) {
//            btnCalendarNext.isEnabled = true
//        } else {
//            btnCalendarNext.isEnabled = false
//        }
        
        // Code to Enable / Disable Previous Button
        let weekStartDate = self.calendar.begining(ofWeek: currentWeek)
        if (weekStartDate.compare(self.calendar.minimumDate) == .orderedDescending) {
            btnCalendarPrevious.isEnabled = true
        } else {
            btnCalendarPrevious.isEnabled = false
        }
        
    }
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date) {
        
        self.view.hideKeyBoard()
        
        NSLog("calendar did select date \(self.formatter.string(from: date))")
        
        lblMonthName.text = monthFormatter.string(from: date)
        selectedTimeSlotIndex = -99
        
        let strDate = Utility.sharedInstance.getStringFromDate("yyyy-MM-dd", date: date)
        
        if (strDate != strSelectedDate) {
            self.callWebServiceForAppointmentBookList()
        }
        
        strSelectedDate = Utility.sharedInstance.getStringFromDate("yyyy-MM-dd", date: calendar.selectedDate)
    }
    
    func calendar(_ calendar: FSCalendar, boundingRectWillChange bounds: CGRect, animated: Bool) {
        calendarHeightConstraint.constant = bounds.height
        view.layoutIfNeeded()
    }
    
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, borderRadiusFor date: Date) -> CGFloat {
        return 0
    }

    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, titleDefaultColorFor date: Date) -> UIColor? {
        
        if (date.compare(Date()) == .orderedSame) {
            return Color.text
        }
//
//        if (date.compare(calendar.maximumDate) == .orderedDescending) {
//            return Color.textDisabled
//        }
//        
        if (date.compare(Date()) == .orderedAscending) {
            return Color.textDisabled
        }
        
        return UIColor.white
        
    }
    
    // MARK: - UICollectionView DataSource Methods
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrTimeSlot.count;
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let aCell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! BookAppointmentCell
        
        aCell.btnTimeSlot.isEnabled = true
        aCell.btnTimeSlot.isSelected = false
        aCell.isUserInteractionEnabled = true
        
        let aDictTimeSlot = arrTimeSlot[indexPath.row]
        let aStrTitle = aDictTimeSlot[APPOINTMENT_START_TIME]! + " - " + aDictTimeSlot[APPOINTMENT_END_TIME]!
        aCell.btnTimeSlot.setTitle(aStrTitle, for: .normal)
        
        aCell.btnTimeSlot.tag = indexPath.row
        aCell.btnTimeSlot.isUserInteractionEnabled = false
        
        if self.checkTimeSlotIsReserved(aDictTimeSlot) {
            aCell.setReservedCell(true)
            aCell.isUserInteractionEnabled = false
        }
        else
        {
//            aCell.setReservedCell(false)

            if selectedTimeSlotIndex == indexPath.row {
                aCell.setButtonSelected(true)
            }
            else
            {
                aCell.setButtonSelected(false)
            }

            let strToday = Utility.sharedInstance.getStringFromDate("yyyy-MM-dd", date: Date())
            let selectedDate = Utility.sharedInstance.getStringFromDate("yyyy-MM-dd", date: calendar.selectedDate)

            // For completed timeslot of today, disable user interaction...
            if strToday == selectedDate {

                let dateFormatter  = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"

//                let startTime = dateFormatter.date(from: aDictTimeSlot[APPOINTMENT_START_TIME]!)
//            
//                if (startTime?.compare(Date()) == .orderedAscending) {
//                    aCell.isUserInteractionEnabled = false
//                }
                
            }
            
        }
        
        return aCell
        
    }
    
    // MARK: - UICollectionView Delegate Methods
    func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        print("Cell clicked At : \(indexPath.row)")
        let aCell = collectionView.cellForItem(at: indexPath) as! BookAppointmentCell
        
        if aCell.btnTimeSlot.isSelected {
            aCell.setButtonSelected(false)
            selectedTimeSlotIndex = -99
        }
        else
        {
            aCell.setButtonSelected(true)
            selectedTimeSlotIndex = indexPath.row
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        
        // DeSelect previously selected cell...
        let aCell = collectionView.cellForItem(at: indexPath) as? BookAppointmentCell
        aCell?.setButtonSelected(false)

    }
    
    // MARK: - UICollectionView FlowLayout Methods
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 16, bottom: 20, right: 16)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let noOfItemsPerRow = 2
        
        let width = Int(self.collectionViewTimings.frame.width)
        let availableWidth = width - (16 * (noOfItemsPerRow + 1))
        
        var widthPerItem = 160
        if Constant.DeviceType.IS_IPHONE_5 {
            widthPerItem = 137
        }
        else
        {
            widthPerItem = (availableWidth / noOfItemsPerRow)
        }
        
        return CGSize(width: widthPerItem, height:50)

    }
    
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat{
//        return 12
//    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    // MARK: -----------------
    // MARK: Calculate TimeSlot
    // MARK: -----------------
    
    func setUpUIAndData() {
     
        self.calculateTimeSlot()
        self.collectionViewTimings.reloadData()
        
        if (arrTimeSlot.count > 0) {
            
            lblMessage.isHidden = true
            viewOptions.isHidden = false
            viewTimeSlot.isHidden = false
            viewBottom.isHidden = false
            self.collectionViewTimings.contentOffset = CGPoint.zero
        }
        else
        {
            lblMessage.isHidden = false
            viewOptions.isHidden = true
            viewTimeSlot.isHidden = true
            viewBottom.isHidden = true
            lblMessage.text = Constant.NoDataFound.kTimeSlotMessage
        }
        
    }
    
    func calculateTimeSlot() {
        
        arrTimeSlot.removeAll()
        
        // We are creating 15 minutes Timeslot between Start Time and End Time...
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(identifier: "UTC")
        
        guard let arrAppointmentHours = self.objAppointmentBookListModel?.data?.appointmentHours else {
            print("Appointment hours not available.")
            return
        }
        
        for appointmentData in arrAppointmentHours {
            
            guard let aStrStartTime = appointmentData.startTime else {
                print("Start Time is not available.")
                return
            }
            
            guard let aStrEndTime = appointmentData.endTime else {
                print("Start Time is not available.")
                return
            }
            
            let aStrCalendarDate = Utility.sharedInstance.getStringFromDate(Constant.kDateFormat_YYYYMMDD, date: self.calendar.selectedDate)
            let aStrTodayDate = Utility.sharedInstance.getStringFromDate(Constant.kDateFormat_YYYYMMDD, date: Date())
            
            var intervalDate : Date?
            var startDate : Date?
            var endDate : Date?
            
            if (aStrTodayDate == aStrCalendarDate) {
                
                let currentServerTime = Utility.sharedInstance.getDateFromString(appointmentData.currentServerTime!, dateFormat:Constant.kDateFormat_YYYYMMDD_HHMMSS)
                dateFormatter.dateFormat = Constant.kDateFormat_YYYYMMDD
                
                // Need to get UTC date string...
                let aStrServerDate = dateFormatter.string(from: currentServerTime)
//                let aStrServerDate = Utility.sharedInstance.getStringFromDate(Constant.kDateFormat_YYYYMMDD, date: currentServerTime)
                
                // Start Time is in only time format, so we are converting it in date time format...
                let aStrStartDate = aStrServerDate + " " + aStrStartTime
                let aStrEndDate = aStrServerDate + " " + aStrEndTime
                
                startDate = Utility.sharedInstance.getDateFromString(aStrStartDate, dateFormat:Constant.kDateFormat_YYYYMMDD_HHMMSS)
                endDate = Utility.sharedInstance.getDateFromString(aStrEndDate, dateFormat:Constant.kDateFormat_YYYYMMDD_HHMMSS)
                
                // We are comparing available start time and current time...
                if (startDate?.compare(currentServerTime) == .orderedAscending) {
                    
                    // Start Time has gone so take current server time as start time...
                    startDate = currentServerTime
                }
                
                let units: Set<Calendar.Component> = [.minute, .hour, .day, .month, .year]
                gregorian.timeZone = TimeZone(abbreviation: "UTC")!
                var dateComponents = gregorian.dateComponents(units, from: startDate!)
                
                let minutes = dateComponents.minute
                
                // If miutes is greater than "15" we are setting to "30" to set "15" minutes each time slot...
                if (minutes! <= 15) {
                    dateComponents.minute = 15
                }
                else if (minutes! <= 30) {
                    dateComponents.minute = 30
                }
                else if minutes! <= 45 {
                    dateComponents.minute = 45
                }
                else
                {
                    dateComponents.minute = 60
                }
                
                intervalDate = gregorian.date(from: dateComponents)

            }
            else
            {
                dateFormatter.dateFormat = "HH:mm:ss"

                startDate = dateFormatter.date(from:aStrStartTime)
                endDate = dateFormatter.date(from:aStrEndTime)
                
                intervalDate = startDate
            }

            var dateComponents15Min = DateComponents()
            dateComponents15Min.minute = 15 //Set interval here
            
            dateFormatter.dateFormat = "hh:mm a"
            while(endDate?.compare(intervalDate!) == ComparisonResult.orderedDescending) {
                
                let dateAfter15Min =  gregorian.date(byAdding: dateComponents15Min, to: intervalDate!)
                
//                let aStrStartTime = Utility.sharedInstance.getStringFromDate("hh:mm a", date: intervalDate!)
//                let aStrEndTime = Utility.sharedInstance.getStringFromDate("hh:mm a", date: dateAfter15Min!)

                let aStrStartTime = dateFormatter.string(from: intervalDate!)
                let aStrEndTime = dateFormatter.string(from: dateAfter15Min!)

                print("Start Time : \(aStrStartTime) End Time : \(aStrEndTime)")
                intervalDate = dateAfter15Min!
                
                let aDictTimeSlot = [APPOINTMENT_START_TIME : aStrStartTime, APPOINTMENT_END_TIME : aStrEndTime]
                arrTimeSlot.append(aDictTimeSlot)
            }

        }
        
    }
    
    /// - returns: true if timeslot is reserved and false if timeslot is available...
    func checkTimeSlotIsReserved(_ dictTimeSlot : [String:String]) -> Bool {
        
        let arrFiltered = self.objAppointmentBookListModel?.data?.appointmentBookListing?.filter({
            $0.availabilityStartTime == dictTimeSlot[APPOINTMENT_START_TIME] && $0.availabilityEndTime == dictTimeSlot[APPOINTMENT_END_TIME]
        })
        
        return (arrFiltered!.count > 0) ? true : false
        
    }
    
    // MARK: - Webservice Methods
    
    func callWebServiceForAppointmentBookList(callSilently : Bool = false) {
        
        // Date Format 2017-05-21
        let aStrDate = Utility.sharedInstance.getStringFromDate(Constant.kDateFormat_YYYYMMDD, date: calendar.selectedDate)

        let userID = AppDelegate.getAppDelegate().userInfoData.usersId!
        
        let aDictParam:[String:Any] = ["date": aStrDate,"users_id":userID,"access_token":token]
        
        WebService.POST(Constant.API.kBaseURLWithAPIName("appointment_book_listing"), param: aDictParam, controller: self, callSilently:
            callSilently, successBlock: { (jsonResponse) in
                
            print("success response is received")
            
            self.objAppointmentBookListModel = nil
            self.objAppointmentBookListModel = AppointmentBookListModel(json: jsonResponse)
            if (self.objAppointmentBookListModel?.settings?.success == "1") {
                self.setUpUIAndData()
            }else if (self.objAppointmentBookListModel?.settings?.success == "100") {
                Utility.reloginUserWith(viewController: self)
            }else{
                UIAlertController.showAlertWithOkButton(self, aStrMessage: self.objAppointmentBookListModel?.settings?.message, completion: nil)
            }
        }) { (error, isTimeOut) in
            
            if isTimeOut {
                print("Request Timeout")
            } else {
                print(error?.localizedDescription ?? "")
            }
            
        }
        
    }

    func callWebServiceForBookAppointment() {
        
        let aStrUserId = AppDelegate.getAppDelegate().userInfoData.usersId!
        
        
        let aDictTimeSlot = arrTimeSlot[selectedTimeSlotIndex]
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "hh:mm a"
        
        let startTime = dateFormatter.date(from: aDictTimeSlot[APPOINTMENT_START_TIME]!)
        let endTime = dateFormatter.date(from: aDictTimeSlot[APPOINTMENT_END_TIME]!)
        
        dateFormatter.dateFormat = "HH:mm:ss"
        let aStrStartTime = dateFormatter.string(from: startTime!)
        let aStrEndTime = dateFormatter.string(from: endTime!)
        
        // Date Format 2017-05-21
        let aStrDate = Utility.sharedInstance.getStringFromDate(Constant.kDateFormat_YYYYMMDD, date: calendar.selectedDate)
        
        let aDictParam:[String:Any] = ["users_id": aStrUserId,"access_token":token, "available_date": aStrDate, "available_start_time": aStrStartTime, "available_end_time": aStrEndTime, "appointment_title":self.txtAppointmentTitle.text!]
        
        WebService.POST(Constant.API.kBaseURLWithAPIName("book_appointment"), param: aDictParam, controller: self, successBlock: { (jsonResponse) in
            
            print("success response is received")
            
            self.btnBookAppointment.isUserInteractionEnabled = true;
            
            self.objAppointmentBookListModel = nil
            self.objAppointmentBookListModel = AppointmentBookListModel(json: jsonResponse)
            if (self.objAppointmentBookListModel?.settings?.success == "1") {
                
                
                UIAlertController.showAlertWithOkButton(self, aStrMessage: "THANK YOU \n Your appointment has been successfully booked.", completion: { (index,_) in
                    
                    if (self.appointmentBookedCallback != nil) {
                        self.appointmentBookedCallback!()
                    }
                })
                
            }else if (self.objAppointmentBookListModel?.settings?.success == "100") {
                Utility.reloginUserWith(viewController: self)
            }else{
                UIAlertController.showAlertWithOkButton(self, aStrMessage: self.objAppointmentBookListModel?.settings?.message, completion: nil)
            } 
        }) { (error, isTimeOut) in
            
            self.btnBookAppointment.isUserInteractionEnabled = true;
            if isTimeOut {
                print("Request Timeout")
            } else {
                print(error?.localizedDescription ?? "")
            }
            
        }
        
    }

    // MARK: - Timer Methods
    func startTimer() {
        
        if timerRefresh == nil {
            timerRefresh =  Timer.scheduledTimer(
                timeInterval: TimeInterval(600),
                target      : self,
                selector    : #selector(refreshData),
                userInfo    : nil,
                repeats     : true)
        }
    }
    
    func stopTimer() {
        
        if timerRefresh != nil {
            timerRefresh?.invalidate()
            timerRefresh = nil
        }
    }
    
    func refreshData() {
        
        // Every 10 minutes get data from server...
        self.callWebServiceForAppointmentBookList(callSilently: true)
    }
    
    // MARK: - Other Methods
    func validationForBookAppointment() -> Bool {
        
        if self.txtAppointmentTitle.text!.isEmptyString() {
            UIAlertController.showAlertWithOkButton(self, aStrMessage: Constant.AlertMessage.kAlertMsgEnterAppointmentTitle, completion: nil)
            return false
        }
        else if selectedTimeSlotIndex < 0 {
            UIAlertController.showAlertWithOkButton(self, aStrMessage: Constant.AlertMessage.kAlertMsgSelectAppointmentTimeSlot, completion: nil)
            return false
        }
        return true

    }
}
