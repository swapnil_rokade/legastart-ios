//
//  QuesionariesCategoryVC.swift
//  LegalStart
//
//  Created by Adapting Social on 03/07/17.
//  Copyright © 2017 Adapting Social. All rights reserved.
//

import UIKit

class QuestionariesCategoryVC: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UIGestureRecognizerDelegate {

    @IBOutlet weak var questionsCategoryCollectionView: UICollectionView!
    let token = UserDefaults.value(forKey: ACCESSTOKEN) as! String
    let users_id = AppDelegate.getAppDelegate().userInfoData.usersId!
    var dataSource : QuestionCategoryListModel?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        self.callWSForGettingQuestionarieCategories()

        // Do any additional setup after loading the view.
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
    }
    @IBAction func btnSidePanelAction(_ sender: UIButton) {
        
        self.mm_drawerController.toggle(.left, animated: true) { (isClosed) in
            
        }
        
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if self.dataSource != nil {
            return self.dataSource!.data!.count + 1
        }
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell : QuesionnarieCategoryCell = collectionView.dequeueReusableCell(withReuseIdentifier: "QuesionnarieCategoryCell", for: indexPath) as! QuesionnarieCategoryCell
        if indexPath.row == self.dataSource!.data!.count {
            cell.queCategoryImgView.image = UIImage.init(named: "OtherServices")
            
        }else{
            if let data = self.dataSource?.data?[indexPath.row] {
                let url : URL = URL(string: data.image!)!
                cell.queCategoryImgView.sd_setImage(with: url)
                
            }
        
        }
        
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if indexPath.row == self.dataSource!.data!.count {
            
            AppDelegate.sharedInstance().selectedMenuIndex = 11
            let aStoryboard = UIStoryboard(name: "ServiceMaster", bundle: nil)
            let centerVC = aStoryboard.instantiateViewController(withIdentifier: "ServiceRequiredVC")
            
            self.mm_drawerController?.setCenterView(centerVC, withCloseAnimation: true, completion: nil)
            
        }else{
            let vc : QuestionarieCategoryDetailVC = self.storyboard?.instantiateViewController(withIdentifier: "QuestionarieCategoryDetailVC") as! QuestionarieCategoryDetailVC
            if let catID = self.dataSource?.data?[indexPath.row]{
                
                
                vc.categoryModel = catID
                
            }
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        
        
    }
    
    
    func callWSForGettingQuestionarieCategories()  {
        var params : [String:Any] = [:]
        params["users_id"] = users_id
        params["access_token"] = token
        
        WebService.GET(Constant.API.kBaseURLWithAPIName("question_category_list"), param: params, controller: self, callSilently: false, successBlock: { (jsonResponse) in
            if (jsonResponse["settings"]["success"] == "1") {
                
                self.dataSource = QuestionCategoryListModel(json: jsonResponse)
                self.questionsCategoryCollectionView.reloadData()
                
                
            }else if (jsonResponse["settings"]["success"] == "100") {
                Utility.reloginUserWith(viewController: self)
            }else{
                UIAlertController.showAlertWithOkButton(self, aStrMessage: jsonResponse["settings"]["message"].string, completion: nil)
            }
        }) { (error, isTimeOut) in
            
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        
        return CGSize(width: UIScreen.main.bounds.size.width, height: (UIScreen.main.bounds.size.height-64) / 3)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
