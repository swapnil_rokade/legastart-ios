//
//  QuestionarieCategoryDetailVC.swift
//  LegalStart
//
//  Created by Adapting Social on 11/08/17.
//  Copyright © 2017 Adapting Social. All rights reserved.
//

import UIKit

class QuestionarieCategoryDetailVC: UIViewController {

    
    var categoryModel : QuestionCategoryListData?
    @IBOutlet weak var categoryDetailWebView: UIWebView!
    @IBOutlet weak var lblScreenTitle: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.lblScreenTitle.text = categoryModel?.categoryName
        let newStr =  categoryModel!.descriptionValue!

        
        self.categoryDetailWebView.loadHTMLString("<html><title></title><body>\(newStr)</body></html>", baseURL: nil)
    }
    @IBAction func btnBackAction(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnNextAction(_ sender: UIButton) {
        
        let vc : QuestionarieCategoryAmountListVC = self.storyboard?.instantiateViewController(withIdentifier: "QuestionarieCategoryAmountListVC") as! QuestionarieCategoryAmountListVC
        vc.categoryModel = categoryModel
        self.navigationController?.pushViewController(vc, animated: true)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    

}
