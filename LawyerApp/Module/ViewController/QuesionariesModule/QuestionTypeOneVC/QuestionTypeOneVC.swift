//
//  QuestionTypeOneVC.swift
//  LegalStart
//
//  Created by Adapting Social on 03/07/17.
//  Copyright © 2017 Adapting Social. All rights reserved.
//

import UIKit

class QuestionTypeOneVC: UIViewController,UITextViewDelegate {

    @IBOutlet weak var txtViewLongAnswer: UITextView!
    var currentIndex : Int?
    var longAnswerSubmited : (String,Int) -> () = {_ in}
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    //MARK: TEXTVIEW DELEGATE
    func textViewDidChange(_ textView: UITextView) {
        if textView.text == "\n" {
            textView.text = ""
            textView.resignFirstResponder()
        }
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "Type here" {
            textView.text = ""
            textView.textColor = UIColor.colorFromCode(0x9b9b9b)
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Type here"
            textView.textColor = UIColor.colorFromCode(0xC7C7CD)
        }
        self.longAnswerSubmited(txtViewLongAnswer.text, currentIndex!)
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    

}
