//
//  QuestionTypeThreeVC.swift
//  LegalStart
//
//  Created by Adapting Social on 04/07/17.
//  Copyright © 2017 Adapting Social. All rights reserved.
//

import UIKit

class QuestionTypeThreeVC: UIViewController,iCheckboxDelegate,UITextViewDelegate {

    @IBOutlet weak var checkBoxView: UIView!
    @IBOutlet weak var checkBoxScrollView: UIScrollView!

    @IBOutlet weak var txtViewOtherOptionAnswer: UITextView!
    @IBOutlet weak var otherOptionHeightConstraint: NSLayoutConstraint!
    var dataSource : QuestionListData?
    var optionListdataSource : [QuestionListOptionListing] = []
    var singleOptionAnswerSubmitted : (String,Int) -> () = {_ in}
    let checkboxBuilderConfig = iCheckboxBuilderConfig()
    var arrSelectedAnswers : [[String:Any]] = []
    var currentIndex : Int?
    var multipleSelectionAnswerSubmition : ([[String:String]],Int) -> () = {_ in}
    var otherOptionIndexForMultiChoice : Int?
    var isOtherSelectedInMultipleChoise : Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if dataSource != nil {
            self.optionListdataSource = dataSource!.optionListing!
            
            self.checkboxBuilderConfig.checkboxSize = CGSize(width: checkBoxView.frame.size.width, height: 0)
            txtViewOtherOptionAnswer.delegate = self
            otherOptionHeightConstraint.constant = 0
            self.checkboxBuilderConfig.startPosition = CGPoint(x: 0, y: 0)
            if checkboxBuilderConfig.selection == .Multiple {
                for title in optionListdataSource {
                    var tempDict : [String:Any] = [:]
                    tempDict["title"] = title.questionOption
                    tempDict["isSelected"] = false
                    arrSelectedAnswers.append(tempDict)
                }
                var selectedAnswers : [[String:String]] = []
                var tempDict : [String:String] = [:]
                tempDict["question_option"] = ""
                selectedAnswers.append(tempDict)
                self.multipleSelectionAnswerSubmition([tempDict], currentIndex!)
            }
            
            self.addCheckboxes()

        }
               // Do any additional setup after loading the view.
    }
    func textViewDidEndEditing(_ textView: UITextView) {

        if checkboxBuilderConfig.selection == .Multiple {
            self.arrSelectedAnswers[otherOptionIndexForMultiChoice!]["title"] = textView.text
            
            var selectedAnswers : [[String:String]] = []
            
            for data in  self.arrSelectedAnswers {
                if let value = data["isSelected"] as? Bool {
                    if value  {
                        var tempDict : [String:String] = [:]
                        tempDict["question_option"] = data["title"] as? String
                        selectedAnswers.append(tempDict)
                        
                    }
                }
            }
            
            self.multipleSelectionAnswerSubmition(selectedAnswers, currentIndex!)
        }else{
            
            self.singleOptionAnswerSubmitted(textView.text, currentIndex!)
        }

    }
    
    func addCheckboxes() {
        
         let checkboxBuilder = iCheckboxBuilder(withCanvas: self.checkBoxView, canvasScrollView: self.checkBoxScrollView, andConfig: checkboxBuilderConfig)
        checkboxBuilder.delegate = self

        var arrOfBoxes : [iCheckboxState] = []
        for data in optionListdataSource {
            let box = iCheckboxState()
            box.title = data.questionOption!
            
            arrOfBoxes.append(box)
        }
        checkboxBuilder.addCheckboxes(withStates: arrOfBoxes)
        
    }
    
    func didSelectCheckbox(withState state: Bool, identifier: Int, andTitle title: String) {
        print("Checkbox '\(title)', has selected state: \(state), has id \(identifier)")
        self.view.endEditing(true)
        if checkboxBuilderConfig.selection == .Single {
            
            if (title == "Other" || dataSource?.textfieldOpen == title)  && state == true {
                otherOptionHeightConstraint.constant = 75
                self.txtViewOtherOptionAnswer.text = ""
                self.singleOptionAnswerSubmitted("", currentIndex!)
            }else{
                otherOptionHeightConstraint.constant = 0
                self.singleOptionAnswerSubmitted(title, currentIndex!)
            }
            
            
        }else{
            
            if title == "Other" {
                
                if !isOtherSelectedInMultipleChoise {
                    self.txtViewOtherOptionAnswer.text = ""
                    otherOptionHeightConstraint.constant = 75
                    isOtherSelectedInMultipleChoise = true
                    self.otherOptionIndexForMultiChoice = identifier
                }else{
                    
                    otherOptionHeightConstraint.constant = 0
                    isOtherSelectedInMultipleChoise = false
                    self.otherOptionIndexForMultiChoice = nil
                }
                
                
                self.arrSelectedAnswers[identifier]["isSelected"] = state
                self.arrSelectedAnswers[identifier]["title"] = ""
                var selectedAnswers : [[String:String]] = []
                
                for data in  self.arrSelectedAnswers {
                    if let value = data["isSelected"] as? Bool {
                        if value  {
                            var tempDict : [String:String] = [:]
                            tempDict["question_option"] = data["title"] as? String
                            selectedAnswers.append(tempDict)
                            
                        }
                    }
                }
                
                self.multipleSelectionAnswerSubmition(selectedAnswers, currentIndex!)
                
                
            }else{
                
                if isOtherSelectedInMultipleChoise{
                    otherOptionHeightConstraint.constant = 75
                    isOtherSelectedInMultipleChoise = true
                    
                }else{
                    otherOptionHeightConstraint.constant = 0
                    isOtherSelectedInMultipleChoise = false
                    
                    
                }
                
                
                self.arrSelectedAnswers[identifier]["isSelected"] = state
                self.arrSelectedAnswers[identifier]["title"] = title
                var selectedAnswers : [[String:String]] = []
                
                for data in  self.arrSelectedAnswers {
                    if let value = data["isSelected"] as? Bool {
                        if value  {
                            var tempDict : [String:String] = [:]
                            tempDict["question_option"] = data["title"] as? String
                            selectedAnswers.append(tempDict)
                            
                        }
                    }
                }
                
                self.multipleSelectionAnswerSubmition(selectedAnswers, currentIndex!)
                
                
            }
            
            
            
            
        }

    
        
  
        
    }

    

}
