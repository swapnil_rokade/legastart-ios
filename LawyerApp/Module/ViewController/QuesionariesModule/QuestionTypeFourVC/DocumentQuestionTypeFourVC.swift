//
//  DocumentQuestionTypeFourVC.swift
//  LegalStart
//
//  Created by Adapting Social on 07/07/17.
//  Copyright © 2017 Adapting Social. All rights reserved.
//

import UIKit

class DocumentQuestionTypeFourVC: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,UITextFieldDelegate {

    @IBOutlet weak var viewAddAnother: UIView!
    @IBOutlet weak var mainCollectionView: UICollectionView!
    var canAddAnother : Bool = true
    var dataSourceTitles : [DocumentQuestionListOptionListing] = []
    
    var arrOfAnswer : [[String:String]] = []
    var currentIndex : Int?
    var multipleAnswersSubmitted : ([[String:String]]?,Int?) -> () = {_ in}
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if canAddAnother {
            viewAddAnother.isHidden = false
        }else{
            viewAddAnother.isHidden = true
        }
        
        self.addAnotherOptions()
        
        
        // Do any additional setup after loading the view.
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return arrOfAnswer.count
        
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        
        return dataSourceTitles.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell : QuestionTypeFourCell = collectionView.dequeueReusableCell(withReuseIdentifier: "QuestionTypeFourCell", for: indexPath) as! QuestionTypeFourCell
        
        
        let key : String = dataSourceTitles[indexPath.row].questionOption!
        cell.lblOptionTitle.text = key
        cell.txtFieldAnswer.text = arrOfAnswer[indexPath.section][key]
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
    
        return CGSize(width: UIScreen.main.bounds.size.width / 2, height: 110)
    }
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        let view : QuestionTypeFourDeleteCell = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionFooter, withReuseIdentifier: "QuestionTypeFourDeleteCell", for: indexPath) as! QuestionTypeFourDeleteCell
        if indexPath.section == 0  {
            view.btnDelete.isHidden = true
        }else{
            view.btnDelete.isHidden = false
            view.btnDelete.tag = indexPath.section
            
        }
        
        return view
        
    }
    
    @IBAction func btnAddAnotherAction(_ sender: UIButton) {
        
        
        self.addAnotherOptions()
        
    }
    @IBAction func btnDeleteAction(_ sender: UIButton) {
        self.view.endEditing(true)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            
            DispatchQueue.main.async {
                self.arrOfAnswer.remove(at: sender.tag)
                self.multipleAnswersSubmitted(self.arrOfAnswer, self.currentIndex!)
                self.mainCollectionView.reloadData()
                self.scrollAtLastOption()
            }
        }
        
        
    }

    func addAnotherOptions() {
        
        self.mainCollectionView.reloadData()
        
        var tempDict : [String:String] = [:]
        for title in dataSourceTitles {
            
            tempDict[title.questionOption!] = ""
        }
        self.arrOfAnswer.append(tempDict)
        self.multipleAnswersSubmitted(self.arrOfAnswer, currentIndex!)
        self.scrollAtLastOption()
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if let point = textField.superview?.superview?.convert(textField.frame.origin, to: self.mainCollectionView){
            if let indexPath = self.mainCollectionView.indexPathForItem(at: point){
                print("ROW : \(indexPath.row),SECTION :\(indexPath.section)")
                
                if let cell = self.mainCollectionView.cellForItem(at: indexPath) as? QuestionTypeFourCell{
                    self.arrOfAnswer[indexPath.section][cell.lblOptionTitle.text!] = cell.txtFieldAnswer.text
                    self.multipleAnswersSubmitted(self.arrOfAnswer, currentIndex!)
                }
                
                
            }
        }
 
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    func scrollAtLastOption() {
        let indexPath : IndexPath = IndexPath(item: 0, section: self.arrOfAnswer.count-1)
        self.mainCollectionView.scrollToItem(at: indexPath, at: .centeredVertically, animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    

   

}
