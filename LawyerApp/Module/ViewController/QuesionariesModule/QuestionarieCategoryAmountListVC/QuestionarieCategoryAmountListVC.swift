//
//  QuestionarieCategoryAmountListVC.swift
//  LegalStart
//
//  Created by Adapting Social on 11/08/17.
//  Copyright © 2017 Adapting Social. All rights reserved.
//

import UIKit

class QuestionarieCategoryAmountListVC: UIViewController,UITableViewDelegate,UITableViewDataSource,iCheckboxDelegate {

    @IBOutlet weak var tblViewAmountList: UITableView!
    @IBOutlet weak var lblScreenTitle: UILabel!
    let token = UserDefaults.value(forKey: ACCESSTOKEN) as! String
    let users_id = AppDelegate.getAppDelegate().userInfoData.usersId!
    var stateListModal : QuestionarieStateListModel?
    var categoryModel : QuestionCategoryListData?
    var delawareAmountAddionalOptionSelected : Bool = true
    override func viewDidLoad() {
        super.viewDidLoad()

        self.lblScreenTitle.text = categoryModel?.categoryName
        self.tblViewAmountList.estimatedRowHeight = 190
        self.tblViewAmountList.rowHeight = UITableViewAutomaticDimension
        self.callWSToGetAmountList()
        // Do any additional setup after loading the view.
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if stateListModal != nil {
            
            return stateListModal!.data!.count
        }
        
        return 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : QuestionarieAmountListVCCell = tableView.dequeueReusableCell(withIdentifier: "QuestionarieAmountListVCCell") as! QuestionarieAmountListVCCell
        
        
        if let data = self.stateListModal!.data?[indexPath.row] {
            cell.lblStateTitle.text = data.stateName
            cell.lblCategoryTitle.text = self.categoryModel?.categoryName
            cell.lblAmount.text = "$\(data.amount!)"
            
            
            if data.otherInformation == "" {
            
                cell.checkBoxViewHeightConstraint.constant = 0
            }else{
                cell.checkBoxViewHeightConstraint.constant = 75
                
                cell.checkboxBuilder?.delegate = self
                cell.box.title = "24 hour processing - $\(data.otherInformation!) additional"
                cell.box.selected = true
                cell.arrOfBoxes.append(cell.box)
                cell.box2.title = "Standard processing (2-3 Weeks)"
                
                cell.arrOfBoxes.append(cell.box2)
                cell.checkboxBuilder?.addCheckboxes(withStates: cell.arrOfBoxes)
                
            }
            
            
            
        }
        
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let data = self.stateListModal!.data?[indexPath.row] {
            let vc : QuestionsHandlerVC = self.storyboard?.instantiateViewController(withIdentifier: "QuestionsHandlerVC") as! QuestionsHandlerVC
            
        
            let basicAmount =  Float(data.amount!)!
            
            if data.stateName == "Delaware" && delawareAmountAddionalOptionSelected == true {
                
                
                let extraCharges = Float(data.otherInformation!)!
                vc.finalAmountToPay = basicAmount+extraCharges
                
            }else{
                
                vc.finalAmountToPay = basicAmount
                
                
            
            }
            vc.categoryModel = self.categoryModel
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func didSelectCheckbox(withState state: Bool, identifier: Int, andTitle title: String) {
        if identifier == 0 && state == true{
        
            self.delawareAmountAddionalOptionSelected = true
            
        }else{
        
            self.delawareAmountAddionalOptionSelected = false
        }
    }
    @IBAction func btnBackAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }

    func callWSToGetAmountList()  {

        var params : [String:Any] = [:]
        params["users_id"] = users_id
        params["access_token"] = token
        params["category_name"] = self.categoryModel?.categoryName
        WebService.GET(Constant.API.kBaseURLWithAPIName("start_my_business_state_listing"), param: params, controller: self, callSilently: false, successBlock: { (jsonResponse) in
            
            
            if (jsonResponse["settings"]["success"].string == "1"){
                
                self.stateListModal = QuestionarieStateListModel(json: jsonResponse)
                
                self.tblViewAmountList.reloadData()
                
            }else if (jsonResponse["settings"]["success"].string == "0"){
                
                UIAlertController.showAlertWithOkButton(self, aStrMessage: jsonResponse["settings"]["message"].string, completion: nil)
                
            }else if (jsonResponse["settings"]["success"].string == "100"){
                
                
            }else{
                
                UIAlertController.showAlertWithOkButton(self, aStrMessage: Constant.AlertMessage.kAlertMsgSomeThingWentWrong, completion: nil)
            }
            

            
        }) { (error, isTimeOut) in
            
        }
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
