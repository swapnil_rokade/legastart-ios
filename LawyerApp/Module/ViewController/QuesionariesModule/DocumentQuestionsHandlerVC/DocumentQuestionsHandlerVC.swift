//
//  DocumentQuestionsHandlerVC.swift
//  LegalStart
//
//  Created by Adapting Social on 07/07/17.
//  Copyright © 2017 Adapting Social. All rights reserved.
//

import UIKit
class DocumentQuestionsHandlerVC: UIViewController {

    
    @IBOutlet weak var lblScreenTitle: UILabel!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var btnPrevious: UIButton!
    @IBOutlet weak var lblTotalQuestionNumber: UILabel!
    @IBOutlet weak var lblCurrentQuestionNumber: UILabel!
    @IBOutlet weak var lblQuestionNumber: UILabel!
    @IBOutlet weak var lblQuestion: UILabel!
    @IBOutlet weak var mainContainerHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var mainContainerView: UIView!
    var lastViewController : UIViewController?
    let token = UserDefaults.value(forKey: ACCESSTOKEN) as! String
    let users_id = AppDelegate.getAppDelegate().userInfoData.usersId!
    var document : DocumentListingData?
    var dataSource : DocumentQuestionListModel?
    var currentIndex : Int = 0
    var arrOfAnswer : [[String:Any]] = []
    var masterBrowser : MasterWebBrowser?
    var shouldReloadDocuments : (Bool) -> () = {_ in}
    override func viewDidLoad() {
        super.viewDidLoad()

        if document?.documentType == "Paid" || document?.usersDocumentStatus == "1"{
        

        }else{
            
            
            self.btnSubmit.layer.cornerRadius = 25
            self.btnSubmit.setImage(nil, for: .normal)
            self.btnSubmit.setTitle("SUBMIT", for: .normal)
            self.btnSubmit.backgroundColor = UIColor.colorFromCode(0x2EB863)
            self.btnSubmit.frame.size = CGSize.init(width: 100, height: 50)
            
        
        }
        
        
        self.lblScreenTitle.text = document?.documentName
        callWSToGetQuestionList()
        // Do any additional setup after loading the view.
    }

    //MARK: BUTTON ACTION
    @IBAction func btnBackAction(_ sender: UIButton) {
        shouldReloadDocuments(false)
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnNextAction(_ sender: UIButton) {
        self.view.endEditing(true)
        
        if validation(withIndex: currentIndex) {
            if self.dataSource != nil {
                if currentIndex < self.dataSource!.data!.count-1 {
                    self.currentIndex += 1
                    self.changeQuestionWithCode(currentIndex: self.currentIndex)
                    
                }
            }
        }
    }
    @IBAction func btnPreviousAction(_ sender: UIButton) {
        self.view.endEditing(true)
//        if validation(withIndex: currentIndex) {
            if self.dataSource != nil {
                if currentIndex <= self.dataSource!.data!.count-1 {
                    self.currentIndex -= 1
                    self.changeQuestionWithCode(currentIndex: self.currentIndex)
                    
                }
            }
//        }
        
    }
    @IBAction func btnSubmitAction(_ sender: UIButton) {
        self.view.endEditing(true)
        print(self.arrOfAnswer)
        
        if self.validation(withIndex: currentIndex) {
            
            if document?.documentType == "Paid"{
            
//                UIAlertController.showAlert(self, aStrMessage: "Select Payment option", style: .actionSheet, aCancelBtn: "Cancel", aDistrutiveBtn: nil, otherButtonArr: ["Credit Card"], completion: { (index, title) in
//                    
////                    if title == "Paypal"{
////                        
////                        self.paymentWithPayPal(data: self.document!)
////                    }else
//                        if title == "Credit Card"{
//                        
//              self.paymentWithStripe(data: self.document!)
////                        self.paymentWithAuthorizeDotNet(data: self.document!)
//                    }
//                    
//                    
//                })
                self.paymentWithStripe(data: self.document!)
                
            }else{
                if document?.usersDocumentStatus == "0" {
                    SVProgressHUD.show()
                    var param : [String:Any] = [:]
                    param["users_id"] = users_id
                    param["document_id"] = document?.documentId
                    WebService.GET(Constant.API.kBaseURLWithAPIName("free_document"), param: param, controller: self, callSilently: true, successBlock: { (jsonResponse) in
                        
                        if (jsonResponse["settings"]["success"].string == "1"){
                            
                            let id : String = jsonResponse["data"][0]["insert_id"].string!
                            self.callWSToSendAnswers(paymentID: id)
                            
                        }else if (jsonResponse["settings"]["success"].string == "0"){
                            
                            UIAlertController.showAlertWithOkButton(self, aStrMessage: jsonResponse["settings"]["message"].string, completion: nil)
                            
                        }else if (jsonResponse["settings"]["success"].string == "100"){
                            
                        }else{
                            
                            UIAlertController.showAlertWithOkButton(self, aStrMessage: Constant.AlertMessage.kAlertMsgSomeThingWentWrong, completion: nil)
                        }
                        
                        
                        
                    }, failureBlock: { (error, isTimeOut) in
                        
                    })
                    
                    
                }else{
                
//                    UIAlertController.showAlert(self, aStrMessage: "Select Payment option", style: .actionSheet, aCancelBtn: "Cancel", aDistrutiveBtn: nil, otherButtonArr: ["Credit Card"], completion: { (index, title) in
//                        
////                        if title == "Paypal"{
////                            
////                            self.paymentWithPayPal(data: self.document!)
//////                        }else
//                            if title == "Credit Card"{
                    
                            self.paymentWithStripe(data: self.document!)
//                            self.paymentWithAuthorizeDotNet(data: self.document!)
//                        }
                    
                        
//                        
//                    })
//                
                
                }
            
            }
            
            
            
        }
        
    }
    //MARK: PAYMENT
    
    func paymentWithPayPal(data : DocumentListingData) {
        let url : String = "\(Constant.API.kBaseURLWithAPIName("paypal_payment"))?document_name=\(data.documentName!)&amount=\(data.amount!)&users_id=\(self.users_id)&document_id=\(data.documentId!)&access_token=\(self.token)"
        
        self.masterBrowser = MasterWebBrowser(nibName: "MasterWebBrowser", bundle: nil)
        self.masterBrowser?.urlToLoad = url
        self.masterBrowser?.paymentDidFinish = {
            success,jsonResponse in
            if success {
                SVProgressHUD.show()
                self.callWSToSendAnswers(paymentID: jsonResponse["data"][0]["document_payment_id"].string!)
            }else{
                
                UIAlertController.showAlertWithOkButton(self, aStrMessage: "Payment cancel please try again later", completion: nil)
                
            }
            
        }
        self.navigationController?.present(self.masterBrowser!, animated: true, completion: nil)
    }
    func paymentWithStripe(data : DocumentListingData) {
        
//        let stripeVC : StripePaymentViewController = StripePaymentViewController(nibName: "StripePaymentViewController", bundle: nil)
//        stripeVC.amount = Float(data.amount!)
//        let newNav : UINavigationController = UINavigationController(rootViewController: stripeVC)
//        newNav.navigationBar.isHidden = true
//        self.navigationController?.present(newNav, animated: true, completion: nil)
//        stripeVC.didFinishStripePayment = {
//            
//            stripController,token,error in
//            if error == nil{
//                SVProgressHUD.show()
        
        
        
        
        
//                self.callWSToStripePayWith(token: token!, document_id: data.documentId!, amount: data.amount!)
        
                
//            }else{
//                
//                UIAlertController.showAlertWithOkButton(self, aStrMessage: "Payment cancel please try again later", completion: nil)
//                
//            }
//            
//        }
        

        
        
        
        LawyerAppStripePaymentAPI.sharedInstance.presentStripPaymentVC(viewController: self, amount:data.amount!, completionHandler: { (stripToken, status,error) in
            
            
            if stripToken != nil{
                
                
                self.callWSToStripePayWith(token: stripToken!, document_id: data.documentId!, amount: data.amount!)
          
            }else{
                
                if status == .error {
                    UIAlertController.showAlertWithOkButton(self, aStrMessage: Constant.AlertMessage.kAlertMsgNotAbleToCompletePayment, completion: nil)
                
                }
                
                
                
                
                
            }
            
        })
        
    }
//    func paymentWithAuthorizeDotNet(data : DocumentListingData)  {
//        let vc : AuthorizePaymentVC = AuthorizePaymentVC(nibName: "AuthorizePaymentVC", bundle: nil)
//        vc.shouldPayWithFramework = true
//        vc.strSubscriptionAmount = data.amount
//        vc.strItemId = "1"
//        vc.strSubscriptionPackageName = self.document?.documentName
//        vc.subscriptionSuccessCallBack = {
//            response in
//            
//            self.callWSToSendTransactionID(transactionID: response!.transactionResponse.transId, amount: data.amount!)
//        }
//        self.present(vc, animated: true, completion: nil)
//        
//        
//        
//    }
    //MARK: WEB-SERVICE
    func callWSToSendTransactionID(transactionID : String,amount : String) {
//http://php54.Adapting Social.com/lawyerapp/WS/document_authorise_net_payment?users_id=1&document_id=1&amount=12&payment_id=123
        var params : [String:Any] = [:]
        params["document_id"] = self.document?.documentId
        params["users_id"] = users_id
        params["payment_id"] = transactionID
        params["amount"] = amount
        WebService.POST(Constant.API.kBaseURLWithAPIName("document_authorise_net_payment"), param: params, controller: self, successBlock: { (jsonResponse) in
            
            
            if (jsonResponse["settings"]["success"].string == "1"){
                
                //                UIAlertController.showAlertWithOkButton(self, aStrMessage: "Payment successfull accepted", completion: { (index, title) in
                //                    //                    self.moveToDashBoard()
                //                })
                self.callWSToSendAnswers(paymentID: jsonResponse["data"][0]["document_payment_id"].string!)
                
            }else if (jsonResponse["settings"]["success"].string == "0"){
                
                UIAlertController.showAlertWithOkButton(self, aStrMessage: jsonResponse["settings"]["message"].string, completion: nil)
                
            }else if (jsonResponse["settings"]["success"].string == "100"){
                
            }else{
                
                UIAlertController.showAlertWithOkButton(self, aStrMessage: Constant.AlertMessage.kAlertMsgSomeThingWentWrong, completion: nil)
            }
            
            
            
        }) { (error, isTimeOut) in
            
        }
    }
    func callWSToSendAnswers(paymentID : String)  {
        var param : [String:Any] = [:]
        param["document_id"] = self.document?.documentId
        param["document_payment_id"] = paymentID
        
        var answerToSend : [[String:Any]] = []
        for data in self.arrOfAnswer {
            
            var tempData : [String:Any] = data
            tempData.removeValue(forKey: "viewController")
            
            if (data["flow_finish"] as! Bool) == true {
                tempData.removeValue(forKey: "flow_finish")
                answerToSend.append(tempData)
                break;
            }else{
                tempData.removeValue(forKey: "flow_finish")
                answerToSend.append(tempData)
            }
            
        }
        param["question_answer"] = Utility.convertObjectToJson(from: answerToSend)
        print(param)
        WebService.POST(Constant.API.kBaseURLWithAPIName("document_question_answer_by_users"), param: param, controller: self,callSilently : true,successBlock: { (jsonResponse2) in
            SVProgressHUD.dismiss()
            if jsonResponse2["settings"]["success"].string == "1"{
                UIAlertController.showAlertWithOkButton(self, aStrMessage: "Congratulations! your answers have been successfully submitted", completion: { (index, title) in
                    self.shouldReloadDocuments(true)
                    self.navigationController?.popViewController(animated: true)
                })
                
            }else if jsonResponse2["settings"]["success"].string == "0"{
                
                
                Utility.reloginUserWith(viewController: self)
                
                
            }else{
                
                UIAlertController.showAlertWithOkButton(self, aStrMessage: jsonResponse2["settings"]["message"].string, completion: nil)
                
            }
            
            
            
        }, failureBlock: { (error, isTimeOut) in
            SVProgressHUD.dismiss()
            
        })
    }
    

    func callWSToStripePayWith(token : String,document_id : String,amount : String)  {
        
        var params : [String:Any] = [:]
        params["token"] = token
        params["users_id"] = users_id
        params["access_token"] = self.token
        params["document_id"] = document_id
        let amountValue : Float = Float(amount)! * 100
        params["amount"] = String(Int(amountValue))
        WebService.POST(Constant.API.kBaseURLWithAPIName("stripe_charge_document"), param: params, controller: self,callSilently: true ,successBlock: { (jsonResponse) in
            print(jsonResponse)
            SVProgressHUD.dismiss()
            if jsonResponse["settings"]["success"].string == "1"{
                
        
                self.callWSToSendAnswers(paymentID: jsonResponse["data"][0]["document_payment_id"].string!)
                
            }else if jsonResponse["settings"]["success"].string == "100"{
                
                Utility.reloginUserWith(viewController: self)
                
            }else{
                
                UIAlertController.showAlertWithOkButton(self, aStrMessage: jsonResponse["settings"]["message"].string, completion: nil)
                
            }
        }) { (error, isTimeOut) in
            SVProgressHUD.dismiss()
        }
    }
    
    func callWSToGetQuestionList()  {
        var params : [String:Any] = [:]
        params["users_id"] = users_id
        params["access_token"] = token
        params["document_id"] = document?.documentId
        WebService.GET(Constant.API.kBaseURLWithAPIName("document_question_listing"), param: params, controller: self, callSilently: false, successBlock: { (jsonResponse) in
            
            if (jsonResponse["settings"]["success"].string == "1"){
                
                self.dataSource = DocumentQuestionListModel(json: jsonResponse)
                if (self.dataSource?.data?.count)! > 0{
                    
                    for data in self.dataSource!.data!{
                        
                        if let vc = self.getViewControllerWithCode(code: data.questionTypeCode!){
                            
                            var tempDict : [String:Any] = [:]
                            tempDict["type"] = data.questionTypeCode
                            tempDict["question_id"] = data.documentQuestionId
                            tempDict["answer"] = ""
                            tempDict["users_id"] = self.users_id
                            tempDict["viewController"] = vc
                            tempDict["flow_finish"] = false
                            self.arrOfAnswer.append(tempDict)
                        }
                    }
                    self.lblTotalQuestionNumber.text = "\(self.dataSource!.data!.count)"
                    self.changeQuestionWithCode(currentIndex: self.currentIndex)
                }
                
            }else if (jsonResponse["settings"]["success"].string == "0"){
                
                UIAlertController.showAlertWithOkButton(self, aStrMessage: jsonResponse["settings"]["message"].string, completion: { (index, title) in
                    self.navigationController?.popViewController(animated: true)
                })
                
            }else if (jsonResponse["settings"]["success"].string == "100"){
                Utility.reloginUserWith(viewController: self)
            }else{
                UIAlertController.showAlertWithOkButton(self, aStrMessage: Constant.AlertMessage.kAlertMsgSomeThingWentWrong, completion: { (index, title) in
                    self.navigationController?.popViewController(animated: true)
                })
                
            }
            
            
            
        }) { (error, isTimeOut) in
            
        }
    }
    
    
    //MARK: UTILITY
    func changeQuestionWithCode(currentIndex : Int) {
        
        if let data = self.dataSource?.data?[currentIndex] {
            lblQuestion.text = data.questionName
            var newIndex = currentIndex
            newIndex += 1
            lblQuestionNumber.text = "\(String(newIndex))."
            lblCurrentQuestionNumber.text = String(newIndex)
            
            
            if data.flowFinish!.characters.count > 0 && (data.flowFinish == (self.arrOfAnswer[currentIndex]["answer"] as! String)){
                
                self.arrOfAnswer[currentIndex]["flow_finish"] = true
                if currentIndex == 0 {
                    self.btnPrevious.isHidden = true
                    self.btnNext.isHidden = true
                    self.btnSubmit.isHidden = false
                }else{
                    self.btnPrevious.isHidden = false
                    self.btnNext.isHidden = true
                    self.btnSubmit.isHidden = false
                }
                
                
            }else{
                self.arrOfAnswer[currentIndex]["flow_finish"] = false
                if currentIndex != 0 && currentIndex < self.dataSource!.data!.count-1 {
                    self.btnPrevious.isHidden = false
                    self.btnNext.isHidden = false
                    self.btnSubmit.isHidden = true
                    
                }else if currentIndex == 0{
                    
                    self.btnPrevious.isHidden = true
                    self.btnNext.isHidden = false
                    self.btnSubmit.isHidden = true
                    
                }else if currentIndex == self.dataSource!.data!.count-1 {
                    
                    self.btnPrevious.isHidden = false
                    self.btnNext.isHidden = true
                    self.btnSubmit.isHidden = false
                    
                }
                
            }
            
            
            if let vc =  self.arrOfAnswer[currentIndex]["viewController"] as? UIViewController{
                
                if let firstVC = vc as? QuestionTypeOneVC,data.questionTypeCode == "LongAnswerQuestion" {
                    firstVC.currentIndex = currentIndex
                    firstVC.longAnswerSubmited = {
                        answer,index in
                        self.arrOfAnswer[index]["answer"] = answer
                    }
                    
                    self.changeViewController(viewController: firstVC)
                    
                }else if let secondVC = vc as? QuestionTypeTwoVC,data.questionTypeCode == "ShortAnswerQuestion" {
                    secondVC.currentIndex = currentIndex
                    secondVC.shortAnswerSubmitted = {
                        
                        answer,index in
                        self.arrOfAnswer[index]["answer"] = answer
                        
                    }
                    
                    self.changeViewController(viewController: secondVC)
                }else if let thirdMultiVC = vc as? DocumentQuestionTypeThreeVC,data.questionTypeCode == "Multiselect" {
                    
                    thirdMultiVC.currentIndex = currentIndex

                    thirdMultiVC.multipleSelectionAnswerSubmition = {
                    
                        answer,index in
                        
                        self.arrOfAnswer[index]["answer"] = answer
                    
                    }
                    
                    
                    thirdMultiVC.checkboxBuilderConfig.imageNameForNormalState = "unselected"
                    thirdMultiVC.checkboxBuilderConfig.imageNameForSelectedState = "selected"
                    thirdMultiVC.checkboxBuilderConfig.titleColorForSelectedState = UIColor.colorFromCode(0x2EB863)
                    thirdMultiVC.checkboxBuilderConfig.selection = .Multiple
                    thirdMultiVC.checkboxBuilderConfig.headerTitle = ""
                    thirdMultiVC.checkboxBuilderConfig.style = .OneColumn
                    thirdMultiVC.checkboxBuilderConfig.borderStyle = .None
                    
                    
                    thirdMultiVC.documentQuestionListModel = data
                    
                    
                    self.changeViewController(viewController: thirdMultiVC)
                    
                }else if let thirdSingleVC = vc as? DocumentQuestionTypeThreeVC,data.questionTypeCode == "SingleChoice" {
                    
                    thirdSingleVC.currentIndex = currentIndex

                    thirdSingleVC.singleSelectionAnswerSubmition = {
                    
                        answer,index in
                        var flow_finish = false
                        if answer == data.flowFinish && answer.characters.count > 0 {
                            flow_finish = true
                            if currentIndex == 0 {
                                self.btnPrevious.isHidden = true
                                self.btnNext.isHidden = true
                                self.btnSubmit.isHidden = false
                            }else{
                                self.btnPrevious.isHidden = false
                                self.btnNext.isHidden = true
                                self.btnSubmit.isHidden = false
                            }
                        }else{
                            flow_finish = false
                            if currentIndex != 0 && currentIndex < self.dataSource!.data!.count-1 {
                                self.btnPrevious.isHidden = false
                                self.btnNext.isHidden = false
                                self.btnSubmit.isHidden = true
                                
                            }else if currentIndex == 0{
                                
                                self.btnPrevious.isHidden = true
                                self.btnNext.isHidden = false
                                self.btnSubmit.isHidden = true
                                
                            }else if currentIndex == self.dataSource!.data!.count-1{
                                
                                self.btnPrevious.isHidden = false
                                self.btnNext.isHidden = true
                                self.btnSubmit.isHidden = false
                                
                            }
                            
                        }
                        self.arrOfAnswer[index]["flow_finish"] = flow_finish
                        self.arrOfAnswer[index]["answer"] = answer
                    
                    }
                    
                    
                    thirdSingleVC.checkboxBuilderConfig.imageNameForNormalState = "uncheckk"
                    thirdSingleVC.checkboxBuilderConfig.imageNameForSelectedState = "checkk"
                    thirdSingleVC.checkboxBuilderConfig.titleColorForSelectedState = UIColor.colorFromCode(0x2EB863)
                    thirdSingleVC.checkboxBuilderConfig.selection = .Single
                    thirdSingleVC.checkboxBuilderConfig.headerTitle = ""
                    thirdSingleVC.checkboxBuilderConfig.style = .OneColumn
                    thirdSingleVC.checkboxBuilderConfig.borderStyle = .None
                    
                    thirdSingleVC.documentQuestionListModel = data
                    
                    
                    self.changeViewController(viewController: thirdSingleVC)
                }else if let forthVC = vc as? DocumentQuestionTypeFourVC,data.questionTypeCode == "AddNew" {
                    forthVC.multipleAnswersSubmitted = {
                        
                        answer,index in
                        
                        self.arrOfAnswer[index!]["answer"] = answer
                        
                        
                    }
                    forthVC.currentIndex = currentIndex
                    forthVC.dataSourceTitles = data.optionListing!
                    
                    if let can = data.addAnother {
                        if can == "No" {
                            forthVC.canAddAnother = false
                        }else{
                            forthVC.canAddAnother = true
                        }
                    }
                    
                    self.changeViewController(viewController: forthVC)
                }
                
                
                
            }
            
        }
        
        
    }
    func changeViewController(viewController : UIViewController) {
        mainContainerHeightConstraint.constant = viewController.view.frame.size.height
        if lastViewController != nil{
            lastViewController?.view.removeFromSuperview()
            lastViewController?.removeFromParentViewController()
        }
        
        self.addChildViewController(viewController)
        viewController.view.frame = CGRect(x: 0, y: 0, width: mainContainerView.frame.size.width, height: mainContainerView.frame.size.height)
        
        mainContainerView.addSubview(viewController.view)
        lastViewController = viewController
        
    }
    func getViewControllerWithCode(code : String) -> UIViewController? {
        
        if code == "AddNew" {
            let vc1 : DocumentQuestionTypeFourVC = self.storyboard?.instantiateViewController(withIdentifier: "DocumentQuestionTypeFourVC") as! DocumentQuestionTypeFourVC
            
            return vc1
        }else if code == "LongAnswerQuestion"{
            let vc1 : QuestionTypeOneVC = self.storyboard?.instantiateViewController(withIdentifier: "QuestionTypeOneVC") as! QuestionTypeOneVC
            return vc1
        }else if code == "ShortAnswerQuestion"{
            let vc1 : QuestionTypeTwoVC = self.storyboard?.instantiateViewController(withIdentifier: "QuestionTypeTwoVC") as! QuestionTypeTwoVC
            return vc1
        }else if code == "Multiselect"{
            let vc1 : DocumentQuestionTypeThreeVC = self.storyboard?.instantiateViewController(withIdentifier: "DocumentQuestionTypeThreeVC") as! DocumentQuestionTypeThreeVC
            return vc1
        }else if code == "SingleChoice"{
            let vc1 : DocumentQuestionTypeThreeVC = self.storyboard?.instantiateViewController(withIdentifier: "DocumentQuestionTypeThreeVC") as! DocumentQuestionTypeThreeVC
            return vc1
        }
        
        
        return nil
    }
    //MARK: VALIDATIONS
    func validation(withIndex : Int)  ->Bool{
        
        let data = self.arrOfAnswer[withIndex]
        
        if data["type"] as! String == "AddNew" {
            if let answer = data["answer"] as? [[String:String]]{
                
                for datadict in answer {
                    
                    for key in datadict.keys {
                        if datadict[key]!.isEmptyString(){
                            
                            UIAlertController.showAlertWithOkButton(self, aStrMessage: Constant.AlertMessage.kAlertMessageRequiredField(key), completion: nil)
                            return false
                        }
                    }
                    
                }
            }
        }else if data["type"] as! String == "LongAnswerQuestion"{
            if (data["answer"] as! String).isEmptyString() || (data["answer"] as! String) == "Type here" {
                
                UIAlertController.showAlertWithOkButton(self, aStrMessage: Constant.AlertMessage.kAlertMessageRequiredField("answer"), completion: nil)
                
                return false
            }
        }else if data["type"] as! String == "ShortAnswerQuestion"{
            if (data["answer"] as! String).isEmptyString() {
                
                UIAlertController.showAlertWithOkButton(self, aStrMessage: Constant.AlertMessage.kAlertMessageRequiredField("answer"), completion: nil)
                
                return false
            }
        }else if data["type"] as! String == "Multiselect"{
            if let arrData = data["answer"] as? [[String:Any]]{
                
                if arrData.count > 0 {
                    for dataDict in arrData {
                        if (dataDict["question_option"] as! String).isEmptyString() {
                            UIAlertController.showAlertWithOkButton(self, aStrMessage: "Please answer the current question", completion: nil)
                            
                            return false
                        }
                    }
                }else{
                    UIAlertController.showAlertWithOkButton(self, aStrMessage: "Please answer the current question", completion: nil)
                    
                    return false
                }
            }
        }else if data["type"] as! String == "SingleChoice"{
            if (data["answer"] as! String).isEmptyString() {
                
                UIAlertController.showAlertWithOkButton(self, aStrMessage: "Please answer the current question", completion: nil)
                
                return false
            }
        }
        
        
        
        return true
    }


    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
