//
//  QuestionsHandlerVC.swift
//  LegalStart
//
//  Created by Adapting Social on 03/07/17.
//  Copyright © 2017 Adapting Social. All rights reserved.
//

import UIKit
import SwiftyJSON
class QuestionsHandlerVC: UIViewController {

    
    //MARK: GLOBAL PROPERTIES
    
    
    @IBOutlet weak var viewForInfoWindow: UIView!
    @IBOutlet weak var lblFirstInfo: UILabel!
    @IBOutlet weak var lblSecondInfo: UILabel!
    
    @IBOutlet weak var lblScreenTitle: UILabel!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var btnPrevious: UIButton!
    @IBOutlet weak var lblTotalQuestionNumber: UILabel!
    @IBOutlet weak var lblCurrentQuestionNumber: UILabel!
    @IBOutlet weak var lblQuestionNumber: UILabel!
    @IBOutlet weak var lblQuestion: UILabel!
    @IBOutlet weak var mainContainerHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var mainContainerView: UIView!
    var lastViewController : UIViewController?
    let token = UserDefaults.value(forKey: ACCESSTOKEN) as! String
    let users_id = AppDelegate.getAppDelegate().userInfoData.usersId!
    var categoryModel : QuestionCategoryListData?
    var dataSource : QuestionListModel?
    var currentIndex : Int = 0
    var arrOfAnswer : [[String:Any]] = []
    var masterBrowser : MasterWebBrowser?
    var finalAmountToPay : Float = 0
    //MARK: VIEW LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        
        if categoryModel?.categoryName == "LLC" {
            self.lblFirstInfo.text = "- If you register your LLC in New York,you are required to complete the New York Publication Requirement.Please see our 'other services' if you would like to purchase it now."
            self.lblSecondInfo.text = "- If you formed your business in a state different than the one you will be 'transacting business' in,you are required to obtain a Certificate of Authority.Please see our 'Other Services' if you would like to purchase it now."
        }else{
            self.lblFirstInfo.text = "- If you formed your business in a state different than the one you will be 'transacting business' in,you are required to obtain a Certificate of Authority.Please see our 'Other Services' if you would like to purchase it now."
            self.lblSecondInfo.text = ""
        }
        
        self.lblScreenTitle.text = self.categoryModel?.categoryName
        
        self.callWSToGetCategoryList()
        
    }
    
    //MARK: BUTTON ACTION
    @IBAction func btnBackAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnNextAction(_ sender: UIButton) {
        self.view.endEditing(true)
        
        if validation(withIndex: currentIndex) {
            if self.dataSource != nil {
                if currentIndex < self.dataSource!.data!.count-1 {
                    self.currentIndex += 1
                    self.changeQuestionWithCode(currentIndex: self.currentIndex)
                    
                }
            }
        }
    }
    @IBAction func btnPreviousAction(_ sender: UIButton) {
        self.view.endEditing(true)
//        if validation(withIndex: currentIndex) {
            if self.dataSource != nil {
                if currentIndex <= self.dataSource!.data!.count-1 {
                    self.currentIndex -= 1
                    self.changeQuestionWithCode(currentIndex: self.currentIndex)
                    
                }
            }
//        }
        
    }
    @IBAction func btnSubmitAction(_ sender: UIButton) {
        self.view.endEditing(true)
        print(self.arrOfAnswer)

        if self.validation(withIndex: currentIndex) {
            
//            UIAlertController.showAlert(self, aStrMessage: "Select Payment option", style: .actionSheet, aCancelBtn: "Cancel", aDistrutiveBtn: nil, otherButtonArr: ["Credit Card"], completion: { (index, title) in
//                
////                if title == "Paypal"{
////                    
////                    self.paymentWithPayPal()
////                }else
//                    if title == "Credit Card"{
//                    
//                    self.paymentWithStripe()
////                    self.paymentWithAuthorizeDotNet()
//                }
//                
//                
//            })
            self.paymentWithStripe()

        }
        
    }
    @IBAction func btnCloseInfoWindowAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
        
    }
    //MARK: PAYMENT
    func paymentWithPayPal() {
        
        let url : String = "\(Constant.API.kBaseURLWithAPIName("questionnaire_paypal_payment"))?amount=\(self.finalAmountToPay)&users_id=\(self.users_id)&access_token=\(self.token)&question_category_id=\(self.categoryModel!.questionCategoryId!)"
        
        self.masterBrowser = MasterWebBrowser(nibName: "MasterWebBrowser", bundle: nil)
        self.masterBrowser?.urlToLoad = url
        self.masterBrowser?.paymentDidFinish = {
            success,jsonResponse1 in
            if success {

                self.callWSToSendAnswers(paymentID: jsonResponse1["data"]["startup_payment_id"].string!)
            }else{
                
                UIAlertController.showAlertWithOkButton(self, aStrMessage: "Payment cancel please try again later", completion: nil)
                
            }
            
        }
        self.navigationController?.present(self.masterBrowser!, animated: true, completion: nil)
    }
    func paymentWithStripe() {
        
//        let stripeVC : StripePaymentViewController = StripePaymentViewController(nibName: "StripePaymentViewController", bundle: nil)
//        stripeVC.amount = self.finalAmountToPay
//        let newNav : UINavigationController = UINavigationController(rootViewController: stripeVC)
//        newNav.navigationBar.isHidden = true
//        self.navigationController?.present(newNav, animated: true, completion: nil)
//        stripeVC.didFinishStripePayment = {
//            
//            stripController,token,error in
//            if error == nil{
//                
//               
//                self.callWSToSendStripeToken(stripeToken: (token?.tokenId)!)
//                
//                
//                
//            }else{
//                
//                UIAlertController.showAlertWithOkButton(self, aStrMessage: "Payment cancel please try again later", completion: nil)
//                
//            }
//            
//        }
        
        LawyerAppStripePaymentAPI.sharedInstance.presentStripPaymentVC(viewController: self, amount:String(self.finalAmountToPay) , completionHandler: { (stripToken, status,error) in
            
            
            if stripToken != nil{
                
                
                self.callWSToSendStripeToken(stripeToken: stripToken!)
                
                
                
            }else{
                
                if status == .error {
                    UIAlertController.showAlertWithOkButton(self, aStrMessage: Constant.AlertMessage.kAlertMsgNotAbleToCompletePayment, completion: nil)
                    
                }
                
            }
            
        })

        
        
        
    }
//    func paymentWithAuthorizeDotNet()  {
//        let vc : AuthorizePaymentVC = AuthorizePaymentVC(nibName: "AuthorizePaymentVC", bundle: nil)
//        vc.shouldPayWithFramework = true
//        vc.strSubscriptionAmount = String(self.finalAmountToPay)
//        vc.strItemId = "1"
//        vc.strSubscriptionPackageName = self.categoryModel?.categoryName
//        vc.subscriptionSuccessCallBack = {
//            response in
//            
//            self.callWSToSendTransactionID(transactionID: response!.transactionResponse.transId, amount: String(self.finalAmountToPay))
//        }
//        self.present(vc, animated: true, completion: nil)
//        
//        
//
//    }
    //MARK: WEB-SERVICE
    func callWSToSendTransactionID(transactionID : String,amount : String) {
//        http://php54.Adapting Social.com/lawyerapp/WS/questionnaire_authorisece_net_payment?users_id=1&question_category_id=1&amount=12&payment_id=123
        var params : [String:Any] = [:]
        params["question_category_id"] = self.categoryModel?.questionCategoryId
        params["users_id"] = users_id
        params["payment_id"] = transactionID
        params["amount"] = amount
        WebService.POST(Constant.API.kBaseURLWithAPIName("questionnaire_authorisece_net_payment"), param: params, controller: self, successBlock: { (jsonResponse) in
            
            
            if (jsonResponse["settings"]["success"].string == "1"){
                
//                UIAlertController.showAlertWithOkButton(self, aStrMessage: "Payment successfull accepted", completion: { (index, title) in
//                    //                    self.moveToDashBoard()
//                })
                self.callWSToSendAnswers(paymentID: jsonResponse["data"][0]["startup_payment_id"].string!)
                
            }else if (jsonResponse["settings"]["success"].string == "0"){
                
                UIAlertController.showAlertWithOkButton(self, aStrMessage: jsonResponse["settings"]["message"].string, completion: nil)
                
            }else if (jsonResponse["settings"]["success"].string == "100"){
                
            }else{
                
                UIAlertController.showAlertWithOkButton(self, aStrMessage: Constant.AlertMessage.kAlertMsgSomeThingWentWrong, completion: nil)
            }
            
            
            
        }) { (error, isTimeOut) in
            
        }
    }
    func callWSToSendStripeToken(stripeToken : String)  {
        
        var param : [String:Any] = [:]
        param["question_category_id"] = self.categoryModel?.questionCategoryId
        param["amount"] = String(Int(self.finalAmountToPay * 100))
        param["users_id"] = self.users_id
        param["access_token"] = self.token
        param["token"] = stripeToken
        
        WebService.POST(Constant.API.kBaseURLWithAPIName("questionnaire_stripe_payment"), param: param, controller: self, successBlock: { (jsonResponse1) in
            
            
            if jsonResponse1["settings"]["success"].string == "1"{
                
                self.callWSToSendAnswers(paymentID: jsonResponse1["data"][0]["startup_payment_id"].string!)
            }else if jsonResponse1["settings"]["success"].string == "100"{
                
                
                Utility.reloginUserWith(viewController: self)
                
                
            }else{
                
                UIAlertController.showAlertWithOkButton(self, aStrMessage: jsonResponse1["settings"]["message"].string, completion: nil)
                
            }
            
            
        }, failureBlock: { (error, isTimeOut) in
            
        })
    }
    
    func callWSToSendAnswers(paymentID : String)  {
        var param : [String:Any] = [:]
        param["question_category_id"] = self.categoryModel?.questionCategoryId
        param["startup_payment_id"] = paymentID
        
        var answerToSend : [[String:Any]] = []
        for data in self.arrOfAnswer {
            
            var tempData : [String:Any] = data
            tempData.removeValue(forKey: "viewController")
            
            if (data["flow_finish"] as! Bool) == true {
                tempData.removeValue(forKey: "flow_finish")
                answerToSend.append(tempData)
                break;
            }else{
            
                tempData.removeValue(forKey: "flow_finish")
                answerToSend.append(tempData)
            }
        }
        param["question_answer"] = Utility.convertObjectToJson(from: answerToSend)
        print(param)
        WebService.POST(Constant.API.kBaseURLWithAPIName("question_answer_by_users"), param: param, controller: self, successBlock: { (jsonResponse2) in
            
            if jsonResponse2["settings"]["success"].string == "1"{
                
                
                UIAlertController.showAlertWithOkButton(self, aStrMessage: "Congratulations! your answers have been successfully submitted", completion: { (index, title) in
                
                    self.viewForInfoWindow.isHidden = false
                    
                })
                
            }else if jsonResponse2["settings"]["success"].string == "0"{
                
                
                Utility.reloginUserWith(viewController: self)
                
                
            }else{
                
                UIAlertController.showAlertWithOkButton(self, aStrMessage: jsonResponse2["settings"]["message"].string, completion: nil)
                
            }
            
            
            
        }, failureBlock: { (error, isTimeOut) in
            
            
        })
    }
    func callWSToGetCategoryList()  {
        var params : [String:Any] = [:]
        params["users_id"] = users_id
        params["access_token"] = token
        params["category_id"] = categoryModel?.questionCategoryId
        WebService.GET(Constant.API.kBaseURLWithAPIName("question_list"), param: params, controller: self, callSilently: false, successBlock: { (jsonResponse) in
            
            if (jsonResponse["settings"]["success"].string == "1"){
                
                self.dataSource = QuestionListModel(json: jsonResponse)
                if (self.dataSource?.data?.count)! > 0{
                    
                    for data in self.dataSource!.data!{
                    
                        if let vc = self.getViewControllerWithCode(code: data.questionTypeCode!){
                        
                            var tempDict : [String:Any] = [:]
                            tempDict["type"] = data.questionTypeCode
                            tempDict["question_id"] = data.questionId
                            tempDict["answer"] = ""
                            tempDict["users_id"] = self.users_id
                            tempDict["viewController"] = vc
                            tempDict["flow_finish"] = false
                            self.arrOfAnswer.append(tempDict)
                        }
                        
                        
                    }
                    
                    
                    self.lblTotalQuestionNumber.text = "\(self.dataSource!.data!.count)"
                    self.changeQuestionWithCode(currentIndex: self.currentIndex)
                }
                
            }else if (jsonResponse["settings"]["success"].string == "0"){
                
                UIAlertController.showAlertWithOkButton(self, aStrMessage: jsonResponse["settings"]["message"].string, completion: nil)
                
            }else if (jsonResponse["settings"]["success"].string == "100"){
                Utility.reloginUserWith(viewController: self)
            }else{
                
                UIAlertController.showAlertWithOkButton(self, aStrMessage: Constant.AlertMessage.kAlertMsgSomeThingWentWrong, completion: nil)
            }
            
            
            
        }) { (error, isTimeOut) in
            
        }
    }
    //MARK: UTILITY
    func changeQuestionWithCode(currentIndex : Int) {
        
        if let data = self.dataSource?.data?[currentIndex] {
            lblQuestion.text = data.question
            var newIndex = currentIndex
            newIndex += 1
            lblQuestionNumber.text = "\(String(newIndex))."
            lblCurrentQuestionNumber.text = String(newIndex)
            
            if data.flowFinish!.characters.count > 0 && (data.flowFinish == (self.arrOfAnswer[currentIndex]["answer"] as! String)){
            
                self.arrOfAnswer[currentIndex]["flow_finish"] = true
                if currentIndex == 0 {
                    self.btnPrevious.isHidden = true
                    self.btnNext.isHidden = true
                    self.btnSubmit.isHidden = false
                }else{
                    self.btnPrevious.isHidden = false
                    self.btnNext.isHidden = true
                    self.btnSubmit.isHidden = false
                }
                
                
            }else{
                self.arrOfAnswer[currentIndex]["flow_finish"] = false
                if currentIndex != 0 && currentIndex < self.dataSource!.data!.count-1 {
                    self.btnPrevious.isHidden = false
                    self.btnNext.isHidden = false
                    self.btnSubmit.isHidden = true
                    
                }else if currentIndex == 0{
                    
                    self.btnPrevious.isHidden = true
                    self.btnNext.isHidden = false
                    self.btnSubmit.isHidden = true
                    
                }else if currentIndex == self.dataSource!.data!.count-1 {
                    
                    self.btnPrevious.isHidden = false
                    self.btnNext.isHidden = true
                    self.btnSubmit.isHidden = false
                    
                }
            
            }
            
            if let vc =  self.arrOfAnswer[currentIndex]["viewController"] as? UIViewController{
            
                if let firstVC = vc as? QuestionTypeOneVC,data.questionTypeCode == "LongAnswerQuestion" {
                    firstVC.currentIndex = currentIndex
                    firstVC.longAnswerSubmited = {
                        answer,index in
                        self.arrOfAnswer[index]["answer"] = answer
                    }

                    self.changeViewController(viewController: firstVC)
                    
                }else if let secondVC = vc as? QuestionTypeTwoVC,data.questionTypeCode == "ShortAnswerQuestion" {
                    secondVC.currentIndex = currentIndex
                    secondVC.shortAnswerSubmitted = {
                    
                        answer,index in
                        self.arrOfAnswer[index]["answer"] = answer
                        
                    }
                    
                    self.changeViewController(viewController: secondVC)
                }else if let thirdMultiVC = vc as? QuestionTypeThreeVC,data.questionTypeCode == "Multiselect" {
                    
                    thirdMultiVC.currentIndex = currentIndex

                    thirdMultiVC.multipleSelectionAnswerSubmition = {
                    
                        answer,index in
                        
                        self.arrOfAnswer[index]["answer"] = answer
                        
                    
                    }
                    
                    thirdMultiVC.checkboxBuilderConfig.imageNameForNormalState = "unselected"
                    thirdMultiVC.checkboxBuilderConfig.imageNameForSelectedState = "selected"
                    thirdMultiVC.checkboxBuilderConfig.titleColorForSelectedState = UIColor.colorFromCode(0x2EB863)
                    thirdMultiVC.checkboxBuilderConfig.selection = .Multiple
                    thirdMultiVC.checkboxBuilderConfig.headerTitle = ""
                    thirdMultiVC.checkboxBuilderConfig.style = .OneColumn
                    thirdMultiVC.checkboxBuilderConfig.borderStyle = .None
                    
                    

                    
            
                    thirdMultiVC.dataSource = data
                    
                    
                    self.changeViewController(viewController: thirdMultiVC)
                    
                }else if let thirdSingleVC = vc as? QuestionTypeThreeVC,data.questionTypeCode == "SingleChoice" {
                    
                    thirdSingleVC.currentIndex = currentIndex

                    thirdSingleVC.singleOptionAnswerSubmitted = {
                    
                        answer,index in
                        var flow_finish = false
                        if answer == data.flowFinish && answer.characters.count > 0 {
                            flow_finish = true
                            if currentIndex == 0 {
                                self.btnPrevious.isHidden = true
                                self.btnNext.isHidden = true
                                self.btnSubmit.isHidden = false
                            }else{
                                self.btnPrevious.isHidden = false
                                self.btnNext.isHidden = true
                                self.btnSubmit.isHidden = false
                            }
                        }else{
                            flow_finish = false
                            if currentIndex != 0 && currentIndex < self.dataSource!.data!.count-1 {
                                self.btnPrevious.isHidden = false
                                self.btnNext.isHidden = false
                                self.btnSubmit.isHidden = true
                                
                            }else if currentIndex == 0{
                                
                                self.btnPrevious.isHidden = true
                                self.btnNext.isHidden = false
                                self.btnSubmit.isHidden = true
                                
                            }else if currentIndex == self.dataSource!.data!.count-1{
                                
                                self.btnPrevious.isHidden = false
                                self.btnNext.isHidden = true
                                self.btnSubmit.isHidden = false
                                
                            }
                        
                        }
                        self.arrOfAnswer[index]["flow_finish"] = flow_finish
                        self.arrOfAnswer[index]["answer"] = answer
                    
                    }
                    
                    thirdSingleVC.checkboxBuilderConfig.startPosition = CGPoint(x: 0, y: 0)
                    thirdSingleVC.checkboxBuilderConfig.imageNameForNormalState = "uncheckk"
                    thirdSingleVC.checkboxBuilderConfig.imageNameForSelectedState = "checkk"
                    thirdSingleVC.checkboxBuilderConfig.titleColorForSelectedState = UIColor.colorFromCode(0x2EB863)
                    thirdSingleVC.checkboxBuilderConfig.selection = .Single
                    thirdSingleVC.checkboxBuilderConfig.headerTitle = ""
                    thirdSingleVC.checkboxBuilderConfig.style = .OneColumn
                    thirdSingleVC.checkboxBuilderConfig.borderStyle = .None
                    thirdSingleVC.checkboxBuilderConfig.checkboxSize = CGSize(width: 300, height: 0)

                    thirdSingleVC.dataSource = data
                    
                    self.changeViewController(viewController: thirdSingleVC)
                }else if let forthVC = vc as? QuestionTypeFourVC,data.questionTypeCode == "AddNew" {
                    forthVC.multipleAnswersSubmitted = {
                    
                        answer,index in
                        print(answer ?? "nil")
                        print(index ?? "nil")
                        self.arrOfAnswer[index!]["answer"] = answer
                        
                    
                    }
                    let arr = data.optionListing
                    forthVC.currentIndex = currentIndex
                    forthVC.dataSourceTitles = arr!
                    
                    if let can = data.addAnother {
                        if can == "No" {
                            forthVC.canAddAnother = false
                        }else{
                            forthVC.canAddAnother = true
                        }
                    }
                    
                    self.changeViewController(viewController: forthVC)
                    
                }

            
            
            }
            
        }
        
        
    }
    func changeViewController(viewController : UIViewController) {
        mainContainerHeightConstraint.constant = viewController.view.frame.size.height
        if lastViewController != nil{
            lastViewController?.view.removeFromSuperview()
            lastViewController?.removeFromParentViewController()
        }
        
        self.addChildViewController(viewController)
        viewController.view.frame = CGRect(x: 0, y: 0, width: mainContainerView.frame.size.width, height: mainContainerView.frame.size.height)
        
        mainContainerView.addSubview(viewController.view)
        lastViewController = viewController
        
    }
    func getViewControllerWithCode(code : String) -> UIViewController? {
        
        if code == "AddNew" {
            let vc1 : QuestionTypeFourVC = self.storyboard?.instantiateViewController(withIdentifier: "QuestionTypeFourVC") as! QuestionTypeFourVC
            
            return vc1
        }else if code == "LongAnswerQuestion"{
            let vc1 : QuestionTypeOneVC = self.storyboard?.instantiateViewController(withIdentifier: "QuestionTypeOneVC") as! QuestionTypeOneVC
            return vc1
        }else if code == "ShortAnswerQuestion"{
            let vc1 : QuestionTypeTwoVC = self.storyboard?.instantiateViewController(withIdentifier: "QuestionTypeTwoVC") as! QuestionTypeTwoVC
            return vc1
        }else if code == "Multiselect"{
            let vc1 : QuestionTypeThreeVC = self.storyboard?.instantiateViewController(withIdentifier: "QuestionTypeThreeVC") as! QuestionTypeThreeVC
            return vc1
        }else if code == "SingleChoice"{
            let vc1 : QuestionTypeThreeVC = self.storyboard?.instantiateViewController(withIdentifier: "QuestionTypeThreeVC") as! QuestionTypeThreeVC
            return vc1
        }
        
        
        return nil
    }
    //MARK: VALIDATION
//    func validation(withIndex : Int)  ->Bool{
//        
//        let data = self.arrOfAnswer[withIndex]
//        
//        if data["type"] as! String == "AddNew" {
//            if let answer = data["answer"] as? [[String:String]]{
//            
//                for datadict in answer {
//                    
//                    for key in datadict.keys {
//                        if datadict[key]!.isEmptyString(){
//                        
//                        UIAlertController.showAlertWithOkButton(self, aStrMessage: Constant.AlertMessage.kAlertMessageRequiredField(key), completion: nil)
//                            return false
//                        }
//                    }
//                    
//                }
//            }
//        }else if data["type"] as! String == "LongAnswerQuestion"{
//            if (data["answer"] as! String).isEmptyString() {
//                
//                UIAlertController.showAlertWithOkButton(self, aStrMessage: Constant.AlertMessage.kAlertMessageRequiredField("answer"), completion: nil)
//                
//                return false
//            }
//        }else if data["type"] as! String == "ShortAnswerQuestion"{
//            if (data["answer"] as! String).isEmptyString() {
//                
//                UIAlertController.showAlertWithOkButton(self, aStrMessage: Constant.AlertMessage.kAlertMessageRequiredField("answer"), completion: nil)
//                
//                return false
//            }
//        }else if data["type"] as! String == "Multiselect"{
//            if (data["answer"] as! String).isEmptyString() {
//                
//                UIAlertController.showAlertWithOkButton(self, aStrMessage: "Please select atlease one from options", completion: nil)
//                
//                return false
//            }
//        }else if data["type"] as! String == "SingleChoice"{
//            if (data["answer"] as! String).isEmptyString() {
//                
//                UIAlertController.showAlertWithOkButton(self, aStrMessage: "Please select answer from options", completion: nil)
//                
//                return false
//            }
//        }
//        
//        
//        
//        return true
//    }
    
    func validation(withIndex : Int)  ->Bool{
        
        let data = self.arrOfAnswer[withIndex]
        
        if data["type"] as! String == "AddNew" {
            if let answer = data["answer"] as? [[String:String]]{
                
                for datadict in answer {
                    
                    for key in datadict.keys {
                        if datadict[key]!.isEmptyString(){
                            
                            UIAlertController.showAlertWithOkButton(self, aStrMessage: Constant.AlertMessage.kAlertMessageRequiredField(key), completion: nil)
                            return false
                        }
                    }
                    
                }
            }
        }else if data["type"] as! String == "LongAnswerQuestion"{
            if (data["answer"] as! String).isEmptyString() || (data["answer"] as! String) == "Type here"{
                
                UIAlertController.showAlertWithOkButton(self, aStrMessage: Constant.AlertMessage.kAlertMessageRequiredField("answer"), completion: nil)
                
                return false
            }
        }else if data["type"] as! String == "ShortAnswerQuestion"{
            if (data["answer"] as! String).isEmptyString() {
                
                UIAlertController.showAlertWithOkButton(self, aStrMessage: Constant.AlertMessage.kAlertMessageRequiredField("answer"), completion: nil)
                
                return false
            }
        }else if data["type"] as! String == "Multiselect"{
//            if let arrData = data["answer"] as? [[String:Any]]{
//                
//                for dataDict in arrData {
//                    if (dataDict["question_option"] as! String).isEmptyString() {
//                        UIAlertController.showAlertWithOkButton(self, aStrMessage: "Please select atlease one from options", completion: nil)
//                        
//                        return false
//                    }
//                }
//                
//                
//                
//            }
            
            if let arrData = data["answer"] as? [[String:Any]]{
                
                if arrData.count > 0 {
                    for dataDict in arrData {
                        if (dataDict["question_option"] as! String).isEmptyString() {
                            UIAlertController.showAlertWithOkButton(self, aStrMessage: "Please answer the current question", completion: nil)
                            
                            return false
                        }
                    }
                }else{
                    UIAlertController.showAlertWithOkButton(self, aStrMessage: "Please answer the current question", completion: nil)
                    
                    return false
                }
            }
            
            
        }else if data["type"] as! String == "SingleChoice"{
            if (data["answer"] as! String).isEmptyString() {
                
                UIAlertController.showAlertWithOkButton(self, aStrMessage: "Please select answer from options", completion: nil)
                
                return false
            }
        }
        
        
        
        return true
    }

    //MARK: MEMORY-WARNING
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    

}
