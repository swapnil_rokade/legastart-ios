//
//  ChatHandlerViewController.swift
//  LegalStart
//
//  Created by Adapting Social on 10/05/17.
//  Copyright © 2017 Adapting Social. All rights reserved.
//

import UIKit

class ChatHandlerViewController: UIViewController {

    
    @IBOutlet weak var viewOnlineIndicator: UIView!
    @IBOutlet weak var viewForChatController: UIView!
    var chtVC : ChatViewController! = nil
    
    var isviewLoaded : Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()

        self.chtVC =  self.storyboard?.instantiateViewController(withIdentifier: "ChatViewController") as? ChatViewController
        
        SVProgressHUD.show()
        let user : QBUUser = QBUUser()
        user.login = AppDelegate.getAppDelegate().userInfoData.blogEmail!
        user.password = AppDelegate.getAppDelegate().userInfoData.blogPasswsord!
        
//        ServicesManager.instance().chatService.connect { (error) in
//            
//        }
        Utility.sharedInstance.loginUseToQuickBloxWithUser(user: user, completionHandler: { (user, error) in
            self.loadChat()
        })
        
   
    }
    override func viewDidAppear(_ animated: Bool) {
        
        
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if isviewLoaded{
        chtVC?.view.frame = CGRect(x: 0, y: 0, width: self.viewForChatController.frame.size.width, height: self.viewForChatController.frame.size.height)
        }
        
    }
    func loadChat() {
     
        self.viewOnlineIndicator.layer.cornerRadius = self.viewOnlineIndicator.frame.size.height / 2
        self.viewOnlineIndicator.layer.masksToBounds = true
       
        let opponentUser : QBUUser = QBUUser()
        opponentUser.id = UInt(AppDelegate.getAppDelegate().userInfoData.blogId!)!
        
        Utility.sharedInstance.getPrivateDiaLogWithOpponentUser(user: opponentUser) { (response, dialog) in
            SVProgressHUD.dismiss()
            if dialog != nil{

                UserDefaults.setValue(dialog!.id!, forKey: CHAT_DIALOG_ID)
                self.chtVC?.dialog = dialog
                if dialog?.type == .private {
                    self.chtVC?.opponentUser = opponentUser
                    self.chtVC?.dialog.name = ServicesManager.instance().currentUser.email
                    self.chtVC?.senderID = (ServicesManager.instance().currentUser.id)
                    self.chtVC?.senderDisplayName = (ServicesManager.instance().currentUser.login)!
                   
                }
                
                self.chtVC?.view.frame = CGRect(x: 0, y: 0, width: self.viewForChatController.frame.size.width, height: self.viewForChatController.frame.size.height)
                self.addChildViewController(self.chtVC!)
                self.viewForChatController.addSubview((self.chtVC?.view)!)
                self.chtVC?.didMove(toParentViewController: self)
                self.isviewLoaded = true
                self.viewDidLayoutSubviews()
                self.chtVC?.isUserOnline = {
                    isOnline in
                    
                    if isOnline != nil {
                        self.viewOnlineIndicator.isHidden = false
                        if isOnline! == true {
                            
                            self.viewOnlineIndicator.backgroundColor = UIColor.green
                            
                        }else{
                            self.viewOnlineIndicator.backgroundColor = UIColor.lightGray
                            
                        }
                        
                    }else{
                        
                        self.viewOnlineIndicator.isHidden = true
                        
                    }
                    
                }
            }else{
            
                
            }

        }
        
        
     

    }
    
    @IBAction func btnSidePanelAction(_ sender: UIButton) {
        self.view.endEditing(true)
        self.mm_drawerController.toggle(.left, animated: true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
 

}
