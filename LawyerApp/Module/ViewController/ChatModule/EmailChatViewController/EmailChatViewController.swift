//
//  EmailChatViewController.swift
//  LegalStart
//
//  Created by Adapting Social on 10/05/17.
//  Copyright © 2017 Adapting Social. All rights reserved.
//

import UIKit

class EmailChatViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,UITextViewDelegate,UIGestureRecognizerDelegate {

    
    @IBOutlet weak var tblEmailChatDetails: UITableView!
    let token = UserDefaults.value(forKey: ACCESSTOKEN) as! String
    let user_id = AppDelegate.getAppDelegate().userInfoData.usersId!
    override func viewDidLoad() {
        super.viewDidLoad()

       self.navigationController?.interactivePopGestureRecognizer?.delegate = self
       
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
    }
    //MARK: TABLEVIEW DATASOURCE
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell : EmailChatDetailCell = (tableView.dequeueReusableCell(withIdentifier: "EmailChatDetailCell") as? EmailChatDetailCell)!
            cell.txtEmail.text = AppDelegate.getAppDelegate().userInfoData.blogEmail
            cell.txtViewPurpose.text = "Main reason of this chat"
            cell.txtViewPurpose.textColor = UIColor.lightGray
            cell.txtViewPurpose.delegate = self
            return cell
        }else{
            let cell : UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "LetsChatBtnCell")!
            
            return cell
        }
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            
            return 350
        }else{
        
        return 200
        }
    }
    //MARK: TEXTVIEW DELEGATE
    func textViewDidChange(_ textView: UITextView) {
        if textView.text == "\n" {
            textView.text = ""
            textView.resignFirstResponder()
        }
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "Main reason of this chat" {
            textView.text = ""
            textView.textColor = UIColor.colorFromCode(0x9b9b9b)
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Main reason of this chat"
            textView.textColor = UIColor.colorFromCode(0xC7C7CD)
        }
    }
    
    //MARK: BUTTON ACTION
    
    @IBAction func btnLetsChatAction(_ sender: UIButton) {
        if validation() {
            self.callWSToNotifyAdmin()
        }
    }
    func callWSToNotifyAdmin() {

        var param : [String:Any] = [:]
        param["access_token"] = token
        param["users_id"] = user_id
        for cell in tblEmailChatDetails.visibleCells {
            if let cellDetails = cell as? EmailChatDetailCell {
                
                param["name"] = cellDetails.txtName.text
                param["email"] = cellDetails.txtEmail.text
                param["purpose"] = cellDetails.txtViewPurpose.text
                break
            }
        }
        
        WebService.POST(Constant.API.kBaseURLWithAPIName("email_notify_by_admin"), param: param, controller: self, successBlock: { (jsonResponse) in
            if (jsonResponse["settings"]["success"].string == "1"){
            
                UIAlertController.showAlertWithOkButton(self, aStrMessage: Constant.AlertMessage.kAlertMsgAdminEmailSuccess, completion: { (index, value) in
                    UserDefaults.setValue("YES", forKey: IS_CHAT_DIALOG_CREATED)
                    let centerVC = self.storyboard?.instantiateViewController(withIdentifier: "ChatHandlerViewController") as! ChatHandlerViewController
                    self.mm_drawerController?.setCenterView(centerVC, withCloseAnimation: true, completion: nil)
                })
            }else if (jsonResponse["settings"]["success"].string == "100") {
                Utility.reloginUserWith(viewController: self)
            }else{
            
                UIAlertController.showAlertWithOkButton(self, aStrMessage: jsonResponse["settings"]["message"].string, completion: nil)
            }
        }) { (error, isTimeOut) in
            
        }
    }
    func validation() -> Bool {
        
        for cell in tblEmailChatDetails.visibleCells {
            if let cellDetails = cell as? EmailChatDetailCell {

                if cellDetails.txtName.text!.isEmptyString() {
                    
                    UIAlertController.showAlertWithOkButton(self, aStrMessage: Constant.AlertMessage.kAlertMessageRequiredField("name"), completion: nil)
                    return false
                }
                if cellDetails.txtEmail.text!.isEmptyString() {
                    
                    UIAlertController.showAlertWithOkButton(self, aStrMessage: Constant.AlertMessage.kAlertMessageRequiredField("email"), completion: nil)
                    return false
                }
                if !cellDetails.txtEmail.text!.isEmail {
                    
                    UIAlertController.showAlertWithOkButton(self, aStrMessage: Constant.AlertMessage.kAlertMessageRequiredField("valid email"), completion: nil)
                    return false
                }
                if cellDetails.txtViewPurpose.text.isEmptyString() {
                    UIAlertController.showAlertWithOkButton(self, aStrMessage: Constant.AlertMessage.kAlertMessageRequiredField("reason"), completion: nil)
                    return false
                }
                
            }
        }
        
        
        return true
    }
    @IBAction func btnSidePanelAction(_ sender: UIButton) {
        
        self.mm_drawerController.toggle(.left, animated: true, completion: nil)
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
