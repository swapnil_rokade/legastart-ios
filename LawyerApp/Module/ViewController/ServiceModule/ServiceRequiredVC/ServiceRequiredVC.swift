//
//  ServiceRequiredVC.swift
//  LegalStart
//
//  Created by Adapting Social on 17/08/17.
//  Copyright © 2017 Adapting Social. All rights reserved.
//

import UIKit

class ServiceRequiredVC: UIViewController,iCheckboxDelegate,UIGestureRecognizerDelegate {

    @IBOutlet weak var mainContainerHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var checkBoxScrollView: UIScrollView!
    @IBOutlet weak var checkBoxView: UIView!
    @IBOutlet weak var mainContainerView: UIView!
    var lastViewController : UIViewController?
    let checkboxBuilderConfig = iCheckboxBuilderConfig()
    var selectedAnswer : String?
    var vc1 : ServiceRequiredPaymentOptionVC?
    var vc2 : ServiceRequiredUploadDocumentsVC?
    var masterBrowser : MasterWebBrowser?
    let token = UserDefaults.value(forKey: ACCESSTOKEN) as! String
    let users_id = AppDelegate.getAppDelegate().userInfoData.usersId!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        self.addCheckboxes()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
    }
    func addCheckboxes() {
        
        self.checkboxBuilderConfig.startPosition = CGPoint(x: 0, y: 10)
        self.checkboxBuilderConfig.imageNameForNormalState = "uncheckk"
        self.checkboxBuilderConfig.imageNameForSelectedState = "checkk"
        self.checkboxBuilderConfig.titleColorForSelectedState = UIColor.colorFromCode(0x2EB863)
        self.checkboxBuilderConfig.selection = .Single
        self.checkboxBuilderConfig.headerTitle = ""
        self.checkboxBuilderConfig.style = .OneColumn
        self.checkboxBuilderConfig.borderStyle = .None
        self.checkboxBuilderConfig.checkboxSize = CGSize(width: checkBoxView.frame.size.width, height: 0)
        
        let checkboxBuilder = iCheckboxBuilder(withCanvas: self.checkBoxView, canvasScrollView: self.checkBoxScrollView, andConfig: checkboxBuilderConfig)
        checkboxBuilder.delegate = self
        
        var arrOfBoxes : [iCheckboxState] = []
        let box = iCheckboxState()
        box.title = "Yes"
        let box1 = iCheckboxState()
        box1.title = "No"
        arrOfBoxes.append(box)
        arrOfBoxes.append(box1)
        checkboxBuilder.addCheckboxes(withStates: arrOfBoxes)
        
    }


    
    @IBAction func btnSidePanelAction(_ sender: UIButton) {
        
        self.mm_drawerController.toggle(.left, animated: true, completion: nil)
        
    }
    
    @IBAction func btnSubmitAction(_ sender: UIButton) {
        
        if selectedAnswer == "Yes" {
            
            if self.vc1?.selectedAnswer == nil {
                UIAlertController.showAlertWithOkButton(self, aStrMessage: "Please select atleast one option", completion: { (index, title) in
                    
                })
            }else {
            
                var amount = "0"
                var packagename = ""
                if self.vc1?.selectedAnswer == "New York Publication Requirement-$699" {
                    amount = "699"
                    packagename = "NewYorkPublication"
                }else if self.vc1?.selectedAnswer == "New York Certificate of Authority-$399" {
                    amount = "399"
                    packagename = "NewYorkCertificate"
                }else if self.vc1?.selectedAnswer == "New Jersey Certificate of Authority-$399" {
                    amount = "399"
                    packagename = "NewJerseyCertificate"
                }

                
                LawyerAppStripePaymentAPI.sharedInstance.presentStripPaymentVC(viewController: self, amount: amount, completionHandler: { (stripeToken, status,error) in
                    
                    if stripeToken != nil{
                        
                        
                        self.callWSToSendTransactionID(token: stripeToken!, amount: amount)
                        
                    }else{
                        
                        if status == .error {
                            UIAlertController.showAlertWithOkButton(self, aStrMessage: Constant.AlertMessage.kAlertMsgNotAbleToCompletePayment, completion: nil)
                            
                        }
                       
                        
                    }
                   
                    
                })
                
            
            
            
            }
            
        }else if selectedAnswer == "No"{
        
            if self.vc2!.arrOfDocuments.count > 0 {
                if self.vc2?.selectedAnswer == nil {
                    UIAlertController.showAlertWithOkButton(self, aStrMessage: "Please select atleast one option", completion: { (index, title) in
                        
                    })
                }else {
                    
                    var amount = "0"
                    var packagename = ""
                    if self.vc2?.selectedAnswer == "New York Publication Requirement-$699" {
                        amount = "699"
                        packagename = "NewYorkPublication"
                    }else if self.vc2?.selectedAnswer == "New York Certificate of Authority-$399" {
                        amount = "399"
                        packagename = "NewYorkCertificate"
                    }else if self.vc2?.selectedAnswer == "New Jersey Certificate of Authority-$399" {
                        amount = "399"
                        packagename = "NewJerseyCertificate"
                    }
                    
                    LawyerAppStripePaymentAPI.sharedInstance.presentStripPaymentVC(viewController: self, amount: amount, completionHandler: { (stripeToken, status,error) in
                        
                        if stripeToken != nil{
                            
                            
                            self.callWSToSendTransactionID(token: stripeToken!, amount: amount)
                            
                        }else{
                            
                            if status == .error {
                                UIAlertController.showAlertWithOkButton(self, aStrMessage: Constant.AlertMessage.kAlertMsgNotAbleToCompletePayment, completion: nil)
                                
                            }
                            
                          
                        }
                      
                    })

                    
                    
                    
                    
                }
            //    self.callWSToSendDocuments(arrayData: self.vc2!.arrOfDocuments)
            }else{
                UIAlertController.showAlertWithOkButton(self, aStrMessage: "Please upload atleast one document", completion: { (index, title) in
                    
                })
            }
        
        }
        
        
    }
    func callWSToSendTransactionID(token : String,amount : String) {
        
        
        var params : [String:Any] = [:]
        params["users_id"] = users_id
        params["token"] = token
        let tempAmount = Float.init(amount)! * 100
        params["amount"] = String(Int(tempAmount))
        params["access_token"] = self.token
        WebService.POST(Constant.API.kBaseURLWithAPIName("service_required_stripe_payment"), param: params, controller: self, successBlock: { (jsonResponse) in
            
            
            if (jsonResponse["settings"]["success"].string == "1"){
                if self.selectedAnswer == "No" {
                    
                    self.callWSToSendDocuments(arrayData: self.vc2!.arrOfDocuments)
                    
                }else{
                    
                    UIAlertController.showAlertWithOkButton(self, aStrMessage: Constant.AlertMessage.kAlertMsgForPaymentSuccess, completion: { (index, title) in
                        
                        
                         self.moveToDashBoard()
                    })
 
                }
                
                
            }else if (jsonResponse["settings"]["success"].string == "0"){
                
                UIAlertController.showAlertWithOkButton(self, aStrMessage: jsonResponse["settings"]["message"].string, completion: nil)
                
            }else if (jsonResponse["settings"]["success"].string == "100"){
                
            }else{
                
                UIAlertController.showAlertWithOkButton(self, aStrMessage: Constant.AlertMessage.kAlertMsgSomeThingWentWrong, completion: nil)
            }
            
            
           
        }) { (error, isTimeOut) in
            
        }
    }
    func changeViewController(viewController : UIViewController) {

        mainContainerHeightConstraint.constant = viewController.view.frame.size.height
        if lastViewController != nil{
            lastViewController?.view.removeFromSuperview()
            lastViewController?.removeFromParentViewController()
        }
        
        self.addChildViewController(viewController)
        viewController.view.frame = CGRect(x: 0, y: 0, width: mainContainerView.frame.size.width, height: mainContainerView.frame.size.height)
        
        mainContainerView.addSubview(viewController.view)
        lastViewController = viewController
        
    }

    func didSelectCheckbox(withState state: Bool, identifier: Int, andTitle title: String) {
    
        if state == true && title == "Yes" {
            selectedAnswer = "Yes"
            self.btnSubmit.isHidden = false
            self.btnSubmit.setTitle("PAY", for: .normal)
            self.vc1 = self.storyboard?.instantiateViewController(withIdentifier: "ServiceRequiredPaymentOptionVC") as? ServiceRequiredPaymentOptionVC
            self.changeViewController(viewController: vc1!)
        }else if state == true && title == "No" {
            selectedAnswer = "No"
            self.vc2 = self.storyboard?.instantiateViewController(withIdentifier: "ServiceRequiredUploadDocumentsVC") as? ServiceRequiredUploadDocumentsVC
            self.changeViewController(viewController: vc2!)
            self.btnSubmit.isHidden = false
//            self.btnSubmit.setTitle("UPLOAD DOCUMENT", for: .normal)
            
        }else{
            selectedAnswer = nil
            self.btnSubmit.isHidden = true
            lastViewController?.view.removeFromSuperview()
            lastViewController?.removeFromParentViewController()
            lastViewController = nil
        }
    
    }
    func callWSToSendDocuments(arrayData : [[String:Any]])  {
        
        var params : [String:Any] = [:]
        params["users_id"] = users_id
        params["access_token"] = token
        params["option"] = "No"
        
        for (index,obj) in arrayData.enumerated() {
            
            params["document[\(index)]"] = URL(fileURLWithPath: obj["document"] as! String)
            params["document_name[\(index)]"] = obj["document_name"] as! String
            params["document_type[\(index)]"] = obj["document_type"] as! String
            
        }
        
        
        WebService.GET(Constant.API.kBaseURLWithAPIName("service_document_uploaded"), param: params, controller: self, callSilently: false, successBlock: { (jsonResponse) in
            
            if (jsonResponse["settings"]["success"].string == "1"){
                
                UIAlertController.showAlertWithOkButton(self, aStrMessage: "Answer submitted successfully", completion: { (index, title) in
                    self.moveToDashBoard()
                })
            
                
            }else if (jsonResponse["settings"]["success"].string == "0"){
                
                UIAlertController.showAlertWithOkButton(self, aStrMessage: jsonResponse["settings"]["message"].string, completion: nil)
                
            }else if (jsonResponse["settings"]["success"].string == "100"){
                Utility.reloginUserWith(viewController: self)
            }else{
                
                UIAlertController.showAlertWithOkButton(self, aStrMessage: Constant.AlertMessage.kAlertMsgSomeThingWentWrong, completion: nil)
            }
            
        }) { (error, isTimeOut) in
            
        }
    }
    func callWSToPayWithPaypal(amount : String)  {
        let url : String = "\(Constant.API.kBaseURLWithAPIName("services_required_paypal_payment"))?amount=\(amount)&users_id=\(self.users_id)&access_token=\(self.token)&option=Yes"
        
        self.masterBrowser = MasterWebBrowser(nibName: "MasterWebBrowser", bundle: nil)
        self.masterBrowser?.urlToLoad = url
        self.masterBrowser?.paymentDidFinish = {
            success,jsonResponse in
            if success {
                if self.selectedAnswer == "No" {
                    self.callWSToSendDocuments(arrayData: self.vc2!.arrOfDocuments)
                }else {
                    UIAlertController.showAlertWithOkButton(self, aStrMessage: Constant.AlertMessage.kAlertMsgForPaymentSuccess, completion: { (index, title) in
                         self.moveToDashBoard()
                        
                        
                    })
                }
               
                
            }else{
                
                UIAlertController.showAlertWithOkButton(self, aStrMessage: "Payment cancel please try again later", completion: nil)
                
            }
            
        }
        self.navigationController?.present(self.masterBrowser!, animated: true, completion: nil)
    }
    func moveToDashBoard() {
        let storyBoard : UIStoryboard = UIStoryboard(name: "DashBoardMaster", bundle: nil)
        
        let centerVC : DashBoardViewController = storyBoard.instantiateViewController(withIdentifier: "DashBoardViewController") as! DashBoardViewController
        AppDelegate.sharedInstance().selectedMenuIndex = 0
        self.mm_drawerController?.setCenterView(centerVC, withCloseAnimation: true, completion: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
