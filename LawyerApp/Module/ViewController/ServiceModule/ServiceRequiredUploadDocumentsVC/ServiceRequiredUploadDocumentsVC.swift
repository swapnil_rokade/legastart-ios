//
//  ServiceRequiredUploadDocumentsVC.swift
//  LegalStart
//
//  Created by Adapting Social on 17/08/17.
//  Copyright © 2017 Adapting Social. All rights reserved.
//

import UIKit
class ServiceRequiredUploadDocumentsVC: UIViewController,UITableViewDelegate,UITableViewDataSource,UIButtonMasterDocumentPickerDelegate,iCheckboxDelegate {

    @IBOutlet weak var checkBoxScrollView: UIScrollView!
    @IBOutlet weak var checkBoxView: UIView!
    @IBOutlet weak var tblViewDocuments: UITableView!
    let checkboxBuilderConfig = iCheckboxBuilderConfig()
    var selectedAnswer : String?
    var arrOfDocuments : [[String:Any]] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addCheckboxes()
        tblViewDocuments.estimatedRowHeight = 100
        tblViewDocuments.rowHeight = UITableViewAutomaticDimension
        // Do any additional setup after loading the view.
    }
    func shouldOpenMasterDocumentPicker(picker: UIButtonMasterDocumentPicker) -> Bool {
       return true
    }
    func masterDocumentDidFinishPicking(picker: UIButtonMasterDocumentPicker, fileURL: URL, fileExtension: String) {
        
        var aMutDict :  [String:Any] = [:]
        aMutDict["document"] = fileURL.path
        aMutDict["document_type"] = fileExtension
        aMutDict["document_name"] = fileURL.lastPathComponent
        aMutDict["date"] = Date().stringFromDate(format: "dd MMM yyyy")
        
        arrOfDocuments.append(aMutDict)
        
        self.tblViewDocuments.reloadData()
        
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrOfDocuments.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : CaseAddVCDocumentCell = tableView.dequeueReusableCell(withIdentifier: "CaseAddVCDocumentCell") as! CaseAddVCDocumentCell
        cell.documentTitle.text = self.arrOfDocuments[indexPath.row]["document_name"] as? String
        let fileExtension = self.arrOfDocuments[indexPath.row]["document_type"] as! String
        cell.btnRemoveDocument.tag = indexPath.row
        cell.btnRemoveDocument.addTarget(self, action: #selector(btnRemoveDocumentAction(_:)), for: .touchUpInside)
        cell.documentImageView.image = Utility.checkFileTypeWith(fileExtension: fileExtension)
        cell.documentUploadedDate.text = self.arrOfDocuments[indexPath.row]["date"] as? String
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
    }
    
    func addCheckboxes() {
        
        self.checkboxBuilderConfig.startPosition = CGPoint(x: 0, y: 10)
        self.checkboxBuilderConfig.imageNameForNormalState = "uncheckk"
        self.checkboxBuilderConfig.imageNameForSelectedState = "checkk"
        self.checkboxBuilderConfig.titleColorForSelectedState = UIColor.colorFromCode(0x2EB863)
        self.checkboxBuilderConfig.selection = .Single
        self.checkboxBuilderConfig.headerTitle = ""
        self.checkboxBuilderConfig.style = .OneColumn
        self.checkboxBuilderConfig.borderStyle = .None
        self.checkboxBuilderConfig.checkboxSize = CGSize(width: checkBoxView.frame.size.width, height: 0)
        
        let checkboxBuilder = iCheckboxBuilder(withCanvas: self.checkBoxView, canvasScrollView: self.checkBoxScrollView, andConfig: checkboxBuilderConfig)
        checkboxBuilder.delegate = self
        
        var arrOfBoxes : [iCheckboxState] = []
        let box = iCheckboxState()
        box.title = "New York Publication Requirement-$699"
        let box1 = iCheckboxState()
        box1.title = "New York Certificate of Authority-$399"
        let box2 = iCheckboxState()
        box2.title = "New Jersey Certificate of Authority-$399"
        arrOfBoxes.append(box)
        arrOfBoxes.append(box1)
        arrOfBoxes.append(box2)
        checkboxBuilder.addCheckboxes(withStates: arrOfBoxes)
        
    }
    
    func didSelectCheckbox(withState state: Bool, identifier: Int, andTitle title: String) {
        if state == true {
            
            selectedAnswer = title
        }else{
            
            selectedAnswer = nil
        }
    }
    @IBAction func btnRemoveDocumentAction(_ sender: UIButton) {
        
       
        arrOfDocuments.remove(at: sender.tag)
        self.tblViewDocuments.reloadData()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}
