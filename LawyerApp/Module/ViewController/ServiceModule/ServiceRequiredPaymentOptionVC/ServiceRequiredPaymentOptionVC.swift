//
//  ServiceRequiredPaymentOptionVC.swift
//  LegalStart
//
//  Created by Adapting Social on 17/08/17.
//  Copyright © 2017 Adapting Social. All rights reserved.
//

import UIKit

class ServiceRequiredPaymentOptionVC: UIViewController,iCheckboxDelegate {

    @IBOutlet weak var checkBoxScrollView: UIScrollView!
    @IBOutlet weak var checkBoxView: UIView!
    let checkboxBuilderConfig = iCheckboxBuilderConfig()
    var selectedAnswer : String?
    override func viewDidLoad() {
        super.viewDidLoad()

        self.addCheckboxes()
    }

    func addCheckboxes() {
        
        self.checkboxBuilderConfig.startPosition = CGPoint(x: 0, y: 10)
        self.checkboxBuilderConfig.imageNameForNormalState = "uncheckk"
        self.checkboxBuilderConfig.imageNameForSelectedState = "checkk"
        self.checkboxBuilderConfig.titleColorForSelectedState = UIColor.colorFromCode(0x2EB863)
        self.checkboxBuilderConfig.selection = .Single
        self.checkboxBuilderConfig.headerTitle = ""
        self.checkboxBuilderConfig.style = .OneColumn
        self.checkboxBuilderConfig.borderStyle = .None
        self.checkboxBuilderConfig.checkboxSize = CGSize(width: checkBoxView.frame.size.width, height: 0)
        
        let checkboxBuilder = iCheckboxBuilder(withCanvas: self.checkBoxView, canvasScrollView: self.checkBoxScrollView, andConfig: checkboxBuilderConfig)
        checkboxBuilder.delegate = self
        
        var arrOfBoxes : [iCheckboxState] = []
        let box = iCheckboxState()
        box.title = "New York Publication Requirement-$699"
        let box1 = iCheckboxState()
        box1.title = "New York Certificate of Authority-$399"
        let box2 = iCheckboxState()
        box2.title = "New Jersey Certificate of Authority-$399"
        arrOfBoxes.append(box)
        arrOfBoxes.append(box1)
        arrOfBoxes.append(box2)
        checkboxBuilder.addCheckboxes(withStates: arrOfBoxes)
        
    }

    func didSelectCheckbox(withState state: Bool, identifier: Int, andTitle title: String) {
        if state == true {
        
            selectedAnswer = title
        }else{
        
            selectedAnswer = nil
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

 

}
