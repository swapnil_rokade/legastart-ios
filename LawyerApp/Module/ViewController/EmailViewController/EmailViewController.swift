//
//  EmailViewController.swift
//  LegalStart
//
//  Created by Adapting Social on 16/06/17.
//  Copyright © 2017 Adapting Social. All rights reserved.
//

import UIKit
enum EmailVCIsFrom {
    case Drawer
    case CompanyDetail
}
class EmailViewController: UIViewController,UITextViewDelegate,UIGestureRecognizerDelegate {

   
    @IBOutlet weak var btnSidePanel: UIButton!
    @IBOutlet weak var lblNavTitle: UILabel!
    @IBOutlet weak var lblScreenTitle: UILabel!
    @IBOutlet weak var txtFrom: CustomTextField!
    @IBOutlet weak var txtTo: CustomTextField!
    @IBOutlet weak var txtSubject: CustomTextField!
    @IBOutlet weak var txtMessage: UITextView!
    let token = UserDefaults.value(forKey: ACCESSTOKEN) as! String
    let users_id = AppDelegate.getAppDelegate().userInfoData.usersId!
//    var isFrom : EmailVCIsFrom = .Drawer
    var companyEmail : String = ""
    override func viewDidLoad() {
        super.viewDidLoad()

        self.txtFrom.text = AppDelegate.getAppDelegate().userInfoData.blogEmail
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
    }
    //MARK: TEXTVIEW DELEGATE
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "Message" {
            textView.text = ""
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == "" {
            textView.text = "Message"
        }
    }
    @IBAction func btnSendAction(_ sender: UIButton) {
        
        if validation() {
            var params : [String:Any] = [:]
//            if isFrom == .CompanyDetail {
//                params["email_type"] = "Company"
//                params["to_email"] = txtTo.text
//            }else if isFrom == .Drawer{
                params["email_type"] = "Admin"
//                params["to_email"] = ""
//            }
            
            params["from_email"] = txtFrom.text
            params["message"] = txtMessage.text
            params["subject"] = txtSubject.text
            params["users_id"] = users_id
            params["access_token"] = token
            self.callWSToSendEmail(parmas: params)
    
        }
        
    }
    func validation() -> Bool {
        
        if (txtFrom.text?.isEmptyString())! {
            UIAlertController.showAlertWithOkButton(self, aStrMessage: Constant.AlertMessage.kAlertMessageRequiredField("from email"), completion: nil)
            return false
        }
        if !txtFrom.text!.isEmail {
            UIAlertController.showAlertWithOkButton(self, aStrMessage: "Please enter valid email", completion: nil)
            return false
        }
        if (txtSubject.text?.isEmptyString())! {
            UIAlertController.showAlertWithOkButton(self, aStrMessage: Constant.AlertMessage.kAlertMessageRequiredField("subject"), completion: nil)
            return false
        }
        
        return true
    }

    @IBAction func btnSidePanelAction(_ sender: UIButton) {
        
            self.mm_drawerController.toggle(.left, animated: true, completion: nil)
       
        
    }
    
    func callWSToSendEmail(parmas : [String:Any]) {
        
        WebService.POST(Constant.API.kBaseURLWithAPIName("eamil_notify_lawyer"), param: parmas, controller: self, callSilently: false, successBlock: { (jsonResponse) in
            
            if (jsonResponse["settings"]["success"].string == "1"){
                
                UIAlertController.showAlertWithOkButton(self, aStrMessage: "Email has been sent successfully", completion: { (index, title) in
                    if index == 0{
                        
                            let storyBoard : UIStoryboard = UIStoryboard(name: "DashBoardMaster", bundle: nil)
                            let dashBoardVC : DashBoardViewController = storyBoard.instantiateViewController(withIdentifier: "DashBoardViewController") as! DashBoardViewController
                            
                            self.mm_drawerController.setCenterView(dashBoardVC, withFullCloseAnimation: true, completion: nil)
                       
                    
                    }
                })
                
            }else if (jsonResponse["settings"]["success"].string == "0"){
                
                UIAlertController.showAlertWithOkButton(self, aStrMessage: jsonResponse["settings"]["message"].string, completion: nil)
                
            }else if (jsonResponse["settings"]["success"].string == "100"){
                Utility.reloginUserWith(viewController: self)
            }else{
                
                UIAlertController.showAlertWithOkButton(self, aStrMessage: Constant.AlertMessage.kAlertMsgSomeThingWentWrong, completion: nil)
            }

            
            
        }) { (error, isTimeOut) in
            
        }
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
