//
//  MyDocumentViewController.swift
//  LegalStart
//
//  Created by Adapting Social on 08/07/17.
//  Copyright © 2017 Adapting Social. All rights reserved.
//

import UIKit

class MyDocumentViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,UIGestureRecognizerDelegate{

    @IBOutlet weak var tblView: UITableView!
    var dataSourceModel : MyDocumentListModel?
    let token = UserDefaults.value(forKey: ACCESSTOKEN) as! String
    let users_id = AppDelegate.getAppDelegate().userInfoData.usersId!
    var masterDocView : MasterWebBrowser?
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        self.tblView.estimatedRowHeight = 130
        self.tblView.rowHeight = UITableViewAutomaticDimension
        self.callWSToGetMyDocumentList()
        // Do any additional setup after loading the view.
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if dataSourceModel != nil {
            
            return dataSourceModel!.data!.count
        }
        return 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : MyDocumentVCCell = tableView.dequeueReusableCell(withIdentifier: "MyDocumentVCCell") as! MyDocumentVCCell
        
        if let data = self.dataSourceModel?.data?[indexPath.row] {
            cell.lblDocumentTitle.text = data.documemtName
            
        }
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func callWSToGetMyDocumentList()  {
        
        var param : [String:Any] = [:]
        param["users_id"] = self.users_id
        param["access_token"] = self.token
        WebService.GET(Constant.API.kBaseURLWithAPIName("users_document_listing"), param: param, controller: self, callSilently: false, successBlock: { (jsonResponse) in
            
            if jsonResponse["settings"]["success"].string == "1"{
            
            self.dataSourceModel = MyDocumentListModel(json: jsonResponse)
                
            self.tblView.reloadData()
            }else if jsonResponse["settings"]["success"].string == "100"{
                
                Utility.reloginUserWith(viewController: self)
                
            }else if jsonResponse["settings"]["success"].string == "0"{
            
                UIAlertController.showAlertWithOkButton(self, aStrMessage: jsonResponse["settings"]["message"].string, completion: { (index, title) in
                    
                })
            
            }else{
            
                UIAlertController.showAlertWithOkButton(self, aStrMessage: Constant.AlertMessage.kAlertMsgSomeThingWentWrong, completion: { (index, title) in
                    
                })
            
            }
            
        }) { (error, isTimeOut) in
            
            
        }
        
        
    }
    @IBAction func btnViewAction(_ sender: UIButton) {
        
        if let data = self.dataSourceModel?.data?[sender.tag]{
            masterDocView = MasterWebBrowser(nibName: "MasterWebBrowser", bundle: nil)
            
            masterDocView?.urlToLoad = data.docFile!
            self.present(masterDocView!, animated: true, completion: nil)
        
        }
        
    }
    @IBAction func btnSidePanelAction(_ sender: UIButton) {
        
        self.mm_drawerController.toggle(.left, animated: true) { (index) in
            
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

   

}
