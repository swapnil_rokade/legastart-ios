//
//  SubscriptionListingViewController.swift
//  LegalStart
//
//  Created by Adapting Social on 23/06/17.
//  Copyright © 2017 Adapting Social. All rights reserved.
//

import UIKit
import Stripe
import PassKit
class SubscriptionListingViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,UIGestureRecognizerDelegate {

    
    @IBOutlet weak var btnCancelService: CustomButton!
    @IBOutlet weak var tblViewSubscription: UITableView!
    let token = UserDefaults.value(forKey: ACCESSTOKEN) as! String
    let user_id = AppDelegate.getAppDelegate().userInfoData.usersId!
    var subcriptionListModel : SubscriptionListModel?
    var masterBrowser : MasterWebBrowser?
    
    
    let SupportedPaymentNetworks = [PKPaymentNetwork.visa, PKPaymentNetwork.masterCard, PKPaymentNetwork.amex]
    let ApplePaySwagMerchantID = "merchant.com.adaptingsocial.laywer"
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        if let isSub =  UserDefaults.value(forKey: USER_PAYMENT_SUBSCRIPTION) as? String,isSub == "succeeded" {
            self.btnCancelService.isHidden = false
        }else{
            self.btnCancelService.isHidden = true
        }
        
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        self.tblViewSubscription.estimatedRowHeight = 230
        self.tblViewSubscription.rowHeight = UITableViewAutomaticDimension
        self.callWSForSubscriptionListing()
        // Do any additional setup after loading the view.
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if subcriptionListModel != nil {
            return subcriptionListModel!.data!.count + 1
        }
        return 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        
        if self.subcriptionListModel == nil || indexPath.row == self.subcriptionListModel!.data!.count{
            let cell : SubscriptionStaticContentTableViewCell = tableView.dequeueReusableCell(withIdentifier: "SubscriptionStaticContentTableViewCell") as! SubscriptionStaticContentTableViewCell
            
            return cell
        }else{
        
//            if indexPath.row == self.subcriptionListModel!.data!.count{
//
//                let cell : SubscriptionStaticContentTableViewCell = tableView.dequeueReusableCell(withIdentifier: "SubscriptionStaticContentTableViewCell") as! SubscriptionStaticContentTableViewCell
//
//                return cell
//
//            }else{
            
                
                let cell : SubscriptionVCCell = tableView.dequeueReusableCell(withIdentifier: "SubscriptionVCCell") as! SubscriptionVCCell
                
                if let data = subcriptionListModel?.data?[indexPath.row]{
                    
                    cell.subscriptionAmount.text = data.amount
                    cell.subscriptionTitle.text = data.title
                    cell.subscriptionDescription.text = data.descriptionValue
                    if let value = UserDefaults.value(forKey: USER_PAYMENT_SUBSCRIPTION) as? String,value == "succeeded"{
                        cell.btnStart.isHidden = true
                    }else{
                        cell.btnStart.isHidden = false
                        cell.btnStart.tag = indexPath.row
                        cell.btnStart.addTarget(self, action: #selector(btnStartAction(_:)), for: .touchUpInside)
                    }
                    
                }
                
                
                return cell
//            }
            
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func callWSForSubscriptionListing()  {
        var params : [String:Any] = [:]
        params["users_id"] = user_id
        params["access_token"] = token
        WebService.GET(Constant.API.kBaseURLWithAPIName("subscription_listing"), param: params, controller: self, callSilently: false, successBlock: { (jsonResponse) in
            
            if jsonResponse["settings"]["success"].string == "1"{
            self.subcriptionListModel = SubscriptionListModel(json: jsonResponse)
                
            self.tblViewSubscription.reloadData()
            
            }else if jsonResponse["settings"]["success"].string == "100"{
                
                Utility.reloginUserWith(viewController: self)
                
            }else{
            
                UIAlertController.showAlertWithOkButton(self, aStrMessage: jsonResponse["settings"]["message"].string, completion: nil)
                
            }
            
            
            
        }) { (error, isTimeOut) in
            
        }
    }
    @IBAction func btnStartAction(_ sender: UIButton) {
        if let data = subcriptionListModel?.data?[sender.tag]{
//            
//            UIAlertController.showAlert(self, aStrMessage: "Select Payment option", style: .actionSheet, aCancelBtn: "Cancel", aDistrutiveBtn: nil, otherButtonArr: ["Credit Card"], completion: { (index, title) in
//                
////                if title == "Paypal"{
////                    
////                    self.paymentWithPayPal(data: data)
////                }else
//                    if title == "Credit Card"{
//                    
////                    self.paymentWithStripe(data: data)
//                    self.paymentWithAuthorzieDotNet(data: data)
//                }
//                
//                
//            })
            
            
            LawyerAppStripePaymentAPI.sharedInstance.presentStripPaymentVC(viewController: self, amount:data.amount! , completionHandler: { (stripToken, status,error) in
                
                if stripToken != nil{
                    
                    self.callWSToStripePayWith(token: stripToken!, amount: self.subcriptionListModel!.data![0].amount!)
                
                }else{
                
                    if status == .error {
                        
                        if error != nil{
                            UIAlertController.showAlertWithOkButton(self, aStrMessage: error?.localizedDescription, completion: nil)

                        }else{
                        
                            UIAlertController.showAlertWithOkButton(self, aStrMessage: Constant.AlertMessage.kAlertMsgNotAbleToCompletePayment, completion: nil)

                        }
                    }
                
                }
               
            })
            
            
            
        }
        
        
    }
    @IBAction func btnCancelServiceAction(_ sender: UIButton) {
        
        UIAlertController.showAlert(self, aStrMessage: "Are sure want to cancel service?", style: .alert, aCancelBtn: "NO", aDistrutiveBtn: nil, otherButtonArr: ["YES"]) { (index, title) in
            

            if title == "YES"{
                var params : [String:Any] = [:]
                params["users_id"] = self.user_id
                params["access_token"] = self.token
                params["subcription_id"]  = UserDefaults.value(forKey: USER_SUBSCRIPTION_ID) as! String
                WebService.GET(Constant.API.kBaseURLWithAPIName("cancel_subscription_ios"), param: params, controller: self, callSilently: false, successBlock: { (jsonResponse) in
                    
                    
                    if (jsonResponse["settings"]["success"].string == "1"){
                        
                        UIAlertController.showAlertWithOkButton(self, aStrMessage: "Your request has been sent to admin successfully", completion: { (index, title) in
                            let u_payment_status = jsonResponse["data"][0]["u_payment_status"].string
                            UserDefaults.setValue(u_payment_status, forKey: USER_PAYMENT_SUBSCRIPTION)
                            UserDefaults.removeObject(forKey: USER_SUBSCRIPTION_ID)
                            
                            self.btnCancelService.isHidden = true
                            self.callWSForSubscriptionListing()
                        })
                        
                    }else if (jsonResponse["settings"]["success"].string == "0"){
                        
                        UIAlertController.showAlertWithOkButton(self, aStrMessage: jsonResponse["settings"]["message"].string, completion: nil)
                        
                    }else if (jsonResponse["settings"]["success"].string == "100"){
                        
                    }else{
                        
                        UIAlertController.showAlertWithOkButton(self, aStrMessage: Constant.AlertMessage.kAlertMsgSomeThingWentWrong, completion: nil)
                    }
                    
                    
                }) { (error, isTimeOut) in
                    
                }
                
                
            
            }
        }
        
        
       
        
        
    }
    func paymentWithPayPal(data : SubscriptionListData) {

        let url : String = "\(Constant.API.kBaseURLWithAPIName("subscription_paypal_payment"))?amount=\(data.amount!)&users_id=\(self.user_id)&access_token=\(self.token)"
        
        self.masterBrowser = MasterWebBrowser(nibName: "MasterWebBrowser", bundle: nil)
        self.masterBrowser?.urlToLoad = url
        self.masterBrowser?.paymentDidFinish = {
            success,jsonResponse in
            if success {
                UIAlertController.showAlertWithOkButton(self, aStrMessage: Constant.AlertMessage.kAlertMsgForPaymentSuccess, completion: nil)
                self.callWSForSubscriptionListing()
            }else{
                
                UIAlertController.showAlertWithOkButton(self, aStrMessage: "Payment cancel please try again later", completion: nil)
                
            }
            
        }
        self.navigationController?.present(self.masterBrowser!, animated: true, completion: nil)
    }
//    func paymentWithStripe(data : SubscriptionListData) {
//        
//        let stripeVC : StripePaymentViewController = StripePaymentViewController(nibName: "StripePaymentViewController", bundle: nil)
//        stripeVC.amount = Float(data.amount!)
//        let newNav : UINavigationController = UINavigationController(rootViewController: stripeVC)
//        newNav.navigationBar.isHidden = true
//        self.navigationController?.present(newNav, animated: true, completion: nil)
//        stripeVC.didFinishStripePayment = {
//            
//            stripController,token,error in
//            if error == nil{
//                
//            self.callWSToStripePayWith(token: token!, amount: data.amount!)
//                
//            }else{
//                
//                UIAlertController.showAlertWithOkButton(self, aStrMessage: "Payment cancel please try again later", completion: nil)
//                
//            }
//            
//        }
//        
//    }
//    func paymentWithAuthorzieDotNet(data : SubscriptionListData)  {
//        
//        let vc : AuthorizePaymentVC = AuthorizePaymentVC(nibName: "AuthorizePaymentVC", bundle: nil)
//        vc.shouldPayWithFramework = false
//        vc.strSubscriptionAmount = data.amount
//        vc.strItemId = "1"
//        vc.strSubscriptionPackageName = data.title
//        
//        vc.cardDetailsBlock = {
//            card,date,verificationNumber in
//            print(card)
//            print(date)
//            print(verificationNumber)
//            
//            let slashRange = date!.components(separatedBy: "/")
//            
//            let newDate = "20\(slashRange[1])" + "-" + slashRange[0]
//            
//            print(newDate)
//            
//            self.callWSToSendTransactionID(card: card!, date: newDate, id: data.subscriptionId!, amount: data.amount!)
//        
//        }
//
//        
//        self.present(vc, animated: true, completion: nil)
//        
//        
//        
//        
//    }
    func callWSToSendTransactionID(card : String,date : String,id : String,amount : String) {

        var params : [String:Any] = [:]
        params["users_id"] = user_id
        params["card_number"] = card
        params["exp_date"] = date
        params["order_unique_number"] = id
        params["amount"] = amount
        params["first_name"] = AppDelegate.getAppDelegate().userInfoData.firstName
        params["last_name"] = AppDelegate.getAppDelegate().userInfoData.lastName
        WebService.POST(Constant.API.kBaseURLWithAPIName("subscription_authorise_net_payment"), param: params, controller: self, successBlock: { (jsonResponse) in
            
            
            if (jsonResponse["settings"]["success"].string == "1"){
                
                UIAlertController.showAlertWithOkButton(self, aStrMessage: Constant.AlertMessage.kAlertMsgForPaymentSuccess, completion: { (index, title) in
                    let u_payment_status = jsonResponse["data"][0]["u_payment_status"].string
                    UserDefaults.setValue(u_payment_status, forKey: USER_PAYMENT_SUBSCRIPTION)
                    UserDefaults.set(jsonResponse["data"][0]["u_charge_id"].string, forKey: USER_SUBSCRIPTION_ID)
                    
                    self.btnCancelService.isHidden = false
                    self.callWSForSubscriptionListing()
                })
                
            }else if (jsonResponse["settings"]["success"].string == "0"){
                
                UIAlertController.showAlertWithOkButton(self, aStrMessage: jsonResponse["settings"]["message"].string, completion: nil)
                
            }else if (jsonResponse["settings"]["success"].string == "100"){
                
            }else{
                
                UIAlertController.showAlertWithOkButton(self, aStrMessage: Constant.AlertMessage.kAlertMsgSomeThingWentWrong, completion: nil)
            }
            
            
            
        }) { (error, isTimeOut) in
            
        }
    }
    func callWSToStripePayWith(token : String,amount : String)  {
        
        var params : [String:Any] = [:]
        params["token"] = token
        params["users_id"] = user_id
        params["access_token"] = self.token
        let amountValue : Float = Float(amount)! * 100
        params["amount"] = String(Int(amountValue))
        WebService.POST(Constant.API.kBaseURLWithAPIName("stripe_charge"), param: params, controller: self, successBlock: { (jsonResponse) in
            print(jsonResponse)
            if jsonResponse["settings"]["success"].string == "1"{
                
                let u_payment_status = jsonResponse["data"][0]["u_payment_status"].string
                let u_charge_id = jsonResponse["data"][0]["u_charge_id"].string
                UserDefaults.setValue(u_payment_status, forKey: USER_PAYMENT_SUBSCRIPTION)
                UserDefaults.setValue(u_charge_id, forKey: USER_SUBSCRIPTION_ID)
                self.btnCancelService.isHidden = false
                self.callWSForSubscriptionListing()
                
            }else if jsonResponse["settings"]["success"].string == "100"{
                
                Utility.reloginUserWith(viewController: self)
                
            }else{
                
                UIAlertController.showAlertWithOkButton(self, aStrMessage: jsonResponse["settings"]["message"].string, completion: nil)
                
            }
        }) { (error, isTimeOut) in
            
        }
    }

    @IBAction func btnSidePanelAction(_ sender: UIButton) {
        
        self.mm_drawerController.toggle(.left, animated: true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}

