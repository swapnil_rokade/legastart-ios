//
//  RequestDocumentViewController.swift
//  LegalStart
//
//  Created by Adapting Social on 23/05/17.
//  Copyright © 2017 Adapting Social. All rights reserved.
//

import UIKit
import Stripe
class RequestDocumentViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,UIGestureRecognizerDelegate {

    @IBOutlet weak var tblViewDocuments: UITableView!
    var documentListing : DocumentListingModel?
    var vc : MasterWebBrowser?
    let token = UserDefaults.value(forKey: ACCESSTOKEN) as! String
    let user_id = AppDelegate.getAppDelegate().userInfoData.usersId!
    var masterBrowser : MasterWebBrowser?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        // Do any additional setup after loading the view.
        self.tblViewDocuments.estimatedRowHeight = 130
        self.tblViewDocuments.rowHeight = UITableViewAutomaticDimension
        self.loadAllDocuments()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if documentListing != nil {
            return (documentListing?.data?.count)!
        }
        
        return 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : RequestDocumentVCCell = tableView.dequeueReusableCell(withIdentifier: "RequestDocumentVCCell") as! RequestDocumentVCCell
        let data = self.documentListing?.data?[indexPath.row]
        cell.lblDocumentType.text = data?.documentType
        cell.lblDocumentName.text = data?.documentName
        cell.btnRequestApprove.tag = indexPath.row
        cell.btnRequestApprove.setTitle("START", for: .normal)
        cell.btnRequestApprove.addTarget(self, action: #selector(btnOpenDocument(_:)), for: .touchUpInside)
        if data?.documentType == "Free" {
            
            
            if data?.usersDocumentStatus == "1" {
                cell.lblDocumentType.text = "Paid"
                cell.lblDocumentType.backgroundColor = UIColor.colorFromCode(0xff7561)
                cell.lblDocumentAmount.isHidden = false
                cell.lblDocumentAmount.text = ("$ \(data!.amount!)")
            }else{
                cell.lblDocumentType.text = "Free"
                cell.lblDocumentType.backgroundColor = UIColor.colorFromCode(0x27ac4d)
                cell.lblDocumentAmount.isHidden = true
            }
            
        }else if data?.documentType == "Paid" {
            cell.lblDocumentType.backgroundColor = UIColor.colorFromCode(0xff7561)
            cell.lblDocumentAmount.isHidden = false
            cell.lblDocumentAmount.text = ("$ \(data!.amount!)")
        }
        
        
        return cell
    }
    @IBAction func btnOpenDocument(_ sender: UIButton) {
        
        if let data = self.documentListing?.data?[sender.tag]{
        

                if let status = UserDefaults.value(forKey: USER_PAYMENT_SUBSCRIPTION) as? String,status == "succeeded" {

                    let storyBoard : UIStoryboard = UIStoryboard(name: "QuestionarieMaster", bundle: nil)
                    let vc : DocumentQuestionsHandlerVC = storyBoard.instantiateViewController(withIdentifier: "DocumentQuestionsHandlerVC") as! DocumentQuestionsHandlerVC
                    vc.shouldReloadDocuments = {
                    
                    shouldReload in
                        
                        if shouldReload {
                            self.loadAllDocuments()
                        }
                    
                    }
                    vc.document = data
                    self.navigationController?.pushViewController(vc, animated: true)
                    
                    
                    
                    
                }else{
                    

    
                    UIAlertController.showAlert(self, aStrMessage: Constant.AlertMessage.kAlertMsgForPaidSubscriber, style: .alert, aCancelBtn: "Cancel", aDistrutiveBtn: nil, otherButtonArr: ["Subscribe"], completion: { (index, title) in
                        if title == "Subscribe"{
                            
                            let storyBoard : UIStoryboard = UIStoryboard(name: "DocumentMaster", bundle: nil)
                            
                            let centerVC : SubscriptionListingViewController = storyBoard.instantiateViewController(withIdentifier: "SubscriptionListingViewController") as! SubscriptionListingViewController
                            AppDelegate.sharedInstance().selectedMenuIndex = 10
                            self.mm_drawerController?.setCenterView(centerVC, withCloseAnimation: true, completion: nil)
                            

                        }
                    })
                }
        }
        
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func paymentWithPayPal(data : DocumentListingData) {
        let url : String = "\(Constant.API.kBaseURLWithAPIName("paypal_payment"))?document_name=\(data.documentName!)&amount=\(data.amount!)&users_id=\(self.user_id)&document_id=\(data.documentId!)&access_token=\(self.token)"
        
        self.masterBrowser = MasterWebBrowser(nibName: "MasterWebBrowser", bundle: nil)
        self.masterBrowser?.urlToLoad = url
        self.masterBrowser?.paymentDidFinish = {
            success,jsonResponse in
            if success {
                UIAlertController.showAlertWithOkButton(self, aStrMessage: "Payment successfull accepted", completion: nil)
                self.loadAllDocuments()
            }else{
                
                UIAlertController.showAlertWithOkButton(self, aStrMessage: "Payment cancel please try again later", completion: nil)
                
            }
            
        }
        self.navigationController?.present(self.masterBrowser!, animated: true, completion: nil)
    }
    func paymentWithStripe(data : DocumentListingData) {
        
//        let stripeVC : StripePaymentViewController = StripePaymentViewController(nibName: "StripePaymentViewController", bundle: nil)
//        stripeVC.amount = Float(data.amount!)
//        let newNav : UINavigationController = UINavigationController(rootViewController: stripeVC)
//        newNav.navigationBar.isHidden = true
//        self.navigationController?.present(newNav, animated: true, completion: nil)
//        stripeVC.didFinishStripePayment = {
//            
//            stripController,token,error in
//            if error == nil{
//                
//                self.callWSToStripePayWith(token: token!, document_id: data.documentId!, amount: data.amount!)
//                
//                
//            }else{
//                
//                UIAlertController.showAlertWithOkButton(self, aStrMessage: "Payment cancel please try again later", completion: nil)
//                
//            }
//            
//        }
        
        
        LawyerAppStripePaymentAPI.sharedInstance.presentStripPaymentVC(viewController: self, amount:data.amount! , completionHandler: { (stripToken, status,error) in
            
            
            if stripToken != nil{
                
                
                self.callWSToStripePayWith(token: stripToken!, document_id: data.documentId!, amount: data.amount!)
                
                
                
            }else{
                
                if status == .error {
                    UIAlertController.showAlertWithOkButton(self, aStrMessage: Constant.AlertMessage.kAlertMsgNotAbleToCompletePayment, completion: nil)
                    
                }
                
            }
            
        })

    }
    func callWSToStripePayWith(token : String,document_id : String,amount : String)  {
        
        var params : [String:Any] = [:]
        params["token"] = token
        params["users_id"] = user_id
        params["access_token"] = self.token
        params["document_id"] = document_id
        let amountValue : Float = Float(amount)! * 100
        params["amount"] = String(Int(amountValue))
        WebService.POST(Constant.API.kBaseURLWithAPIName("stripe_charge_document"), param: params, controller: self, successBlock: { (jsonResponse) in
            print(jsonResponse)
            if jsonResponse["settings"]["success"].string == "1"{
                
                
                self.loadAllDocuments()
                
            }else if jsonResponse["settings"]["success"].string == "100"{
                
                Utility.reloginUserWith(viewController: self)
                
            }else{
                
                UIAlertController.showAlertWithOkButton(self, aStrMessage: jsonResponse["settings"]["message"].string, completion: nil)
                
            }
        }) { (error, isTimeOut) in
            
        }
    }

    //MARK: CALL WEBSERVICE
    func loadAllDocuments()  {
        let param : [String:Any] = ["users_id":user_id,"access_token":token]
        WebService.GET(Constant.API.kBaseURLWithAPIName("document_listing"), param: param, controller: self, callSilently: false, successBlock: { (jsonResponse) in
            let tempData : DocumentListingModel = DocumentListingModel(json: jsonResponse)
            if tempData.settings?.success == "1"{
            self.documentListing = tempData
            self.tblViewDocuments.reloadData()
            }else if tempData.settings?.success == "100" {
                Utility.reloginUserWith(viewController: self)
            }else{
                UIAlertController.showAlertWithOkButton(self, aStrMessage: tempData.settings?.message, completion: nil)
            }
        }) { (error, true) in
            
        }
    }
    
    //MARK: BUTTON ACTIONS
    @IBAction func btnSidePanelAction(_ sender: UIButton) {
        
        self.mm_drawerController.toggle(.left
            , animated: true, completion: nil)
        
    }

    
    //MARK: MEMORY WARNING
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
