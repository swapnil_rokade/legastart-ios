//
//  ProfileVC.swift
//  LegalStart
//
//  Created by Adapting Social on 4/25/17.
//  Copyright © 2017 Adapting Social. All rights reserved.
//

import UIKit
import SDWebImage

class ProfileVC: UIViewController, UITextFieldDelegate {
    
    // MARK: - Global Outlets
    @IBOutlet weak var btnProfilePic: UIButton!
    @IBOutlet weak var btnSaveProfile: UIButton!
    
    @IBOutlet weak var imgViewProfilePic: UIImageView!
    @IBOutlet weak var txtFirstName: UITextField!
    @IBOutlet weak var txtLastName: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtMobileNo: UITextField!
    @IBOutlet weak var txtDateOfBirth: UITextField!

    // MARK: - Global Variables
    var isViewSetUp = false
    var objProfileModel : ProfileModel?
    var dateOfBirth : Date?
    var imgSelected : UIImage?
    var imgBackgroundSelected : UIImage?
    let token = UserDefaults.value(forKey: ACCESSTOKEN) as! String
    // MARK: - ViewController Life Cycle

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.callWebServiceForMyProfile()
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleTapGesture))
        self.imgViewProfilePic.addGestureRecognizer(tapGesture)

    }
    
    override func viewDidLayoutSubviews() {
        
        if !isViewSetUp {
            self.addMasklayerOnProfileImage()
            isViewSetUp = true
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - UI Setup
    func addMasklayerOnProfileImage(){
        // Set background view mask layer...
        let path = UIBezierPath()
        path.move(to: self.view.bounds.origin)
        path.addLine(to: CGPoint(x: self.view.frame.width, y:self.view.bounds.origin.y))
        
        let imgHeight = self.view.frame.height * 0.262
        let difference = imgHeight * 0.45
//        if Constant.DeviceType.IS_IPHONE_5 {
//            difference = 50.0
//        }
//        else
//        {
//            difference = 78.0
//        }
        path.addLine(to: CGPoint(x: self.view.frame.width, y:imgHeight - difference))
        path.addLine(to: CGPoint(x: self.view.bounds.origin.x, y:imgHeight))
        path.addLine(to: self.view.bounds.origin)
        
        let maskLayer = CAShapeLayer()
        
        maskLayer.path = path.cgPath
        imgViewProfilePic.layer.mask = maskLayer
        
        imgViewProfilePic.clipsToBounds = true
        
        // Set Profile pic corner radius...
        btnProfilePic.layer.cornerRadius = btnProfilePic.frame.size.width/2
        btnProfilePic.layer.borderWidth = 1.0
        btnProfilePic.layer.borderColor = UIColor.colorFromCode(0xFF7561).cgColor
        btnProfilePic.clipsToBounds = true

    }
    
    func handleTapGesture() {
        self.view.hideKeyBoard()
        
        Utility.sharedInstance.openCameraInController(self, completionBlock: { (img) in
            if img != nil {
                
                self.imgBackgroundSelected = img
                self.imgViewProfilePic.image = self.imgBackgroundSelected
            }
        })

    }

    // MARK: - Button Click Events
    
    @IBAction func btnSideMenuClick(_ sender: UIButton)
    {
        self.mm_drawerController?.toggle(.left, animated: true, completion: nil)
    }

    @IBAction func btnProfileClick(_ sender: UIButton) {
        
        self.view.hideKeyBoard()
        
        Utility.sharedInstance.openCameraInController(self, completionBlock: { (img) in
            if img != nil {
            
                self.imgSelected = img
                self.btnProfilePic.imageView?.contentMode = .scaleAspectFill
                self.btnProfilePic.setImage(self.imgSelected, for: .normal)
            }
        })

    }

    @IBAction func btnDateOfBirthClick(_ sender: UIButton) {
        
        Utility.sharedInstance.addPicker(self, onTextField: self.txtDateOfBirth , typePicker: "Date", pickerArray: [], setMaxDate:true) { (picker,buttonindex,firstindex) in
            
            if (picker != nil)
            {
                let datePicker = picker as! UIDatePicker
                
                let strDate = Utility.sharedInstance.getStringFromDate(Constant.kDateFormat_MMDDYYYY, date: datePicker.date)
                
                self.dateOfBirth = datePicker.date
                self.txtDateOfBirth.text = strDate
                
                print(strDate)
            }
            
            self.txtDateOfBirth.resignFirstResponder()
        }

    }
    
    @IBAction func btnSaveProfileClick(_ sender: UIButton) {
        
        self.view.hideKeyBoard()
        sender.isUserInteractionEnabled = false;
        
        if validationForUpdateProfile() {
            
            self.callWebServiceForUpdateProfile()
            
        }else{
            
            let delayTime = DispatchTime.now() + Double(Int64(1.5 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
            DispatchQueue.main.asyncAfter(deadline: delayTime) {
                sender.isUserInteractionEnabled = true;
            };
        }

    }

    // MARK: - TextField Delegates
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {

        if (textField == txtFirstName) {
            txtLastName.becomeFirstResponder()
        }
        else if (textField == txtLastName) {
            txtMobileNo.becomeFirstResponder()
        }

        return true
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        if textField == txtMobileNo {
            
            // Validate phone no value up to 15 digits...
            let newString = (textField.text! as NSString).replacingCharacters(in: range, with: string)
            
            return newString.characters.count > MAX_LENGTH_FOR_PHONE_NO ? false : true
        }
        
        return true
        
    }
    
    // MARK: - Webservice Methods
    
    func callWebServiceForMyProfile() {
        
        let aStrUserId = AppDelegate.getAppDelegate().userInfoData.usersId!
        let aDictParam:[String:Any] = ["users_id":aStrUserId,"access_token":token]
        
        WebService.POST(Constant.API.kBaseURLWithAPIName("view_users_profile"), param: aDictParam, controller: self, successBlock: { (jsonResponse) in
            
            print("success response is received")
            self.objProfileModel = nil
            self.objProfileModel = ProfileModel(json: jsonResponse)
            if (self.objProfileModel?.settings?.success == "1") {
                
                self.showProfileData()
            }else if (self.objProfileModel?.settings?.success == "100") {
                Utility.reloginUserWith(viewController: self)
            }else{
                UIAlertController.showAlertWithOkButton(self, aStrMessage: self.objProfileModel?.settings?.message, completion: nil)
            }
        }) { (error, isTimeOut) in
            
            if isTimeOut {
                print("Request Timeout")
            } else {
                print(error?.localizedDescription ?? "")
            }
            
        }
        
    }
    
    func callWebServiceForUpdateProfile() {
        
        let aStrUserId = AppDelegate.getAppDelegate().userInfoData.usersId!
        
        var aStrDateOfBirth = ""
        if dateOfBirth != nil {
            aStrDateOfBirth = Utility.sharedInstance.getStringFromDate(Constant.kDateFormat_YYYYMMDD, date: dateOfBirth!)
        }
        
        var aDictParam:[String:Any] = ["users_id":aStrUserId, "first_name":self.txtFirstName.text!, "last_name":self.txtLastName.text!, "email":self.txtEmail.text!, "mobile_number":self.txtMobileNo.text!, "date_of_birth": aStrDateOfBirth,"access_token":token]
        
        if self.imgSelected != nil {
            aDictParam["profile_image"] = self.imgSelected! as UIImage
        }

        if self.imgBackgroundSelected != nil {
            aDictParam["users_background_image"] = self.imgBackgroundSelected! as UIImage
        }
        
        WebService.WSPostAPIMultiPart(url: Constant.API.kBaseURLWithAPIName("update_users_profile"), params: aDictParam, controller: self, progressBlock: { (progress) in
            
        }, successBlock: { (jsonResponse) in
            
            print(jsonResponse)
            
            self.btnSaveProfile.isUserInteractionEnabled = true;
            if (jsonResponse["settings"]["success"].string == "1"){
                
                
                let aStrProfilePic = jsonResponse["data"][0]["profile_image"].string
                let aStrBgProfilePic = jsonResponse["data"][0]["users_background_image"].string
                

                let userData = AppDelegate.getAppDelegate().userInfoData

                userData!.blogPasswsord = jsonResponse["data"][0]["blog_passwsord"].string
                userData!.subscriptionId = jsonResponse["data"][0]["subscription_id"].string
                userData!.blogEmail = jsonResponse["data"][0]["blog_email"].string
                userData!.moblieNumber = jsonResponse["data"][0]["moblie_number"].string
                userData!.uPaymentStatus = jsonResponse["data"][0]["u_payment_status"].string
                userData!.profilePicture = aStrProfilePic
                userData!.firstName = jsonResponse["data"][0]["first_name"].string
                userData!.usersId = jsonResponse["data"][0]["users_id"].string
                userData!.lastName = jsonResponse["data"][0]["last_name"].string
                userData!.blogId = jsonResponse["data"][0]["blog_id"].string
                
                let userDataInfo = NSKeyedArchiver.archivedData(withRootObject: userData!)
                UserDefaults.setValue(userDataInfo, forKey: Constant.UserDefaultKeys.kUserDataModelKey)
                UserDefaults.synchronize()
                AppDelegate.getAppDelegate().userInfoData = userData
                
                
            
                SDImageCache.shared().store(self.imgBackgroundSelected, forKey:aStrBgProfilePic)
                SDImageCache.shared().store(self.imgSelected, forKey:aStrProfilePic)
                
                UIAlertController.showAlertWithOkButton(self, aStrMessage: jsonResponse["settings"]["message"].string, completion: nil)
            }else if (jsonResponse["settings"]["success"].string == "0"){
                
                UIAlertController.showAlertWithOkButton(self, aStrMessage: jsonResponse["settings"]["message"].string, completion: nil)
                
            }else if (jsonResponse["settings"]["success"].string == "100") {
                Utility.reloginUserWith(viewController: self)
            }else{
                
                UIAlertController.showAlertWithOkButton(self, aStrMessage: Constant.AlertMessage.kAlertMsgSomeThingWentWrong, completion: nil)
            }
        }) { (error, isTimeOut) in
            
            self.btnSaveProfile.isUserInteractionEnabled = true;
            if isTimeOut {
                print("Request Timeout")
            } else {
                print(error?.localizedDescription ?? "")
            }

        }

//        WebService.UPLOAD(Constant.API.kBaseURLWithAPIName("update_users_profile"), param: aDictParam, controller: self, successBlock: { (jsonResponse) in
//            
//            print("success response is received")
//            self.btnSaveProfile.isUserInteractionEnabled = true;
//            
//            if  (jsonResponse["settings"]["success"].string == "1") {
//                
//                
//            }else{
//                UIAlertController.showAlertWithOkButton(self, aStrMessage: jsonResponse["settings"]["message"].string, completion: nil)
//            }
//        }) { (error, isTimeOut) in
//            
//            self.btnSaveProfile.isUserInteractionEnabled = true;
//            if isTimeOut {
//                print("Request Timeout")
//            } else {
//                print(error?.localizedDescription ?? "")
//            }
//            
//        }
//        
    }
    // MARK: - Other Methods
    
    func showProfileData() {
        
        self.txtFirstName.text = self.objProfileModel?.data?[0].firstName
        self.txtLastName.text = self.objProfileModel?.data?[0].lastName
        self.txtEmail.text = self.objProfileModel?.data?[0].email
        self.txtMobileNo.text = self.objProfileModel?.data?[0].moblieNumber
        
        if self.objProfileModel?.data?[0].dateOfBirth != nil && self.objProfileModel?.data?[0].dateOfBirth != "0000-00-00" {
            dateOfBirth = Utility.sharedInstance.getDateFromString((self.objProfileModel?.data?[0].dateOfBirth)!, dateFormat: Constant.kDateFormat_YYYYMMDD)
            self.txtDateOfBirth.text = Utility.sharedInstance.getStringFromDate(Constant.kDateFormat_MMDDYYYY, date:dateOfBirth!)
        }
        
        self.imgViewProfilePic.sd_setImage(with: URL.init(string: (self.objProfileModel?.data?[0].backGroundImage)!), placeholderImage: UIImage(named: "profile_pic"))
        self.imgViewProfilePic.clipsToBounds = true
        self.btnProfilePic.sd_setImage(with: URL.init(string: (self.objProfileModel?.data?[0].profileImage)!), for: .normal, placeholderImage: UIImage(named: "profile_pic"))
        self.btnProfilePic.imageView?.contentMode = .scaleAspectFill
        
//        self.imgSelected = self.btnProfilePic.imageView?.image
//        self.imgBackgroundSelected = self.imgViewProfilePic.image
        
        // Set Profile pic corner radius...
        btnProfilePic.layer.cornerRadius = btnProfilePic.frame.size.width/2
        btnProfilePic.layer.borderWidth = 1.0
        btnProfilePic.layer.borderColor = UIColor.colorFromCode(0xFF7561).cgColor
        btnProfilePic.clipsToBounds = true

//        UserDefaults.set(self.objProfileModel?.data?[0].backGroundImage, forKey: USER_BACKGROUND_PIC)
        
    }
  
    func validationForUpdateProfile() -> Bool {
        
        if self.txtFirstName.text!.isEmptyString() {
            UIAlertController.showAlertWithOkButton(self, aStrMessage: Constant.AlertMessage.kAlertMsgEnterFisrtName, completion: nil)
            return false
        }
        else if self.txtLastName.text!.isEmptyString(){
            UIAlertController.showAlertWithOkButton(self, aStrMessage: Constant.AlertMessage.kAlertMsgEnterLastName, completion: nil)
            return false
        }
        else if !self.txtEmail.isValidEmail() {
            UIAlertController.showAlertWithOkButton(self, aStrMessage: Constant.AlertMessage.kAlertMsgValidEmail, completion: nil)
            return false
        }
        else if self.txtMobileNo.text!.isEmptyString(){
            UIAlertController.showAlertWithOkButton(self, aStrMessage: Constant.AlertMessage.kAlertMsgEnterMobileNo, completion: nil)
            return false
        }
        else if !self.txtMobileNo.text!.validateMinimumLength(10) {
            UIAlertController.showAlertWithOkButton(self, aStrMessage: Constant.AlertMessage.kAlertMsgValidPhoneNumber, completion: nil)
            return false
        }
        else if self.txtDateOfBirth.text!.isEmptyString(){
            UIAlertController.showAlertWithOkButton(self, aStrMessage: Constant.AlertMessage.kAlertMsgSelectDateOfBirth, completion: nil)
            return false
        }

        return true
    }
}
