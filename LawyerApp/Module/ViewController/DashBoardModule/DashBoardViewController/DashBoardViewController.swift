//
//  DashBoardViewController.swift
//  LegalStart
//
//  Created by Adapting Social on 29/05/17.
//  Copyright © 2017 Adapting Social. All rights reserved.
//

import UIKit

class DashBoardViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {

    
    
    @IBOutlet weak var mainCollectionView: UICollectionView!
    
    
     var arrMenuOptions = [[String:String]]()
    override func viewDidLoad() {
        super.viewDidLoad()
 
        if let path = Bundle.main.path(forResource: "LawyerApp_LeftOptions", ofType: "plist") {
            
            if let arrMenu = NSArray(contentsOfFile: path)
            {
                let tempArr = arrMenu[0] as! [[String : String]]
                for data in tempArr {
                    if data["DisplayName"] == "Home" || data["DisplayName"] == "Other Services" || data["DisplayName"] == "Terms & Conditions" || data["DisplayName"] == "Privacy Policy" || data["DisplayName"] == "Change Password" || data["DisplayName"] == "Support" {
                        
                    }else{
                        arrMenuOptions.append(data)
                    }
                }
            }
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.mainCollectionView.contentSize = CGSize(width: self.mainCollectionView.frame.size.width, height: self.mainCollectionView.frame.size.width)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
            return arrMenuOptions.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
            let cell : DashBoardVCCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "DashBoardVCCollectionViewCell", for: indexPath) as! DashBoardVCCollectionViewCell
            
            let aStrImageName = arrMenuOptions[indexPath.row]["DashBoardImage"]
            
            cell.icon.image = UIImage.init(named: aStrImageName!)
            cell.title.text = arrMenuOptions[indexPath.row]["DisplayName"]
            
        
            return cell
       
       
    }
    func collectionView(_ collectionView: UICollectionView,layout collectionViewLayout: UICollectionViewLayout,sizeForItemAt indexPath: IndexPath) -> CGSize {
        
            let data = arrMenuOptions[indexPath.row]
        
            let title : String = data["DisplayName"]!
            var size : CGFloat = 0
            if Constant.DeviceType.IS_IPHONE_5 {
                size = 110
            }else{
                size = 130
            }
        
            
            let font : UIFont = UIFont(name: "Roboto-Regular", size: 15.0)!
            let height : CGFloat = title.heightOfString(usingFont: font)

            return CGSize(width: UIScreen.main.bounds.size.width/2, height: (collectionView.frame.size.height / CGFloat(arrMenuOptions.count)) * 2 )
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
//        if indexPath.row == 0 || indexPath.row == 2 || indexPath.row == 1 || indexPath.row == 4 || indexPath.row == 5 || indexPath.row == 7 || indexPath.row == 8,
//            let status = UserDefaults.value(forKey: USER_PAYMENT_SUBSCRIPTION) as? String,status != "succeeded"{
//
//            UIAlertController.showAlert(self, aStrMessage: Constant.AlertMessage.kAlertMsgForPaidSubscriber, style: .alert, aCancelBtn: "Cancel", aDistrutiveBtn: nil, otherButtonArr: ["Subscribe"], completion: { (index, title) in
//                if title == "Subscribe"{
//                    let subscribeIndexPath : IndexPath = IndexPath(row: 9, section: 0)
//                    self.toggleViewControllerWith(indexPath: subscribeIndexPath)
//                }
//            })
//            return;
//        }else{
            AppDelegate.sharedInstance().selectedMenuIndex = indexPath.row + 1
            if arrMenuOptions[indexPath.row]["DisplayName"] == "Create a Document" {
                UIAlertController.showAlert(self, aStrMessage: "All documents are custom made by an attorney in 1-3 Business Days. \n No Forms. No Templates", style: .alert, aCancelBtn: nil, aDistrutiveBtn: nil, otherButtonArr: ["Got it"], completion: { (index, title) in
                    self.toggleViewControllerWith(indexPath: indexPath)
                    
                })
            }else if arrMenuOptions[indexPath.row]["DisplayName"] == "Appointments"{
                UIAlertController.showAlert(self, aStrMessage: "All LegalStart members get one 15-minute phone call with an attorney each month.", style: .alert, aCancelBtn: nil, aDistrutiveBtn: nil, otherButtonArr: ["Got it"], completion: { (index, title) in
                    self.toggleViewControllerWith(indexPath: indexPath)
                })
            }else{
                self.toggleViewControllerWith(indexPath: indexPath)
            }
        //}
    }
    
    func toggleViewControllerWith(indexPath : IndexPath) {
    
        
        let aStrDisplayName = arrMenuOptions[indexPath.row]["DisplayName"]
        let aStrStoryboardName = arrMenuOptions[indexPath.row]["Storyboard"]
        var aStrStoryboardId = arrMenuOptions[indexPath.row]["StoryboardId"]
        let aStoryboard = UIStoryboard(name: aStrStoryboardName!, bundle: nil)
        
        
        if aStrStoryboardId == "EmailChatViewController"  {
            if let isDiaLogCreated = UserDefaults.value(forKey: IS_CHAT_DIALOG_CREATED) as? String,isDiaLogCreated == "YES"{
                
                aStrStoryboardId = "ChatHandlerViewController"
                
            }
            let centerVC = aStoryboard.instantiateViewController(withIdentifier: aStrStoryboardId!)
            
            self.mm_drawerController?.setCenterView(centerVC, withCloseAnimation: true, completion: nil)
            
            
            
        }else{
            let centerVC = aStoryboard.instantiateViewController(withIdentifier: aStrStoryboardId!)
            
            self.mm_drawerController?.setCenterView(centerVC, withCloseAnimation: true, completion: nil)
        }

    
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnSidePanelAction(_ sender: UIButton) {
        self.mm_drawerController.toggle(.left, animated: true, completion: nil)
    }


}
