//
//  TestingViewController.swift
//  LawyerApp
//
//  Created by indianic on 18/04/17.
//  Copyright © 2017 Indianic. All rights reserved.
//

import UIKit
import SwiftyDropbox
import GoogleAPIClientForREST
import GTMOAuth2
class TestingViewController: UIViewController,UIButtonMasterDocumentPickerDelegate,UIButtonMasterDocumentUploaderDelegate {

    var selectedURL : URL?
    
    @IBOutlet var btnUpload: UIButtonMasterDocumentUploader!
    override func viewDidLoad() {
        super.viewDidLoad()
//     
//        GTMOAuth2ViewControllerTouch.removeAuthFromKeychain(forName: kAuthKeyChainName)
//        print(GIDSignIn.sharedInstance().hasAuthInKeychain())
//        GIDSignIn.sharedInstance().signOut()
//        GIDSignIn.sharedInstance().disconnect()
//        
    }

    func shouldOpenMasterDocumentPicker(picker : UIButtonMasterDocumentPicker) -> Bool {
        return true
    }
    func masterDocumentDidFinishPicking(fileURL: URL, fileExtension: String) {
        selectedURL = fileURL
        btnUpload.exportFileURL = selectedURL
    }
    
    func shouldUploadDocument(uploaded : UIButtonMasterDocumentUploader) -> Bool{

        return true
    }
    func masterDocumentUploaded(fileURL: URL, fileExtension: String) {
        
    }
    
}
