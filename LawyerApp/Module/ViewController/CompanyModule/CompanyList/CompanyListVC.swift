//
//  CompanyListVC.swift
//  LegalStart
//
//  Created by Adapting Social on 5/1/17.
//  Copyright © 2017 Adapting Social. All rights reserved.
//

import UIKit

class CompanyListVC: UIViewController, UITableViewDelegate,UITableViewDataSource, UIGestureRecognizerDelegate {

    @IBOutlet weak var lblMessage : UILabel!
    @IBOutlet var tblViewCompanyList: UITableView!
    @IBOutlet var tblViewCategoryList: UITableView!
    @IBOutlet var viewCategoryData: UIView!
    @IBOutlet weak var activityIndicatorBottom: UIActivityIndicatorView!
    
    var objCompanyListModel : CompanyListModel?
    var objCompanyCategoryModel : CompanyCategoryModel?
    var arrCompanyList : [CompanyListData] = []
    var strSelectedCategoryId = ""
    var selectedCategoryIndex = 0
    var isDataLoading = false
    let token = UserDefaults.value(forKey: ACCESSTOKEN) as! String
    let user_id = AppDelegate.getAppDelegate().userInfoData.usersId!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        tblViewCompanyList.estimatedRowHeight = 80.0
        tblViewCompanyList.rowHeight = UITableViewAutomaticDimension

        self.callWebServiceForCompanyCategoryList()
        self.callWebServiceForCompanyList()

        // Add tap gesture in CategoryData View...
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleTapGesture))
        tapGesture.numberOfTapsRequired = 1
        tapGesture.delegate = self
        viewCategoryData.addGestureRecognizer(tapGesture)

    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - UITableViewDataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if tableView == tblViewCategoryList {
            if (self.objCompanyCategoryModel?.data != nil) {
                return (self.objCompanyCategoryModel?.data?.count)!
            }
            else
            {
                return 0
            }

        }
        else
        {
            return arrCompanyList.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if tableView == tblViewCategoryList {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell")
            let objCategoryData = self.objCompanyCategoryModel?.data?[indexPath.row]
            
            cell?.textLabel?.text = objCategoryData?.companyCategoryName
            
            
            if indexPath.row == selectedCategoryIndex {
                cell?.textLabel?.textColor = UIColor.colorFromCode(0x4A4A4A)
            }
            else
            {
                cell?.textLabel?.textColor = UIColor.colorFromCode(0x9B9B9B)
            }
            
            return cell!
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CompanyListCell") as! CompanyListCell
            let objCompanyData = arrCompanyList[indexPath.row]
            
            cell.imgViewCompany.sd_setImage(with: URL.init(string: (objCompanyData.companyImage)!), placeholderImage: UIImage(named: "profile_pic"))
            cell.lblCompanyName.text = objCompanyData.companyName
            cell.lblAddress.text = objCompanyData.cityState
            cell.lblCompanyCategoryName.text = objCompanyData.companyCategoryName
//            cell.lblEmail.text = objCompanyData.companyEmail
////            cell.btnPhoneNo.tag = indexPath.row
////            cell.btnEmail.tag = indexPath.row
            cell.btnDirections.tag = indexPath.row
            
            return cell

        }
    }
    
    // MARK: - UITableViewDelegate
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if tableView == tblViewCategoryList {
            selectedCategoryIndex = indexPath.row
            self.hideCategoryData()
        }
        else
        {
//            let objCompanyDetailVC = self.storyboard?.instantiateViewController(withIdentifier: "CompanyDetailVC") as! CompanyDetailVC
//            objCompanyDetailVC.objCompanyData = arrCompanyList[indexPath.row]
//            self.navigationController?.pushViewController(objCompanyDetailVC, animated: true)
        }
    }

    // MARK: - Button Click Events
    
    @IBAction func btnSideMenuClick(_ sender: UIButton)
    {
        self.mm_drawerController?.toggle(.left, animated: true, completion: nil)
    }
    
    @IBAction func btnFilterClick(_ sender: UIButton) {
        
        self.showCategoryData()
    }
    
    @IBAction func btnPhoneNoClick(_ sender: UIButton){
        
        let objCompanyData = arrCompanyList[sender.tag]
        if objCompanyData.mobileNumber != nil {
            Utility.sharedInstance.doCall(strPhoneNo: objCompanyData.mobileNumber!, controller: self)
        }
        
    }

    @IBAction func btnEmailClick(_ sender: UIButton){
        
        let objCompanyData = arrCompanyList[sender.tag]
        if objCompanyData.companyEmail != nil {
            Utility.sharedInstance.doEmail(strEmail: objCompanyData.companyEmail!, controller: self)
        }

    }

    @IBAction func btnDirectionsClick(_ sender: UIButton){
        
//        let objCompanyData = arrCompanyList[sender.tag]
//        let objMapVC = self.storyboard?.instantiateViewController(withIdentifier: "MapVC") as! MapVC
//        objMapVC.objCompanyData = objCompanyData
//        self.navigationController?.pushViewController(objMapVC, animated: true)
        if let status = UserDefaults.value(forKey: USER_PAYMENT_SUBSCRIPTION) as? String,status == "succeeded" {
            let objCompanyDetailVC = self.storyboard?.instantiateViewController(withIdentifier: "CompanyDetailVC") as! CompanyDetailVC
            objCompanyDetailVC.objCompanyData = arrCompanyList[sender.tag]
            self.navigationController?.pushViewController(objCompanyDetailVC, animated: true)
        }else{
        

            UIAlertController.showAlert(self, aStrMessage: Constant.AlertMessage.kAlertMsgForPaidSubscriber, style: .alert, aCancelBtn: "Cancel", aDistrutiveBtn: nil, otherButtonArr: ["Subscribe"], completion: { (index, title) in
                if title == "Subscribe"{
                    
                    let storyBoard : UIStoryboard = UIStoryboard(name: "DocumentMaster", bundle: nil)
                    
                    let centerVC : SubscriptionListingViewController = storyBoard.instantiateViewController(withIdentifier: "SubscriptionListingViewController") as! SubscriptionListingViewController
                    AppDelegate.sharedInstance().selectedMenuIndex = 10
                    self.mm_drawerController?.setCenterView(centerVC, withCloseAnimation: true, completion: nil)
                    
                    
                }
            })
        }
        

    }
    
    // MARK: - UIGestureRecognizerDelegate Methods
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        
        // Disable tap gesture for tableview...
        if (touch.view?.isDescendant(of: tblViewCategoryList))! {
            return false
        }
        return true
    }
    // MARK: - Other Methods
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let offset = (scrollView.contentOffset.y - (scrollView.contentSize.height - scrollView.frame.size.height))
        if offset >= 0 && offset <= 40{
            self.loadMoreData()
        }
    }
    
    func loadMoreData(){
        if self.objCompanyListModel?.settings?.nextPage == "1" && !isDataLoading {
            isDataLoading = true
            let aCurPage = Int((self.objCompanyListModel?.settings?.currPage)!)
            self.callWebServiceForCompanyList(isShowActivityIndicator: false, pageIndex: aCurPage! + 1)
        }
    }
    
    func handleTapGesture() {
        
        self.hideCategoryData()
    }
    
    func showCompanyData(){
        
        if (self.arrCompanyList.count > 0) {
            
            lblMessage.isHidden = true
            tblViewCompanyList.isHidden = false
            tblViewCompanyList.reloadData()
        }
        else
        {
            lblMessage.isHidden = false
            tblViewCompanyList.isHidden = true
            lblMessage.text = Constant.NoDataFound.kCompanyMessage
        }
        
    }

    func showCategoryData() {
        
//        viewCategoryData.isHidden = false
//        viewCategoryData.alpha = 0.0
        
        UIView.animate(withDuration: 0.5, delay: 0.0, options: UIViewAnimationOptions.curveEaseIn, animations: {
            self.viewCategoryData.isHidden = false
        }, completion: nil)
        
        self.tblViewCategoryList.reloadData()
    }
    
    func hideCategoryData() {
        
        self.viewCategoryData.isHidden = false
        viewCategoryData.alpha = 1.0
        
        UIView.animate(withDuration: 0.5, delay: 0.0, options: UIViewAnimationOptions.curveEaseIn, animations: {
//            self.viewCategoryData.alpha = 0.0
            self.viewCategoryData.isHidden = true
        }, completion: nil)

        // Reload data for selected category...
        let strCategoryId = self.objCompanyCategoryModel?.data?[selectedCategoryIndex].companyCategoryId
        
        if strSelectedCategoryId != strCategoryId {
            
            strSelectedCategoryId = strCategoryId!
            // Reload company list data for new selected category...
            self.arrCompanyList.removeAll()
            self.callWebServiceForCompanyList()
        }
        
    }
    
    // MARK: - Webservice Methods
    
    func callWebServiceForCompanyCategoryList() {
        
        WebService.POST(Constant.API.kBaseURLWithAPIName("company_category_listing"), param:["access_token":token,"users_id":user_id], controller: self, successBlock: { (jsonResponse) in
            
            print("success response is received")
            self.objCompanyCategoryModel = CompanyCategoryModel(json: jsonResponse)
            if (self.objCompanyCategoryModel?.settings?.success == "1") {
                
                // Add "All" option...
                var aMutDict :  [String:Any] = [:]
                aMutDict["company_category_id"] = ""
                aMutDict["company_category_name"] = "All"
                
                let objCompanyCategoryData = CompanyCategoryData(object: aMutDict)
                var arrData = self.objCompanyCategoryModel?.data
                arrData?.insert(objCompanyCategoryData, at: 0)
                self.objCompanyCategoryModel?.data = arrData

                self.tblViewCategoryList.reloadData()
                
                let delayTime = DispatchTime.now() + Double(Int64(1.5 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
                DispatchQueue.main.asyncAfter(deadline: delayTime) {
                    if (self.tblViewCategoryList.contentSize.height < self.tblViewCategoryList.frame.size.height) {
                        self.tblViewCategoryList.isScrollEnabled = false
                    }
                }

            }else if (self.objCompanyCategoryModel?.settings?.success == "100") {
                Utility.reloginUserWith(viewController: self)
            }else{
                UIAlertController.showAlertWithOkButton(self, aStrMessage: self.objCompanyListModel?.settings?.message, completion: nil)
            }
        }) { (error, isTimeOut) in
            
            if isTimeOut {
                print("Request Timeout")
            } else {
                print(error?.localizedDescription ?? "")
            }
            
        }
        
    }
    
    func callWebServiceForCompanyList(isShowActivityIndicator : Bool = true, pageIndex: Int = 1) {
    
        let aDictParam:[String:Any] = ["company_category_id":strSelectedCategoryId, "page_index":pageIndex,"access_token":token,"users_id":user_id]
        
        if !isShowActivityIndicator {
            activityIndicatorBottom.startAnimating()
        }

        WebService.GET(Constant.API.kBaseURLWithAPIName("company_listing"), param: aDictParam, controller: self, callSilently:
            !isShowActivityIndicator, successBlock: { (jsonResponse) in
            
                if !isShowActivityIndicator {
                    self.activityIndicatorBottom.stopAnimating()
                }
                
                self.isDataLoading = false
                print("success response is received")
                self.objCompanyListModel = CompanyListModel(json: jsonResponse)
                if (self.objCompanyListModel?.settings?.success == "1") {
                    
                    self.arrCompanyList.append(contentsOf:(self.objCompanyListModel?.data)!)
                    self.showCompanyData()
                }else if (self.objCompanyCategoryModel?.settings?.success == "100") {
                    Utility.reloginUserWith(viewController: self)
                }else{
                    UIAlertController.showAlertWithOkButton(self, aStrMessage: self.objCompanyListModel?.settings?.message, completion: nil)
                }


        }) { (error, isTimeOut) in
            
            if !isShowActivityIndicator {
                self.activityIndicatorBottom.stopAnimating()
            }
            
            self.isDataLoading = false
            if isTimeOut {
                print("Request Timeout")
            } else {
                print(error?.localizedDescription ?? "")
            }
            
        }
        
    }

}
