//
//  MapVC.swift
//  LegalStart
//
//  Created by Adapting Social on 5/2/17.
//  Copyright © 2017 Adapting Social. All rights reserved.
//

import UIKit
import GoogleMaps

class MapVC: UIViewController, GMSMapViewDelegate {

    @IBOutlet weak var lblHeader: UILabel!
    @IBOutlet weak var viewMap: GMSMapView!
    
    // MARK: - Global Variables
    var objCompanyData : CompanyListData?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.setUpMapAndDrawRoue()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func setUpMapAndDrawRoue() {
        
        lblHeader.text = self.objCompanyData?.companyName
        
        viewMap.delegate = self
        viewMap.isMyLocationEnabled = true

        // Create a marker for destination location...
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: Double((self.objCompanyData?.latitude)!)!, longitude: Double((self.objCompanyData?.longitude)!)!)
        marker.title = self.objCompanyData?.companyName
        marker.snippet = self.objCompanyData?.address
        marker.map = viewMap

        
//        let userLocation = Utility.sharedInstance.currentLocation!
        
//        let userLocation = CLLocation.init(latitude: 39.0997, longitude: 94.5786)
        
        if let userLocation = Utility.sharedInstance.currentLocation {
        
            let camera = GMSCameraPosition.camera(withLatitude: (userLocation.coordinate.latitude), longitude: (userLocation.coordinate.longitude), zoom: 12.0)
            viewMap.camera = camera
            
            let path = GMSMutablePath()
            
            path.add(CLLocationCoordinate2D(latitude: (userLocation.coordinate.latitude), longitude: (userLocation.coordinate.longitude)))
            path.add(CLLocationCoordinate2D(latitude: Double((self.objCompanyData?.latitude)!)!, longitude: Double((self.objCompanyData?.longitude)!)!))

//            path.add(CLLocationCoordinate2D(latitude: 21.1702, longitude:72.8311))

            let polyline = GMSPolyline(path: path)
            polyline.strokeWidth = 2.0
            polyline.strokeColor = UIColor.red
            polyline.map = viewMap

        }
        
    }
    
    /*
    func drawRoute() {
        
        mapView.delegate = self
        
        let sourceLocation = CLLocationCoordinate2D(latitude: 40.759011, longitude: -73.984472)
//        let sourceLocation = Utility.sharedInstance.current
        let destinationLocation = CLLocationCoordinate2D(latitude: 40.748441, longitude: -73.985564)
        
        let sourcePlacemark = MKPlacemark(coordinate: sourceLocation, addressDictionary: nil)
        let destinationPlacemark = MKPlacemark(coordinate: destinationLocation, addressDictionary: nil)
        
        let sourceMapItem = MKMapItem(placemark: sourcePlacemark)
        let destinationMapItem = MKMapItem(placemark: destinationPlacemark)
        
        let sourceAnnotation = MKPointAnnotation()
        sourceAnnotation.title = "Times Square"
        
        if let location = sourcePlacemark.location {
            sourceAnnotation.coordinate = location.coordinate
        }
        
        let destinationAnnotation = MKPointAnnotation()
        destinationAnnotation.title = "Empire State Building"
        
        if let location = destinationPlacemark.location {
            destinationAnnotation.coordinate = location.coordinate
        }
        
    
        self.mapView.showAnnotations([sourceAnnotation,destinationAnnotation], animated: true )
        
    
        let directionRequest = MKDirectionsRequest()
        directionRequest.source = sourceMapItem
        directionRequest.destination = destinationMapItem
        directionRequest.transportType = .automobile
        
        // Calculate the direction
        let directions = MKDirections(request: directionRequest)
        

        directions.calculate {
            (response, error) -> Void in
            
            guard let response = response else {
                if let error = error {
                    print("Error: \(error)")
                }
                
                return
            }
            
            let route = response.routes[0]
            self.mapView.add((route.polyline), level: MKOverlayLevel.aboveRoads)
            
            let rect = route.polyline.boundingMapRect
            self.mapView.setRegion(MKCoordinateRegionForMapRect(rect), animated: true)
        }

    }
    */
 
    // MARK: - Button Click Events
    @IBAction func btnBackClick(_ sender: UIButton) {
        _ = self.navigationController?.popViewController(animated: true)
    }


}
