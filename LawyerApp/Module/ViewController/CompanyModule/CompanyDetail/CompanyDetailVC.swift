//
//  CompanyDetailVC.swift
//  LegalStart
//
//  Created by Adapting Social on 5/3/17.
//  Copyright © 2017 Adapting Social. All rights reserved.
//

import UIKit

class CompanyDetailVC: UIViewController {

    // MARK: - Global Outlets
//    @IBOutlet weak var callRoundedView: UIView!
    @IBOutlet weak var emailRoundedView: UIView!
    @IBOutlet weak var imgViewBgCompanyPic: UIImageView!
    @IBOutlet weak var imgViewCompanyPic: UIImageView!
    
    @IBOutlet weak var viewTopBg: UIView!
    @IBOutlet weak var lblCompanyName: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
//    @IBOutlet weak var lblPhoneNo: UILabel!
    
    @IBOutlet weak var txtViewCompanyInfo: UITextView!

    // MARK: - Global Variables
    var isViewSetUp = false
    var objCompanyData : CompanyListData?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.setUpUIAndData()
        
    }
    
    override func viewDidLayoutSubviews() {
        
        if !isViewSetUp {
            self.addMasklayerOnProfileImage()
            isViewSetUp = true
        }
    }
    override func viewDidAppear(_ animated: Bool) {
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - UI Setup
    func addMasklayerOnProfileImage() {
        
        // Set background view mask layer...
        let path = UIBezierPath()
        path.move(to: self.view.bounds.origin)
        path.addLine(to: CGPoint(x: self.view.frame.width, y:self.view.bounds.origin.y))
        
        let imgHeight = self.view.frame.height * 0.262
        let difference = imgHeight * 0.45
        //        if Constant.DeviceType.IS_IPHONE_5 {
        //            difference = 50.0
        //        }
        //        else
        //        {
        //            difference = 78.0
        //        }
        path.addLine(to: CGPoint(x: self.view.frame.width, y:imgHeight - difference))
        path.addLine(to: CGPoint(x: self.view.bounds.origin.x, y:imgHeight))
        path.addLine(to: self.view.bounds.origin)
        
        let maskLayer = CAShapeLayer()
        
        maskLayer.path = path.cgPath
        imgViewBgCompanyPic.layer.mask = maskLayer
        
        imgViewBgCompanyPic.clipsToBounds = true
        
    }

    func setUpUIAndData() {
        
//        callRoundedView.layer.cornerRadius = callRoundedView.frame.size.height / 2
//        callRoundedView.clipsToBounds = true
//        emailRoundedView.layer.cornerRadius = callRoundedView.frame.size.height / 2
        emailRoundedView.clipsToBounds = true
        // Set Company pic corner radius...
        imgViewCompanyPic.layer.cornerRadius = 4.0
        imgViewCompanyPic.layer.borderWidth = 1.0
        imgViewCompanyPic.layer.borderColor = UIColor.colorFromCode(0xFF7561).cgColor
        imgViewCompanyPic.clipsToBounds = true
        
        // Set top View shadow...
        // Make the shadow
        viewTopBg.layer.shadowOffset = CGSize(width: 0, height: 1.0)
        viewTopBg.layer.shadowRadius = 1.0
        viewTopBg.layer.shadowColor = UIColor.black.cgColor
        viewTopBg.layer.shadowOpacity = 0.4
        
        // Remove left right padding in textview...
        txtViewCompanyInfo.textContainer.lineFragmentPadding = 0.0;
        txtViewCompanyInfo.textContainerInset = UIEdgeInsets(top: 5, left: 0, bottom: 5, right: 0)

        if self.objCompanyData?.companyBackGroundImage != nil {
            
            self.imgViewBgCompanyPic.sd_setImage(with: URL.init(string: (self.objCompanyData?.companyBackGroundImage)!), placeholderImage: UIImage(named: ""))
            self.imgViewBgCompanyPic.clipsToBounds = true

        }
        
        if self.objCompanyData?.companyImage != nil {
            
            self.imgViewCompanyPic.sd_setImage(with: URL.init(string: (self.objCompanyData?.companyImage)!), placeholderImage: UIImage(named: ""))
            self.imgViewCompanyPic.clipsToBounds = true

        }

        self.lblCompanyName.text = self.objCompanyData?.companyName
        self.lblAddress.text = self.objCompanyData?.cityState
//        self.lblPhoneNo.text = self.objCompanyData?.mobileNumber
//        self.lblEmail.text = self.objCompanyData?.companyEmail
//        self.lblEmail.text = "Contact US"
        self.txtViewCompanyInfo.text = self.objCompanyData?.companyDescription
        
    }
    
    // MARK: - Button Click Events
    
    @IBAction func btnBackClick(_ sender: UIButton) {
        _ = self.navigationController?.popViewController(animated: true)
    }

    @IBAction func btnPhoneNoClick(_ sender: UIButton){
        
        if self.objCompanyData?.mobileNumber != nil {
            Utility.sharedInstance.doCall(strPhoneNo: (self.objCompanyData?.mobileNumber)!, controller: self)
        }
        
    }
    
    @IBAction func btnEmailClick(_ sender: UIButton){
        
//        if self.objCompanyData?.companyEmail != nil {
//            Utility.sharedInstance.doEmail(strEmail: (self.objCompanyData?.companyEmail)!, controller: self)
//        }

//        let storyBoard : UIStoryboard = UIStoryboard(name: "UserMaster", bundle: nil)
//        let emailVC : EmailViewController = storyBoard.instantiateViewController(withIdentifier: "EmailViewController") as! EmailViewController
//        emailVC.isFrom = .CompanyDetail
//        if let email = self.objCompanyData?.companyEmail  {
//           emailVC.companyEmail = email
//        }
//        
//        self.navigationController?.pushViewController(emailVC, animated: true)
        
        var params : [String:Any] = [:]
        
        
        params["email_type"] = "Company"
        params["from_email"] = AppDelegate.getAppDelegate().userInfoData.blogEmail
        params["message"] = "MESSAGE"
        params["subject"] = "SUBJECT"
        params["company_name"] = self.objCompanyData?.companyName
        params["name"] = AppDelegate.getAppDelegate().userInfoData.firstName! + " " + AppDelegate.getAppDelegate().userInfoData.lastName!
        params["phone_number"] = AppDelegate.getAppDelegate().userInfoData.moblieNumber
        params["to_email"] = self.objCompanyData?.companyEmail
        params["users_id"] =  AppDelegate.getAppDelegate().userInfoData.usersId!
        params["access_token"] = UserDefaults.value(forKey: ACCESSTOKEN) as! String
        self.callWSToSendEmail(parmas: params)

    }
    func callWSToSendEmail(parmas : [String:Any]) {
        
        WebService.POST(Constant.API.kBaseURLWithAPIName("eamil_notify_lawyer"), param: parmas, controller: self, callSilently: false, successBlock: { (jsonResponse) in
            
            if (jsonResponse["settings"]["success"].string == "1"){
                
                UIAlertController.showAlertWithOkButton(self, aStrMessage: "Email has been sent successfully", completion: { (index, title) in
                    if index == 0{

                        
//                        let storyBoard : UIStoryboard = UIStoryboard(name: "DashBoardMaster", bundle: nil)
//                        let dashBoardVC : DashBoardViewController = storyBoard.instantiateViewController(withIdentifier: "DashBoardViewController") as! DashBoardViewController
//                        
//                        self.mm_drawerController.setCenterView(dashBoardVC, withFullCloseAnimation: true, completion: nil)
                        
                        
                    }
                })
                
            }else if (jsonResponse["settings"]["success"].string == "0"){
                
                UIAlertController.showAlertWithOkButton(self, aStrMessage: jsonResponse["settings"]["message"].string, completion: nil)
                
            }else if (jsonResponse["settings"]["success"].string == "100"){
                Utility.reloginUserWith(viewController: self)
            }else{
                
                UIAlertController.showAlertWithOkButton(self, aStrMessage: Constant.AlertMessage.kAlertMsgSomeThingWentWrong, completion: nil)
            }
            
            
            
        }) { (error, isTimeOut) in
            
        }
        
        
    }


}
