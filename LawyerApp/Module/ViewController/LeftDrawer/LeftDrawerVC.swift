//
//  LeftDrawerVC.swift
//  LegalStart
//
//  Created by Adapting Social on 4/26/17.
//  Copyright © 2017 Adapting Social. All rights reserved.
//

import UIKit
import SDWebImage
import MessageUI
class LeftDrawerVC: UIViewController,UITableViewDataSource,UITableViewDelegate,MFMailComposeViewControllerDelegate {
    
    // MARK: - IBOutlets
    @IBOutlet weak var btnProfilePic: UIButton!
    @IBOutlet weak var lblUsername: UILabel!
    @IBOutlet weak var tblView: UITableView!
    
    var composeVC : MFMailComposeViewController! = nil
    // MARK: - Global Variables
    var arrMenuOptions = [[String:String]]()

//    override func viewDidLoad() {
//        super.viewDidLoad()
//
//        // Do any additional setup after loading the view.
//        if let path = Bundle.main.path(forResource: "LawyerApp_LeftOptions", ofType: "plist") {
//
//            if let arrMenu = NSArray(contentsOfFile: path)
//            {
//                arrMenuOptions = arrMenu[0] as! [[String : String]]
//            }
//        }
//
//    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let path = Bundle.main.path(forResource: "LawyerApp_LeftOptions", ofType: "plist") {
            
            if let arrMenu = NSArray(contentsOfFile: path)
            {
                let tempArr = arrMenu[0] as! [[String : String]]
                for data in tempArr {
                    if data["DisplayName"] == "Appointments" || data["DisplayName"] == "Upload Documents" || data["DisplayName"] == "Startup Pros" || data["DisplayName"] == "Create a Document" || data["DisplayName"] == "Start My Business" || data["DisplayName"] == "Chat with Lawyer" || data["DisplayName"] == "The Resource" || data["DisplayName"] == "Email Lawyer" || data["DisplayName"] == "My Documents"  {
                        
                    }else{
                        arrMenuOptions.append(data)
                    }
                }
            }
        }
        
    }
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(true)
        
        self.tblView.reloadData()
        // Set Profile pic corner radius...
        btnProfilePic.layer.cornerRadius = btnProfilePic.frame.size.height/2
        btnProfilePic.layer.borderWidth = 1.0
        btnProfilePic.layer.borderColor = UIColor.colorFromCode(0xFF7561).cgColor
        btnProfilePic.clipsToBounds = true
        
        lblUsername.text = ""
        // Set Profile Picture and name
//        print(UserDefaults.string(forKey: "profile_pic")!)
        if (AppDelegate.getAppDelegate().userInfoData.firstName != nil) {
            lblUsername!.text = AppDelegate.getAppDelegate().userInfoData.firstName! + " " + AppDelegate.getAppDelegate().userInfoData.lastName!
        }
        
        if let aStrProfilePic = AppDelegate.getAppDelegate().userInfoData.profilePicture {
            btnProfilePic.sd_setImage(with: URL.init(string:aStrProfilePic), for: .normal, placeholderImage: UIImage(named: "profile_pic"))
            btnProfilePic.imageView?.contentMode = .scaleAspectFill
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - Other Methods
    
    func setProfileData(){
        
//        imgViewProfilePic.sd_setImage(with: URL(string: UserDefaults.string(forKey: "profile_pic")!))
//        lblUsername!.text = UserDefaults.string(forKey: "name")
    }
    
    // MARK: - UITableViewDataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrMenuOptions.count + 1 // One more for Logout button
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if indexPath.row == arrMenuOptions.count {
            
            // Last row...
            let footerCell = tableView.dequeueReusableCell(withIdentifier: "LeftDrawerFooterCell") as! LeftDrawerFooterCell
            return footerCell
        }
        else
        {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "LeftDrawerCell") as! LeftDrawerCell
            let aStrImageName = arrMenuOptions[indexPath.row]["Image"]
            
            if indexPath.row == AppDelegate.sharedInstance().selectedMenuIndex {
                cell.backgroundColor = UIColor.colorFromCode(0xD8D8D8, AndAlpha: 0.2)
            }else{
                cell.backgroundColor = UIColor.clear
            }
            
            cell.imgViewMenu.image = UIImage.init(named: aStrImageName!)
            cell.lblDrawerMenu.text = arrMenuOptions[indexPath.row]["DisplayName"]
            
            return cell
        }

    }
    
    // MARK: - UITableViewDelegate
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
        self.tblView.reloadData()
        if indexPath.row == arrMenuOptions.count {
            // Last row...
            return
        }
        
        print("Arr:\(arrMenuOptions)")
        
//            if indexPath.row == 1 || indexPath.row == 2 || indexPath.row == 5 || indexPath.row == 6 || indexPath.row == 8 || indexPath.row == 9,let status = UserDefaults.value(forKey: USER_PAYMENT_SUBSCRIPTION) as? String,status != "succeeded"{
//
//                UIAlertController.showAlert(self, aStrMessage: Constant.AlertMessage.kAlertMsgForPaidSubscriber, style: .alert, aCancelBtn: "Cancel", aDistrutiveBtn: nil, otherButtonArr: ["Subscribe"], completion: { (index, title) in
//                    if title == "Subscribe"{
//                        let subscribeIndexPath : IndexPath = IndexPath(row: 10, section: 0)
//                        AppDelegate.sharedInstance().selectedMenuIndex = subscribeIndexPath.row
//                        self.toggleViewControllerWith(indexPath: subscribeIndexPath)
//                    }
//                })
//                return;
//            }else{
                AppDelegate.sharedInstance().selectedMenuIndex = indexPath.row
                
                if arrMenuOptions[indexPath.row]["DisplayName"] == "Create a Document" {
                    
                    UIAlertController.showAlert(self, aStrMessage: "All documents are custom made by an attorney in 1-3 Business Days. \n No Forms. No Templates", style: .alert, aCancelBtn: nil, aDistrutiveBtn: nil, otherButtonArr: ["Got it"], completion: { (index, title) in
                        
                    self.toggleViewControllerWith(indexPath: indexPath)
                        
                    })
                }else if arrMenuOptions[indexPath.row]["DisplayName"] == "Appointments"{
                    
                    UIAlertController.showAlert(self, aStrMessage: "All LegalStart members get one 15-minute phone call with an attorney each month.", style: .alert, aCancelBtn: nil, aDistrutiveBtn: nil, otherButtonArr: ["Got it"], completion: { (index, title) in
                    self.toggleViewControllerWith(indexPath: indexPath)
                        
                    })
                
                }else if arrMenuOptions[indexPath.row]["DisplayName"] == "Support"{
                    
                    
                    if MFMailComposeViewController.canSendMail()
                    {
                        
                        composeVC = MFMailComposeViewController()
                        composeVC.mailComposeDelegate = self
                        
                        // Configure the fields of the interface.
                        composeVC.setToRecipients(["info@legalstartapp.com"])
                        
                        composeVC.setSubject("")
                        composeVC.setMessageBody("", isHTML: false)
                        
                        // Present the view controller modally.
                        AppDelegate.getAppDelegate().window?.rootViewController?.present(composeVC, animated: true, completion: nil)
                        
                        
                        
                        return
                    }
                    
                }else{
            
                    self.toggleViewControllerWith(indexPath: indexPath)
                
                }
        //}
    }
    func toggleViewControllerWith(indexPath : IndexPath) {
        
        let aStrDisplayName = arrMenuOptions[indexPath.row]["DisplayName"]
        let aStrStoryboardName = arrMenuOptions[indexPath.row]["Storyboard"]
        var navName = arrMenuOptions[indexPath.row]["NavigationStoryboardId"]
        var aStrStoryboardId = arrMenuOptions[indexPath.row]["StoryboardId"]
        let aStoryboard = UIStoryboard(name: aStrStoryboardName!, bundle: nil)
        
        if navName == "NavigationEmailChatViewController"  {
            if let isDiaLogCreated = UserDefaults.value(forKey: IS_CHAT_DIALOG_CREATED) as? String,isDiaLogCreated == "YES"{
                
                navName = "NavigationChatHandlerViewController"
                
            }
            let aStoryboard = UIStoryboard(name: aStrStoryboardName!, bundle: nil)
            let centerVC = aStoryboard.instantiateViewController(withIdentifier: navName!)
            
            self.mm_drawerController?.setCenterView(centerVC, withCloseAnimation: true, completion: nil)
            
        }else{
            
            let centerVC = aStoryboard.instantiateViewController(withIdentifier: navName!)
            
            self.mm_drawerController?.setCenterView(centerVC, withCloseAnimation: true, completion: nil)
            
            
        }
    }
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Swift.Error?)
    {
        controller.dismiss(animated: true, completion: nil)
        if(result == .sent)
        {
            UIAlertController.showAlertWithOkButton(self, aStrMessage: "Email has beed sent successfully.", completion: nil)
        }
        
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }

    // MARK: - Button Click Events
    @IBAction func btnEditProfileClick(_ sender: UIButton) {
     
        let aStoryboardUser = UIStoryboard.init(name: SB_USER_MASTER, bundle: nil)
        let centerVC = aStoryboardUser.instantiateViewController(withIdentifier: "ProfileVC")
        self.mm_drawerController?.setCenterView(centerVC, withCloseAnimation: true, completion: nil)
    }
    
    @IBAction func btnSignOutClick(_ sender: UIButton) {
        
        UIAlertController.showAlert(self, aStrMessage: Constant.AlertMessage.kAlertMsgLogoutConfirmation, style: .alert, aCancelBtn: "Cancel", aDistrutiveBtn: nil, otherButtonArr: ["OK"], completion: { (index,_) in
            
            if (index == 0) {
                
                Utility.reloginUserWith(viewController: self)
            }
        })
    }
    
}

