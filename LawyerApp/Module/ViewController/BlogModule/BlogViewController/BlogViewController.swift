//
//  BlogViewController.swift
//  LegalStart
//
//  Created by Adapting Social on 02/05/17.
//  Copyright © 2017 Adapting Social. All rights reserved.
//

import UIKit

class BlogViewController: UIViewController,UIPageViewControllerDataSource,UIPageViewControllerDelegate {

    //MARK: ATTRIBUTES
    var pageViewController : UIPageViewController?
    @IBOutlet weak var blogDetailView: UIView!
    var blogListModel : BlogListModel! = nil
    var wsPageIndex : Int = 1
    var screenIndex : Int = 0
    let token = UserDefaults.value(forKey: ACCESSTOKEN) as! String
    @IBOutlet weak var imgFingure: UIImageView!
    //MARK: VIEWDID-LOAD
    override func viewDidLoad() {
        super.viewDidLoad()
        if self.imgFingure != nil && UserDefaults.value(forKey: TOOL_TIP_FOR_BLOG) != nil {
            
            self.imgFingure.removeFromSuperview()
        }
       self.callWSWithPageIndex(pageIndex: wsPageIndex, userID: AppDelegate.getAppDelegate().userInfoData.usersId!, callSilently: false)
    }
    //MARK: UI UPDATE
    func setUpPagination()  {
        self.pageViewController = self.storyboard?.instantiateViewController(withIdentifier: "BlogPageViewController") as? UIPageViewController
        self.pageViewController?.dataSource = self
        self.pageViewController?.delegate = self
        let objVC : BlogDetailViewController = self.viewControllerWithIndex(index: screenIndex)
        let arr = [objVC]
        self.pageViewController?.setViewControllers(arr, direction: .forward, animated: false, completion: nil)
        self.pageViewController?.view.frame = CGRect(x: 0, y: 0, width: self.blogDetailView.frame.size.width, height: self.blogDetailView.frame.size.height)
        self.addChildViewController(pageViewController!)
        self.blogDetailView.addSubview((pageViewController?.view)!)
        self.pageViewController?.didMove(toParentViewController: self)
    }
    //MARK: PAGEVIEWCONTROLLER DATASOURCE
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController?{

        let vc = viewController as? BlogDetailViewController
        var index = vc?.pageIndex
        if index == 0 {
                
            return nil
        }
        index = index! - 1
        screenIndex = index!
       return self.viewControllerWithIndex(index: screenIndex)
    }
    
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController?{
        
        let vc = viewController as? BlogDetailViewController
        let index = (vc?.pageIndex)! + 1
        if index == blogListModel.data?.count {
            
            return nil
        }
        if (blogListModel.settings?.nextPage)! == "1" && Double(index) == floor(Double((blogListModel.data?.count)! / 2))  {
            self.callWSWithPageIndex(pageIndex: wsPageIndex+1, userID: AppDelegate.getAppDelegate().userInfoData.usersId!, callSilently: true)
        }
        screenIndex = index
        return self.viewControllerWithIndex(index: screenIndex)
    }
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        
        if self.imgFingure != nil && UserDefaults.value(forKey: TOOL_TIP_FOR_BLOG) == nil {
            UserDefaults.setValue("TRUE", forKey: TOOL_TIP_FOR_BLOG)
            self.imgFingure.removeFromSuperview()
        }
        
    }
    
    //MARK: UTILITY
    func viewControllerWithIndex(index : Int) -> BlogDetailViewController {
        
        let blogDetailViewController : BlogDetailViewController = self.storyboard?.instantiateViewController(withIdentifier: "BlogDetailViewController") as! BlogDetailViewController
        blogDetailViewController.pageIndex = index
        blogDetailViewController.blogListData = blogListModel.data?[index]
       
        return blogDetailViewController
    }
    //MARK: WEB-SERVICES
    func callWSWithPageIndex(pageIndex : Int,userID : String,callSilently : Bool)  {
        
        
        var param : [String:Any] = [:]
        param["page_index"] = pageIndex
        param["users_id"] = userID
        param["access_token"] = token
        
        WebService.GET(Constant.API.kBaseURLWithAPIName("blog_listing"), param: param, controller: self, callSilently: callSilently, successBlock: { (jsonResponse) in
            
            let blogList = BlogListModel(json: jsonResponse)
            if blogList.settings?.success == "1"{
                self.showToolTip()
                if !callSilently{
                    self.blogListModel = blogList
                    self.setUpPagination()
                }else{
                    
                    self.blogListModel.settings = blogList.settings
                    let arr : [BlogListData] = blogList.data!
                    self.blogListModel.data?.append(contentsOf: arr)
                }
            }else if blogList.settings?.success == "100" {
                Utility.reloginUserWith(viewController: self)
            }else{
                
                
                UIAlertController.showAlertWithOkButton(self, aStrMessage: blogList.settings?.message, completion: nil)
                
            }
            
            
        }) { (error, isTimeOut) in
            
            
        }
        
    }

    func showToolTip()  {
        if UserDefaults.value(forKey: TOOL_TIP_FOR_BLOG) == nil {
            fingureStartAnimating()
        }
        
    }
    func fingureStartAnimating()  {
        if self.imgFingure != nil && UserDefaults.value(forKey: TOOL_TIP_FOR_BLOG) == nil{
            let delay = 12
            self.imgFingure.alpha = 1
            imgFingure.transform = CGAffineTransform(translationX: UIScreen.main.bounds.size.width - (imgFingure.frame.size.width * 2), y: 0)
            UIView.animate(withDuration: 2.5, delay: Double(delay) * 0.05, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: UIViewAnimationOptions.curveEaseInOut, animations: {
                self.imgFingure.alpha = 0.2
                self.imgFingure.transform = CGAffineTransform.identity
            }, completion: { (isCompleted) in
            if self.imgFingure != nil{
                self.imgFingure.alpha = 0.7
            }
            
            if UserDefaults.value(forKey: TOOL_TIP_FOR_BLOG) == nil {
                self.fingureStartAnimating()
            }
            
        })

        
        }
    }

    //MARK: BUTTON-ACTION
    @IBAction func btnSidePanelAction(_ sender: UIButton) {
        
        self.mm_drawerController.toggle(.left, animated: true, completion: nil)
    }
    @IBAction func btnShareBlogAction(_ sender: UIButton) {
        
        let asc = [self.blogListModel.data![screenIndex].blogDescripation!]
        let shar = UIActivityViewController(activityItems: asc, applicationActivities: nil)
        
        self.present(shar, animated: true, completion: nil)
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
   
}
