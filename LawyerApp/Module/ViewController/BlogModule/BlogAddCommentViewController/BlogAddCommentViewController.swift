//
//  BlogAddCommentViewController.swift
//  LegalStart
//
//  Created by Adapting Social on 03/05/17.
//  Copyright © 2017 Adapting Social. All rights reserved.
//

import UIKit

class BlogAddCommentViewController: UIViewController {

    //MARK: ATTRIBUTES
    @IBOutlet weak var txtViewComment: UITextView!
    var isCommented : (Bool,String?) -> () = {_ in}
    override func viewDidLoad() {
        super.viewDidLoad()
        self.isCommented(false,nil)
        setUpUI()
    }
    //MARK: VIEWDID-LOAD
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
       setUpUI()
        
    }
    //MARK: UI-UPDATE
    func setUpUI()  {
        self.txtViewComment.layer.cornerRadius = 5
        self.txtViewComment.layer.masksToBounds = true
    }
    //MARK: BUTTON-ACTION
    @IBAction func btnPostCommentAction(_ sender: CustomButton) {
        
        if validation() {
            self.dismiss(animated: true) {
                self.isCommented(true,self.txtViewComment.text)
            }
        }
    }
    @IBAction func btnCancelAction(_ sender: UIButton) {
        
        self.dismiss(animated: true) {
            self.isCommented(false,nil)
        }
        
    }
    //MARK: UTILITY
    func validation() -> Bool  {
        
        if self.txtViewComment.text.isEmptyString() {
            UIAlertController.showAlertWithOkButton(self, aStrMessage: Constant.AlertMessage.kAlertMessageRequiredField("comment"), completion: nil)
            
            return false
        }
        
        return true
    }
    //MARK: MEMORY-WARNING
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
