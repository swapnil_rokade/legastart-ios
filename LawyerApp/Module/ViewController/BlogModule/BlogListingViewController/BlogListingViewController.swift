//
//  BlogListingViewController.swift
//  LegalStart
//
//  Created by Adapting Social on 15/06/17.
//  Copyright © 2017 Adapting Social. All rights reserved.
//

import UIKit

class BlogListingViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UIGestureRecognizerDelegate {

    @IBOutlet weak var blogListTblView: UITableView!
    let token = UserDefaults.value(forKey: ACCESSTOKEN) as! String
    let userID = AppDelegate.getAppDelegate().userInfoData.usersId!
    var blogListModel : BlogListModel! = nil
    var wsPageIndex : Int = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        self.blogListTblView.estimatedRowHeight = 110
        self.blogListTblView.rowHeight = UITableViewAutomaticDimension

        self.callWSWithPageIndex(pageIndex: wsPageIndex, callSilently: false)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if blogListModel != nil{
            return blogListModel.data!.count
        }
        return 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        if indexPath.row == 0 {
            let cell : BlogListingVCFirstCell = tableView.dequeueReusableCell(withIdentifier: "BlogListingVCFirstCell") as! BlogListingVCFirstCell
            if let data = blogListModel.data?[indexPath.row] {
                let imgURL : URL = URL(string: data.blogImage!)!
                cell.blogImgView.sd_setImage(with: imgURL)
                cell.lblTitle.text = data.blogName
                cell.lblDate.text = data.blogDate
                
            }
            
            
            return cell
        }else{
            let cell : BlogListingVCTableViewCell = tableView.dequeueReusableCell(withIdentifier: "BlogListingVCTableViewCell") as! BlogListingVCTableViewCell
            if let data = blogListModel.data?[indexPath.row] {
                let imgURL : URL = URL(string: data.blogImage!)!
                cell.blogImgView.sd_setImage(with: imgURL)
                cell.blogTitle.text = data.blogName
                cell.blogDate.text = data.blogDate
                
            }
            
            return cell
        
        
        }
        
       
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let status = UserDefaults.value(forKey: USER_PAYMENT_SUBSCRIPTION) as? String,status == "succeeded" {
            let blogDetailViewController : BlogDetailViewController = self.storyboard?.instantiateViewController(withIdentifier: "BlogDetailViewController") as! BlogDetailViewController
            blogDetailViewController.pageIndex = indexPath.row
            blogDetailViewController.blogListData = blogListModel.data?[indexPath.row]
            self.navigationController?.pushViewController(blogDetailViewController, animated: true)
        }else{
            

            UIAlertController.showAlert(self, aStrMessage: Constant.AlertMessage.kAlertMsgForPaidSubscriber, style: .alert, aCancelBtn: "Cancel", aDistrutiveBtn: nil, otherButtonArr: ["Subscribe"], completion: { (index, title) in
                if title == "Subscribe"{
                    
                    let storyBoard : UIStoryboard = UIStoryboard(name: "DocumentMaster", bundle: nil)
                    
                    let centerVC : SubscriptionListingViewController = storyBoard.instantiateViewController(withIdentifier: "SubscriptionListingViewController") as! SubscriptionListingViewController
                    AppDelegate.sharedInstance().selectedMenuIndex = 10
                    self.mm_drawerController?.setCenterView(centerVC, withCloseAnimation: true, completion: nil)
                    
                    
                }
            })
        }
        
       
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if indexPath.row == blogListModel.data!.count - 1 && (blogListModel.settings?.nextPage)! == "1"{
            blogListTblView.tableFooterView?.isHidden = false
            
            
            self.callWSWithPageIndex(pageIndex: wsPageIndex+1, callSilently: true)
            
            
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
         
            return 180
        }else{
        
            return UITableViewAutomaticDimension
        }
    }
    @IBAction func btnSidePanelAction(_ sender: UIButton) {
        
        self.mm_drawerController.toggle(.left, animated: true, completion: nil)
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func callWSWithPageIndex(pageIndex : Int,callSilently : Bool)  {
        
        
        var param : [String:Any] = [:]
        param["page_index"] = pageIndex
        param["users_id"] = self.userID
        param["access_token"] = token
        
        WebService.GET(Constant.API.kBaseURLWithAPIName("blog_listing"), param: param, controller: self, callSilently: callSilently, successBlock: { (jsonResponse) in
            
            
            self.blogListTblView.tableFooterView?.isHidden = true
            
            let blogList = BlogListModel(json: jsonResponse)
            if blogList.settings?.success == "1"{
                
                if !callSilently{
                    self.blogListModel = blogList
                    self.blogListTblView.reloadData()
                }else{
                    
                    self.blogListModel.settings = blogList.settings
                    let arr : [BlogListData] = blogList.data!
                    self.blogListModel.data?.append(contentsOf: arr)
                    self.blogListTblView.reloadData()
                }
            }else if blogList.settings?.success == "100" {
                Utility.reloginUserWith(viewController: self)
            }else{
                
                
                UIAlertController.showAlertWithOkButton(self, aStrMessage: blogList.settings?.message, completion: nil)
                
            }
            
            
        }) { (error, isTimeOut) in
            
            
        }
        
    }


}
