//
//  BlogDetailViewController.swift
//  LegalStart
//
//  Created by Adapting Social on 02/05/17.
//  Copyright © 2017 Adapting Social. All rights reserved.
//

import UIKit

class BlogDetailViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {

    //MARK: ATTRIBUTES
    var pageIndex : Int?
    @IBOutlet weak var tblViewBlog: UITableView!
    var blogListData : BlogListData?
    let token = UserDefaults.value(forKey: ACCESSTOKEN) as! String
    var blogDiscription : NSAttributedString! = nil
    //MARK: VIEWDID LOAD
    override func viewDidLoad() {
        super.viewDidLoad()
       
        self.tblViewBlog.estimatedRowHeight = 149
        self.tblViewBlog.rowHeight = UITableViewAutomaticDimension
        self.blogListData!.blogDescripation?.attributedStringFromHTML(completionBlock: { (attString) in
            self.blogDiscription = attString
            self.tblViewBlog.reloadData()
        })
       
    }

    //MARK: TABLEVIEW DATASOURCE
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if let count = blogListData?.commentListing?.count,count > 0 {
            return count+1
        }
        
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            let aHeaderView : BlogDetailVCTableViewDetailCell = (self.tblViewBlog.dequeueReusableCell(withIdentifier: "BlogDetailVCTableViewDetailCell") as? BlogDetailVCTableViewDetailCell)!
            if blogListData != nil {
                let url = URL(string: (blogListData?.blogImage)!)
                aHeaderView.blogImgView.sd_setImage(with: url, placeholderImage: UIImage(named:"logo"))
                aHeaderView.lblBlogTitle.text = blogListData?.blogName
                aHeaderView.lblBlogDate.text = blogListData?.blogDate
                aHeaderView.lblBlogDescription.attributedText = blogDiscription
                
                aHeaderView.btnAddComment.addTarget(self, action: #selector(btnCommentAction(_:)), for: .touchUpInside)
            }
            return aHeaderView
        }else{
        
        let commentCell :BlogDetailVCTableViewCommentCell = (tableView.dequeueReusableCell(withIdentifier: "BlogDetailVCTableViewCommentCell") as? BlogDetailVCTableViewCommentCell)!
            let comment = blogListData?.commentListing?[indexPath.row-1]
            let url = URL(string: (comment?.profilePicture)!)
            commentCell.imgViewProfileImage.sd_setImage(with: url, placeholderImage: UIImage(named:"logo"))
            commentCell.lblCommentTitle.text = comment?.name
            commentCell.lblCommentDescription.text = comment?.comment
            commentCell.lblTotalLikes.text = comment?.countLike
            let likeCout = Int(comment!.countLike!)
            
            if likeCout! <= 1 {
             commentCell.lblLike.text = "Like"
            }else{
             commentCell.lblLike.text = "Likes"
                
            }
            if comment?.blogLike == "1" {
                commentCell.btnCommentLike.setImage(UIImage(named:"Like-selected"), for: .normal)
                let color = UIColor.colorFromCode(0x041B54)
                commentCell.btnCommentLike.setTitleColor(color, for: .normal)
            }else{
                commentCell.btnCommentLike.setImage(UIImage(named:"Like-normal"), for: .normal)
                commentCell.btnCommentLike.setTitleColor(UIColor.gray, for: .normal)
            }
            commentCell.lblCommentDate.text = comment?.date
            commentCell.btnCommentLike.tag = indexPath.row - 1
            commentCell.btnCommentLike.addTarget(self, action: #selector(btnLikeAction(_:)), for: .touchUpInside)
            
        return commentCell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    //MARK: BUTTON-ACTION
    @IBAction func btnLikeAction(_ sender: UIButton) {
       
        let indexPath =  IndexPath(row: sender.tag, section: 0)
        let comment = blogListData!.commentListing![indexPath.row]
        
        
        self.callWSForLikeOnCommentWithComment(comment: comment, indexPath: indexPath, user_id: AppDelegate.getAppDelegate().userInfoData.usersId!)
    }
    @IBAction func btnCommentAction(_ sender: UIButton) {
        
        let addCommentVC : BlogAddCommentViewController = (self.storyboard?.instantiateViewController(withIdentifier: "BlogAddCommentViewController") as? BlogAddCommentViewController)!
        addCommentVC.isCommented = {
        
            isCommented , value in
        
            if isCommented {
                self.callWSForCommentOnBlogWith(blog_id: (self.blogListData?.blogId)!, user_id: AppDelegate.getAppDelegate().userInfoData.usersId!, comment: value!)
            }
        
        }
        addCommentVC.modalPresentationStyle = .overFullScreen
        self.navigationController?.present(addCommentVC, animated: true, completion: nil)
        
    }
    //MARK: WEB-SERVICES
    func callWSForLikeOnCommentWithComment(comment : BlogListCommentListing,indexPath : IndexPath,user_id : String) {
        var param : [String:Any] = [:]
        param["blog_comment_id"] = comment.blogCommentId
        param["users_id"] = user_id
        param["access_token"] = token
        WebService.POST(Constant.API.kBaseURLWithAPIName("blog_comment_like_or_dislike"), param: param, controller: self,callSilently: true, successBlock: { (jsonResponse) in
            print(jsonResponse)
            let blogLikeDisLike = BlogCommentLikeDislikeModel(json: jsonResponse)
            if (blogLikeDisLike.settings?.success == "1"){
                
                if (blogLikeDisLike.data?.count)! > 0{
                
                    let commentLike = blogLikeDisLike.data?[0]
                    comment.blogLike = commentLike?.blogLike
                    comment.countLike = commentLike?.countLike
                    
                }else{
                
                    comment.blogLike = "0"
                    let likeCount = Int(comment.countLike!)
                    if likeCount! > 0{
                        comment.countLike = "\(likeCount! - 1)"
                    }
                
                }
                DispatchQueue.main.async {
                    
//                    let contentOffset = self.tblViewBlog.contentOffset
//                    self.tblViewBlog.reloadData()
//                    self.tblViewBlog.contentOffset = contentOffset
                    
//                    let indexPath = IndexPath.init(row: indexPath.row + 1, section: 0)
//                    let commentCell = self.tblViewBlog.cellForRow(at: indexPath) as! BlogDetailVCTableViewCommentCell
//                    let likeCout = Int(comment.countLike!)
//                    
//                    if likeCout! < 1 {
//                        commentCell.lblLike.text = "Like"
//                    }else{
//                        commentCell.lblLike.text = "Likes"
//                        
//                    }
//                    if comment.blogLike == "1" {
//                        commentCell.btnCommentLike.setImage(UIImage(named:"Like-selected"), for: .normal)
//                        let color = UIColor.colorFromCode(0x041B54)
//                        commentCell.btnCommentLike.setTitleColor(color, for: .normal)
//                    }else{
//                        commentCell.btnCommentLike.setImage(UIImage(named:"Like-normal"), for: .normal)
//                        commentCell.btnCommentLike.setTitleColor(UIColor.gray, for: .normal)
//                    }

                    let indexPath = IndexPath.init(row: indexPath.row + 1, section: 0)
                    self.tblViewBlog.reloadRows(at: [indexPath], with: UITableViewRowAnimation.none)
                    
//                    self.tblViewBlog.scrollToRow(at: indexPath, at: .bottom, animated: false)
                }
                
                
                
            }else if (blogLikeDisLike.settings?.success == "0"){
                
                UIAlertController.showAlertWithOkButton(self, aStrMessage: blogLikeDisLike.settings?.message, completion: nil)
                
            }else if (blogLikeDisLike.settings?.success == "100") {
                Utility.reloginUserWith(viewController: self)
            }else{
                
                UIAlertController.showAlertWithOkButton(self, aStrMessage: Constant.AlertMessage.kAlertMsgSomeThingWentWrong, completion: nil)
            }
            
            
        }) { (error, isTimeOut) in
            
        }
    }
    func callWSForCommentOnBlogWith(blog_id : String,user_id : String,comment : String)  {
        var param : [String:Any] = [:]
        param["users_id"] = user_id
        param["blog_id"] = blog_id
        param["comment"] = comment
        param["access_token"] = token
        WebService.POST(Constant.API.kBaseURLWithAPIName("add_comment"), param: param, controller: self, successBlock: { (jsonResponse) in
            print(jsonResponse)
            
            if (jsonResponse["settings"]["success"].string == "1") {
                
                if let items = jsonResponse["data"]["comment_listing"].array {
                    let arrCommentListing = items.map { BlogListCommentListing(json: $0) }
                    self.blogListData?.commentListing = arrCommentListing
                    self.tblViewBlog.reloadData()
                    self.blogListData!.blogDescripation?.attributedStringFromHTML(completionBlock: { (attString) in
                        self.blogDiscription = attString
                        self.tblViewBlog.reloadData()
                    })
                }
                
            }else if (jsonResponse["settings"]["success"].string == "100") {
                Utility.reloginUserWith(viewController: self)
            }else{
                UIAlertController.showAlertWithOkButton(self, aStrMessage: jsonResponse["settings"]["message"].string, completion: nil)
            }


            
        }) { (error, isTimeOut) in
            
        }
        
        
        
    }
    @IBAction func btnShareBlogAction(_ sender: UIButton) {
        
        if self.blogListData != nil {
            let asc = [self.blogListData!.blogDescripation!]
            let shar = UIActivityViewController(activityItems: asc, applicationActivities: nil)
            
            self.present(shar, animated: true, completion: nil)
        }
        
        
    }
    @IBAction func btnSidePanelAction(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}
