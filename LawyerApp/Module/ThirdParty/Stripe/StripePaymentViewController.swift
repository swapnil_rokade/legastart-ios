//
//  StripePaymentViewController.swift
//  LegalStart
//
//  Created by Adapting Social on 08/06/17.
//  Copyright © 2017 Adapting Social. All rights reserved.
//

import UIKit
import Stripe
class StripePaymentViewController: UIViewController,STPPaymentCardTextFieldDelegate,STPPaymentContextDelegate {

    @IBOutlet weak var payButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var paymentIndicator: UIActivityIndicatorView!
    @IBOutlet weak var paymentTextField: STPPaymentCardTextField!
    var amount : Float?
    
    
    var didFinishStripePayment : (StripePaymentViewController,STPToken?,Swift.Error?) -> () = {_ in}
    override func viewDidLoad() {
        super.viewDidLoad()

        
        let title : String = "Pay \(amount!)"
        self.payButton.setTitle(title, for: .normal)
        self.payButton.isEnabled = false
        self.payButton.isHidden = true
      
    }

    
    
    func paymentContext(_ paymentContext: STPPaymentContext, didFailToLoadWithError error: Swift.Error){
    
    
    }
    func paymentContextDidChange(_ paymentContext: STPPaymentContext){
    
    
    
    }
  
    func paymentContext(_ paymentContext: STPPaymentContext, didFinishWith status: STPPaymentStatus, error: Swift.Error?){
    
    
    }
    func paymentContext(_ paymentContext: STPPaymentContext,
                        didCreatePaymentResult paymentResult: STPPaymentResult,
                        completion: @escaping STPErrorBlock){
    
    
    }
    func paymentCardTextFieldDidChange(_ textField: STPPaymentCardTextField) {
        self.payButton.isEnabled = textField.isValid
        self.payButton.isHidden = !textField.isValid
    }
    @IBAction func btnPayAction(_ sender: UIButton) {
        if !(self.paymentTextField?.isValid)! {
            
            return;
        }
        if Stripe.defaultPublishableKey() == nil {
            let error : NSError = NSError(domain: StripeDomain, code: STPError.STPInvalidRequestError.rawValue, userInfo: [NSLocalizedDescriptionKey:"Please specify a Stripe Publishable Key"])
            self.presentingViewController?.dismiss(animated: true, completion: {
                self.didFinishStripePayment(self, nil, error as Swift.Error)
            })
            
        }
        self.paymentIndicator.startAnimating()
        self.payButton.isEnabled = false
        self.payButton.isHidden = true
        let cardParams = STPCardParams()
        cardParams.number = self.paymentTextField?.cardNumber
        cardParams.expMonth = (self.paymentTextField?.expirationMonth)!
        cardParams.expYear = (self.paymentTextField?.expirationYear)!
        cardParams.cvc = self.paymentTextField?.cvc
        STPAPIClient.shared().createToken(withCard: cardParams) { (token, error) in
            self.paymentIndicator.stopAnimating()
            if error == nil{
                
                print(token)
                self.presentingViewController?.dismiss(animated: true, completion: {
                    self.didFinishStripePayment(self, token, error)
                })
            }else{
                self.payButton.isHidden = false
            
            }
            
        }
        
        

    }
    @IBAction func btnCancelAction(_ sender: UIButton) {
        self.presentingViewController?.dismiss(animated: true, completion: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
