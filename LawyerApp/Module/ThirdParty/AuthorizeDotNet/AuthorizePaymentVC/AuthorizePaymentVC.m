//
//  PaymentVC.m
//  TutorPursuit
//
//  Created by Adapting Social on 9/28/16.
//  Copyright © 2016 Adapting Social. All rights reserved.
//

#import "AuthorizePaymentVC.h"
#import "SVProgressHUD.h"
#import "LawyerApp-Swift.h"
#import "Reachability.h"
#import "QMMessageNotificationManager.h"

#define kUsername @"Start2851"
#define kPassword @"summer_51"
#define AES256ENCRYPTIONKEY @"vjkd#g&m36@k8?j3m2dvm0ke5f"
//#define kUsername @"umesh278"
//#define kPassword @"Umesh0819"

#define kCreditCardLength 16
#define kCreditCardLengthPlusSpaces (kCreditCardLength + 3)
#define kExpirationLength 4
#define kExpirationLengthPlusSlash  kExpirationLength + 1
#define kCVV2Length 3
#define kCVV2LengthForAmex 4
#define kZipLength 5

#define kCreditCardObscureLength (kCreditCardLength - 4)

#define kSpace @" "
#define kSlash @"/"

#define kEnterCardNumber                        @"Please enter card number"
#define kEnterValidCardNumber                   @"Please enter valid card number"
#define kEnterCardExpirationDate                @"Please enter expiry date"
#define kEnterValidCardExpirationDate           @"Please enter valid expiry date"
#define kEnterCardCVVNumber                     @"Please enter CVV number"
#define kEnterValidCardCVVNumber                @"Please enter valid CVV number"

#define kPaymentCancelError                     @"Payment failed. Please try again."
#define kCheckConnection                        @"Please check your internet connection."

#define kPaymentSuccessMessage                  @"Thank you for subscription with Tutor Pursuit."
#define kPaymentErrorMessage                    @"We are not able to process your payment due to a system error. Please contact to support team."

@interface AuthorizePaymentVC ()

@end

@implementation AuthorizePaymentVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initializeViews];
    self.lblSubscriptionAmount.text = [NSString stringWithFormat:@"$%@", self.strSubscriptionAmount];
    
    _mainStrArray = [[NSMutableArray alloc] initWithObjects:@"!",@"*",@"'",@"(",@")",@";",@":",@"@",@"&",@"=",@"+",@"$",@",",@"/",@"?",@"%",@"#",@"[",@"]", nil];
    
    _replaceStrArray = [[NSMutableArray alloc] initWithObjects:@"e_O21",@"e_O2A",@"e_O27",@"e_O28",@"e_O29",@"e_O3B",@"e_O3A",@"e_O40",@"e_O26",@"e_O3D",@"e_O2B",@"e_O24",@"e_O2C",@"e_O2F",@"e_O3F",@"e_O25",@"e_O23",@"e_O5B",@"e_O5D", nil];
#warning Change for Live mode
    // Set payment sdk mode...
    [AuthNet authNetWithEnvironment:ENV_LIVE];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark --------------------------------
#pragma mark UIButton Action Events

- (IBAction)btnBackClick:(id)sender {
    
    [self dismissViewControllerAnimated:true completion:nil];

}

- (IBAction)btnPaymentClick:(id)sender {
    
    [self.view endEditing:YES];
    if (![strCardNumberBuffer length]) {
        
        
        [QMMessageNotificationManager showNotificationWithTitle:kEnterCardNumber subtitle:@"" type:QMMessageNotificationTypeError];
        
        return;
    }
    
    if (![CreditCardType isValidCreditCardNumber:strCardNumberBuffer]) {
        
        [QMMessageNotificationManager showNotificationWithTitle:kEnterValidCardNumber subtitle:@"" type:QMMessageNotificationTypeError];
        
        return;
    }
    
    if (![self.txtFCardExpirationDate.text length]) {
        
        [QMMessageNotificationManager showNotificationWithTitle:kEnterCardExpirationDate subtitle:@"" type:QMMessageNotificationTypeError];
        
        return;
    }
    
    if ([strCardExpirationBuffer length] != EXPIRATION_DATE_LENGTH) {

        [QMMessageNotificationManager showNotificationWithTitle:kEnterValidCardExpirationDate subtitle:@"" type:QMMessageNotificationTypeError];
        
        return;
        
    }
    else
    {
        // Validate
        NSArray *components = [[self.txtFCardExpirationDate text] componentsSeparatedByString:@"/"];
        NSString *month = [components objectAtIndex:0];
        NSString *year = [components objectAtIndex:1];
        
        // Check to see if month is correct
        if ([month intValue] == 0 || [month intValue] > 12) {

        [QMMessageNotificationManager showNotificationWithTitle:kEnterValidCardExpirationDate subtitle:@"" type:QMMessageNotificationTypeError];
            
            return;
        }
        
        // Convert string to date object
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        NSDate *currentDate = [NSDate date];
        
        // Convert date object to desired output format
        [dateFormat setDateFormat:@"M/yyyy"];
        NSString *currentDateString = [dateFormat stringFromDate:currentDate];
        components = [currentDateString componentsSeparatedByString:@"/"];
        NSString *currentMonth = [components objectAtIndex:0];
        NSString *currentYear = [[components objectAtIndex:1] substringFromIndex:2];
        
        // Check if we are correct
        if ([year intValue] == [currentYear intValue]) {
            if ([month intValue] < [currentMonth intValue]) {
                [QMMessageNotificationManager showNotificationWithTitle:kEnterValidCardExpirationDate subtitle:@"" type:QMMessageNotificationTypeError];
                
                return;
            }
        } else if ([year intValue] < [currentYear intValue]) {
            [QMMessageNotificationManager showNotificationWithTitle:kEnterValidCardExpirationDate subtitle:@"" type:QMMessageNotificationTypeError];
            
            return;
        }
    }

    if (![self.txtFCardVerificationValue.text length]) {
        [QMMessageNotificationManager showNotificationWithTitle:kEnterCardCVVNumber subtitle:@"" type:QMMessageNotificationTypeError];
       
        return;
    }
    
    if ([self.txtFCardVerificationValue.text length] != kCVV2Length && [self.txtFCardVerificationValue.text length] != kCVV2LengthForAmex ) {
        [QMMessageNotificationManager showNotificationWithTitle:kEnterValidCardCVVNumber subtitle:@"" type:QMMessageNotificationTypeError];
       
        return;
    }
    

    [SVProgressHUD show];

    if (_shouldPayWithFramework) {
        [self loginToGateway];
    }else{
    
        [self dismissViewControllerAnimated:YES completion:^{
            
            NSString *enCrptedCardNumber =[self encryptUsingRNCryptor:strCardNumberBuffer];
            
        _cardDetailsBlock(enCrptedCardNumber,_txtFCardExpirationDate.text,_txtFCardVerificationValue.text);
            [SVProgressHUD dismiss];
        }];
    
    }
    
}

#pragma mark --------------------------------
#pragma mark UITextField Delegate Methods

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if (textField == self.txtFCardNumber) {
        if ([string length] > 0) { //NOT A BACK SPACE Add it
            
            if ([self isMaxLength:textField])
                return NO;
            
            strCardNumberBuffer  = [NSString stringWithFormat:@"%@%@", strCardNumberBuffer, string];
        } else {
            
            //Back Space do manual backspace
            if ([strCardNumberBuffer length] > 1) {
                strCardNumberBuffer = [strCardNumberBuffer substringWithRange:NSMakeRange(0, [strCardNumberBuffer length] - 1)];
            } else {
                strCardNumberBuffer = @"";
            }
        }
        
        [self formatValue:textField];
    }
    else if (textField == self.txtFCardExpirationDate) {
        if ([string length] > 0) { //NOT A BACK SPACE Add it
            
            if ([self isMaxLength:textField])
                return NO;
            
            strCardExpirationBuffer = [NSString stringWithFormat:@"%@%@", strCardExpirationBuffer, string];
        } else {
            
            //Back Space do manual backspace
            if ([strCardExpirationBuffer length] > 1) {
                strCardExpirationBuffer = [strCardExpirationBuffer substringWithRange:NSMakeRange(0, [strCardExpirationBuffer length] - 1)];
                [self formatValue:textField];
            } else {
                strCardExpirationBuffer = @"";
            }
        }
        
        [self formatValue:textField];
        
    } else if (textField == self.txtFCardVerificationValue) {
        if ([string length] > 0) {
            
            if ([self isMaxLength:textField])
                return NO;
            
            self.txtFCardVerificationValue.text = [NSString stringWithFormat:@"%@%@", self.txtFCardVerificationValue.text, string];
        }else {
            
            //Back Space do manual backspace
            if ([self.txtFCardVerificationValue.text length] > 1) {
                self.txtFCardVerificationValue.text = [self.txtFCardVerificationValue.text substringWithRange:NSMakeRange(0, [self.txtFCardVerificationValue.text length] - 1)];
            } else {
                self.txtFCardVerificationValue.text = @"";
            }
        }
    }
    
    return NO;
}


- (BOOL)textFieldShouldClear:(UITextField *)textField {
    
    if (textField == self.txtFCardNumber) {
        strCardNumberBuffer = [NSString string];
    }
    
    if (textField == self.txtFCardExpirationDate) {
        strCardExpirationBuffer = [NSString string];
    }
    
    return YES;
}

#pragma mark --------------------------------
#pragma mark Validation Methods

- (void) formatValue:(UITextField *)textField {
    NSMutableString *value = [NSMutableString string];
    
    if (textField == self.txtFCardNumber ) {
        NSInteger length = [strCardNumberBuffer length];
        
        for (int i = 0; i < length; i++) {
            
            // Reveal only the last character.
            if (length <= kCreditCardObscureLength) {
                
                if (i == (length - 1)) {
                    [value appendString:[strCardNumberBuffer substringWithRange:NSMakeRange(i,1)]];
                } else {
                    [value appendString:@"●"];
                }
            }
            // Reveal the last 4 characters
            else {
                
                if (i < kCreditCardObscureLength) {
                    [value appendString:@"●"];
                } else {
                    [value appendString:[strCardNumberBuffer substringWithRange:NSMakeRange(i,1)]];
                }
            }
            
            //After 4 characters add a space
            if ((i +1) % 4 == 0 &&
                ([value length] < kCreditCardLengthPlusSpaces)) {
                [value appendString:kSpace];
            }
        }
        textField.text = value;
    }
    if (textField == self.txtFCardExpirationDate) {
        for (int i = 0; i < [strCardExpirationBuffer length]; i++) {
            [value appendString:[strCardExpirationBuffer substringWithRange:NSMakeRange(i,1)]];
            
            // After two characters append a slash.
            if ((i + 1) % 2 == 0 &&
                ([value length] < kExpirationLengthPlusSlash)) {
                [value appendString:kSlash];
            }
        }
        textField.text = value;
    }
}

- (BOOL) isMaxLength:(UITextField *)textField {
    
    if (textField == self.txtFCardNumber && [textField.text length] >= kCreditCardLengthPlusSpaces) {
        return YES;
    }
    else if (textField == self.txtFCardExpirationDate && [textField.text length] >= kExpirationLengthPlusSlash) {
        return YES;
    }
    else if (textField == self.txtFCardVerificationValue && [textField.text length] >= kCVV2LengthForAmex) {
        return YES;
    }
    return NO;
}

#pragma mark --------------------------------
#pragma mark Payment SDK Methods

- (void)loginToGateway {
    
    MobileDeviceLoginRequest *mobileDeviceLoginRequest = [MobileDeviceLoginRequest mobileDeviceLoginRequest];
    
    mobileDeviceLoginRequest.anetApiRequest.merchantAuthentication.mobileDeviceId = [self getDeviceId];
    
    mobileDeviceLoginRequest.anetApiRequest.merchantAuthentication.name = kUsername;
    mobileDeviceLoginRequest.anetApiRequest.merchantAuthentication.password = kPassword;
    
    AuthNet *authNet = [AuthNet getInstance];
    [authNet setDelegate:self];
    [authNet mobileDeviceLoginRequest: mobileDeviceLoginRequest];
}

- (void)createTransaction {
    
    AuthNet *authNet = [AuthNet getInstance];
    
    [authNet setDelegate:self];
    
    CreditCardType *aCreditCardType = [CreditCardType creditCardType];
    aCreditCardType.cardNumber = strCardNumberBuffer;
    aCreditCardType.expirationDate = [self.txtFCardExpirationDate.text stringByReplacingOccurrencesOfString:kSlash withString:@""];
    
    if ([self.txtFCardVerificationValue.text length]) {
        aCreditCardType.cardCode = [NSString stringWithString:self.txtFCardVerificationValue.text];
    }
    
    PaymentType *paymentType = [PaymentType paymentType];
    paymentType.creditCard = aCreditCardType;
    
    LineItemType *lineItem = [LineItemType lineItem];

    lineItem.itemName = self.strSubscriptionPackageName;
    lineItem.itemDescription = self.strSubscriptionPackageName;
    lineItem.itemQuantity = @"1";
    lineItem.itemPrice = self.strSubscriptionAmount;
    lineItem.itemID = self.strItemId;


    TransactionRequestType *requestType = [TransactionRequestType transactionRequest];
    requestType.lineItems = [NSMutableArray arrayWithObject:lineItem];
    requestType.amount = self.strSubscriptionAmount;
    requestType.payment = paymentType;
    CreateTransactionRequest *request = [CreateTransactionRequest createTransactionRequest];
    request.transactionRequest = requestType;
    request.anetApiRequest.merchantAuthentication.mobileDeviceId = [self getDeviceId];
    request.anetApiRequest.merchantAuthentication.sessionToken = self.strSessionToken;
    
    [authNet purchaseWithRequest:request];

}


- (void)mobileDeviceLoginSucceeded:(MobileDeviceLoginResponse *)response {
    
//    NSLog(@"ViewController : mobileDeviceLoginSucceeded - %@",response);
    self.strSessionToken = response.sessionToken;
    
    [self createTransaction];
}

#pragma mark -
#pragma mark - AuthNetDelegate Methods

- (void)paymentSucceeded:(CreateTransactionResponse*)response {
    
//    NSLog(@"Payment Success ********************** ");
    
//    NSString *title = @"Successfull Transaction";
    [SVProgressHUD dismiss];
    TransactionResponse *transResponse = response.transactionResponse;
    
//    NSLog(@"%@",response.responseReasonText);
    
    
    // Store transaction Id...
//    [[NSUserDefaults standardUserDefaults] setValue:transResponse.transId forKey:@"transactionId"];
//    [[NSUserDefaults standardUserDefaults] synchronize];

    // Update payment status...
    [self dismissViewControllerAnimated:YES completion:^{
         _subscriptionSuccessCallBack(response);
    }];
    
}

- (void)paymentCanceled {
    
//    NSLog(@"Payment Canceled ********************** ");
    
    
    [QMMessageNotificationManager showNotificationWithTitle:kPaymentCancelError subtitle:@"" type:QMMessageNotificationTypeError];
    [SVProgressHUD dismiss];

}

/**
 * Optional delegate: method is called when a non is CreateTransactionResponse is returned from the server.
 * The errorType data member of response should indicate TRANSACTION_ERROR or SERVER_ERROR.
 * TRANSACTION_ERROR are non-APRROVED response code.  SERVER_ERROR are due to non
 * non gateway responses.  Typically these are non successful HTTP responses.  The actual
 * HTTP response is returned in the AuthNetResponse object's responseReasonText instance variable.
 */

-(void) requestFailed:(AuthNetResponse *)response {
    
//    NSLog(@"Request Failed ********************** ");
    
    NSString *title = nil;
    NSString *alertErrorMsg = nil;
    
    [SVProgressHUD dismiss];
//    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    if ( [response errorType] == SERVER_ERROR)
    {
        title = NSLocalizedString(@"Server Error", @"");
        alertErrorMsg = [response responseReasonText];
    }
    else if([response errorType] == TRANSACTION_ERROR)
    {
        title = NSLocalizedString(@"Transaction Error", @"");
        alertErrorMsg = [response responseReasonText];
    }
    else if([response errorType] == CONNECTION_ERROR)
    {
        title = NSLocalizedString(@"Connection Error", @"");
        alertErrorMsg = [response responseReasonText];
    }
    
    Messages *ma = response.anetApiResponse.messages;
    
    AuthNetMessage *m = [ma.messageArray objectAtIndex:0];
    
//    NSLog(@"Response Msg Array Count: %lu", (unsigned long)[ma.messageArray count]);
    
//    NSLog(@"Response Msg Code %@ ", m.code);
    
    NSString *errorCode = [NSString stringWithFormat:@"%@",m.code];
    NSString *errorText = [NSString stringWithFormat:@"%@",m.text];
    
    NSString *errorMsg = [NSString stringWithFormat:@"%@ : %@", errorCode, errorText];
    
    if (alertErrorMsg == nil) {
        alertErrorMsg = errorText;
    }
    
//    NSLog(@"Error Code and Msg %@", errorMsg);
    
    
    if ( ([m.code isEqualToString:@"E00027"]) || ([m.code isEqualToString:@"E00007"]) || ([m.code isEqualToString:@"E00096"]))
    {
        
        
        [QMMessageNotificationManager showNotificationWithTitle:alertErrorMsg subtitle:@"" type:QMMessageNotificationTypeError];
    }
    else if ([m.code isEqualToString:@"E00008"]) // Finger Print Value is not valid.
    {
        [QMMessageNotificationManager showNotificationWithTitle:errorText subtitle:@"" type:QMMessageNotificationTypeError];
        
    }
    else
    {
        [QMMessageNotificationManager showNotificationWithTitle:alertErrorMsg subtitle:@"" type:QMMessageNotificationTypeError];
        
    }
    
    return;
}

/**
 * Optional delegate: method is called when a connection error occurs while connecting to the server..
 * The errorType data member of response should indicate CONNECTION_ERROR.  More detail
 * may be included in the AuthNetResponse object's responseReasonText.
 */

- (void) connectionFailed:(AuthNetResponse *)response {
    
//    NSLog(@"%@", response.responseReasonText);
//    NSLog(@"Connection Failed");
    
    
    [SVProgressHUD dismiss];
    
    NSString *title = nil;
    NSString *message = nil;
    
    if ([response errorType] == NO_CONNECTION_ERROR)
    {
        title = NSLocalizedString(@"No Signal", @"");
        message = NSLocalizedString(@"Unable to complete your request. No Internet connection.", @"");
    }
    else
    {
        title = NSLocalizedString(@"Connection Error", @"");
        message = NSLocalizedString(@"A connection error occurred.  Please try again.", @"");
    }
    
    [QMMessageNotificationManager showNotificationWithTitle:message subtitle:@"" type:QMMessageNotificationTypeError];
    
    
}

#pragma mark --------------------------------
#pragma mark Other Methods

- (void)initializeViews
{
    NSString *aStrBuffer = [self.txtFCardNumber.text stringByReplacingOccurrencesOfString:kSpace withString:@""];
    if(aStrBuffer==nil)
        aStrBuffer=@"";
    strCardNumberBuffer = [NSString stringWithString:aStrBuffer];
    
    aStrBuffer = [self.txtFCardNumber.text stringByReplacingOccurrencesOfString:kSlash withString:@""];
    if(aStrBuffer==nil)
        aStrBuffer=@"";
    strCardExpirationBuffer = [NSString stringWithString:aStrBuffer];
    
}

- (NSString *)getDeviceId {
    
    return [[[UIDevice currentDevice] identifierForVendor] UUIDString];
}

// Check Internet Connection is available or not...
- (BOOL)isConnectedToInternet {
    
    BOOL isReachable = NO;
    
    // Check Internet Connection reachability.
    Reachability *reachability = [Reachability reachabilityWithHostName:@"www.apple.com"];
    
    switch (reachability.currentReachabilityStatus) {
        case NotReachable:
//            NSLog(@"Internet Connection is not available.");
            isReachable = NO;
            break;
            
        case ReachableViaWiFi:
            isReachable = YES;
//            NSLog(@"Internet Connection is available via Wifi.");
            break;
            
        case ReachableViaWWAN:
            isReachable = YES;
//            NSLog(@"Internet Connection is available via WWAN.");
            break;
            
        default:
            break;
    }
    
    return isReachable;
}
-(NSString *)encryptUsingRNCryptor:(NSString*)plainString
{
    NSData *data = [plainString dataUsingEncoding:NSUTF8StringEncoding];
    NSError *error = nil;
    NSString *encode_base64_string = @"";
    NSData *encryptedData = [RNEncryptor encryptData:data
                                        withSettings:kRNCryptorAES256Settings
                                            password:AES256ENCRYPTIONKEY
                                               error:&error];
    
    if(error == nil)
    {
        encryptedData = [encryptedData base64EncodedDataWithOptions:0];
        encode_base64_string = [[NSString alloc] initWithData:encryptedData encoding:NSUTF8StringEncoding];
//        NSLog(@"=======encodestring --> %@ ====================",encode_base64_string);
        encode_base64_string = [self encodeString:encode_base64_string];
    }
    else
    {
//        NSLog(@"=================== Error in encryption ================");
    }
    
    return encode_base64_string;
}
-(NSString *)encodeString :(NSString*)myStr
{
    NSString *encodedStr = @"";
    
    for (int i=0; i < [myStr length]; i++)
    {
        NSString *currentChar = [NSString stringWithFormat:@"%c",[myStr characterAtIndex:i]];
        if([_mainStrArray containsObject: currentChar])
        {
            NSInteger index = [_mainStrArray indexOfObject:currentChar];
            NSString *needToAppendStr = [_replaceStrArray objectAtIndex:index];
            encodedStr = [NSString stringWithFormat:@"%@%@",encodedStr,needToAppendStr];
        }
        else
        {
            encodedStr = [NSString stringWithFormat:@"%@%@",encodedStr,currentChar];
        }
    }
    return encodedStr;
}

@end
