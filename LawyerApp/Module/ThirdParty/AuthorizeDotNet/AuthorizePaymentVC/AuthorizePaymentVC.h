//
//  PaymentVC.h
//  TutorPursuit
//
//  Created by Adapting Social on 9/28/16.
//  Copyright © 2016 Adapting Social. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AuthNet.h"
#import "RNCryptor.h"
#import "RNEncryptor.h"
typedef void(^subscriptionSuccessBlock)(CreateTransactionResponse *);
typedef void(^encryptedCardDetailsSuccessBlock)(NSString *,NSString *,NSString *);

@interface AuthorizePaymentVC : UIViewController<AuthNetDelegate> {
    
    NSString *strCardExpirationMonth;
    NSString *strCardExpirationYear;
    NSString *strCardVerificationValue;
    NSString *strCardNumberBuffer;
    NSString *strCardExpirationBuffer;
    NSInteger intRetryCount;
    
}
@property (weak, nonatomic) IBOutlet UILabel *lblSubscriptionAmount;

@property (weak, nonatomic) IBOutlet UITextField *txtFCardNumber;
@property (weak, nonatomic) IBOutlet UITextField *txtFCardExpirationDate;
@property (weak, nonatomic) IBOutlet UITextField *txtFCardVerificationValue;
@property (weak, nonatomic) IBOutlet UIButton *btnBack;
@property (weak, nonatomic) IBOutlet UIButton *btnPayment;

@property (strong, nonatomic) NSString *strSessionToken;
@property (strong, nonatomic) NSString *strItemId;
@property (strong, nonatomic) NSString *strSubscriptionPackageName;
@property (strong, nonatomic) NSString *strSubscriptionAmount;
@property (strong, nonatomic) NSMutableArray *mainStrArray;
@property (strong, nonatomic) NSMutableArray *replaceStrArray;
@property (nonatomic) BOOL shouldPayWithFramework;
@property (strong, nonatomic) subscriptionSuccessBlock subscriptionSuccessCallBack;
@property (strong, nonatomic) encryptedCardDetailsSuccessBlock cardDetailsBlock;

@end
