//
//  MasterPageController.swift
//  LegalStart
//
//  Created by Adapting Social on 09/05/17.
//  Copyright © 2017 Adapting Social. All rights reserved.
//

import UIKit
protocol MasterPageControllerDataSource {
    func numberOfViewController(pageController : MasterPageController) -> ([String],[UIViewController])
    func willMoveToViewController(pageController : MasterPageController,index : Int,viewController : UIViewController)
    func didMoveToViewController(pageController : MasterPageController,index : Int,viewController : UIViewController)
}
class MasterPageController: UIView,UIPageViewControllerDataSource,UIPageViewControllerDelegate {

    public var datasource1: MasterPageControllerDataSource?
    private var selectionView : MasterPageSelectionView?
    @IBOutlet public var datasource: AnyObject? {
        get { return datasource1 as AnyObject }
        set { datasource1 = newValue as? MasterPageControllerDataSource }
    }
    private var numberOfVC : Int = 0
    var currentPageIndex : Int = 0
    private var pageViewController : UIPageViewController?
    var arrOfTitles : [String] = []
    var arrOfVC : [UIViewController] = []
    private var currentViewController : UIViewController?
    @IBInspectable var underLineColor: UIColor = UIColor.green
    @IBInspectable var selectedIndexFontColor: UIColor = UIColor.purple
    @IBInspectable var unSelectedIndexFontColor: UIColor = UIColor.lightGray
    @IBInspectable var topBarBackGroundColor: UIColor = UIColor.white
    var isTopTabBarHidden : Bool = false
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        reloadData()
    }
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {

        var vctag = viewController.view.tag
        vctag += 1
        if (vctag == numberOfVC) {
            
            return nil
        }
        let vc = self.viewControllerAtIndex(index: vctag)
        vc.view.tag = vctag
        
        return vc
        
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        var vctag = viewController.view.tag
        
        if (vctag == 0) {
            return nil
        }
        vctag -= 1
        let vc = self.viewControllerAtIndex(index: vctag)
        vc.view.tag = vctag
        return vc

    }
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        if completed {
            let currentVC = pageViewController.viewControllers![0]
            let vctag : Int = currentVC.view.tag
            self.currentViewController = currentVC
            selectionView?.selectMasterCellAtIndex(index: vctag)
            datasource1?.didMoveToViewController(pageController: self, index: vctag, viewController: currentVC)
            
        }
    }
    func viewControllerAtIndex(index : Int) -> UIViewController {
    
        datasource1?.willMoveToViewController(pageController: self, index: index, viewController: arrOfVC[index])
        return arrOfVC[index]
    }
    
    func reloadData()  {
        
        if datasource1 != nil {
            let tupple : ([String],[UIViewController]) = datasource1!.numberOfViewController(pageController: self)
            
            arrOfVC = tupple.1
            arrOfTitles = tupple.0
            
            if arrOfVC.count != arrOfTitles.count {
                print("number of viewcontrollers should be equal to title")
                return;
            }else{
                
                numberOfVC = arrOfVC.count
                
            }
            if self.pageViewController != nil {
                self.pageViewController = nil
            }
            pageViewController = UIPageViewController.init(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
            pageViewController?.dataSource = self
            pageViewController?.delegate = self
            
            
            
            if selectionView != nil{
            
                selectionView?.removeFromSuperview()
            }
            selectionView = MasterPageSelectionView(frame: CGRect(x: 0, y: 0, width: self.frame.size.width, height: 44))
            selectionView?.selectedIndexFontColor = self.selectedIndexFontColor
            selectionView?.unSelectedIndexFontColor = self.unSelectedIndexFontColor
            selectionView?.collectionView?.backgroundColor = self.topBarBackGroundColor
            selectionView?.underLineBackGroundColor = self.underLineColor
            self.pageViewController?.view.frame = CGRect(x: 0, y: (selectionView?.frame.size.height)!, width: self.frame.size.width, height: self.frame.size.height - (selectionView?.frame.size.height)!)
            (datasource as! UIViewController).addChildViewController(pageViewController!)
            
            
            
            self.addSubview((pageViewController?.view)!)
            self.pageViewController?.didMove(toParentViewController: (datasource as! UIViewController))
            
            
            
            selectionView?.titles = arrOfTitles
            selectionView?.selectMasterCellAtIndex(index: currentPageIndex)
            self.addSubview(selectionView!)
            let vc = self.viewControllerAtIndex(index: currentPageIndex)
            vc.view.tag = currentPageIndex
            self.currentViewController = vc
            self.pageViewController?.setViewControllers([vc], direction: .forward, animated: true, completion: { (completed) in
                if completed{
                    self.datasource1?.didMoveToViewController(pageController: self, index: vc.view.tag, viewController: vc)
                }
            })
            self.selectionView?.selectedIndex = {
                value in
                
                if value != self.currentViewController?.view.tag {
                    self.selectionView?.selectMasterCellAtIndex(index: value)
                    let vc = self.viewControllerAtIndex(index: value)
                    vc.view.tag = value
                    
                    if vc.view.tag < (self.currentViewController?.view.tag)!  {
                        
                        
                        self.pageViewController?.setViewControllers([vc], direction: .reverse, animated: true, completion: { (completed) in
                            if completed{
                                self.datasource1?.didMoveToViewController(pageController: self, index: vc.view.tag, viewController: vc)
                            }
                        })
                        
                    }else{
                        
                        self.pageViewController?.setViewControllers([vc], direction: .forward, animated: true, completion: { (completed) in
                            if completed{
                                self.datasource1?.didMoveToViewController(pageController: self, index: vc.view.tag, viewController: vc)
                            }
                        })
                    }
                    self.currentViewController = vc
                }
                
                
            }
        
        }
        
        
        
    }

}
