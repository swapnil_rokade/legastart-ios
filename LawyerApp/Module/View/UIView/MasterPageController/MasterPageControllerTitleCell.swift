    //
//  MasterPageControllerTitleCell.swift
//  LegalStart
//
//  Created by Adapting Social on 17/05/17.
//  Copyright © 2017 Adapting Social. All rights reserved.
//

import UIKit

class MasterPageControllerTitleCell: UICollectionViewCell {
 
    var titleLabel: UILabel = UILabel()
    var bottomView : UIView = UIView()
    var bottomViewHeight : CGFloat = 2
    override init(frame: CGRect) {
        super.init(frame: frame)
        

        titleLabel.frame = CGRect.zero
        
        titleLabel.backgroundColor = UIColor.clear
        titleLabel.textAlignment = .center
        
        self.contentView.addSubview(titleLabel)
        bottomView.frame = CGRect(x: 0, y: self.frame.size.height-bottomViewHeight, width: frame.size.width, height: bottomViewHeight)
        bottomView.backgroundColor = UIColor.red
        self.contentView.addSubview(bottomView)
        
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        
        titleLabel.sizeToFit()
        titleLabel.frame.origin.x = 0
        titleLabel.frame.origin.y = 10
        bottomView.frame = CGRect(x: 0, y: self.frame.size.height-bottomViewHeight, width: titleLabel.frame.size.width, height: bottomViewHeight)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
}
