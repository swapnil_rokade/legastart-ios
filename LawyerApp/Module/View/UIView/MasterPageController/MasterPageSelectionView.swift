//
//  MasterPageSelectionView.swift
//  LegalStart
//
//  Created by Adapting Social on 16/05/17.
//  Copyright © 2017 Adapting Social. All rights reserved.
//

import UIKit
extension UIColor {
    
//    public static func colorFromCode(_ code: Int) -> UIColor {
//        let red = CGFloat(((code & 0xFF0000) >> 16)) / 255
//        let green = CGFloat(((code & 0xFF00) >> 8)) / 255
//        let blue = CGFloat((code & 0xFF)) / 255
//
//        return UIColor(red: red, green: green, blue: blue, alpha: 1)
//    }
//
//    public static func colorFromCode(_ code: Int, AndAlpha alpha: CGFloat) -> UIColor {
//        let red = CGFloat(((code & 0xFF0000) >> 16)) / 255
//        let green = CGFloat(((code & 0xFF00) >> 8)) / 255
//        let blue = CGFloat((code & 0xFF)) / 255
//
//        return UIColor(red: red, green: green, blue: blue, alpha: alpha)
//    }
  
}
class MasterPageSelectionView: UIView,UICollectionViewDelegateFlowLayout,UICollectionViewDataSource,UICollectionViewDelegate {

   
    var titles : [String] = []
    
    private var selectedCell: Int = 0
    var collectionView : UICollectionView?
    var selectedIndex : (Int) -> () = {_ in}
    
    var selectedIndexFontColor : UIColor = UIColor.colorFromCode(0x3A3A3A)
    var unSelectedIndexFontColor : UIColor = UIColor.colorFromCode(0x798387)
    var underLineBackGroundColor : UIColor = UIColor.colorFromCode(0x0AB8BA)
    
    
    override init(frame: CGRect) {
        
        super.init(frame: frame)
        self.selectedIndex(selectedCell)
        
        self.backgroundColor = UIColor.clear
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 14, bottom: 0, right: 14)
        layout.scrollDirection = .horizontal
        collectionView = UICollectionView(frame: CGRect(x: 0, y: 0, width: frame.size.width, height: frame.size.height), collectionViewLayout: layout)
        
        collectionView?.dataSource = self
        collectionView?.delegate = self
        collectionView?.showsHorizontalScrollIndicator = false
        collectionView?.showsVerticalScrollIndicator = false
        collectionView?.register(MasterPageControllerTitleCell.classForCoder(), forCellWithReuseIdentifier: "MasterPageControllerTitleCell")
        collectionView?.backgroundColor = UIColor.clear
    
        self.addSubview(collectionView!)
        
        
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return titles.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MasterPageControllerTitleCell", for: indexPath) as!MasterPageControllerTitleCell
        
        cell.titleLabel.text = titles[indexPath.row]
        cell.bottomView.backgroundColor = underLineBackGroundColor
        if indexPath.row == selectedCell {
            cell.bottomView.isHidden = false
            cell.titleLabel.textColor = selectedIndexFontColor
        }else{
        cell.bottomView.isHidden = true
            cell.titleLabel.textColor = unSelectedIndexFontColor
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView,layout collectionViewLayout: UICollectionViewLayout,sizeForItemAt indexPath: IndexPath) -> CGSize {
        let title : String = titles[indexPath.row]
        
        let width : CGFloat = title.widthOfString(usingFont: UIFont.systemFont(ofSize: 17))
       
        return CGSize(width: width+20, height: frame.size.height)
        
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.selectedIndex(indexPath.row)
    }
    func selectMasterCellAtIndex(index : Int)  {
        
        selectedCell = index
        
        let indexPath : IndexPath = IndexPath(item: selectedCell, section: 0)
        collectionView?.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        collectionView?.reloadData() 
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}

