//
//  PrefectRoundView.swift
//  LegalStart
//
//  Created by Adapting Social on 06/07/17.
//  Copyright © 2017 Adapting Social. All rights reserved.
//

import UIKit

class PrefectRoundView: UIView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setUp()
    }
    
    override init(frame: CGRect){
        super.init(frame: frame)
        self.setUp()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setUp()
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        setUp()
    }
    func setUp(){
        
        self.layer.cornerRadius = self.frame.size.height / 2
        self.layer.masksToBounds = true
        
        
        
    }
}
