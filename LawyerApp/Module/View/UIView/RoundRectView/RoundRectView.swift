//
//  RoundRectView.swift
//  LegalStart
//
//  Created by Adapting Social on 4/27/17.
//  Copyright © 2017 Adapting Social. All rights reserved.
//

import UIKit

class RoundRectView: UIView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    @IBInspectable var roundedRadius : CGFloat = 8.0
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setUp()
    }
    
    override init(frame: CGRect){
        super.init(frame: frame)
        self.setUp()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setUp()
    }
    
    func setUp(){
        
        self.layer.cornerRadius = roundedRadius
        self.layer.borderWidth = 1.0
        self.layer.borderColor = UIColor.colorFromCode(0xCFD0D1).cgColor
        
    }

}
