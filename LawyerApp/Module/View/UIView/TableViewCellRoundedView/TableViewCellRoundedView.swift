//
//  TableViewCellRoundedView.swift
//  LegalStart
//
//  Created by Adapting Social on 25/04/17.
//  Copyright © 2017 Adapting Social. All rights reserved.
//

import UIKit

class TableViewCellRoundedView: UIView {

   
    @IBInspectable var cornerRadius: CGFloat = 4.0
    
    
    private var customBackgroundColor = UIColor.white
    override var backgroundColor: UIColor?{
        didSet {
            customBackgroundColor = backgroundColor!
            super.backgroundColor = UIColor.clear
        }
    }
    
    func setup() {
        layer.shadowColor = UIColor.black.cgColor;
        layer.shadowOffset = CGSize.zero;
        layer.shadowRadius = 1.0;
        layer.shadowOpacity = 0.3;
        super.backgroundColor = UIColor.clear
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setup()
    }
    
    override func draw(_ rect: CGRect) {
        customBackgroundColor.setFill()
        UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius ).fill()

    }
}
