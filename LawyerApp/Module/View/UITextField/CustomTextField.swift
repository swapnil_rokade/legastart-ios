//
//  CustomTextField.swift
//  LegalStart
//
//  Created by Adapting Social on 4/24/17.
//  Copyright © 2017 Adapting Social. All rights reserved.
//

import UIKit

class CustomTextField: UITextField {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setUp()
    }
    
    override init(frame: CGRect){
        super.init(frame: frame)
        self.setUp()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setUp()
    }
    
    func setUp(){
        self.font = UIFont.init(name: Constant.FontName.kFontRobotoRegular, size: 14.0)
        self.textColor = UIColor.colorFromCode(0x9B9B9B)
        
        let attributes = [ NSFontAttributeName: UIFont(name: Constant.FontName.kFontRobotoRegular, size: 14.0)!, NSForegroundColorAttributeName: UIColor.colorFromCode(0x9B9B9B)]
        if let aStrPlaceholder = self.placeholder {
            self.attributedPlaceholder = NSAttributedString(string: aStrPlaceholder, attributes: attributes)
        }
    }

}
