//
//  QuestionTypeFourCell.swift
//  LegalStart
//
//  Created by Adapting Social on 05/07/17.
//  Copyright © 2017 Adapting Social. All rights reserved.
//

import UIKit

class QuestionTypeFourCell: UICollectionViewCell {
    
    @IBOutlet weak var txtFieldAnswer: UITextField!
    @IBOutlet weak var lblOptionTitle: UILabel!
}
