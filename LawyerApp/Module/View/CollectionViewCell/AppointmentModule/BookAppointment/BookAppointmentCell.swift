//
//  BookAppointmentCell.swift
//  LegalStart
//
//  Created by Adapting Social on 5/17/17.
//  Copyright © 2017 Adapting Social. All rights reserved.
//

import UIKit

class BookAppointmentCell: UICollectionViewCell {
    
    @IBOutlet weak var btnTimeSlot: UIButton!
    @IBOutlet weak var imgCheckmark : UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
        self.btnTimeSlot.isEnabled = true
        
        self.btnTimeSlot.layer.cornerRadius = 4.0
        self.btnTimeSlot.layer.borderWidth = 1.0
        self.imgCheckmark.isHidden = true
        
        self.btnTimeSlot.titleLabel?.numberOfLines = 1
        self.btnTimeSlot.titleLabel?.adjustsFontSizeToFitWidth = true
        self.btnTimeSlot.titleLabel?.minimumScaleFactor = 0.5;
    }
    
    func setReservedCell(_ isReserved : Bool) {
        
        self.btnTimeSlot.isEnabled = !isReserved
        self.imgCheckmark.isHidden = true
        
        if isReserved {
            self.btnTimeSlot.backgroundColor = UIColor.colorFromCode(0x27AC4D, AndAlpha: 0.1)
            self.btnTimeSlot.layer.borderColor = UIColor.colorFromCode(0x27AC4D).cgColor
            self.btnTimeSlot.setTitleColor(UIColor.colorFromCode(0x27AC4D), for: .normal)
        }
        else
        {
            self.btnTimeSlot.backgroundColor = UIColor.colorFromCode(0xEFEFEF, AndAlpha: 1.0)
            self.btnTimeSlot.layer.borderColor = UIColor.colorFromCode(0xAFAFAF).cgColor
            self.btnTimeSlot.setTitleColor(UIColor.colorFromCode(0x9B9B9B), for: .normal)
        }
    }
    
    func setButtonSelected(_ isSelected : Bool) {
        
        self.btnTimeSlot.isSelected = isSelected
        
        if isSelected {
            self.btnTimeSlot.backgroundColor = UIColor.colorFromCode(0x4A4A4A, AndAlpha: 0.1)
            self.btnTimeSlot.layer.borderColor = UIColor.colorFromCode(0x4A4A4A).cgColor
            self.btnTimeSlot.setTitleColor(UIColor.colorFromCode(0x4A4A4A), for: .selected)
            self.imgCheckmark.isHidden = false
        }
        else
        {
            self.btnTimeSlot.backgroundColor = UIColor.colorFromCode(0xEFEFEF, AndAlpha: 1.0)
            self.btnTimeSlot.layer.borderColor = UIColor.colorFromCode(0xAFAFAF).cgColor
            self.btnTimeSlot.setTitleColor(UIColor.colorFromCode(0x9B9B9B), for: .normal)
            self.imgCheckmark.isHidden = true

        }
        
    }

}
