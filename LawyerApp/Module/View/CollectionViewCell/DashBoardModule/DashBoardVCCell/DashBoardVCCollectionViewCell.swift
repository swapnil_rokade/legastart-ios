//
//  DashBoardVCCollectionViewCell.swift
//  LegalStart
//
//  Created by Adapting Social on 01/06/17.
//  Copyright © 2017 Adapting Social. All rights reserved.
//

import UIKit

class DashBoardVCCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var title: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    override func layoutSubviews() {
        super.layoutSubviews()

    }
}
