//
//  CaseAddVCDocumentCell.swift
//  LegalStart
//
//  Created by Adapting Social on 26/04/17.
//  Copyright © 2017 Adapting Social. All rights reserved.
//

import UIKit

class CaseAddVCDocumentCell: UITableViewCell {

    @IBOutlet weak var documentImageView: UIImageView!
    @IBOutlet weak var documentTitle: UILabel!
    @IBOutlet weak var documentUploadedBy: UILabel!
    @IBOutlet weak var documentUploadedDate: UILabel!
    @IBOutlet weak var btnRemoveDocument: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
