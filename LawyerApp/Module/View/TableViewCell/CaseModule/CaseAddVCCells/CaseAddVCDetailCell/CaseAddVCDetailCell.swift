//
//  CaseAddVCDetailCell.swift
//  LegalStart
//
//  Created by Adapting Social on 26/04/17.
//  Copyright © 2017 Adapting Social. All rights reserved.
//

import UIKit

class CaseAddVCDetailCell: UITableViewCell {

    @IBOutlet weak var masterDocumentPicker: UIButtonMasterDocumentPicker!
    @IBOutlet weak var txtCaseTitle: CustomTextField!
    @IBOutlet weak var txtDocumentType: CustomTextField!
    @IBOutlet weak var txtPartiesInAgreeMent: CustomTextField!
    
    @IBOutlet weak var txtViewConcers: UITextView!
    
    @IBOutlet weak var lblDetailTitle: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        masterDocumentPicker.allowedFileTypes = kAllowedFileTypes
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
