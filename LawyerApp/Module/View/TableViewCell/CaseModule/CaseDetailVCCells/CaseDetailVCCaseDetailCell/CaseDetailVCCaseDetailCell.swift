//
//  CaseDetailVCCaseDetailCell.swift
//  LegalStart
//
//  Created by Adapting Social on 25/04/17.
//  Copyright © 2017 Adapting Social. All rights reserved.
//

import UIKit

class CaseDetailVCCaseDetailCell: UITableViewCell {

    @IBOutlet weak var caseTitle: UILabel!
    @IBOutlet weak var caseConcerns: UILabel!
    @IBOutlet weak var documentType: UILabel!
    @IBOutlet weak var partiesInAgreement: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize.zero;
        self.layer.shadowRadius = 1.0;
        self.layer.shadowOpacity = 0.5;
        self.layer.masksToBounds = false
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
