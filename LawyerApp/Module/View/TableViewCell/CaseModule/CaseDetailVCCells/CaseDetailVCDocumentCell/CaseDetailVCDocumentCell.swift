//
//  CaseDetailVCDocumentCell.swift
//  LegalStart
//
//  Created by Adapting Social on 25/04/17.
//  Copyright © 2017 Adapting Social. All rights reserved.
//

import UIKit

class CaseDetailVCDocumentCell: UITableViewCell {

    @IBOutlet weak var documentImageView: UIImageView!
    @IBOutlet weak var documentTitle: UILabel!
    @IBOutlet weak var uploadedBy: UILabel!
    @IBOutlet weak var uploadedDate: UILabel!
    
    @IBOutlet weak var btnDownloadDocument: UIButtonMasterDocumentUploader!
    @IBOutlet weak var btnShareDocument: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
