//
//  CaseDetailVCCaseDetailCell.swift
//  LawyerApp
//
//  Created by indianic on 25/04/17.
//  Copyright © 2017 Indianic. All rights reserved.
//

import UIKit

class CaseDetailVCCaseDetailCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize.zero;
        self.layer.shadowRadius = 1.0;
        self.layer.shadowOpacity = 0.5;
        self.layer.masksToBounds = false
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
