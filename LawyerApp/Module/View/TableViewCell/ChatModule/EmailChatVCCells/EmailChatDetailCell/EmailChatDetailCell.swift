//
//  EmailChatDetailCell.swift
//  LegalStart
//
//  Created by Adapting Social on 10/05/17.
//  Copyright © 2017 Adapting Social. All rights reserved.
//

import UIKit

class EmailChatDetailCell: UITableViewCell {

    @IBOutlet weak var txtName: CustomTextField!
    @IBOutlet weak var txtEmail: CustomTextField!
    @IBOutlet weak var txtViewPurpose: UITextView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
