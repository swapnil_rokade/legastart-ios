//
//  CompanyListCell.swift
//  LegalStart
//
//  Created by Adapting Social on 5/1/17.
//  Copyright © 2017 Adapting Social. All rights reserved.
//

import UIKit

class CompanyListCell: UITableViewCell {

    @IBOutlet var imgViewCompany: UIImageView!
    @IBOutlet var lblCompanyName: UILabel!
    @IBOutlet var lblAddress: UILabel!
    @IBOutlet var lblCompanyCategoryName: UILabel!
    @IBOutlet var btnDirections: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

}
