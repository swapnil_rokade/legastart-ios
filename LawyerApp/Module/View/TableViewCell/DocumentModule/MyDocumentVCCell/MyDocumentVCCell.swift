//
//  MyDocumentVCCell.swift
//  LegalStart
//
//  Created by Adapting Social on 08/07/17.
//  Copyright © 2017 Adapting Social. All rights reserved.
//

import UIKit

class MyDocumentVCCell: UITableViewCell {

    @IBOutlet weak var lblDocumentTitle: UILabel!
    @IBOutlet weak var btnView: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        btnView.layer.cornerRadius = btnView.frame.size.height / 2
        btnView.layer.masksToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
