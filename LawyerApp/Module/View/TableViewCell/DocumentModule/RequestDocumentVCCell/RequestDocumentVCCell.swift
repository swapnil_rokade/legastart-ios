//
//  RequestDocumentVCCell.swift
//  LegalStart
//
//  Created by Adapting Social on 23/05/17.
//  Copyright © 2017 Adapting Social. All rights reserved.
//

import UIKit

class RequestDocumentVCCell: UITableViewCell {

    @IBOutlet weak var btnRequestApprove: UIButton!
    @IBOutlet weak var lblDocumentType: UILabel!
    @IBOutlet weak var lblDocumentName: UILabel!
    @IBOutlet weak var lblDocumentAmount: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        btnRequestApprove.layer.cornerRadius = btnRequestApprove.frame.size.height / 2
        btnRequestApprove.layer.masksToBounds = true
        let maskLayer = CAShapeLayer()
        if lblDocumentType != nil {
            maskLayer.path = UIBezierPath(roundedRect: lblDocumentType.bounds, byRoundingCorners: [.topRight,.bottomRight], cornerRadii: CGSize(width: 10, height: 10)).cgPath
            lblDocumentType.layer.mask = maskLayer
        }
        
    }
   
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
