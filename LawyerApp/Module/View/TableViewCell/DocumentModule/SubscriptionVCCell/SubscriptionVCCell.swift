//
//  SubscriptionVCCell.swift
//  LegalStart
//
//  Created by Adapting Social on 23/06/17.
//  Copyright © 2017 Adapting Social. All rights reserved.
//

import UIKit

class SubscriptionVCCell: UITableViewCell {

    @IBOutlet weak var subscriptionAmount: UILabel!
    @IBOutlet weak var subscriptionTitle: UILabel!
    @IBOutlet weak var btnStart: UIButton!
    @IBOutlet weak var subscriptionDescription: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
