//
//  BlogListingVCTableViewCell.swift
//  LegalStart
//
//  Created by Adapting Social on 15/06/17.
//  Copyright © 2017 Adapting Social. All rights reserved.
//

import UIKit

class BlogListingVCTableViewCell: UITableViewCell {

    @IBOutlet weak var blogImgView: UIImageView!
    @IBOutlet weak var blogTitle: UILabel!
    @IBOutlet weak var blogDate: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
