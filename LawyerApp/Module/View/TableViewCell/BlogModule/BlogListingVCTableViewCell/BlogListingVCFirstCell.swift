//
//  BlogListingVCFirstCell.swift
//  LegalStart
//
//  Created by Adapting Social on 06/07/17.
//  Copyright © 2017 Adapting Social. All rights reserved.
//

import UIKit

class BlogListingVCFirstCell: UITableViewCell {

    @IBOutlet weak var blogImgView: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
