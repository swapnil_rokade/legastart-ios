//
//  BlogDetailVCTableViewCell.swift
//  LegalStart
//
//  Created by Adapting Social on 02/05/17.
//  Copyright © 2017 Adapting Social. All rights reserved.
//

import UIKit

class BlogDetailVCTableViewDetailCell: UITableViewCell {

    
    @IBOutlet weak var blogImgView: UIImageView!
    
    @IBOutlet weak var lblBlogTitle: UILabel!
    @IBOutlet weak var lblBlogDate: UILabel!
    
    @IBOutlet weak var lblBlogDescription: UILabel!
    @IBOutlet weak var btnAddComment: UIButton!
    
    
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        
        
        
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.btnAddComment.layer.cornerRadius = self.btnAddComment.frame.size.height / 2
        self.btnAddComment.layer.masksToBounds = true
        
        let path = UIBezierPath()
        path.move(to: self.blogImgView.frame.origin)
        path.addLine(to: CGPoint(x: self.frame.width, y:self.blogImgView.frame.origin.y))
        
        let imgHeight = self.blogImgView.frame.height / 3
        
        
        path.addLine(to: CGPoint(x: self.frame.width, y:self.blogImgView.frame.size.height - imgHeight))
        path.addLine(to: CGPoint(x: self.blogImgView.frame.origin.x, y:self.blogImgView.frame.height))
        path.addLine(to: self.blogImgView.frame.origin)
        
        let maskLayer = CAShapeLayer()
        
        maskLayer.path = path.cgPath
        blogImgView.layer.mask = maskLayer
        
        blogImgView.clipsToBounds = true
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
