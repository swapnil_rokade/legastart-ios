//
//  BlogDetailVCTableViewCommentCell.swift
//  LegalStart
//
//  Created by Adapting Social on 02/05/17.
//  Copyright © 2017 Adapting Social. All rights reserved.
//

import UIKit

class BlogDetailVCTableViewCommentCell: UITableViewCell {

    @IBOutlet weak var lblCommentTitle: UILabel!
    @IBOutlet weak var imgViewProfileImage: UIImageView!
    @IBOutlet weak var btnCommentLike: UIButton!
    @IBOutlet weak var lblCommentDescription: UILabel!
    @IBOutlet weak var lblTotalLikes: UILabel!
    @IBOutlet weak var lblLike: UILabel!
    @IBOutlet weak var lblCommentDate: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
