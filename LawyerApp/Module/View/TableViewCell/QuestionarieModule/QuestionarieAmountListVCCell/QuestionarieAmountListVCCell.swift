//
//  QuestionarieAmountListVCCell.swift
//  LegalStart
//
//  Created by Adapting Social on 11/08/17.
//  Copyright © 2017 Adapting Social. All rights reserved.
//

import UIKit

class QuestionarieAmountListVCCell: UITableViewCell {

    @IBOutlet weak var checkBoxViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var checkBoxScrollView: UIScrollView!
    @IBOutlet weak var lblStateTitle: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var lblCategoryTitle: UILabel!
    @IBOutlet weak var checkBoxView: UIView!
    var arrOfBoxes : [iCheckboxState] = []
    let box = iCheckboxState()
    let box2 = iCheckboxState()
    var checkboxBuilder : iCheckboxBuilder?
    let checkboxBuilderConfig = iCheckboxBuilderConfig()
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.checkboxBuilderConfig.startPosition = CGPoint(x: 0, y: 0)
        self.checkboxBuilderConfig.imageNameForNormalState = "uncheckk"
        self.checkboxBuilderConfig.imageNameForSelectedState = "checkk"
        self.checkboxBuilderConfig.titleColorForSelectedState = UIColor.colorFromCode(0x2EB863)
        self.checkboxBuilderConfig.selection = .Single
        self.checkboxBuilderConfig.headerTitle = ""
        self.checkboxBuilderConfig.style = .OneColumn
        self.checkboxBuilderConfig.borderStyle = .None
        self.checkboxBuilderConfig.checkboxSize = CGSize(width: checkBoxView.frame.size.width, height: 0)
        
        checkboxBuilder = iCheckboxBuilder(withCanvas: self.checkBoxView, canvasScrollView: self.checkBoxScrollView, andConfig: checkboxBuilderConfig)

        
        
    }
   
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
