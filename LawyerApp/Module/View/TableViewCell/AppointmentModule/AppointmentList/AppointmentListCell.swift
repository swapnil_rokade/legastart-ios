//
//  AppointmentListCell.swift
//  LegalStart
//
//  Created by Adapting Social on 5/8/17.
//  Copyright © 2017 Adapting Social. All rights reserved.
//

import UIKit

class AppointmentListCell: UITableViewCell {

    @IBOutlet var lblAppointmentTitle: UILabel!
    @IBOutlet var lblDate: UILabel!
    @IBOutlet var lblTime: UILabel!
    var indexPath : IndexPath?
    @IBOutlet weak var btnDeleteAppointment: UIButton!
    var didDeleteButtonClicked : (AppointmentListCell,IndexPath) -> () = {_ in}
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    @IBAction func btnDeleteAction(_ sender: UIButton) {
        
        self.didDeleteButtonClicked(self,indexPath!)
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
