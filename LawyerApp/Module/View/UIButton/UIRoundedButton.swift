//
//  UIRoundedButton.swift
//  SocialEaseApp
//
//  Created by Adapting Social on 24/06/17.
//  Copyright © 2017 Adapting Social. All rights reserved.
//

import UIKit

class UIRoundedButton: UIButton {

    
    @IBInspectable var shadowColor : UIColor = UIColor.black
    @IBInspectable var shadowWidth : CGFloat = 0.0
    @IBInspectable var shadowHeight : CGFloat = 0.0
    @IBInspectable var shadowOpacity : Float = 0.0
    @IBInspectable var shadowRadius : CGFloat = 0.0
    @IBInspectable var radiusCorner : CGFloat = 0.0
    @IBInspectable var borderWidth : CGFloat = 0.0
    @IBInspectable var borderColor : UIColor = UIColor.black
    override func draw(_ rect: CGRect) {
        self.setup()
    }
 
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setup()
    }
    
    override init(frame: CGRect){
        super.init(frame: frame)
        self.setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setup()
    }
    
    func setup() {
//        layer.shadowColor = shadowColor.cgColor;
//        layer.shadowOffset = CGSize(width: shadowWidth, height: shadowHeight)
//        layer.shadowRadius = shadowRadius;
//        layer.shadowOpacity = shadowOpacity;
        layer.cornerRadius = radiusCorner
        layer.masksToBounds = true
        layer.borderColor = self.borderColor.cgColor
        layer.borderWidth = self.borderWidth
        
    }
}
