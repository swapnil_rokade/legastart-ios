//
//  CustomButton.swift
//  LegalStart
//
//  Created by Adapting Social on 4/24/17.
//  Copyright © 2017 Adapting Social. All rights reserved.
//

import UIKit

class CustomButton: UIButton {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setUp()
    }
    
    override init(frame: CGRect){
        super.init(frame: frame)
        self.setUp()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setUp()
    }
    
    func setUp(){
        
        self.titleLabel?.font = UIFont.init(name: Constant.FontName.kFontRobotoRegular, size: 14.0)
        self.backgroundColor = UIColor.colorFromCode(0x041b54)
        
        self.layer.cornerRadius = 25
        setTitleColor(UIColor.white, for: .normal)
        
        // Make the shadow
        self.layer.shadowOffset = CGSize(width: 0, height: 15.0)
        
        // Set the radius
        self.layer.shadowRadius = 25
        
        // Change the color of the shadow (has to be CGColor)
        self.layer.shadowColor = UIColor.colorFromCode(0x041a54).cgColor
        
        // Display the shadow
        self.layer.shadowOpacity = 0.4
    }


}
